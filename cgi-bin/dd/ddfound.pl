#!/usr/bin/perl -T
###############################CODE SUMMARY#####################################
#Brijal Padia
#Commented May 14, 2001
#We are accessing two DBs, "census" & "page" which are linked by the the common fields
#census.page and page.pagenumbercode
#ddfound receives data from ddquery.pl and utilizes it to create the query to the underlying
#sql databases and format the output.  In addition, ddfound is able to sort by any of the categories
#retrieved.
# The script works by manipulating the syntax of the input to match the representation in the census and
#page db, and passing a series of hidden fields during each subsequent data retrieval.
     
#Remaining Issues:  
	#Have not yet resolved the issue of standard vs literal fields pertaining to both 
#relationship and occupation queries.
#
# Revised 28 September 2004 by Jim Coble
# - Taint checking added to command line
# - "use lib" command added to point to location of DDTools.pm
# - added CGI::DISABLE_UPLOADS
# - added ENV{'PATH'}
#####################################################################################
use strict;
use lib qw(./);
use CGI qw/:standard :html3 :cgi-lib *table *Tr *td *div *ul/;
use CGI::Carp 'fatalsToBrowser';
use DBI;
use POSIX 'strftime';
use DDTools;
#use Text::Soundex qw(:NARA-Ruleset);
use Config::Simple '-strict';

$CGI::POST_MAX=1024 * 5000;
$CGI::DISABLE_UPLOADS = 1;
$ENV{'PATH'} = '/bin:/usr/bin:/usr/local/bin';

# load database configfuration
my $cfg = new Config::Simple();
$cfg->read('../../data/config.ini');

# Determine if the site is in staging or production mode
my $db_mode = $cfg->param('setup.env');
$db_mode = lc $db_mode;

my $mysql_db = '';
my $mysql_host = '';
my $mysql_user = '';
my $mysql_pswd = '';

if ($db_mode eq "production") {
	# Database to connect to
	$mysql_db = $cfg->param('production.database');
	$mysql_host = $cfg->param('production.host');
	# User 'dduser' is permitted to read (but not modify) Digital Durham database
	$mysql_user = $cfg->param('production.user');
	$mysql_pswd = $cfg->param('production.password');# Connect to DigitalDurham database
}
elsif ($db_mode eq "staging") {
	# Database to connect to
	$mysql_db = $cfg->param('staging.database');
	$mysql_host = $cfg->param('staging.host');
	# User 'dduser' is permitted to read (but not modify) Digital Durham database
	$mysql_user = $cfg->param('staging.user');
	$mysql_pswd = $cfg->param('staging.password');# Connect to DigitalDurham database
}

# Database connection string
my $mysql_conn = "DBI:mysql:database=$mysql_db:host=$mysql_host";
# Password set based on development or production server
my $dbh = DBI->connect($mysql_conn,$mysql_user,$mysql_pswd);

# Handle error condition where connection could not be made
if (!defined $dbh) {
    die "Cannot connect to mySQL server: $DBI::errstr\n";
}

my(@list);
my(@errors);
my(%sortHash);
#$sortHash makes the output more succinct so that instead of outputting FKenumerationdistrict, the
#table header will be 'ED#'.  
$sortHash{'ED#'} = 'FKenumerationdistrict';
$sortHash{'HR'} = 'Householdrank';
$sortHash{'suffix'} = 'Namesuffix';
$sortHash{'POB'} = 'Placeofbirth';
$sortHash{'Occupation'} = 'Occupationliteral';
$sortHash{'Relationship'} = 'Relationshipliteral';
$sortHash{'Last'} = 'Lastname';
$sortHash{'First'} = 'Firstname';
$sortHash{'Middle'} = 'Middlename';
$sortHash{'CC'} = 'Civilcondition';
$sortHash{'Infant Age'} = 'Infantage';
$sortHash{'Page'} = 'page.pagenumbercode';
$sortHash{'Soundex'} = 'lastname_soundex';

sub sortButtons {
#Procedure allows for us to retrieve the same data set when a user wants to reorder them by creating
#a number of hidden fields that are needed to re-issue the query.
	my(@prms) = @list;
	my($x) = 0;
	print start_form(-action=>"ddfound.pl", -TARGET=>"myInlineFrame2");
        for ($x = 0; $x < @prms; $x++) {
                 print (hidden({-name=>$prms[$x], -value=>param($prms[$x])}));
        }
	print (hidden({-name=>'Black', -value=>param('Black')})); #have to hardwire b/c they are
								  #check boxes
	print (hidden({-name=>'White', -value=>param('White')}));
	print (hidden({-name=>'Mulatto', -value=>param('Mulatto')}));
	print (hidden({-name=>'minAge', -value=>param('minAge')}));
        print (hidden({-name=>'maxAge', -value=>param('maxAge')}));
        print (hidden({-name=>'pageComp', -value=>param('pageComp')}));
        print (hidden({-name=>'dwellComp', -value=>param('dwellComp')}));
        print (hidden({-name=>'famComp', -value=>param('famComp')}));
        print (hidden({-name=>'compOperator', -value=>param('compOperator')}));
	print (hidden({-name=>'hiddenST', -value=>param('hiddenST')}));
	print (hidden({-name=>'searchType', -value=>param('searchType')}));
	print (hidden({-name=>'submit', -value=>param('submit')}));
	print (hidden({-name=>'occupationliteral2',
-value=>param('occupationliteral2')}));
	print (hidden({-name=>'relatstandard', -value=>param('relatstandard')}));
#        print end_form;
}


sub tableHeaders {
#Designed to support the sorting.  Note the name of each field is 'Sort', but the values differ.
#Consequently we will know what to sort by in future queries.  
my ($fieldType) = shift;
	if (param('submit') eq 'Show All Fields') {
		$fieldType =4;
	}
	sortButtons;
	if ($fieldType==1) {
				#Simple Search
		print font({-size=>-4}, Tr(th{-align=>'center'},
([(submit({-name=>'Sort', -value=>'ED#'})),
submit({-name=>'Sort', -value=>'Page'}),
submit({-name=>'Sort', -value=>'Last'}),
submit({-name=>'Sort', -value=>'First'}), 
submit({-name=>'Sort', -value=>'Age'}),
submit({-name=>'Sort', -value=>'Sex'}), 
submit({-name=>'Sort', -value=>'Color'}),
				#The Occupation Standard field is temporarily commented out until we 
				#consolidate the
				#two occupation fields in the mysql database.
				#submit({-name=>'Sort', -value=>'Occupationstandard'}), 
submit({-name=>'Sort', -value=>'Occupation'})])));
	}
	elsif ($fieldType ==2) {
				#Gen Search, no Occupation in the so can't do a greater 
				#than comparision for fieldType. 
		print Tr(th{-align=>'center'}, ([(submit({-name=>'Sort', -value=>'ED#'})),
submit({-name=>'Sort', -value=>'Page'}),
submit({-name=>'Sort', -value=>'Soundex'}),
submit({-name=>'Sort', -value=>'Dwelling'}),
submit({-name=>'Sort', -value=>'Family'}),
submit({-name=>'Sort', -value=>'Last'}),
submit({-name=>'Sort', -value=>'First'}),
submit({-name=>'Sort', -value=>'Middle'}),
submit({-name=>'Sort', -value=>'Suffix'}),
submit({-name=>'Sort', -value=>'Age'}),
submit({-name=>'Sort', -value=>'Sex'}),
submit({-name=>'Sort', -value=>'Color'})]));
	}
	elsif ($fieldType==3) {
	#inter Search
                print Tr(th{-align=>'center'}, ([(submit({-name=>'Sort', -value=>'ED#'})),
submit({-name=>'Sort', -value=>'Page'}),
submit({-name=>'Sort', -value=>'Dwelling'}),
submit({-name=>'Sort', -value=>'Family'}),
submit({-name=>'Sort', -value=>'HR'}),
submit({-name=>'Sort', -value=>'Last'}),
submit({-name=>'Sort', -value=>'First'}),
submit({-name=>'Sort', -value=>'Middle'}),
submit({-name=>'Sort', -value=>'Age'}),
submit({-name=>'Sort', -value=>'Infant Age'}),
submit({-name=>'Sort', -value=>'Sex'}),
submit({-name=>'Sort', -value=>'Color'}),
					#submit({-name=>'Sort', -value=>'Occupationstandard'}),
submit({-name=>'Sort', -value=>'Occupation'}),
					#submit({-name=>'Sort', -value=>'Relatstandard'}),
submit({-name=>'Sort', -value=>'Relationshipliteral'}),
submit({-name=>'Sort', -value=>'CC'}),
submit({-name=>'Sort', -value=>'POB'})]));
	}
	elsif ($fieldType ==4) {
                print Tr(th{-align=>'center'}, ([(submit({-name=>'Sort', -value=>'ED#'})),
submit({-name=>'Sort', -value=>'Page'}),
submit({-name=>'Sort', -value=>'Dwelling'}),
submit({-name=>'Sort', -value=>'Family'}),
submit({-name=>'Sort', -value=>'HR'}),   
submit({-name=>'Sort', -value=>'Last'}),
submit({-name=>'Sort', -value=>'First'}),
submit({-name=>'Sort', -value=>'Middle'}),
submit({-name=>'Sort', -value=>'Suffix'}),
submit({-name=>'Sort', -value=>'Color'}),
submit({-name=>'Sort', -value=>'Sex'}),
submit({-name=>'Sort', -value=>'Age'}),
submit({-name=>'Sort', -value=>'Infant Age'}), 
					#submit({-name=>'Sort', -value=>'Relationshipstandard'}),
submit({-name=>'Sort', -value=>'Relationshipliteral'}),
submit({-name=>'Sort', -value=>'CC'}),
					#submit({-name=>'Sort', -value=>'Occupationstandard'}),
submit({-name=>'Sort', -value=>'Occupation'}),
submit({-name=>'Sort', -value=>'Monthsunemployed'}),
submit({-name=>'Sort', -value=>'Sickness'}),
submit({-name=>'Sort', -value=>'Blind'}),
submit({-name=>'Sort', -value=>'Deaf'}),
submit({-name=>'Sort', -value=>'Idiotic'}),
submit({-name=>'Sort', -value=>'Insane'}),
submit({-name=>'Sort', -value=>'Attendedschool'}),
submit({-name=>'Sort', -value=>'Cannotread'}),
submit({-name=>'Sort', -value=>'Cannotwrite'}),
submit({-name=>'Sort', -value=>'POB'}),
submit({-name=>'Sort', -value=>'Foreignbirth'}),
submit({-name=>'Sort', -value=>'Fathersbirth'}),
submit({-name=>'Sort', -value=>'Fathersforeignborn'}),
submit({-name=>'Sort', -value=>'Mothersbirth'}),
submit({-name=>'Sort', -value=>'Motherforeignborn'}),
submit({-name=>'Sort', -value=>'Notes'})]));
	}
	print end_form;
}


sub determineFields{
#Procedure instantiates the global array, @list to the fields requested by the user.  The array is
#comprised of the names of the fields in the underlying databases.
#fields such as minAge, maxAge, dwellComp, famComp, compOp not included b/c the list
#forms backbone of many things such as table headings, etc.
	my($fieldType) = shift;
      if (param('submit') eq 'Show All Fields') {
                $fieldType =4;
        }
	if ($fieldType ==1) {
	#Simple Search
		@list =
("FKenumerationdistrict","page.pagenumbercode","lastname","firstname","age","sex","color","occupationliteral");
	}
	#MUST LEAVE SOUNDEX OUT FOR NOW
	elsif ($fieldType == 2) {
	#Gen Search
		@list = ("FKenumerationdistrict","page.pagenumbercode", "lastname_soundex","dwelling",
"family",
"lastname", "firstname", "middlename", "namesuffix", "age", "sex","color");
	}
	elsif ($fieldType == 3) {
	#Inter Search 
		@list =
("FKenumerationdistrict","page.pagenumbercode","dwelling","family","householdrank","lastname","firstname","middlename","age","infantage","sex","color","occupationliteral","relationshipliteral","civilcondition","placeofbirth");
	}
	elsif($fieldType == 4) {
                @list =
("FKenumerationdistrict","page.pagenumbercode","dwelling","family","householdrank","lastname","firstname","middlename","namesuffix",
"color","sex","age","infantage","relationshipliteral","civilcondition","occupationliteral","monthsunemployed","sickness",
"blind","deaf","idiotic","insane","attendedschool","cannotread","cannotwrite","placeofbirth","foreignbirth","fathersbirth",
"fatherforeignborn","mothersbirth","motherforeignborn","notes");
	}
return @list;
}


sub isNum {
#checks to see if parameter is number
#returns 1 if param is NUmber, else 0

	my($str) = shift;
	my($digit);
	my($index);
	for ($index=0; $index<length($str); $index++) {
		$digit = substr($str, $index, 1);
		if (('0' gt $digit) || ('9' lt $digit)) {
			return 0;
		}

	}
  return 1;
}

sub inPop {
	my($temp) = shift;
	my(@array) = ("", "Sex", "Relationship", "Color", "district");
	for (my($counter) = 0; $counter < @array; $counter++) {
		if ($temp eq $array[$counter]) {
			return 1;
		}
	}
	return 0;
}


sub didSearch{
#Checks to see if a search was done.
	determineFields(param('hiddenST'));
	for (my($index)=0; $index<@list; $index++) {
		if (!(inPop(param($list[$index])))) {
			return 1;			
		}
	}
	if (!((param('sndx') eq "") && (param('minAge') eq "") &&
(param('maxAge') eq "") && (param('Black') eq "") && (param('White') eq "") && (param('Mulatto') eq "")))
{
		return 1;
	}  
	return 0;
}



sub validate {
#Going to validate Age as Number
#Infant Age between 0 and 12
#minAge < maxAge
#dwelling, family and household rank as numbers as well
#should Make errors an array so that can output in tabular format.

my($st) = shift;
        if (!(didSearch)) {
                @errors = (@errors, "YOU ENTERED NO SEARCH PARAMETERS");
		return 0;
        }
        if (!(param('sndx') eq "")) {
		my($sndLetter) = substr(param('sndx'), 0, 1);
		my($sndDigs) = substr(param('sndx'), 1);
                if (isNum($sndLetter)) {
                        @errors = (@errors, "SNDX CODE INVALID -- IT MUST BEGIN WITH A LETTER");
                }
		if (!(isNum($sndDigs))) {
			@errors = (@errors, "SNDX MUST FINISH WITH 3 NUMBERS-- NO LETTERS");
		}
        }
	if (!(param('age') eq "")) {
		if (!(isNum(param('age')))) {
			@errors = (@errors, "AGE INVALID -- AGE MUST BE A NUMBER");
		}
	}
	if ((!(param('minAge') eq "")) || (!(param('maxAge') eq ""))) {
		if (!(isNum(param('minAge')))) {
			@errors = (@errors, "The Lower Age Bound Must Be a Number");
		}
		if (!(isNum(param('maxAge')))) {
			@errors = (@errors, "The Upper Age Bound Must Be a Number");
		}
		if (param('minAge') >= param('maxAge')) {
			@errors = (@errors, "Minimum Age is Greater than Maximum Age.  Cannnot Perform Search");
		}
	}
	if (!(param('lastname') eq "")) {
		if (isNum(param('lastname'))) {
			@errors = (@errors, "You entered a number in the last name.  I do not
understand the query.");
		}
	}
	if ($st>1) {
	#check family/dwelling
		if (!(param('dwelling') eq "")) {
			if (!(isNum(param('dwelling')))) {
				@errors = (@errors, "Dwelling Must Be A Number");
			}
		}
		if (!(param('family') eq "")) {
			if (!(isNum(param('family')))) {
				@errors = (@errors,  "Family Must Be a Number. " );
			}
		}
	}

	if ($st > 2) {
	#check infantAge, householdrank
		if (!(param('infantage') eq "")) {
			if (!(isNum(param('infantage')))) {
				@errors = (@errors, "Infant Age Must Be a Number. ");
			}
			elsif (param('infantage') > 12) {
				@errors = (@errors, "Infant Age is in Months...There are only 12 months in a year. ");				
			}
		}
		if (!(param('householdrank') eq "")) {
			if (!(isNum(param('householdrank')))) {
				@errors = (@errors, "Household Rank Must Be A Number.    ");
			}
		}
	}
	if ($st>3) {
	#check mo.unemployed
		if (!(param('monthsunemployed') eq "")) {
			if (!(isNum(param('monthsunemployed')))) {
				@errors = (@errors, "Months Unemployed Must Be a Number.   ");
			}
		}
	}

	if (!(@errors)) {
		return 1;
	}
	else {
		return 0;
	}

} #end validate



sub format_input{
	#essentially the backbone of the query process
	#formats the input and stores in a hash table
        my(%hash);
	my($temp) = "";
	my(@formatFields) = @list;

	for (my($count)=0; $count < @formatFields; $count++) {
#	print ($count, ". ", $formatFields[$count], "-->", param($formatFields[$count]), br());
		if (param($formatFields[$count]) eq "") {
			$hash{"$formatFields[$count]"} = "";
		}
		################################################################
		#NOTE: WE KNOW FROM HERE ON, either the field was a pop-up     #
		#or there was an entry made in it.  Therefore Act Accordingly  #
		#Might Want to modify using inPop function.                  #
		#################################################################
		elsif (($formatFields[$count] eq "page.pagenumbercode") ||($formatFields[$count] eq
"dwelling") || ($formatFields[$count] eq "family") || $formatFields[$count] eq "householdrank") {
			$hash{"$formatFields[$count]"} = param($formatFields[$count]);
		}
		elsif (($formatFields[$count] eq "lastname") || ($formatFields[$count] eq "firstname") || $formatFields[$count] eq "middlename") {
			 $hash{"$formatFields[$count]"} = param($formatFields[$count]);
		}
		elsif (($formatFields[$count] eq "lastname_soundex")){
			$hash{"$formatFields[$count]"} =
param($formatFields[$count]);
		}
		elsif (inPop(param($formatFields[$count])))  {  
			#formerly "eq "Sex"
                        $hash{"$formatFields[$count]"} = "";
                }                
#############The following code allows for the user to search using either the occupliteral field or
				#occupstd
		elsif ($formatFields[$count] eq "occupationliteral") {
			if (param($formatFields[$count]) eq "Occupation") {
				if (!(param('occupationliteral2') eq "")) {
					$hash{"$formatFields[$count]"} = "%" .
						param('occupationliteral2') . "%";
				}
			}
		else {
	$hash{"$formatFields[$count]"} = "%" . param($formatFields[$count]) . "%";
			}
		}
		#Give it a y or n for yes or no
		elsif ((param($formatFields[$count]) eq "Yes") || (param($formatFields[$count]) eq "No")
|| (param($formatFields[$count]) eq "Male") || (param($formatFields[$count]) eq "Female")) {
                        $hash{"$formatFields[$count]"} = substr(param($formatFields[$count]), 0, 1) . "%"; 
                }
  		else {
		#APPEND A WILDCARD TO INCREASE SEARCH POTENTIAL
                   	$hash{"$formatFields[$count]"} = param($formatFields[$count]) . "%";
                }
	}
	return %hash;                        
} #end format_input


sub createQuery {
#Actually creates the statement to send to the underlying DB
	my(%hash) = @_;
	#$places stores the order of the search for bind parameters
	my(@plArray) = @list;
	my($places)="";

	my($index);
	for ($index=0; $index< @plArray; $index++) {
		if (!($hash{"$plArray[$index]"} eq "")) {
			if ($plArray[$index] eq 'age') {
				$places = $places . "age " . param('compOperator') . " ? AND ";
			}
			elsif($plArray[$index] eq 'page.pagenumbercode') {
				$places = $places . "page.pagenumbercode " . param('pageComp') . " ? AND 
";
			}

			elsif ($plArray[$index] eq 'dwelling') {                                       
                                $places = $places . "dwelling " . param('dwellComp') . " ? AND ";
                        }
			elsif ($plArray[$index] eq 'family') {
                                $places = $places . "family " . param('famComp') . " ? AND ";
                        }
			else {
		                $places = $places . $plArray[$index] . " LIKE ? AND ";
			}
		}
	}	

        if (!(param('minAge') eq "")) {
          $places = $places . "age >= " . param('minAge') . " AND age <= " . param('maxAge') . " AND ";
	}
	if (param('White') || param('Black') || param('Mulatto')) {
		$places = $places . "(";
	}
         if (param('White')) {
                $places = $places . " color LIKE 'white' OR   ";
                 }
         if (param('Black')) {
                 $places = $places . " color LIKE 'black' OR   ";
                 }
         if (param('Mulatto')) {
                  $places = $places . " color LIKE 'mulatto' AND ";
                 }
	my($limit) = rindex($places, "AND");
	my($limit2) = rindex($places, "OR");
	if ($limit2 > $limit) {
		$limit = $limit2; #this means an or occurs after an AND
	}
	$places = substr($places, 0, $limit);
        if (param('White') || param('Black') || param('Mulatto')) {
                $places = $places . ")";
	}

return $places;
}

sub get_from_db {
      	print start_table{-border=>2}; 
	determineFields(param('hiddenST'));
	tableHeaders(param('hiddenST'));
	my(@fields) = @list;
	my(%data) = format_input;
	my $fields;
	for (my($x)=0; $x<@list; $x++) {
		$fields = $fields . ", " . $list[$x];
	}
	$fields=substr($fields, 1);
	my $searchST = "SELECT $fields FROM census, page WHERE census.page = page.pagenumbercode AND " .
createQuery(%data);
	if (param('Sort')) {
		my $order = param('Sort');
		if ($sortHash{$order}) {
			$order = $sortHash{$order};
		}
		$order .= ',page,family,householdrank';
		$searchST = $searchST . " ORDER BY "  . $order;
	}
	else { #default Search order
		$searchST = $searchST . " ORDER BY FKenumerationdistrict, page, family, householdrank "; 
	}

	my $query = $dbh->prepare($searchST); 
	my(@bindArray) = @fields;
	my($index);
	my($paramCounter)=1;
	for ($index = 0; $index < @bindArray; $index++) {
                if (!($data{"$bindArray[$index]"} eq "")) {
                	$query->bind_param($paramCounter, $data{"$bindArray[$index]"});
			$paramCounter++;
               }
        } #end for loop
       	$query->execute;
        my($count);
        $count=0;  
        while (my (@cells) = $query->fetchrow_array) {
                $count++;
		for (my($i)=0; $i<@cells; $i++) { 
		#for loop inserted to prevent border problem
		#encountered in tables with blank entries
			if ($cells[$i] eq "") {
				$cells[$i] = "&nbsp;";
			}
		}
	         print font({-size=>-4},
Tr(td({-align=>'center'},[@cells])));
        }
        print hr;
      	print end_table;
	print h1("Number of Matching Records = ", $count, "Out of 5507");
}
#----------------------------------------------MAIN-----------------------------------------------------------#
	print header,start_html('myInlineFrame2');

#	print <<HEADER;
#	<CENTER><A HREF="http://digitaldurham.duke.edu" TARGET="_top"><IMG 
#SRC="/images/header.jpg">
#	<br> Click Above to Return to Homepage </Center></a>
#HEADER
	if  ((!(param('submit') eq 'Search' || param('submit') eq 'Show All Fields')) &&
(!param('Sort'))) {
		print "<br><br><b><i><center> No query yet
submitted </center></i></b>";	
	}
	elsif (param('Sort')) {
		get_from_db;
	}
	else {
		if (validate(param('hiddenST'))) {			
			get_from_db;
		}
		else {
			print h1("Could Not Complete Search.");
			print start_table;
			my($i);
			for ($i=0; $i<@errors; $i++) {
				print h1(Tr(td($errors[$i])));
			}
			print end_table;
		}
        }
  print end_html;
$dbh->disconnect;

exit;

