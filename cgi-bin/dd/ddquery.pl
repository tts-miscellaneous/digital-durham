#!/usr/bin/perl -T
#####################################Summary of Code################################
#Brijal Padia
#last modified May 14, 2001

#ddquery creates the fields for the user to input his desired query.  Using a variable to represent
#the specific query type ($searchType) ddquery then passes all the fields, whether or not anything was
#inputed,to ddfound.pl.  
#
# Revised 28 September 2004 by Jim Coble
# - Taint checking added to command line
# - "use lib" command added to point to location of DDTools.pm
# - added CGI::DISABLE_UPLOADS
# - added ENV{'PATH'}
#####################################################################################

use strict;
use lib qw(./);
use CGI qw/:standard :html3 :cgi-lib *table *Tr *td *div *ul/;
use CGI::Carp 'fatalsToBrowser';
use DBI;
use POSIX 'strftime';
use DDTools;
use Config::Simple '-strict';

$CGI::POST_MAX=1024 * 5000;
$CGI::DISABLE_UPLOADS = 1;
$ENV{'PATH'} = '/bin:/usr/bin:/usr/local/bin';


# load database configfuration
my $cfg = new Config::Simple();
$cfg->read('../../data/config.ini');
my $db_mode = $cfg->param('setup.env');

my $mysql_db = '';
my $mysql_host = '';
my $mysql_user = '';
my $mysql_pswd = '';

if ($db_mode eq "production") {
	# Database to connect to
	$mysql_db = $cfg->param('production.database');
	$mysql_host = $cfg->param('production.host');
	# User 'dduser' is permitted to read (but not modify) Digital Durham database
	$mysql_user = $cfg->param('production.user');
	$mysql_pswd = $cfg->param('production.password');# Connect to DigitalDurham database
}
elsif ($db_mode eq "staging") {
	# Database to connect to
	$mysql_db = $cfg->param('staging.database');
	$mysql_host = $cfg->param('staging.host');
	# User 'dduser' is permitted to read (but not modify) Digital Durham database
	$mysql_user = $cfg->param('staging.user');
	$mysql_pswd = $cfg->param('staging.password');# Connect to DigitalDurham database
}

# Database connection string
my $mysql_conn = "DBI:mysql:database=$mysql_db:host=$mysql_host";
# Password set based on development or production server
my $dbh = DBI->connect($mysql_conn,$mysql_user,$mysql_pswd);

# Handle error condition where connection could not be made
if (!defined $dbh) {
    die "Cannot connect to mySQL server: $DBI::errstr\n";
}
# Do whatever you need to do vis-a-vis reading from the database


sub race_field {
        my($sth);
	my(@races);
        $sth = $dbh->prepare("SELECT race FROM race");
        $sth->execute;
	@races = ("Color");
        while (my($raceType) = $sth->fetchrow_array) {
		@races= (@races, $raceType);
        }
return @races;	

}

sub occ_field {
	my($sth);
	my(@occs);
	$sth= $dbh->prepare("SELECT occupation FROM occupstandard");
	$sth->execute;
	@occs = ("Occupation");
	while (my($occType) = $sth->fetchrow_array) {
		@occs = (@occs, $occType);
	}
return @occs;
}

sub rel_field {
	my($sth);
	my(@rels);
	$sth= $dbh->prepare("SELECT relationship FROM relatstandard");
	$sth->execute;
	@rels = ("Relationship");
	while (my($relType) = $sth->fetchrow_array) {
		@rels = (@rels, $relType);
	}
	
return @rels;
}

sub simple{
        print ('Enumeration District: ', popup_menu({-name=>'FKenumerationdistrict',
-"values"=>['district', '195', '196']}), br());
	if (param('searchType') >1) {
		print ('Page Number: ', br(), popup_menu({-name=>'pageComp', -"values"=>['=','>','<']}),
textfield({-size=>4, -name=>'page.pagenumbercode'}), br());
        	print ('Dwelling Number: ', br(), popup_menu({-name=>'dwellComp',
-"values"=>['=', '>', '<']}), textfield({-size=>4, -name=>'dwelling'}), br());
        	print ('Family Number: ', br(),  popup_menu({-name=>'famComp', 
-"values"=>['=', '>', '<']}), textfield({-size=>4, -name=>'family'}), br());
	}
	if (param('searchType') >2) {
		print ('Household Rank:  ', textfield({-size=>2, -name=>'householdrank'}), br());
	}
	print ('Last Name: ', textfield('lastname'), br());
	print ('First Name: ', textfield('firstname'), br());
	if (param('searchType') >1) {
		print ('Middle Name: ', textfield('middlename'), br());
	}
	if (param('searchType') >3) {
		print ('Suffix: ', textfield({-size=>3, -name=>'namesuffix'}), br());
	}
	print ( '  Color: ', br(), checkbox({-name=>'White', -value=>'White'}), br(),
checkbox({-name=>'Black', -value=>'Black'}), br(), checkbox({-name=>'Mulatto', -name=>'Mulatto'}), br());
        print ('Sex: ', popup_menu({-name=>'sex', -"values"=>['Sex', 'Male', 'Female'],
-default=>'Sex'}), br());


	print (' Either Type Age: ', popup_menu({-name=>'compOperator', -"values"=>['<',
'>',
'='], -default=>'='}), textfield({-size=>3, -name=>'age'}), br());
	print ('or Enter Range Below: ', br(), textfield({-size=>3, -name=>'minAge'}), ' - ',
textfield({-size=>3, -name=>'maxAge'}), br());
	if (param('searchType') >2) {
		print ('Infant Age (in months): ', textfield({-size=>3, -name=>'infantage'}), br());
	        print ('Relationship: ', popup_menu({-name=>'relatstandard',
-"values"=>[rel_field], -default=>'Relationship'}), br());
	print('or Enter Your Own: ', textfield('relationshipliteral'), br());
 	       print ('Civil Condition: ', radio_group({-name=>'civilcondition', -"values"=>['Single',
'Married', 'Widowed/Divorced'], -default=>'%'}), br());
	}
	if (param('searchType') != 2) {
                print (br(), 'Select From Occupation List: ',
popup_menu({-name=>'occupationliteral',-"values"=>[occ_field], -default=>'Occupation'}), br());
		print ('or Type Occupation Here: ',
textfield('occupationliteral2'), br());
	}
#return;
}
       
sub buttons {
	print (hidden({-name=>'hiddenST', -value=>[param('searchType')]}));
        print (reset({-name=>'Reset', -value=>'Reset'}));
        print (submit({-name=>'submit',-value=>'Search'}));
	print (submit({-name=>'submit', -value=>'Show All Fields'}));
        return;
}      

sub gen{
	print br();
	print ('Soundex: ', textfield({-size=>4,
-name=>'lastname_soundex'}), br(), br(), hr);
}

sub inter {
	print ('Place of Birth: ', textfield('placeofbirth'), br());
}

sub adv {
	print ('Months Unemployed: ', textfield({-size=>2, -name=>'monthsunemployed'}), br());
	print ('Sickness: ', textfield('sickness'), br());
	print ('Blind? ', radio_group({-name=>'blind', -"values"=>['Yes', 'No'], -default=>'%'}), br());
	print ('Deaf? ', radio_group({-name=>'deaf', -"values"=>['Yes', 'No'], -default=>'%'}), br());
 	print ('Idiotic? ', radio_group({-name=>'idiotic',
-"values"=>['Yes', 'No'], -default=>'%'}), br());
 	print ('Insane? ', radio_group({-name=>'insane', -"values"=>['Yes', 'No'], -default=>'%'}), br());
	print ('Attended School? ', radio_group({-name=>'attendedschool', -"values"=>['Yes', 'No'], -default=>'%'}), br());
	print ('Cannot Read? : ', radio_group({-name=>'cannotread', -values=>['Yes', 'No'], -default=>'%'}), br());
	print ('Cannot Write? : ', radio_group({-name=>'cannotwrite', -values=>['Yes', 'No'], -default=>'%'}), br());
	print ('Place of Birth: ', textfield('placeofbirth'), br());
	print ('Foreign Born? ', radio_group({-name=>'foreignbirth', -values=>['Yes', 'No'], -default=>'%'}), br());
	print ('Father POB: ', textfield('fathersbirth'), br());
	print ('Father Foreign Born? ', radio_group({-name=>'fatherforeignborn', -values=>['Yes',
'No'], -default=>'%'}), br());
	print ('Mother POB: ', textfield('mothersbirth'), br());
	print ('Mother Foreign Born? ', radio_group({-name=>'motherforeignborn', -values=>['Yes',
'No'], -default=>'%'}), br());

}

sub topPage {
        print h4("Please Fill in One or More Fields Below:");
	print hr;
	buttons;
        print br();
}




sub parse_input {

        if (param('searchType') == 1) {
        print <<SIMPLE;
        <CENTER><IMG SRC="/images/simple.jpg"></CENTER>
SIMPLE
		topPage;
		simple;
        }
        elsif (param('searchType') == 2) {
        print <<SOUNDEX;
        <CENTER><IMG SRC="/images/soundex.jpg"></CENTER>
SOUNDEX
		topPage;
		gen;
		simple;
        }
        elsif (param('searchType') == 3) {
        print <<INTERMEDIATE;
        <CENTER><IMG SRC="/images/intermediate.jpg"></CENTER>
INTERMEDIATE
		topPage;
		simple;
		inter;
        }
        elsif (param('searchType') == 4) {
        print <<ADVANCED;
        <CENTER><IMG SRC="/images/advanced.jpg"></CENTER>
ADVANCED

		topPage;
		simple;
		adv;
        }
buttons;
return;
}


sub print_box{
   print start_div{-align=>'left'};
    print start_form(-action=>"ddfound.pl", -TARGET=>"myInlineFrame2");
#    print start_table({-border=>1,
#-cellpadding=>5,-cellpadding=>5,-bordercolor=>'darkblue'});
# will print search term in textfield if you are loading this box
#immediately after a search submission.
#        print "<tr><td>";
	print Tr(td(parse_input));
	print end_table;
	print end_form;
	print end_div;
}

sub genLink {
print<<GEN;
<p align="center"><a href="ddquery.pl?searchType=2"><IMG
SRC="/images/soundex.jpg">
GEN
}

sub simpLink { 
print<<SIMP;
<p align="center"><a href="ddquery.pl?searchType=1"><IMG
SRC="/images/simple.jpg">
SIMP

}
sub interLink {
print<<INT;
<p align="center"><a href="ddquery.pl?searchType=3"><IMG
SRC="/images/intermediate.jpg">
INT

}

sub advLink {
#print<<ADV;
# <p align="center"><b><i><font size="4"><a
#href="ddquery.pl?searchType=4"> Advanced Search </a></font></i></b></p>
#ADV
print<<ADV;
<p align="center"><a href="ddquery.pl?searchType=4"><IMG
SRC="/images/advanced.jpg">
ADV

}

sub determineLinks {
my($st) = shift;

	if ($st==1) {
		genLink;
		interLink;
		advLink;
	}
	elsif ($st==2) {
		simpLink;
		interLink;
		advLink;
	}
	elsif ($st==3) {
		genLink;
		simpLink;
		advLink;
	}
	elsif ($st==4) {
		genLink;
		simpLink;
		interLink;
	}


}

#----------------------------------------------MAIN---------------------------------------#
	print header, start_html('Query');
	my(@queryType);
        if (param('searchType') != 0) {
                print_box;
		print hr;
		determineLinks(param('searchType'));
        }
	else{ 
	print br();
	genLink;
	simpLink;
	interLink;
	advLink;

	} #end if condition


    print end_html;
$dbh->disconnect;


exit;


