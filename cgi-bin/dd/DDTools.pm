package DDTools;

use strict;
use CGI qw/:standard :html3 *table *Tr *td *div/;
use CGI::Carp 'fatalsToBrowser';
use DBI;
use POSIX 'strftime';
use Sys::Hostname;


# do not remove line below
# necessary that something is here besides subroutines. 
return 1;

# version 1.0 bi - 4/17/00

# this is a module containing code that I have been using a lot, things like
# time and date conversion utilities.  It seeks to avoid repetition of code.


# ex: my $mysql_pswd = DDTools::set_password();
# this should be at the top of each file which accesses a MySQL database.
# This function returns a password, depending on the current hostname.
sub set_password {
    my $temp = '';
    my $host = hostname();
    $temp = 'd!g!t4L';
    return $temp;
}

# This converts the date from a yy-mm-dd format to a Month Day, Year format.
# i.e. 00-12-31 becomes January 31, 2000.
# Note: NOT year 10,000 compliant!
sub convert_date {
    my $dat = $_[0];
    my $mont = $dat;
    $mont =~ s/.*-(.*)-.*/$1/;
    if ($mont == 1)
    {
	$mont = "January";
    }
    elsif ($mont == 2)
    {
	$mont = "February";
    }
    elsif ($mont == 3)
    {
	$mont = "March";
    }
    elsif ($mont == 4)
    {
	$mont = "April";
    }
    elsif ($mont == 5)
    {
	$mont = "May";
    }
    elsif ($mont == 6)
    {
	$mont = "June";
    }
    elsif ($mont == 7)
    {
	$mont = "July";
    }
    elsif ($mont == 8)
    {
	$mont = "August";
    }
    elsif ($mont == 9)
    {
	$mont = "September";
    }
    elsif ($mont == 10)
    {
	$mont = "October";
    }
    elsif ($mont == 11)
    {
	$mont = "November";
    }
    elsif ($mont == 12)
    {
	$mont = "December";
    }
    $dat =~ s/(.*)-.*-(.*)/$mont $2\, $1/;
    return $dat
}


# converts time from the 14:00:00 time in a MySQL database to American 
# time with AM and PM
sub convert_time {
    my $tim = $_[0];
    $tim =~ s/(.*:.*):.*/$1/;
    my $r = $tim;
    $r =~ s/(.*):.*/$1/;
    if ($r >= 12)  # P.M.
    {
	if ($r > 12) {
	    $r -= 12;
	}
	$tim =~ s/.*:(.*)/$r:$1pm/;
    }
    else # A.M.
    {
	if ($r == 0){
	    $r += 12;
	}
	$tim =~ s/.*:(.*)/$r:$1am/;
    }
    return $tim
}

# returns the current date as a scalar in form yyyy-mm-dd
# Note: NOT year 10,000 compliant! :)
sub current_date {
    my($year, $month, $day, $date);
    $year = strftime("%Y",localtime);
    $month = strftime("%m",localtime);
    $day = strftime("%d",localtime);
    $date .= $year . "-" . $month . "-" . $day;
    return $date;
}




