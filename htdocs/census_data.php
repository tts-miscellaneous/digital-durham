<html>
<head>
<title>Digital Durham</title>
<link rel="stylesheet" type="text/css" media="screen" href="/ui/css/style.css" />
</head>



<div id="contain">
<div id="top">

<div id="search_box_top"><div id="search_box_left">
<form method="get" action="/search">
      <input class="form_textbox" id="q" name="q" type="text" alt="Search Box" style="width:140px;" />
	  <input type="submit" name="submit" value="Go" />
      <input type="hidden" name="site" value="duke_collection" />
      <input type="hidden" name="client" value="digitaldurham" />
      <input type="hidden" name="proxystylesheet" value="digitaldurham" />
      <input type="hidden" name="output" value="xml_no_dtd" />
    </form>
</div></div>

<a href="/"><img src="/images/dd_logo3.gif" /></a>

</div>

<div id="nav">
  <?php

if(isset($x) && $x != "")
  include_once($x.".php");
else
  include_once("nav.php");

?>
</div>

<div id="content">
<div class="margins">

<div class="home_right"><p class="header"><a href="/public.php">Public 
Records</a></p>
<p class="header">Census Data</p>
<center>
<p class="header">1880 Federal Population Census Database</p>
</center>
<p class="header"><a href="census_help.php">Census Help</a></p>
      <p><b><i><font color="#663333">Durham Township, Orange County - 
</font></i></b><font color="#663333"><b><i>1880 
        Federal Population Census</i></b> </font></p>
      <p>Two men enumerated the inhabitants of Durham Township in 1880. W. W. 
        Woods, a forty-five year old farmer visited the residents of South 
Durham 
        Township, Orange County between June 1, 1880 and June 21, 1880 to 
record 
        their personal data. Another Durham Township resident enumerated 
inhabitants 
        who lived north of the railroad tracks. Between June 1, 1880 and July 
        6, 1880, Archie D. Wilkinson gathered data on residents of the North 
Durham 
        Township. He was 48 and listed his occupation as Deputy U.S. Marshal 
on 
        the census schedules. </p>

      <p>Together, these two men gathered data on 5507 residents. Each man 
collected 
        data on large lined pages. These were sent to Washington and tallied 
by 
        workers in the Census Bureau. In the 1930s, the government microfilmed 
        the manuscript census ledgers. Information in our census database was 
        transcribed from the National Archives, Series no. T-9, reels 975 and 
        976.</p>
      <p>SINCE WE ARE CURRENTLY BETA-TESTING THE DIGITAL DURHAM WEB SITE, WE 
DO 
        NOT AUTHORIZE THE USE OF THIS DATA FOR RESEARCH PURPOSES. THIS DATA 
SET 
        CONTAINS ERRORS WHICH REQUIRE CORRECTION. WE WILL REMOVE THIS NOTICE 
WHEN 
        THE DATA HAS BEEN PROOFED AND THE SITE IS OPEN FOR PUBLIC USE. </p>
      <p><a href="/dd-db.php">Search the Census Database</a></p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p><i><a href="/wills.php"></a></font></i></p>
	   <p><a 
href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a> 
&middot; <a href="/about.php">About this site</a> &middot; Copyright 
� 2006. Trudi J. Abel. All Rights Reserved. </p>
 <div id="copyright">
    <p>The copyright interest in the material in this digital collection has not been transferred to the Digital Durham project. These text and images may not be used for any commercial purpose without the permission of the Duke University Rare Book, Manuscript, and Special Collections Library and the Digital Durham Project. Copyright permission for subsequent uses is the responsibility of the user.</p>
  </div>

</div>
</div></div>

</p>


 









