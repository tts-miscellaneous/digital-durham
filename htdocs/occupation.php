<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Digital Durham</title>
<link rel="stylesheet" type="text/css" media="screen" href="/ui/css/style.css" />
</head>

<body>

<div id="contain">
<div id="top">

<div id="search_box_top"><div id="search_box_left">
<form method="get" action="/search">
      <input class="form_textbox" id="q" name="q" type="text" alt="Search Box" style="width:140px;" />
	  <input type="submit" name="submit" value="Go" />
      <input type="hidden" name="site" value="duke_collection" />
      <input type="hidden" name="client" value="digitaldurham" />
      <input type="hidden" name="proxystylesheet" value="digitaldurham" />
      <input type="hidden" name="output" value="xml_no_dtd" />
    </form></div></div>

<a href="/"><img src="/images/dd_logo3.gif" /></a>

</div>

<div id="nav">
  <?php

if(isset($x) && $x != "")
  include_once($x.".php");
else
  include_once("nav.php");

?>
</div>

<div id="content">
<div class="margins">
   <p class="header"><a href="reference.php">Reference</a></p>  
  <p class="header">Occupational Terms </p>
     <table width="80%" border="0">
          <tr> 
            <td width="37%" height="43"><b>Agent: </b></td>
            <td width="63%" height="43">One intrusted with the business of another; 
            an attorney; a minister; a substitute; a deputy; a factor. </td>
          </tr>
          <tr> 
            <td width="37%" height="43"><b>Blacksmith: </b></td>
            <td width="63%" height="43"> 
              <p>A smith who works in iron, and makes iron utensils; an ironsmith.</p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="28"><b>Book-keeper:</b></td>
            <td width="63%" height="28"> 
              <p>Wanting the sense of hearing either wholly or in part; unable to 
                perceive sounds. One who keeps accounts; one who has the charge 
              of keeping the books and accounts in an office. </p>            </td>
          </tr>
          <tr> 
            <td width="37%"><b>Brick: </b></td>
            <td width="63%"> 
              <p>A hard body composed chiefly of clay and sand, tempered together 
                with water, molded into regular forms, usually rectangular, dried 
                in the sun, and burnt in a kiln, or in a heap or stack called a 
              clamp. </p>            </td>
          </tr>
          <tr> 
            <td width="37%"><b>Bricklayer: </b></td>
            <td width="63%"> One whose occupation is to build with bricks: a mason.</td>
          </tr>
          <tr> 
            <td width="37%" height="25"><b>Brickmaker: </b></td>
            <td width="63%" height="25">One who makes bricks, or whose occupation 
            is to make bricks.</td>
          </tr>
          <tr> 
            <td width="37%" height="32"><b>Brickyard: </b></td>
            <td width="63%" height="32">A place where bricks are made.</td>
          </tr>
          <tr> 
            <td width="37%"><b>Box:</b> </td>
            <td width="63%">A case or receptacle of any size, or made of any material.</td>
          </tr>
          <tr> 
            <td width="37%"><b>Butcher: </b></td>
            <td width="63%">One who slaughters animals, or dresses their flesh for 
            market; one whose occupation is to kill animals for food </td>
          </tr>
          <tr> 
            <td width="37%" height="92"><b>Carriage: </b></td>
            <td width="63%" height="92"> 
              <p>That which carries or conveys on wheels; a vehicle, especially 
                for pleasure or for passengers, sometimes for burdens; as, a close 
              <i>carriage</i>; a gun <i>carriage</i>. </p>            </td>
          </tr>
          <tr> 
            <td width="37%"><b>Clerk: </b></td>
            <td width="63%">An assistant in a shop or store, who sells goods, keeps 
            accounts, &c.</td>
          </tr>
          <tr> 
            <td width="37%" height="69"><b>Clothier:</b> </td>
            <td width="63%" height="69"> 
              <p>1. One who makes cloths. </p>
              <p>2. One who sells cloth or clothing. </p>
              <p>3. One who dresses or fulls cloth.</p>
              <p><b>Fulls:</b> To thicken cloth; to smooth, bleach. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="69"><b>Cook: </b></td>
            <td width="63%" height="69">One whose occupation is to prepare food 
            for the table; one who dresses meat or vegetables for eating. </td>
          </tr>
          <tr> 
            <td width="37%" height="32"><b>Day Laborer:</b></td>
            <td width="63%" height="32">One who works by the day.</td>
          </tr>
          <tr> 
            <td width="37%"><b>Dealer: </b></td>
            <td width="63%">One who deals; one who has to do, or has concern, with 
              others' especially, a trader, a trafficker, a shopkeeper, a broker, 
              or a merchant, as a <i>dealer</i> in dry goods; a <i>dealer</i> in 
            hardware; a <i>dealer</i> in stocks. </td>
          </tr>
          <tr> 
            <td width="37%" height="63"><b>Dentist: </b></td>
            <td width="63%" height="63"> 
              <p>One who makes it his business to clean, extract, and repair natural 
              teeth, and to insert artificial ones. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="49"><b>Domestic: </b></td>
            <td width="63%" height="49"> 
              <p>One who lives in the family of another, as hired assistant; a house 
              servant.</p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="31"><b>Drayman: </b></td>
            <td width="63%" height="31">A man who attends a dray.</td>
          </tr>
          <tr> 
            <td width="37%" height="65"><b>Dray: </b></td>
            <td width="63%" height="65"> 
              <p>1. A low cart or carriage on wheels, drawn by a horse, and used 
              for heavy burdens. </p>
              <p>2. A rude sort of cart without wheels; a drag. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Dressmaker: </b></td>
            <td width="63%" height="52">A maker of gowns, or similar garments; a 
            mantua-maker.</td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Druggist: </b></td>
            <td width="63%" height="52">One who deals in drugs; especially one whose 
              occupation is merely to buy and sell drugs, without compounding or 
            preparation. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Editor: </b></td>
            <td width="63%" height="52">One who edits; especially a person who prepares, 
              superintends, revises, and corrects book, magazine, or newspaper, 
            &c, for publication. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Engineer: </b></td>
            <td width="63%" height="52"> 
              <p>1. A person skilled in the principles and practice of engineering. 
              Either civil or military. </p>
              <p>2. One who manages an engine; an engine driver. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Factory hand:</b></td>
            <td width="63%" height="52"> 
              <p><b>Factory:</b> A building, or collection of buildings, appropriated 
                to the manufacture of goods; the place where workmen are employed 
                in fabricating goods, wares, or utensils; a manufactory; as, a cotton 
              <i>factory</i>. </p>
              <p><b>Hand:</b> See <b>Hand</b> below. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="57"><b>Factory manager:</b></td>
            <td width="63%" height="57"> 
              <p>See <b>Factory</b> above. </p>
              <p>See <b>Manager</b> below. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Farm hand:</b> </td>
            <td width="63%" height="52"> 
              <p><b>Farm:</b></p>
              <blockquote> 
                <p> 1. A tract of land inclosed or set apart for cultivation by 
                a tenant; a piece of ground farmed out or rented for agriculture.              </p>
                <p>2. An extended piece of ground, devoted by its owner to agriculture; 
                a landed estate. </p>
                <p>3. The state of land leased on rent reserved; a lease.</p>
                <p> 4. A tract of country farmed out for the collection of the revenues.              </p>
              </blockquote>
              <p><b>Hand:</b> See below. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Farm labor/Farm laborer: </b></td>
            <td width="63%" height="52">See <b>Laborer </b>below.</td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Farmer: </b></td>
            <td width="63%" height="52">One who farms; as, (<i>a.</i>) One who hires 
              and cultivates a farm; a cultivator of leased ground; a tenant. (<i>b.</i>) 
              One who takes taxes, customs, excise, or other duties, to collect 
              for a certain rate per cent; as, a <i>farmer</i> of revenues. (<i>c.</i>) 
              One who is devoted to the tillage of the soil; an agriculturist; a 
            husband. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Farming: </b></td>
            <td width="63%" height="52">The business of cultivating land.</td>
          </tr>
          <tr> 
            <td width="37%" height="57"><b>Farming lab/ Farming, laborer: </b></td>
            <td width="63%" height="57"> 
              <p>See <b>Farm</b>. </p>
              <p>See <b>Laborer</b>. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Fish dealer: </b></td>
            <td width="63%" height="52">See <b>Dealer.</b></td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Gardener/Gardner: </b></td>
            <td width="63%" height="52">One who makes and tends a garden; a horticulturist.</td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Grocer: </b></td>
            <td width="63%" height="52">A trader who deals in tea, sugar, spices, 
            coffee, liquors, fruits, &c.</td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Hand:</b> </td>
            <td width="63%" height="52">An agent, servant, or laborer; a laborer 
              trained or competent for special service or duty; a performer more 
            or less skillful. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Hand at warehouse: </b></td>
            <td width="63%" height="52"> 
              <p>See <b>Hand</b> above. </p>
              <p>See <b>Warehouse</b> below. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Hardware Merchant: </b></td>
            <td width="63%" height="52"> 
              <p><b>Hardware: </b>Ware made of metal, as cutlery, kitchen furniture, 
              and the like. </p>
              <p><b>Merchant:</b> See below. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Harness Maker: </b></td>
            <td width="63%" height="52"> 
              <p><b>Harness: </b>The equipments of a draught horse, for a wagon, 
              coach, gig, chaise, &c; tackle; tackling. </p>
              <p><b>Maker:</b> See below. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="88"><b>Hired laborer/ Hired out:</b></td>
            <td width="63%" height="88"> 
              <p><b>Hire:</b> To engage in service for a stipulated reward; to contract 
                with for wages; as, to hire a servant for a year; to hire laborers 
              by the day or month. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Hostler: </b></td>
            <td width="63%" height="52"> 
              <p>1. The person who has the care of horses at an inn; -- so called 
              because the innkeeper formerly attended to this duty in person.</p>
              <p> 2. Any one who takes care of horses; a stableboy; a groom. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="52"> <b>Hotel: </b></td>
            <td width="63%" height="52">A house for entertaining strangers or travelers; 
              a hostel or hostelry; an inn or public house; especially, one of some 
            style or pretensions. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Hotel Chambermaid:</b></td>
            <td width="63%" height="52">A woman who has the care of chambers, making 
              the beds, and cleaning the rooms, or who dresses a lady, and waits 
            upon her in her apartment. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Hotel Cook:</b></td>
            <td width="63%" height="52">One whose occupation is to prepare food 
            for the table; one who dresses meat or vegetables for eating. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Hotell Keeper ( Hotel Keeper):</b></td>
            <td width="63%" height="52">One who has the care, custody, or superintendence 
              of any thing; as, the keeper of a park, a pound, of sheep, of a gate, 
            &c. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Hotel Porter:</b></td>
            <td width="63%" height="52">A man that has the charge of a door or gate; 
            a door-keeper; one who waits at the door to receive messages. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Hotel Proprietor:</b></td>
            <td width="63%" height="52">One who has the legal right or exclusive 
              title to any thing, whether in possession or not; an owner; as, the 
            <i>proprietor </i>of a farm, or of a mill. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Hotel Waiter:</b></td>
            <td width="63%" height="52">One who waits; an attendant; a servant in 
            attendance.</td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>House Keeper/Housekeeper: </b></td>
            <td width="63%" height="52">A female servant who has the chief care 
            of the family, and superintends the other servants. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Housekeeping: </b></td>
            <td width="63%" height="52">The family state in a dwelling; care of 
            domestic concerns; management of home affairs. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Hous(e) Mover:</b></td>
            <td width="63%" height="52"> <b>Mover:</b> The person or thing that 
            moves, stirs, or changes place.</td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>House Servant: </b></td>
            <td width="63%" height="52"> See <b>Servant </b>below.</td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Insurance/Insurance agt. (agent): </b></td>
            <td width="63%" height="52"> 
              <p><b>Insurance:</b> The premium paid for insuring property or life.            </p>
              <p><b>Agent: </b>One intrusted with the business of another; an attorney; 
              a minister; a substitute; a deputy; a factor. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Jeweler: </b></td>
            <td width="63%" height="52">One who makes or deals in jewels and other 
            ornaments.</td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Keeping: </b></td>
            <td width="63%" height="52">A holding; restraint; custody; guard; preservation.</td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Keeping House/Keeps house: </b></td>
            <td width="63%" height="52">See <b>Housekeeping.</b></td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Keeps eating saloon:</b></td>
            <td width="63%" height="52"> <b>Saloon:</b> A spacious and elegant apartment 
              for the reception of company, or for works of art ; a hall of reception; 
              a large public room or parlor; applied also to halls for public entertainment 
              or amusement; also, to apartments for specific public uses; as, the 
              <i>saloon</i> of a steamboat, a refreshment <i>saloon</i>, or the 
            like. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Laborer: </b></td>
            <td width="63%" height="52">One who labors in a toilsome occupation; 
              a man who does work that requires little skill, as distinguished from 
            an <i>artisan</i>; -- sometimes called a <i>laboring man</i>. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Labors on farm: </b></td>
            <td width="63%" height="52"> 
              <p>See <b>Farm.</b> </p>
              <p><b>Labor:</b> Physical toil or bodily exertion, especially when 
                fatiguing, irksome, or unavoidable, in distinction from sportive 
                exercise; hard, muscular effort directed to some useful end, as 
              agriculture, manufactures, and the like; servile toil; exertion.            </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Land Lady: </b></td>
            <td width="63%" height="52">1. A woman who has tenants holding from 
            her. 2. The mistress of an inn, or lodging-house. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Laundress: </b></td>
            <td width="63%" height="52">A female who employment is to wash clothes; 
            a washer-woman.</td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Lawyer:</b> </td>
            <td width="63%" height="52">One versed in the laws, or a practitioner 
              of law; one whose profession is to institute suits in courts of law, 
              and to prosecute or defend the cause of clients; -- a general term, 
              comprehending attorneys, counselors, solicitors, barristers, sergeants, 
            and advocates. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Leaf dealer:</b></td>
            <td width="63%" height="52"> 
              <p><b>Leaf:</b> One of the three principal parts or organs of vegetation. 
                It is developed by increase at its base from and about the stem, 
              and has a definite shape and limited growth. </p>
              <p>See <b>Dealer. </b></p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Machinist: </b></td>
            <td width="63%" height="52">A constructor of machines and engines; one 
            versed in the principles of machines. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Maker: </b></td>
            <td width="63%" height="52">One who makes, forms, shapes, or molds; 
            a manufacturer; often, especially, the Creator. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Makes bricks/Making brick:</b></td>
            <td width="63%" height="52"> 
              <p>See <b>Maker.</b></p>
              <p> <b>Brick:</b> A hard body composed chiefly of clay and sand, tempered 
                together with water, molded into regular forms, usually rectangular, 
                dried in the sun, and burnt in a kiln, or in a heap or stack called 
              a clamp. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Makes shoes:</b></td>
            <td width="63%" height="52"><b>Shoe:</b> A covering for the foot, usually 
              of leather, composed of a thick species for the sole, and a thinner 
              kind for the upper part; also, any thing resembling a shoe in form 
            or use. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Manager: </b></td>
            <td width="63%" height="52">One who manages; a conductor or director; 
              one who uses address in bringing about his purposes; as, the <i>manager</i> 
            of a theater; the <i>manager</i> of a lottery, of a ball &c. </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Manager in Warehouse:</b> </td>
            <td width="63%" height="54">See <b>Manager</b> and <b>Warehouse</b>.</td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Mantua Maker: </b></td>
            <td width="63%" height="54">A ladies' dress-maker; one who makes women's 
            clothes.</td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Manufacturer: </b></td>
            <td width="63%" height="54">One who manufactures; a person engaged in 
            the business of working raw materials into wares suitable for use.          </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Manufacturer of Tobacco/Manufacturer 
            Tb:</b></td>
            <td width="63%" height="54">See <b>Manufacturer</b>. See <b>Tobacco</b>.          </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Marble dealer:</b></td>
            <td width="63%" height="54"> 
              <p><b>Marble: </b>Any species of calcareous stone or mineral, of a 
                compact texture, and of a beautiful appearance, susceptible of a 
                good polish; any firm limestone, fitted, either when polished or 
                otherwise, for ornamental uses; also, other rocks of nearly the 
                same hardness, capable of the same uses, as serpentine; also, but 
                improperly, polished slabs of harder rocks, as porphyry, granite, 
              and the like. </p>
              <p>See <b>Dealer.</b> </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Mason: </b></td>
            <td width="63%" height="54">A man whose occupation is to lay bricks 
            and stones in walls or structures of any kind. </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Matress (Mattress) maker:</b></td>
            <td width="63%" height="54"> 
              <p>See <b>Maker</b>. </p>
              <p><b>Mattress:</b> A quilted bed; a bed stuffed with hair, moss or 
              other soft material, and quilted. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Mechanic: </b></td>
            <td width="63%" height="54">One who works with machines or instruments; 
              a workman or laborer other than agricultural; an artisan; an artificer; 
              more specifically, one who practices any mechanic art; one skilled 
              or employed in shaping and uniting materials, as wood, metal, &c, 
              into any kind of structure, machine, or other object, requiring the 
            use of tools, or instrument. </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Merchant: </b></td>
            <td width="63%" height="54">One who traffics or carries on trade: especially 
              upon a large scale; one who buys goods to sell again; any one who 
            is engaged in the purchase and sale of goods; a trafficker; a trader.          </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Militiaman: </b></td>
            <td width="63%" height="54"> 
              <p>One who belongs to the militia. </p>
              <p><b>Militia:</b> The body of soldiers in a state enrolled for discipline, 
                but engaged in actual service only in emergencies, as distinguished 
              from regular troops, whose sole occupation is war or military service.            </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Miller: </b></td>
            <td width="63%" height="54"> 
              <p>One whose occupation is to attend a grist-mill. </p>
              <p><b>Grist-mill</b>: A mill for grinding grain; especially, a mill 
              for grinding grists, or portions of grain brought by different customers.            </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Milliner: </b></td>
            <td width="63%" height="54">A person, usually a woman, who makes and 
            sells head-dresses, hats or bonnets, &c , for women. </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Millwright: </b></td>
            <td width="63%" height="54">A mechanic whose occupation is to build 
            mills.</td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Minister: </b></td>
            <td width="63%" height="54">One who serves at the altar; one who performs 
              sacerdotal duties; the pastor of a church duly authorized or licensed 
            to preach the gospel and administer the sacraments. </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Moulder (Molder):</b> </td>
            <td width="63%" height="54">One who, or that which, molds or forms into 
            shape.</td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Nothing: </b></td>
            <td width="63%" height="54">Not any thing; no thing; -- opposed to <i>any 
            thing</i> or <i>something</i>.</td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Nurse: </b></td>
            <td width="63%" height="54">One who nourishes; a person who supplies 
              food, tends, or brings up; as, (a.) A woman who has the care of young 
              children; especially, one who suckles an infant not her own. (b.) 
            A person, especially a woman, who has the care of sick persons. </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>On farm: </b></td>
            <td width="63%" height="54">See <b>Farm.</b></td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Ornamental Painter:</b></td>
            <td width="63%" height="54"> 
              <p><b>Ornamental: </b>Serving to ornament; giving additional beauty; 
              embellishing. </p>
              <p><b>Painter:</b> See below.</p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Painter:</b> </td>
            <td width="63%" height="54">One whose occupation is to paint; one skilled 
            in representing things in colors. </td>
          </tr>
          <tr> 
            <td width="37%" height="54"> 
              <p><b>Paper Box Mfg.</b><b> (Manufacturing): </b></p>            </td>
            <td width="63%" height="54">See <b>Manufacturer</b>.</td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Peddler: </b></td>
            <td width="63%" height="54">One who peddles; a traveling trader; one 
              who carries about small commodities on his back, or in a cart or wagon, 
            and sells them. </td>
          </tr>
          <tr> 
            <td width="37%" height="54"> 
              <p><b>Phot. Artist (Photographist): </b></p>            </td>
            <td width="63%" height="54">One who practices, or is skilled in, photography.</td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Physician: </b></td>
            <td width="63%" height="54">A person skilled in physic or the art of 
            healing; one whose profession is to prescribe remedies for disease.          </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Plasterer: </b></td>
            <td width="63%" height="54"> 
              <p>One who plasters.</p>
              <p><b>Plaster:</b> To overlay or cover with plaster, as the partitions 
                of a house, walls, and the like. A composition of lime, water, and 
                sand, for coating walls and partitions of houses; also, gypsum or 
                plaster of Paris, as used for making ornaments, figures, moldings, 
              and the like. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Plasterer Mason: </b></td>
            <td width="63%" height="54">See <b>Plasterer </b>and <b>Mason.</b></td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Plough maker:</b></td>
            <td width="63%" height="54"><b>Plough: </b>A well-known implement for 
            turning up the soil, drawn by animal or other power. </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Policeman: </b></td>
            <td width="63%" height="54">One of the ordinary police.</td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Porter: </b></td>
            <td width="63%" height="54">A man that has the charge of a door or gate; 
            a door-keeper; one who waits at the door to receive messages. </td>
          </tr>
          <tr> 
            <td width="37%" height="56"><b>Porter in store: </b></td>
            <td width="63%" height="56">See <b>Porter.</b></td>
          </tr>
          <tr> 
            <td width="37%" height="104"><b>Postmaster: </b></td>
            <td width="63%" height="104">The master of a post; as, (a.) One who 
              has charge of a station for the accommodation of travelers; one who 
              supplies post-horses. (b.) One who has charge of a post-office, and 
            the distribution and forwarding of mails. </td>
          </tr>
          <tr> 
            <td width="37%" height="53"><b>Preacher: </b></td>
            <td width="63%" height="53">One who preaches; one who discourses publicly 
            on religious subjects.</td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Printer: </b></td>
            <td width="63%" height="54">One who prints, impresses, or stamps; especially, 
            one who prints books, newspapers and the like. </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Printing: </b></td>
            <td width="63%" height="54">The act, art, or practice of impressing 
              letters, characters, or figures on paper, cloth, or other material; 
            the business of a printer; typography. </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Printing-office: </b></td>
            <td width="63%" height="54">A place where books, pamphlets, and the 
            like, are printed.</td>
          </tr>
          <tr> 
            <td width="37%" height="54"> 
              <p><b>R.R. Agt. ( Agent ): </b></p>
              <p><b>R.R. Clerk Railroad agt. (Agt. / Agent ) </b></p>            </td>
            <td width="63%" height="54"> 
              <p>See <b>Agent</b>. </p>
              <p>See <b>Clerk</b>. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Railroad: </b></td>
            <td width="63%" height="54">A road or way on which iron rails are laid 
            for wheels to run on, for the conveyance of heavy loads in vehicles.          </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Restaurant: </b></td>
            <td width="63%" height="54">An eating-house.</td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Retail Dealer:</b></td>
            <td width="63%" height="54"> 
              <p><b>Retail: </b>The sale of commodities in small quantities or parcels, 
              or at second hand. </p>
              <p>See <b>Dealer.</b> </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Retail grocer: </b></td>
            <td width="63%" height="54">See <b>Retail</b> and <b>Grocer.</b></td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Retail liquor d./ Retail liquor dealer: 
            </b></td>
            <td width="63%" height="54"> 
              <p>See <b>Dealer. </b></p>
              <p><b>Liquor: </b></p>
              <p>1. Any liquid or fluid substance, as water, milk, blood, sap, juice, 
              and the like.</p>
              <p> 2. Especially, alcoholic or spiritous fluid, either distilled 
              or fermented; a decoction, solution, or tincture. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Retail liquor store: </b></td>
            <td width="63%" height="54"> 
              <p>See <b>Liquor and Retail.</b> </p>
              <p><b>Store: </b></p>
              <p>1. A place of deposit for large quantities; a store house; a ware- 
              house; a magazine. </p>
              <p>2. Hence, any place where goods are sold, whether by wholesale 
              or retail. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Saddler: </b></td>
            <td width="63%" height="54">One whose occupation is to make saddles.</td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Sailor: </b></td>
            <td width="63%" height="54">One who follows the business of navigating 
              ships or other vessels; one who understands the management of ships 
            in navigation; a mariner; a seaman. </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Sawyer: </b></td>
            <td width="63%" height="54">One whose occupation is to saw timber into 
            planks or boards, or to saw wood for fuel. </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>School teacher: </b></td>
            <td width="63%" height="54">One who teaches or instructs a school.</td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Seamstress: </b></td>
            <td width="63%" height="54">A woman whose occupation is sewing; a needle-woman.</td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Servant: </b></td>
            <td width="63%" height="54">One who serves, or does service, voluntarily 
              or involuntarily; a person who is employed by another for menial offices, 
              or for other labor, and is subject to his command; a person who labors 
              or exerts himself for the benefit of another, his master or employer; 
            a subordinate helper. </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Service: </b></td>
            <td width="63%" height="54">The act of serving; the occupation of a 
              servant; the performance of labor for the benefit of another, or at 
              another's command; attendance of an inferior, or hired helper, or 
              slave, &c., on a superior, employer, master, or the like; also, spiritual 
            obedience. </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Setting type:</b></td>
            <td width="63%" height="54"><b>Set: </b>To put in type; as, to <i>set 
              up</i> a page of copy; to arrange in words, lines, &c., ready for 
            printing; as, to <i>set up</i> type. </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Sewing:</b> </td>
            <td width="63%" height="54">The act or occupation of sewing using the 
            needle.</td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Sewing machine agt. (Agent):</b></td>
            <td width="63%" height="54"> 
              <p><b>Sewing machine:</b> A machine for reducing the labor of sewing 
              by hand. </p>
              <p>See <b>Agent.</b> </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Shoe Maker/ Shoemaker: </b></td>
            <td width="63%" height="54">One whose occupation or trade is to make 
            shoes and boots.</td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Shop: </b></td>
            <td width="63%" height="54"> 
              <p>1. A building in which goods, wares, drugs, &c., are sold by retail.            </p>
              <p>2. A building in which mechanics work. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="56"><b>Sportsman: </b></td>
            <td width="63%" height="56">One who pursues the sports of the field; 
            one who hunts, fishes, and fowls. </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Stone cutter:: </b></td>
            <td width="63%" height="54">One whose occupation is to cut or hew stone.</td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Stone mason: </b></td>
            <td width="63%" height="54">A mason who works or builds in stone.</td>
          </tr>
          <tr> 
            <td width="37%" height="54"> 
              <p><b>Supertender </b><b> (Superintender): </b></p>            </td>
            <td width="63%" height="54">One who superintends; a superintendent. 
              <b>Superintendent:</b> One who has the oversight and charge of something, 
              with the power of direction; as, the <i>superintendent</i> of an alms-house 
              or work house; the <i>superintendent</i> of public works; the superintendent 
            of customs or finance. </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Tailor: </b></td>
            <td width="63%" height="54">One whose occupation is to cut out and make 
            men's garments.</td>
          </tr>
          <tr> 
            <td width="37%" height="59"><b>Teach/Teacher/Teaching: </b></td>
            <td width="63%" height="59">One who teaches or instructs, or one whose 
            business or occupation is to instruct others; an instructor; a tutor.          </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Teamster: </b></td>
            <td width="63%" height="54"> 
              <p>One who drives a team.</p>
              <p> <b>Team:</b> Two or more horses, oxen, or other beasts harnessed 
                together to the same vehicle for drawing, as to a coach, chariot, 
              wagon, cart, sled, sleigh, and the like. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="66"><b>Tel. Operator (Telegraph Operator / Electro-magnetic 
            telegraph ):</b></td>
            <td width="63%" height="66">A telegraph in which an operator at one 
              station causes words or signs to be recorded or exhibited at another 
              by means of a current of electricity, generated by a battery, and 
            transmitted over an intervening wire. </td>
          </tr>
          <tr> 
            <td width="37%" height="59"><b>Teller in bank:</b></td>
            <td width="63%" height="59"><b>Teller: </b>An officer of a bank, who 
            counts over money received, and pays it out on checks. </td>
          </tr>
          <tr> 
            <td width="37%" height="59"><b>Tinner: </b></td>
            <td width="63%" height="59"> 
              <p>1. One who works in the tin mines. </p>
              <p>2. One who works in tin ware; a tinman. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="59"><b>Tobacco: </b></td>
            <td width="63%" height="59"> 
              <p>1. A plant, a native of America, of the genus Nicotiana, much used 
                for smoking and chewing, and in snuff. As a medicine, it is narcotic, 
                emetic, and cathartic. Tobacco has a strong, peculiar smell, and 
              an acrid taste. </p>
              <p>2. The leaves of the plant prepared for smoking, chewing, &c, by 
              being dried, and manufactured in various ways. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="64"><b>Tob. Dealer/Tobacco dealer:</b></td>
            <td width="63%" height="64"> See <b>Tobacco</b> and <b>Dealer</b>.</td>
          </tr>
          <tr> 
            <td width="37%" height="59"><b>Tob. Manufactuer/Tob. Manufacturer/Tobacco 
            manfac/tobacco manufacturer:</b></td>
            <td width="63%" height="59">See<b> Tobacco</b> and <b>Manufacturer</b>.          </td>
          </tr>
          <tr> 
            <td width="37%" height="59"><b>Tobacco warehouse: </b></td>
            <td width="63%" height="59">See <b>Tobacco</b> and <b>Warehouse.</b></td>
          </tr>
          <tr> 
            <td width="37%" height="64"> 
              <p><b>Tobacconest/Tobacconist/</b></p>
              <p><b>Tobaconist: </b></p>            </td>
            <td width="63%" height="64">A dealer in tobacco; also, a manufacturer 
            of tobacco. </td>
          </tr>
          <tr> 
            <td width="37%" height="59"><b>Traveling agt. </b></td>
            <td width="63%" height="59"> 
              <p>Possibly a <b>Traveler</b>: </p>
              <p>1. One who travels in any way.</p>
              <p> 2. Specifically, a commercial agent who travels for the purpose 
              of receiving orders for merchants, making collections and the like.            </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="59"><b>Wagoner: </b></td>
            <td width="63%" height="59">One who conducts a wagon; a wagon-driver.          </td>
          </tr>
          <tr> 
            <td width="37%" height="59"><b>Waiter: </b></td>
            <td width="63%" height="59">One who waits; an attendant; a servant in 
            attendance.</td>
          </tr>
          <tr> 
            <td width="37%" height="59"> 
              <p><b>Waiter at hotel: Waiting House: Waits in house: Waits on house: 
              </b></p>            </td>
            <td width="63%" height="59">See <b>Waiter</b>.</td>
          </tr>
          <tr> 
            <td width="37%" height="45"><b>Warehouse: </b></td>
            <td width="63%" height="45">A storehouse for goods.</td>
          </tr>
          <tr> 
            <td width="37%" height="43"><b>Ware house hand: </b></td>
            <td width="63%" height="43">See <b>Warehouse</b> and <b>Hand</b></td>
          </tr>
          <tr> 
            <td width="37%" height="64"><b>Wash woman/ Washer woman/ Washes/Washing: 
            </b> </td>
            <td width="63%" height="64">A woman who washes clothes for others, or 
            for hire. </td>
          </tr>
          <tr> 
            <td width="37%" height="64"><b>Watchman: </b></td>
            <td width="63%" height="64">One who guards the streets of a city or 
            building by night. </td>
          </tr>
          <tr> 
            <td width="37%" height="64"><b>Welldigger: </b></td>
            <td width="63%" height="64">As for <b>Wellborer</b>: One who digs or 
            bores for water; one who makes wells. </td>
          </tr>
          <tr> 
            <td width="37%" height="69"><b>Wheel wright: </b></td>
            <td width="63%" height="69">A man whose occupation it is to make wheels 
            and wheel-carriages, as carts and wagons. </td>
          </tr>
          <tr> 
            <td width="37%" height="72"><b>Wife: </b></td>
            <td width="63%" height="72">The lawful consort of a man; a woman who 
            is united to a man in wedlock; -- correlative of <i>husband</i>. </td>
          </tr>
          <tr> 
            <td width="37%" height="72"><b>Wood cutter: </b></td>
            <td width="63%" height="72"> 
              <p>1. A person who cuts wood.</p>
              <p> 2. One who makes wood-cuts; an engraver on wood. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="72"><b>Work: </b></td>
            <td width="63%" height="72"> To exert one's self for a purpose; to put 
              forth effort for the attainment of an object; to labor; to operate; 
            to be engaged in the performance of a task, a duty, or the like. </td>
          </tr>
          <tr> 
            <td width="37%" height="72"> 
              <p> <b>Work in Tob: Work in tobacco: Works in tob: Works in tobacco: 
              Works in tobacco factory: </b></p>            </td>
            <td width="63%" height="72">See <b>Work</b>, <b>Tobacco</b> and <b>Factory</b>.</td>
          </tr>
          <tr> 
            <td width="37%" height="72"> 
              <p><b>Work on farm: Working on farm: Works at home farm laborer: Works 
              on a farm: </b></p>            </td>
            <td width="63%" height="72">See <b>Work</b>, <b>Farm</b> , <b>Laborer</b>.</td>
          </tr>
          <tr> 
            <td width="37%" height="72"> 
              <p><b>Working in factory: Works at factory: Works factory:Works in 
              a factory: Works in fac Works in factory: </b></p>            </td>
            <td width="63%" height="72">See <b>Work</b>, <b>Factory</b> </td>
          </tr>
          <tr> 
            <td width="37%" height="72"> 
              <p><b>Works at brick yard: Works brickyard: Works in brick: Works 
              in brick yard: Works in brickyd:</b> </p>            </td>
            <td width="63%" height="72">See <b>Work</b>, <b>Brick</b>, <b>Brickyard</b></td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Works in box factory: Works in box shop: 
            </b></td>
            <td width="63%" height="54">See <b>Work</b>, <b>Box</b>, <b>Factory</b>, 
            <b>Shop</b>.</td>
          </tr>
          <tr> 
            <td width="37%" height="57"><b>Works in carriage factory: </b></td>
            <td width="63%" height="57">See <b>Work</b>, <b>Carriage</b>, <b>Factory</b></td>
          </tr>
          <tr> 
            <td width="37%" height="49"><b>Works in Jewellery (Jewelry): </b></td>
            <td width="63%" height="49">Jewels in general; the art or trade of a 
            jeweler.</td>
          </tr>
          <tr> 
            <td width="37%" height="49"><b>Works in marble: </b></td>
            <td width="63%" height="49">See <b>Marble</b>.</td>
          </tr>
          <tr> 
            <td width="37%" height="61"><b>Works in printing/printing office: </b></td>
            <td width="63%" height="61">See <b>Printer</b> or <b>Printing-Office.</b></td>
          </tr>
          <tr> 
            <td width="37%" height="47"><b>Works in restaurant: </b></td>
            <td width="63%" height="47">See <b>Restaurant.</b></td>
          </tr>
          <tr> 
            <td width="37%" height="120"><b>Works in sack factory: </b></td>
            <td width="63%" height="120"> 
              <p><b>Sack: </b>A bag for holding and carrying goods of any kind; 
                a receptacle made of some kind of pliable materials, as cloth, leather, 
              or the like; a large pouch.</p>
              <p> See <b>Factory</b>. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="102"><b>Works in snuff factory: </b></td>
            <td width="63%" height="102"> 
              <p>Pulverized tobacco or other substance, taken, or prepared to be 
              taken, into the nose. </p>
              <p>See <b>Factory</b>.</p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="49"><b>Works in warehouse: </b></td>
            <td width="63%" height="49">See <b>Warehouse.</b></td>
          </tr>
          <tr> 
            <td width="37%" height="46"><b>Works on railroad: </b></td>
            <td width="63%" height="46">See <b>Railroad.</b></td>
          </tr>
  </table>
     <p>&nbsp;</p>
     <p><a 
href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a> 
&middot; <a href="/about.php">About this site</a> &middot; Copyright &#169; 2001 - 2006. Trudi J. Abel. All Rights Reserved. 
</p>
     <div id="copyright">
    <p>The copyright interest in the material in this digital collection has not been transferred to the Digital Durham project. These texts and images may not be used for any commercial purpose without the permission of the Duke University Rare Book, Manuscript, and Special Collections Library and the Digital Durham Project. Copyright permission for subsequent uses is the responsibility of the user.</p>
  </div>
</div>
</div>
</div>


</body>
