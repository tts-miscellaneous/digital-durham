<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- #BeginEditable "doctitle" -->

   
<title>Digital Durham</title>

<!-- #EndEditable --> 

<!-- #BeginEditable "description" -->

<meta name="description" content="An online web resource for educators that uses a database of census information, maps, images, public records, diaries, journals and other documents to explore Durham, North Carolina in the 19th century.">

<!-- #EndEditable -->

<!-- #BeginEditable "keywords" -->

<meta name="keywords" content="Digital Durham, North Carolina, history, census, maps, images, letters, documents, educator's resource, education, classroom activities, research">
   
<!-- #EndEditable -->

<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body bgcolor="#FFFFFF" onLoad="MM_preloadImages('../template-images/home_on.gif','../template-images/project-overview_on.gif','../template-images/letters_on.gif','../template-images/maps_on.gif','../template-images/public-records_on.gif','../template-images/reference_on.gif','../template-images/teacher-corner_on.gif','../template-images/public-forum_on.gif','../template-images/search_on.gif','../template-images/site-index_on.gif')">
<table cellspacing="0" cellpadding="0" border="0">
  <tr> 
    <td height="160" colspan="3" valign="top" bgcolor="#FFFFCC"><img src="../template-images/header.gif" width="592" height="160" alt="Digital Durham: Life and Labor in the New South"></td>
    <td width="2" height="160" valign="top" bgcolor="#FFFFCC"></td>
  </tr>
  <tr> 
    <td rowspan="2" valign="top" bgcolor="#996633" width="142"> 
      <p><a href="../index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image16','','../template-images/home_on.gif',1)"><img name="Image16" border="0" src="../template-images/home_off.gif" width="143" height="24" alt="Home"></a><br>
        <a href="../overview.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image17','','../template-images/project-overview_on.gif',1)"><img name="Image17" border="0" src="../template-images/project-overview_off.gif" width="143" height="24" alt="Project Overview"></a><br>
        <a href="../documents.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image18','','../template-images/letters_on.gif',1)"><img name="Image18" border="0" src="../template-images/letters_off.gif" width="143" height="24" alt="Letters and Journals"></a><br>
        <a href="../images.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image19','','../template-images/maps_on.gif',1)"><img name="Image19" border="0" src="../template-images/maps_off.gif" width="143" height="24" alt="Maps and Images"></a><br>
        <a href="../records.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image20','','../template-images/public-records_on.gif',1)"><img name="Image20" border="0" src="../template-images/public-records_off.gif" width="143" height="24" alt="Public Records"></a><br>
        <a href="../reference.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image21','','../template-images/reference_on.gif',1)"><img name="Image21" border="0" src="../template-images/reference_off.gif" width="143" height="24" alt="Reference"></a><br>
        <a href="../teacher.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image22','','../template-images/teacher-corner_on.gif',1)"><img name="Image22" border="0" src="../template-images/teacher-corner_off.gif" width="143" height="24" alt="Teacher's Corner"></a><br>
        <a href="../forum.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image23','','../template-images/public-forum_on.gif',1)"><img name="Image23" border="0" src="../template-images/public-forum_off.gif" width="143" height="24" alt="Public Forum"></a><br>
        <a href="http://www.lib.duke.edu/texis/webinator/search_dd" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image24','','../template-images/search_on.gif',1)"><img name="Image24" border="0" src="../template-images/search_off.gif" width="143" height="24" alt="Search"></a><br>
        <a href="../site-index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image25','','../template-images/site-index_on.gif',1)"><img name="Image25" border="0" src="../template-images/site-index_off.gif" width="143" height="24" alt="Site Index"></a><br>
      </p>
    </td>
    <td width="1" height="449" valign="top"></td>
    <td rowspan="2" colspan="2" valign="top" bgcolor="#FFFFCC"> <!-- #BeginEditable "Content%20Area" --> 
      <p><br>
        <font face="Times New Roman, Times, serif"><b><font size="+2" color="#663300">Insert 
        Page Title Here</font></b></font></p>
      <p><font face="Times New Roman, Times, serif">Insert body text and other 
        content here.</font></p>
      <!-- #EndEditable --></td>
  </tr>
  <tr> 
    <td width="1" height="387" valign="top"></td>
  </tr>
  <tr> 
    <td width="144" height="1" valign="top"><img width="143" height="1" src="../template-images/transparent.gif"></td>
    <td width="1" height="1" valign="top"><img width="1" height="1" src="../template-images/transparent.gif"></td>
    <td width="448" height="1" valign="top"><img width="448" height="1" src="../template-images/transparent.gif"></td>
    <td width="2" height="1" valign="top"><img width="2" height="1" src="../template-images/transparent.gif"></td>
  </tr>
</table>
<p align="center"><font face="Times New Roman, Times, serif" size="-1">Standard 
  copyright and other information goes here.</font></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
