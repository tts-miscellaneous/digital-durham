<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Digital Durham</title>
<link rel="stylesheet" type="text/css" media="screen" href="/ui/css/style.css" />
</head>

<body>

<div id="contain">
<div id="top">


<div id="search_box_top"><div id="search_box_left">
<form method="get" action="/search">
      <input class="form_textbox" id="q" name="q" type="text" alt="Search Box" style="width:140px;" />
	  <input type="submit" name="submit" value="Go" />
      <input type="hidden" name="site" value="duke_collection" />
      <input type="hidden" name="client" value="digitaldurham" />
      <input type="hidden" name="proxystylesheet" value="digitaldurham" />
      <input type="hidden" name="output" value="xml_no_dtd" />
    </form>
</div></div>
<a href="/"><img src="/images/dd_logo3.gif" /></a>

</div>

<div id="nav">
  <?php

if(isset($x) && $x != "")
  include_once($x.".php");
else
  include_once("nav.php");

?>
</div>

<div id="content">
<div class="margins">
  <p class="header"><a href="/reference.php">Reference</a></p>  
  <p class="header">Editorial Notes </p>

<p>This Glossary is presented to provide definitions of terms appearing 
        in the 1880 Census. The source used for the definitions is the 1870 
edition 
        of <b>Webster's American Dictionary of the English Language</b>.</p>
      <p> The preface to this edition was written by Noah Porter, July, 1864. 
        The following notes describe the conditions under which definitions 
were 
        selected and included in this Glossary. It will be helpful to review 
the 
        notes prior to searching for a particular definition. </p>

      <p>Entries in the Glossary are arranged in categories relating to 
headings 
        used in the form of the Census, as follows: </p>
      <table width="96%" border="0">
        <tr> 
          <td width="76%">Census Heading</td>
          <td width="24%">Glossary Heading</td>
        </tr>
        <tr> 
          <td width="76%">Relationship of person to the head of family</td>

          <td width="24%"><b>Relationships </b></td>
        </tr>
        <tr> 
          <td width="76%">Profession, occupation or trade</td>
          <td width="24%"><b>Occupations</b></td>
        </tr>
        <tr> 
          <td width="76%">Diseased condition of person</td>

          <td width="24%"><b>Medical Terms</b></td>
        </tr>
  </table>
      <p>The definitions for each entry were chosen from those offered in the 
        Dictionary. In those cases where the Dictionary presented several 
definitions 
        for a particular entry, the one or two definitions that relate 
directly 
        to the Census heading were selected for inclusion in this Glossary. 
Other 
        definitions were eliminated for purposes of simplification. </p>
      <p>The main entries in each Glossary category are derived directly from 
        the manuscript Census and are arranged in alphabetical order within 
the 
        category. These entries appear on the left margin and are in bold 
print. 
        As certain entries were included, it was noted that the definitions 
themselves 
        sometimes contained words that, if defined, would assist in making 
clear 
        the language used in the Dictionary. As a result, secondary terms and 
        their definitions are included within an entry, each indented and 
introduced 
        by the secondary term in <b>bold</b>. </p>
      <p>Ex.: </p>

      <p> <b>Dropsy:</b> An unnatural collection of serous fluid in any cavity 
        of the body?. </p>
       
        <blockquote><b>Serous: </b>Thin; watery; like whey?. </blockquote>
      
      <p>Each entry is included as spelled in the Census. Occasionally, the 
Census-taker 
        misspelled terms consistently or perhaps incorrectly here and correct 
        there. Where correct and incorrect spellings occurred, they are both 
listed. 
        Where only incorrect spellings appeared, they are followed in the 
Glossary 
        by a corrected spelling in parentheses. Ex.: </p>
      <p>Two or more spellings used in the Census: </p>

      <p><b>Measels, Measles Neice, Nice, Niece </b></p>
      <p>Incorrect spelling followed by corrected: </p>
      <p><b>Minengitis ( Meningitis) Pneuralgia ( Neuralgia) </b></p>
      <p>Abbreviated terms appear in the Census. In these instances, the 
editors 
        of the Glossary understood them to be intended as in the example 
below: 
      </p>
      <p> <b>Phot. Artist</b>: See <b>Photographic Artist. </b></p>

      <p>The Glossary includes several instances where multiple terms were 
used 
        to described what the editors consider to be essentially the same 
term. 
        This is particularly true of the "occupations" entered in the Census. 
        In these cases, the terms as they appeared in the Census are grouped 
and 
        the definitions in common are listed beneath the group. Ex: </p>
      <p><b>Works on farm </b></p>
      <p><b>Working on farm </b></p>
      <p><b>Works at home farm laborer </b></p>
      <p><b>Works on a farm.</b></p>
      <p>In similar fashion, occupations that have a common introductory term 
        were grouped together as in the following example: </p>

      <p>Hotel Chambermaid </p>
      <p>Hotel Cook </p>
      <p>Hotell Keeper ( Hotel Keeper)</p>
      
        <blockquote> 
          <p><b>Hotel</b>: (defined)</p>
          <p> <b>Chambermaid:</b> (defined)</p>

          <p><b>Cook: </b>(defined)</p>
        </blockquote>
     
      <p>Some terms appearing in the Census simply were not included in the 
Dictionary. 
        In those instances, the terms have been eliminated from the Glossary. 
        Work, Farm, Laborer.</p>
     
      <ul>
        <li><a href="/medical.php">Glossary of Medical Terms</a></li>

        <li><a href="/occupation.php">Glossary of 
Occupations</a></li>
        <li><a href="/relationship.php">Glossary of 
Relationships</a></li>
      </ul>
  <p><a 
href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a> 
&middot; <a href="/about.php">About this site</a> &middot; Copyright &copy; 2001 - 2006. 
Trudi J. Abel. All Rights Reserved. </p> 
 <div id="copyright">
    <p>The copyright interest in the material in this digital collection has not been transferred to the Digital Durham project. These text and images may not be used for any commercial purpose without the permission of the Duke University Rare Book, Manuscript, and Special Collections Library and the Digital Durham Project. Copyright permission for subsequent uses is the responsibility of the user.</p>
  </div>
</div>
</div>
</div>

</body>
</html>
