<?php

include_once("common/teiParse.php");
include_once("common/common.php");

if (isset($_GET['id']) && $_GET['id'] != "" && is_numeric($_GET['id'])) {
	$ledgerID = $_GET['id'];
} else {
	$dOut = "Our apologies.  We are unable to find this item in our collection.  Please try again in a few moments.";
	print ($dOut);
	exit;
}

// Ok, let's try to find this record in our collection

$_query = 
	"SELECT 
		item.collection_prefix, 
		item.notes2, 
		item.item_id, 
		item.category, 
		item.folder, 
		item.item_number, 
		item.dc_title, 
		item.dc_description, 
		item.reference, 
		item.page_count 
	FROM 
		item 
	WHERE 
		item_id = ?
	LIMIT 1;";
$_params = array($ledgerID);

try {
	$result = R::getAll($_query, $_params);
		
	if (count($result) == 0) {
		$dOut = "Our apologies.  We are unable to find this item in our collection.  Please try again in a few moments.";
		print ($dOut);
		exit;
	}
} catch (\Exception $e) {
	$dOut = "Our apologies.  We are unable to find this item in our collection.  Please try again in a few moments.";
	print ($dOut);
	exit;
}


for ($z = 0; $z < count($result); $z++) {
	$xmlDocument = "tei/" . $result[$z]["folder"] . $result[$z]["item_number"] . ".xml";
	$pageCount = $result[$z]["page_count"];
	
	$tempTitle = $result[$z]["dc_title"];

	$collection_prefix = $result[$z]["collection_prefix"];
	$category = $result[$z]["category"];
	$folder = $result[$z]["folder"];
	$item_number = $result[$z]["item_number"];
	$notes2 = $result[$z]["notes2"];
	
}

$tei = true;

//print ("looking for $xmlDocument<P>");
$noticeText = "";
$tei = true;
if (!file_exists($xmlDocument)) {
//	$dOut = "Our apologies.  We are unable to find this item in our collection.  Please try again in a few moments.";
//	print ($dOut);
//	exit;
	$tei = false;
}


//$xmlDocument = "01010.xml";

if ($tei) {
	$xml = new teiParse($xmlDocument);
	$xml->parse();
	
	$dOut = "";
	
	$xmlRecord = $xml->getTitle();
	$title = "";
	
	for ($i = 0; $i < count($xmlRecord); $i++) {
		foreach($xmlRecord[$i] as $heading => $value) {
			if ($heading == "title") {
				$title .= $value . "<br />";
			}
		}
	}
	
	$xmlRecord = $xml->getAuthor();
	$author = "";
	
	for ($i = 0; $i < count($xmlRecord); $i++) {
		foreach($xmlRecord[$i] as $heading => $value) {
			if ($heading == "author") {
				$author .= $value . "<br />";
			}
		}
	}
	
	$xmlRecord = $xml->getFunder();
	$funder = "";
	
	for ($i = 0; $i < count($xmlRecord); $i++) {
		foreach($xmlRecord[$i] as $heading => $value) {
			if ($heading == "funder") {
				$funder .= $value . "<br />";
			}
		}
	}
	
	$xmlRecord = $xml->getTranscription();
	$transcription = "";
	
	for ($i = 0; $i < count($xmlRecord); $i++) {
		foreach($xmlRecord[$i] as $heading => $value) {
				$transcription .= ucfirst($heading) . ": " . $value . "<br />";
		}
	}
	
	$xmlRecord = $xml->getEdition();
	$edition = "";
	
	for ($i = 0; $i < count($xmlRecord); $i++) {
		foreach($xmlRecord[$i] as $heading => $value) {
				$edition .= ucfirst($heading) . ": " . $value . "<br />";
		}
	}
	
	
	$xmlRecord = $xml->getPublication();
	$publication = "";
	
	for ($i = 0; $i < count($xmlRecord); $i++) {
		foreach($xmlRecord[$i] as $heading => $value) {
				$publication .= ucfirst($heading) . ": " . $value . "<br />";
		}
	}
	
	$xmlRecord = $xml->getSource();
	$source = "";
	
	for ($i = 0; $i < count($xmlRecord); $i++) {
		foreach($xmlRecord[$i] as $heading => $value) {
				$source .= ucfirst($heading) . ": " . $value . "<br />";
		}
	}
	
	$xmlRecord = $xml->getProfile();
	$profile = "";
	$profileLite = ""; 
	
	for ($i = 0; $i < count($xmlRecord); $i++) {
		foreach($xmlRecord[$i] as $heading => $value) {
				$heading = ereg_replace("item", "Class", $heading);
				$profile .= ucfirst($heading) . ": " . "<!--<a href=\"hueism.php?dduMenu_0=subjectheading&dduMenu_0_value=$value&SendSearch=Search&x=search&SendSearch=1\">-->$value<!--</a>-->" . "<br />";
				$profileLite .= "<!--<a href=\"hueism.php?dduMenu_0=subjectheading&dduMenu_0_value=$value&SendSearch=Search&x=search&SendSearch=1\">-->$value<!--</a>-->" . "<br />";
		}
	}
	
	
	// Now that we have assembled the metadata, let's actually get
	// the text for this document
	$text = $xml->getText();
	
	if ($notes2 != "") {
		$notesData = "<b>Notes</b>:<br />$notes2<br />";
	} else {
		$notesData = "";
	}

		
} else {

	// No TEI document found.
	$text = "";
	//$text = "A Text Encoding Initative (TEI) file was not found.  Therefore no accompanying metadata or transcription information can be provided for this resource.<br />";
	$title = $tempTitle . "<br />";
	$noticeText = "A Text Encoding Initative (TEI) file was not found.  Therefore no accompanying metadata or transcription information can be provided for this resource.<br />";


	$author = "Unknown<br />";
	$funder = "Unknown<br />";
	$transcription = "Unknown<br />";
	$edition = "Unknown<br />";
	$publication = "Unknown<br />";
	$source = "Unknown<br />";
	$profile = "Unknown<br />";
	$profileLite = "Unknown<br />";
	$dOut = "";

	if ($notes2 != "") {
		$notesData = "<br /><b>Notes</b>:<br />$notes2<br />";
	} else {
		$notesData = "";
	}

	
}


// Now we only want to show the current page that we are viewing. 
// Therefore, we will remove any content, after the body tags that
// is not on the current page.

if (isset($_GET['p'])) {
	$page = (int)$_GET['p'];
} else {
	$page = 1;
}


// Let's get a total page count for the entire
// number of documents


if ($pageCount == "") {
	$pageCount = 1;
}



// If we are on the first page, the we will search for the subsequent 
// next page.  If found, we will remove it

if ($tei) {
	if ($page == 1) {
		$startPattern = "<body>";
		$endPattern = "<pb id=\"p" . ($page+1) . "\" n=\"" . ($page+1) . "\"/>";
		$sPos = strpos($text, $startPattern);
		$ePos = strpos($text, $endPattern);
		$diffLength = $ePos - $sPos;
		$text = substr($text, $sPos, $diffLength);
	
	} else {
	
		// So we are not at the first page.
		// The start position would naturally be 
		// where this portion of the ledger starts and the
		// end position will be either the next page or 
		// the end of the document, whichever comes first
	
		$startPattern = "<pb id=\"p" . ($page) . "\" n=\"" . ($page) . "\"/>";	
		$endPattern = "<pb id=\"p" . ($page+1) . "\" n=\"" . ($page+1) . "\"/>";
		$endOfDoc = "</body>";
		
		$sPos = strpos($text, $startPattern);
		$ePos = strpos($text, $endPattern);
		$ePos2 = strpos($text, $endOfDoc);
			
		if ($sPos == "") {
			// This page is invalid, assume first page
			$page = 1;
			$startPattern = "<body>";
			$endPattern = "<pb id=\"p" . ($page+1) . "\" n=\"" . ($page+1) . "\"/>";
			$sPos = strpos($text, $startPattern);
			$ePos = strpos($text, $endPattern);
			$diffLength = $ePos - $sPos;
			$text = substr($text, $sPos, $diffLength);		
		
		} else {
	
			// The page exists, therefore we will continue to find the end of the 
			// document
			
			if ($ePos == "") {
				// So there is no next page, therefore, use the end of document
				$ePos = $ePos2;
			}
			
			$diffLength = $ePos - $sPos;
			$text = substr($text, $sPos, $diffLength);		
		}
	
	} 

} else {

	// There is no TEI document
	$page = $page;

}


$miniImages = "";

// Now we will create the thumbnails for the images for this personal ledger
if ($page < 10) {
                $cntLargeImage = "00" . ($page) . "0";
}
else {
                $cntLargeImage = "0" . ($page) . "0";
}
for ($i = 0; $i < $pageCount; $i++) {
	
	if (($i+1) < 10) {
		$cntItem = "00" . ($i+1) . "0";
		
	} elseif (($i+1) < 100 && ($i+1) >= 10) {
		$cntItem = "0" . ($i+1) . "0";
		$cntItem = "0" . ($i+1) . "0";

	}
	
	
 	$thumbnailLedger = $imageBase . "/150/" . $collection_prefix . $category . $folder . $item_number . $cntItem . ".jpg";
	
 	$medLedgerImage = $imageBase . "/600/" . $collection_prefix . $category . $folder . $item_number . $cntLargeImage . ".jpg";
 	$largeLedgerImage = $imageBase . "/full/" . $collection_prefix . $category . $folder . $item_number . $cntLargeImage . ".jpg";


	$miniImages .= "<a href=\"hueism.php?x=ledger&p=" . ($i+1) . "&id=$ledgerID\"><img class=\"thumb\" src=\"$thumbnailLedger\" border=\"0\" width=\"50px\"/></a>";
	$largeLedger = "<a href=\"$largeLedgerImage\" target=\"_new\"><img class=\"thumb\" src=\"$medLedgerImage\" border=\"0\" width=\"300px\"/></a>";




}

// We now need to do some preparation before we can send
// the text directly to the browser
$text = ereg_replace("<body>", "", $text);
$text = ereg_replace("</body>", "", $text);
$text = ereg_replace("<lb/>", "<br />", $text);
$text = ereg_replace("</dateline>", "</dateline><br />", $text);
$text = ereg_replace("</salute>", "</salute><p />", $text);
$text = ereg_replace("<row>", "<tr>", $text);
$text = ereg_replace("</row>", "</tr>", $text);
$text = ereg_replace("<cell>", "<td>", $text);
$text = ereg_replace("</cell>", "</td>", $text);
$text = ereg_replace("rend=\"doublestrike\">", "><strike>", $text);
$text = ereg_replace("</hi>", "</hi></strike>", $text);
$text = ereg_replace("rend=\"boldunderline\">", "><b><u>", $text);
$text = ereg_replace("</hi>", "</hi></strike></b></u>", $text);
$text = ereg_replace("rend=\"underline\">", "><u>", $text);
$text = ereg_replace("</hi>", "</hi></strike></b></u>", $text);


$nextPage = $_SERVER['PHP_SELF'];

if ($page == 1) {
	$nextPage = "&middot; &laquo; Previous | <a href=\"$nextPage?x=ledger&p=" . ($page+1) . "&id=$ledgerID\">Next &raquo;</a>";
} elseif ($page < $pageCount) {
	$nextPage = "&middot; <a href=\"$nextPage?x=ledger&p=" . ($page-1) . "&id=$ledgerID\">&laquo; Previous</a> | <a href=\"$nextPage?x=ledger&p=" . ($page+1) . "&id=$ledgerID\">Next &raquo;</a>";
} elseif($page != 1) {
	$nextPage = "&middot; <a href=\"$nextPage?x=ledger&p=" . ($page-1) . "&id=$ledgerID\">&laquo; Previous</a> | Next &raquo;";
} else {
	$nextPage = "";
}

$dOut .=<<< END_DOC


<p class="header"><a href="hueism.php?x=browse">Browse Collection</a> &rsaquo; <a href="hueism.php?x=browse&dduMenu_0_value=1.4&dduMenu_0=series&SendSearch=1">Atlas Rigsbee Ledgerbook</a> &rsaquo; $title</p>

<table cellpadding="0" cellspacing="0">
  <tr>
  <td style="width: 315px;">$largeLedger</td>
  <td>
	<p class="headerInText">Page $page/$pageCount $nextPage</p>
    <p>(<a href="javascript:toggle();">Toggle MetaData View</a> / <a href="javascript:toggleSubject();">View Subject Headings</a> / <a href="$xmlDocument">TEI Transcription</a> /  <a href="printedwork.php?id=$ledgerID&print">Printer-Friendly Version</a>)</p>
    <!--<p><a href="javascript:toggle();"><img src="/images/metadata.gif" border="0" alt="Toggle Metadata" Title="Toggle Metadata"></a>  <a href="javascript:toggleSubject();"><img src="/images/subject.gif" border="0" alt="View Subject Headings" Title="View Subject Headings"></a>  <a href="$xmlDocument"><img src="/images/tei.gif" border="0" alt="TEI Transcription" Title="TEI Transcription"></a> <a href="printedwork.php?id=$ledgerID&print"><img src="/images/printer.gif" border="0" alt="Printer-Friendly Version" Title="Printer-Friendly Version"></a></p>-->
    <div id="metadata" style="margin-bottom: 10px;">
		<b>Title:</b><br />$title
		<b>Author:</b><br />$author
		<b>Funder:</b><br />$funder
		<b>Transcription Information:</b><br />$transcription
		<b>Transcription Edition:</b><br />$edition
		<b>Publication Information:</b><br />$publication
		<b>Source Information:</b><br />$source
		<b>Library of Congress Subject Headings:</b><br />$profile
		<hr />
    </div>
    <div id="subjectHeading" style="margin-bottom: 10px;">
		<b>Library of Congress Subject Headings:</b><br />$profileLite
		<hr />
	</div>
	$text

    <p>
    	$noticeText<br />
    	$notesData
    	$miniImages
   </p>
	
  </td>
</tr>

</table>


<script language="javascript" type="text/javascript">
document.getElementById("metadata").style.display = "none";
document.getElementById("subjectHeading").style.display = "none";
function toggle() {
  if(document.getElementById("subjectHeading").style.display == "block")
    document.getElementById("subjectHeading").style.display = "none";

  if(document.getElementById("metadata").style.display == "none")
    document.getElementById("metadata").style.display = "block";
  else
    document.getElementById("metadata").style.display = "none";
}
function toggleSubject() {
  if(document.getElementById("metadata").style.display == "block")
    document.getElementById("metadata").style.display = "none";

  if(document.getElementById("subjectHeading").style.display == "none")
    document.getElementById("subjectHeading").style.display = "block";
  else
    document.getElementById("subjectHeading").style.display = "none";
}
</script>

END_DOC;


print ($dOut);


function file_url_exists($url) {
	$handle = @fopen($url, "r");
	 if ($handle === false)
		  return false;
	 fclose($handle);
	 return true;
}


?>
