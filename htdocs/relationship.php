<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Digital Durham</title>
<link rel="stylesheet" type="text/css" media="screen" href="/ui/css/style.css" />
</head>

<body>

<div id="contain">
<div id="top">

<div id="search_box_top"><div id="search_box_left"><form method="get" action="/search">
      <input class="form_textbox" id="q" name="q" type="text" alt="Search Box" style="width:140px;" />
	  <input type="submit" name="submit" value="Go" />
      <input type="hidden" name="site" value="duke_collection" />
      <input type="hidden" name="client" value="digitaldurham" />
      <input type="hidden" name="proxystylesheet" value="digitaldurham" />
      <input type="hidden" name="output" value="xml_no_dtd" />
    </form></div></div>

<a href="/"><img src="/images/dd_logo3.gif" /></a>

</div>

<div id="nav">
  <?php

if(isset($x) && $x != "")
  include_once($x.".php");
else
  include_once("nav.php");

?>
</div>

<div id="content">
<div class="margins">
   <p class="header"><a href="reference.php">Reference</a></p>  
  <p class="header">Relationship Terms </p>
       <table width="80%" border="0">
          <tr> 
            <td width="37%" height="43"><b>Adopted: </b></td>
            <td width="63%" height="43">To take into one's family, as son and heir; 
              to take and treat as a child, giving a title to the privileges and 
            rights of a child. </td>
          </tr>
          <tr> 
            <td width="37%" height="43"><b>Aunt: </b></td>
            <td width="63%" height="43"> 
              <p>The sister of one's father or mother; correlative to <i>nephew</i> 
              or <i>niece</i>. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="70"><b>Boarder: </b></td>
            <td width="63%" height="70"> 
              <p>One who has food or diet statedly at another's table for pay; or 
              compensation of any kind. </p>            </td>
          </tr>
          <tr> 
            <td width="37%"><b>Bro-in-law: </b></td>
            <td width="63%"> 
              <p>See <b>Brother-in-law</b>.</p>            </td>
          </tr>
          <tr> 
            <td width="37%"><b>Brother: </b></td>
            <td width="63%"> He who is born of the same father and mother with another, 
              or of one of them only. In the latter case he is called a <i>half-brother</i>, 
            or <i>brother of the</i> <i>half blood</i>. </td>
          </tr>
          <tr> 
            <td width="37%" height="44"><b>Brother-in-law: </b></td>
            <td width="63%" height="44">The brother of a husband or wife; also, 
            a sister's husband.</td>
          </tr>
          <tr> 
            <td width="37%" height="32"><b>Cook: </b></td>
            <td width="63%" height="32">One whose occupation is to prepare food 
            for the table; one who dresses meat or vegetables for eating. </td>
          </tr>
          <tr> 
            <td width="37%"><b>Cousin: </b></td>
            <td width="63%">One collaterally related more remotely than a brother 
            or sister; especially, the son or daughter of an uncle or aunt. </td>
          </tr>
          <tr> 
            <td width="37%" height="48"><b>Daughter: </b></td>
            <td width="63%" height="48">The female offspring of the human species; 
            a female child of any age.</td>
          </tr>
          <tr> 
            <td width="37%" height="31"><b>Daughter-in-law: </b></td>
            <td width="63%" height="31"> 
              <p>The wife of one's son.</p>            </td>
          </tr>
          <tr> 
            <td width="37%"><b>Domestic: </b></td>
            <td width="63%">One who lives in the family of another, as hired assistant; 
            a house- servant. </td>
          </tr>
          <tr> 
            <td width="37%" height="107"><b>Farmer: </b></td>
            <td width="63%" height="107"> 
              <p>a. One who hires and cultivates a farm; a cultivator of leased 
              ground; a tenant. </p>
              <p>b. One who is devoted to the tillage of the soil; an agriculturist; 
              a husband-man. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="69"><b>Father: </b></td>
            <td width="63%" height="69">One who has begotten a child, son or daughter; 
            generator; next male ancestor; male parent. </td>
          </tr>
          <tr> 
            <td width="37%" height="32"><b>Father-in-law:</b> </td>
            <td width="63%" height="32"> 
              <p>The father of one's husband or wife.</p>
              <blockquote> 
                <p> � A man who marries a woman having children already, is popularly 
                called their father-in-law. </p>
              </blockquote>            </td>
          </tr>
          <tr> 
            <td width="37%" height="39"><b>G.Daughter/Granddaughter:</b> </td>
            <td width="63%" height="39">The daughter of a son or daughter.</td>
          </tr>
          <tr> 
            <td width="37%" height="37"><b>G.Mother/Grandmother: </b></td>
            <td width="63%" height="37"> 
              <p>The mother of one's father or mother.</p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="33"><b>G.Son/Grandson: </b></td>
            <td width="63%" height="33"> 
              <p>The son of a son or daughter. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="31"><b>Gr.Dagt: </b></td>
            <td width="63%" height="31">See <b>G.Daughter/Granddaughter.</b></td>
          </tr>
          <tr> 
            <td width="37%" height="68"><b>Grandfather: </b></td>
            <td width="63%" height="68"> 
              <p>A father's or mother's father; an ancestor in the next degree above 
              the father or mother in lineal ascent. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Help: </b></td>
            <td width="63%" height="52">One who helps; especially, a hired man or 
            woman; a domestic servant.</td>
          </tr>
          <tr> 
            <td width="37%" height="155"><b>Husband: </b></td>
            <td width="63%" height="155"> 
              <p>1. A male head of a household; a manager of domestic concerns; 
                one orders the economy of a family; especially, a cultivator; a 
              tiller; a husband-man. </p>
              <p>2. A married man; one wedded to a wife; -- the correlative of <i>wife</i>.            </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="107"><b>Mother : </b></td>
            <td width="63%" height="107"> 
              <p>1. A female parent; especially, one of the human race; a woman 
              who has borne a child; -- correlate to son or daughter. </p>
              <p>2. An old woman or matron. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="41"><b>Mother-in-law: </b></td>
            <td width="63%" height="41"> 
              <p>The mother of one's husband or wife.</p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="105"><b>Neice, Nice, Niece: </b></td>
            <td width="63%" height="105"> 
              <p>1. Formerly, a relative in general, as an aunt; but especially 
              a descendant, whether male or female. </p>
              <p>2. The daughter of a brother or sister. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="64"><b>Nephew: </b></td>
            <td width="63%" height="64"> 
              <p>1. A grandson or remoter lineal descendant. </p>
              <p>2. The son of a brother or sister. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="158"><b>Nurse: </b></td>
            <td width="63%" height="158"> 
              <p>One who nourishes; a person who supplies food, tends, or brings 
                up; as, (a.) A woman who has the care of young children; especially 
                one who suckles an infant not her own. (b.) A person, especially 
              a woman, who has the care of sick persons. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Orphan: </b></td>
            <td width="63%" height="52">A child who is bereaved of both father and 
            mother; sometimes, also, a child who has but one parent living. </td>
          </tr>
          <tr> 
            <td width="37%" height="113"><b>Porter: </b></td>
            <td width="63%" height="113">A man that has the charge of a door or 
              gate; a door-keeper; one who waits at the door to receive messages. 
            A carrier; a person who carries or conveys burdens for hire. </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Servant: </b></td>
            <td width="63%" height="52">One who serves, or does service, voluntarily 
              or involuntarily; a person who is employed by another for menial offices, 
              or for other labor, and is subject to his command; a person who labors 
              or exerts himself for the benefit of another, his master or employer; 
            a subordinate helper. </td>
          </tr>
          <tr> 
            <td width="37%" height="57"><b>Sister: </b></td>
            <td width="63%" height="57"> 
              <p>A female whose parents are the same as those of another person; 
              -- the correlative of <i>brother</i>. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Sister-in-law: </b></td>
            <td width="63%" height="52">A husband's or wife's sister; also, a brother's 
            wife.</td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Son: </b></td>
            <td width="63%" height="52"> 
              <p>1. A male child; the male issue of a parent, father or mother.</p>
              <p> 2. A male descendant, however distant; hence, in the plural, descendants 
              in general.</p>
              <p> 3. Any young male person spoken of as a child; an adopted child; 
              a pupil; or any other young male dependent. </p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="33"><b>Son-in-law: </b></td>
            <td width="63%" height="33">A man married to one's daughter.</td>
          </tr>
          <tr> 
            <td width="37%" height="37"><b>Step-daughter: </b></td>
            <td width="63%" height="37">A daughter by marriage only.</td>
          </tr>
          <tr> 
            <td width="37%" height="37"><b>Step-father: </b></td>
            <td width="63%" height="37"> 
              <p>A father by marriage only.</p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="37"><b>Step-son: </b></td>
            <td width="63%" height="37"> 
              <p>A son by marriage only.</p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="219"><b>Supt.: (Superintendent): </b></td>
            <td width="63%" height="219"> 
              <p>1. One who has the oversight and charge of something, with the 
                power of direction; as, the <i>superintendent </i>of an alms-house 
                or work-house; the <i>superintendent</i> of public works; the <i>superintendent</i> 
              of customs or finance. </p>
              <p>2. A clergyman exercising supervision over the church and clergy 
                of a district, without claiming episcopal authority. Syn: - Inspector; 
              overseer; manager; director; curator.</p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="40"><b>Uncle: </b></td>
            <td width="63%" height="40"> 
              <p>The brother of one's father or mother.</p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="54"><b>Waiter: </b></td>
            <td width="63%" height="54"> 
              <p>One who waits; an attendant; a servant in attendance.</p>            </td>
          </tr>
          <tr> 
            <td width="37%" height="52"> <b>Widow: </b></td>
            <td width="63%" height="52">A woman who has lost her husband by death, 
            and has not taken another; one living bereaved of a husband. </td>
          </tr>
          <tr> 
            <td width="37%" height="49"><b>Widower: </b></td>
            <td width="63%" height="49">A man who lost his wife by death, and has 
            not married again.</td>
          </tr>
          <tr> 
            <td width="37%" height="52"><b>Wife: </b></td>
            <td width="63%" height="52">The lawful consort of a man; a woman who 
            is united to a man in wedlock; -- correlative of <i>husband</i>. </td>
          </tr>
  </table>
       <br>
       <p><a 
href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a> 
&middot; <a href="/about.php">About this site</a> &middot; Copyright &#169; 2001 - 2006. Trudi J. Abel. All Rights Reserved. 
</p>
  <div id="copyright">
    <p>The copyright interest in the material in this digital collection has not been transferred to the Digital Durham project. These text and images may not be used for any commercial purpose without the permission of the Duke University Rare Book, Manuscript, and Special Collections Library and the Digital Durham Project. Copyright permission for subsequent uses is the responsibility of the user.</p>
  </div>
</div></div>
</div>


</body>
