<?php

include_once("common/common.php");
include_once("common/paths.php");
$pdfImages = array();

$workID = null;

if (! empty($_GET['id'])) {
	$workID = $_GET['id'];
} else {
	$dOut = "Our apologies.  We are unable to find this item in our collection.  Please try again in a few moments.";
	print ($dOut);
	exit;
}

if (isset($_GET['p']) && $_GET['p'] != "" && is_numeric($_GET['p'])) {
	$currentPage = $_GET['p'];
} else {
	$currentPage = 1;
}

// Ok, let's try to find this record in our collection

$_query = 
'SELECT 
	item.collection_prefix, 
	item.item_id,
	item.category, 
	item.page_count, 
	item.folder, 
	item.item_number, 
	item.dc_title,
	item.dc_description, 
	item.dc_date, 
	item.copyright, 
	item.Height, 
	item.Width,
	item.dimensions, 
	item.dc_source, 
	item.notes2,
	item.dc_source, 
	item.reference, 
	item.page_count, 
	item_creator.creator, 
	item_creator.role
FROM 
	item
LEFT JOIN 
	item_creator 
ON
	item.item_id = item_creator.item_id
WHERE 
	item.item_id = ?
LIMIT 1';

$result = R::getAll($_query, array($workID));

if (count($result) == 0) {
	$dOut = "Our apologies.  We are unable to find this item in our collection.  Please try again in a few moments.";
	print ($dOut);
	exit;
}


$totalPage = 1;

for ($i = 0; $i < count($result); $i++) {

	$title = $result[$i]["dc_title"];
        $creator = $result[$i]["creator"];
	$collection_prefix = $result[$i]["collection_prefix"];
	$category = $result[$i]["category"];
	$folder = $result[$i]["folder"];
	$item_number = $result[$i]["item_number"];
	$description = $result[$i]["dc_description"];
	$source = $result[$i]["dc_source"];
        $date = $result[$i]["dc_date"];
        $copyright = $result[$i]["copyright"];
	$height = $result[$i]["Height"];
	$width = $result[$i]["Width"];
	$dimension = $result[$i]["dimensions"];
	$totalPage = $result[$i]["page_count"];
}

$PHP_SELF = $_SERVER['PHP_SELF'];
$skipMenu = "<select name=\"jumpTo\" ONCHANGE=\"javascript:change(this);\">\n";

// Generate a menu with quick page
for ($i = 0; $i < $totalPage; $i++) {
	if (($i+1) == $currentPage) {
		$selected = "selected";
	} else {
		$selected = "";
	}
	
	$skipMenu .= "<option value=\"$PHP_SELF?x=printedwork&id=$workID&p=" . ($i+1) . "\" $selected>" . ($i+1) . "</option>\n";
}

$skipMenu .= "</select>\n";

		if ($currentPage < 10) {
			$cntItem = "00" . ($i+1) . "0";
			$cntLargeImage = "00" . "$currentPage" . "0";
		} else if ($currentPage >= 100) {
			$cntItem =  ($i+1) . "0";
			$cntLargeImage =  "$currentPage" . "0";
		} else {
			$cntItem = ($i+1) ;
			$cntLargeImage = "0" . "$currentPage" . "0";
		}
			
	for ($q = 1; $q < $totalPage+1; $q++) {


		if (($q+1) < 10) {
			$pdfImage = "00" . "$q" . "0";
		} else if (($q+1) >= 100) {
			$pdfImage = "$q" . "0";
		} else {
			$pdfImage = "0" . "$q" . "0";
		}	
		
		$pdfItem = $imageBase . "/600/" . $collection_prefix . $category . $folder . $item_number . $pdfImage . ".jpg";
		array_push($pdfImages, $pdfItem);


	}
	
	$largeLetterImage = $imageBase . "/full/" . $collection_prefix . $category . $folder . $item_number . $cntLargeImage . ".jpg";
	$medLetterImage = $imageBase . "/600/" . $collection_prefix . $category . $folder . $item_number . $cntLargeImage . ".jpg";
	$medWork = "<a href=\"$largeLetterImage\" target=\"_new\"><img src=\"$medLetterImage\" border=\"0\" width=\"600px\"/></a>";
	$largeWork = "<a href=\"$imageBase_basepath/$largeLetterImage\" target=\"_new\"><img src=\"$medLetterImage\" border=\"0\" width=\"600px\"/></a>";

	$_query = 
	'SELECT 
		item_subject.subjterm 
	FROM 
		item_subject 
	WHERE 
		item_id = ?';
		
	try {
		$loc = R::getAll($_query, array($workID));
	}
	catch (\Exception $e) {
		$loc = array();
	}
	
	$subject = "";
	
	for ($i = 0; $i < count($loc); $i++) {
		$subject .= "<!--<a 
href=\"hueism.php?dduMenu_0=subjectheading&dduMenu_0_value=" . 
$loc[$i]["subjterm"] . "&SendSearch=Search&x=search&SendSearch=1\">-->" . $loc[$i]["subjterm"] . "<!--</a>--><br />";
	}

	$_query = 
	'SELECT 
		item_spatial.spatialTerm 
	FROM 
		item_spatial 
	WHERE 
		item_id = ?';
	try {
		$spat = R::getAll($_query, array($workID));	
	} catch (\Exception $e) {
		$spat = array();
	}	
	$spatial = "";
	
	if (count($spat) > 0) {
		$spatial .= "<b>Geographic Term:</b><br />";
	}
		
	for ($i = 0; $i < count($spat); $i++) {
		$spatial .= "<a 
href=\"hueism.php?dduMenu_0=spatial&dduMenu_0_value=" . 
$spat[$i]["spatialTerm"] . 
"&SendSearch=Search&x=search&SendSearch=1\">" . $spat[$i]["spatialTerm"] . "</a><br />";
	}

	if ($title != "") {
		$titleData = "<b>Title:</b><br />$title<br />";
	} else {
		$titleData = "";
	}
        if ($creator != "") {
                $creatorData = "<b>Creator(s):</b><br />";
                $_query = 
                	'SELECT 
                		item_creator.creator,
                    item_creator.role
                  FROM 
                  	item_creator 
                  WHERE 
                  	item_id = ?';
                
                try {  	
									$result = R::getAll($_query, array($workID));
								} catch (\Exception $e) {
									$result = array();
								}
               for ($i = 0; $i < count($result); $i++) {
                   $creator = $result[$i]["creator"];
                   $role = $result[$i]["role"];
                   $creatorData .= "$creator, $role<br />";
                }

        } else {
                $creatorData = "";
        }

	if ($description != "") {
		$descriptionData = "<b>Description:</b><br />$description<br />";
	} else {
		$descriptionData = "";
	}

	if ($source != "") {
		$sourceData = "<b>Source:</b><br />$source<br />";
	} else {
		$sourceData = "";
	}

	if ($height != "") {
		$heightData = "<b>Height:</b><br />$height<br />";
	} else {
		$heightData = "";
	}

	if ($width != "") {
		$widthData = "<b>Width:</b><br />$width<br />";
	} else {
		$widthData = "";
	}

	if ($dimension != "") {
		$dimensionData = "<b>Dimensions:</b><br />$dimension<br />";
	} else {
		$dimensionData = "";
	}

	if ($subject != "") {
		$subjectData = "<br /><b>Library of Congress Subject Headings:</b><br />$subject";
	} else {
		$subjectData = "";
	}
	
	if ($spatial != "") {
		$spatialData = "<br />" . $spatial;
	} else {
		$spatialData = "";
	}
        if ($date != "") {
                $dateData = "<b>Date</b>:<br />$date<br />";
        } else {
                $dateData = "";
        }
        if ($copyright != "") {
                $copyrightData = "<b>Copyright</b>:<br />$copyright<br />";
        } else {
                $copyrightData = "";
        }

$nextPage = $_SERVER['PHP_SELF'];

if ($currentPage == 1) {
	$nextPage = "&laquo; Previous | <a href=\"$nextPage?x=printedwork&p=" . ($currentPage+1) . "&id=$workID\">Next &raquo;</a>";
} elseif ($currentPage < $totalPage) {
	$nextPage = "<a href=\"$nextPage?x=printedwork&p=" . ($currentPage-1) . "&id=$workID\">&laquo; Previous</a> | <a href=\"$nextPage?x=printedwork&p=" . ($currentPage+1) . "&id=$workID\">Next &raquo;</a>";
} elseif($currentPage != 1) {
	$nextPage = "<a href=\"$nextPage?x=printedwork&p=" . ($currentPage-1) . "&id=$workID\">&laquo; Previous</a> | Next &raquo;";
} else {
	$nextPage = "";
}

$nextPageMod = ereg_replace("&middot; ", "", $nextPage);


if (isset($_REQUEST['print'])) {
	include_once("print.php");
	makePDF($pdfImages);
	exit;
}



$dOut =<<< END_DATA

<script type="text/javascript">

	function change(obj) {
		window.location.href = obj.value;
	}

</script>

<p class="header"><a href="hueism.php?x=browse">Browse Collection</a> &rsaquo; <a href="hueism.php?x=browse&dduMenu_0_value=pw&dduMenu_0=category&SendSearch=1">Printed Works</a> &rsaquo; $title</p>

<table cellpadding="0" cellspacing="0">
  <tr>
 	<td class="printed_work_left"><div align="center">Go to Page: $skipMenu of $totalPage<br />$medWork</div></td>
    <td>
		<p class="headerInText">Page $currentPage/$totalPage <br />$nextPage</p><P>[ <a href="printedwork.php?id=$workID&print">Printer-Friendly Version</a> ]</P>
      <!-- <p>(<a href="javascript:toggle();">Toggle MetaData View</a>)</p> -->

		<div id="metadata" class="spacing"></div>
			$titleData
                        $creatorData
                        $dateData
			$descriptionData
			$sourceData
                        $copyrightData
			$subjectData
			$spatialData
		<!-- </div>	-->	
				

	</td>
   </tr>
</table>

<script language="javascript" type="text/javascript">
	document.getElementById("metadata").style.display = "none";

function toggle() {
  if(document.getElementById("metadata").style.display == "none")
	document.getElementById("metadata").style.display = "block";
  else
    document.getElementById("metadata").style.display = "none";
}

</script>
END_DATA;

print ($dOut);



function file_url_exists($url) {
	$handle = @fopen($url, "r");
	 if ($handle === false)
		  return false;
	 fclose($handle);
	 return true;
}


?>
