<html>
<head>
<title>Digital Durham</title>
<link rel="stylesheet" type="text/css" media="screen" 
href="/ui/css/style.css" />
</head>

<body>

<div id="contain">
<div id="top">


<div id="search_box_top"><div id="search_box_left">
<form method="get" action="/search">
      <input class="form_textbox" id="q" name="q" type="text" alt="Search Box" style="width:140px;" />
	  <input type="submit" name="submit" value="Go" />
      <input type="hidden" name="site" value="duke_collection" />
      <input type="hidden" name="client" value="digitaldurham" />
      <input type="hidden" name="proxystylesheet" value="digitaldurham" />
      <input type="hidden" name="output" value="xml_no_dtd" />
    </form>


</div></div>

<a href="/"><img src="/images/dd_logo3.gif" /></a>

</div>

<div id="nav"><div>

<?php
echo "<ul>";
if (isset($_GET['x'])) {
	$x_param = $_GET['x'];
} else {
	$x_param = "";
}

$link_array = array("index", "overview", "browse", "reference", "teachers", "news");
$text_array = array("Home", "Project Overview", "Browse Collection", "Reference", "Teachers' Corner", "In the News");

/* check x, teddy@aas.duke.edu, 14/02/2007 */
$ok_list = array("index", "overview", "browse", "reference", 
                 "teachers", "news", 
		 "search", "letter", "map", "photograph","businessrecord",
                 "printedwork","ledger", "ephemera",
                 );

$x = "search";
for($i = 0; $i < count($ok_list); $i++) {
  if ($x_param == $ok_list[$i]) {
    $x = $x_param;
    break;
  }
}
/* end check */
 
for($i = 0; $i < count($link_array); $i++) {
  if(($x == $link_array[$i]) || ($i == 2 && ($x == "search" || $x == "letter" || $x == "map" || $x == "photograph" || $x == "businessrecord" || $x == "printedwork" || $x == "ledger" || $x == "ephemera")))
    echo '<li><a id="on" href="hueism.php?x='.$link_array[$i].'" rel="noopener noreferrer">'.$text_array[$i].'</a></li>';
  elseif ($x == 2) 
     echo '<li><a href="hueism.php?x='.$link_array[$i].'" rel="noopener noreferrer">'.$text_array[$i].'</a></li>';
  else
     echo '<li><a href="'.$link_array[$i].'.php" rel="noopener noreferrer">'.$text_array[$i].'</a></li>';
}
echo "</ul>";
?>

</div></div>

<div id="content">
<div class="margins">

<?php

include_once("common/paths.php");
if(isset($x) && $x != "")
  include_once($x.".php");
else
  include_once("index.php");

?>

</div>
</div>

<div id="copyright">
<div class="margins"><p><a 
href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a>
&middot; <a href="about.php">About this site</a> &middot; Copyright &#169; 2006.
Trudi J. Abel. All Rights Reserved.
 Site created by <a target="_blank" href="http://hueism.com/">Hueism</a>.</p><br>
 <div id="copyright">
    <p>The copyright interest in the material in this digital collection has not been transferred to the Digital Durham project. These text and images may not be used for any commercial purpose without the permission of the Duke University Rare Book, Manuscript, and Special Collections Library and the Digital Durham Project. Copyright permission for subsequent uses is the responsibility of the user.</p>
  </div></div>
</div>


</body>
