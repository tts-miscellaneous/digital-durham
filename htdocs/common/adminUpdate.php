<?php

//error_reporting(E_ALL);
//ini_set('display_errors', true);

include_once("common.php");


/*
$test = new adminUpdate();
$test->showItem();
print ($test->getItemId(2));
print_r($test->getItemType(2));
print_r($test->getItemCreator(2));
print_r($test->getItemFormat(282));
print_r($test->getItemSource(2));
print_r($test->getItemSeries(2));
*/

class adminUpdate {
		
	
	function getItemId($id) {
		
		$_query = 
			'SELECT 
				item.collection_prefix, 
				item.category, 
				item.folder, 
				item.item_number 
			FROM 
				item 
			WHERE 
				item.item_id = ?';
		$result = R::getAll($_query, array($id));
		$output = "";
		
		for ($i = 0; $i < count($result); $i++) {
			$output .= $result[$i]["collection_prefix"] . " " . $result[$i]["category"] . " " . $result[$i]["folder"] . " " . $result[$i]["item_number"];
		
		}
		return($output);
	}
	
	function addItem($collection_prefix, $category, $folder, $item_number) {

		if ($collection_prefix != "" && $category != "" && $folder != "" && $item_number != "") {
				
				$_query = 
					'INSERT INTO 
						item 
					SET 
						collection_prefix = :collectionPrefix,
						category = :category, 
						folder = :folder, 
						item_number = :itemNumber';
				$_params = array(
					':collectionPrefix' => $collection_prefix,
					':category' => $category,
					':folder' => $folder,
					':itemNumber' => $item_number
				);
				
				try {
					$result = R::getAll($_query, $_params);
					return(R::getInsertID());
				} catch (\Exception $e) {
					return(false);
				}	
		} else {
			return (false);
		}
		
	}


	function getItemType($id) {
		
		$output = array();
		$_query = 
			'SELECT 
				item_type.typeterm 
			FROM 
				item_type 
			WHERE 
				item_type.item_id = ? 
			ORDER BY 
				item_type.typeterm ASC';
		
		try {
			$result = R::getAll($_query, array($id));
			for ($i = 0; $i < count($result); $i++) {
					array_push($output, $result[$i]["typeterm"]);
				}
			
				return ($output);
		} 
		catch (\Exception $e) {
			return(false);
		}	
	}
	
	function setItemType($id, $typeterm) {
	
		$_query = 
			'INSERT INTO 
				item_type 
			SET 
				item_id = :id,
				typeterm = :typeterm';
		$_params = array(
			':id' => $id,
			':typeterm' => $typeterm
		);
		
		try {
			$result = R::getAll($_query, $_params);
			return(R::getInsertID());
		} catch (\Exception $e) {
			return(false);	
		}	 
	
	}

	function removeItemType($id, $typeterm) {
	
		$_query = 
			'DELETE FROM 
				item_type 
			WHERE 
				item_type.item_id = :item_id AND 
				item_type.typeterm = :type_term 
				LIMIT 1';
			$_params = array(
				':item_id' => $id,
				':type_term' => $typeterm
			);
			try {
				R::getAll($_query, $_params);
				return(true);
			}	catch (\Exception $e) {
				return(false);
			}
	}

	function getItemCreator($id) {
		
		$output = array();
		
		$_query = 
			'SELECT 
				item_creator.creator, 
				item_creator.role 
			FROM 
				item_creator 
			WHERE 
				item_creator.item_id = ? 
			ORDER BY 
				item_creator.creator ASC, 
				item_creator.role ASC';

		$result = R::getAll($_query, array($id));
		
		for ($i = 0; $i < count($result); $i++) {
			array_push($output, $result[$i]["creator"]);
			array_push($output, $result[$i]["role"]);
		}
			
		return ($output);
	}



	function setItemCreator($id, $creator, $role) {
		
		$_query = 
			'INSERT INTO 
				item_creator 
			SET item_id = :id, 
			creator = :creator, 
			role = :role';
		$_params = array(
			':id' => $id,
			':creator' => $creator,
			':role' => $role
		);
		try {
			$result = R::getAll($_query, $_params);
			return(R::getInsertID());
		}
		catch (\Exception $e) {
			return(false);
		}
	}

	function removeItemCreator($id, $creator, $role) {
	
		$_query =
			'DELETE FROM 
				item_creator 
			WHERE 
				item_creator.item_id = :id AND 
				item_creator.creator = :creator AND 
				item_creator.role = :role 
			LIMIT 1';
			
		$_params = array(
			':id' => $id,
			':creator' => $creator,
			':role' => $role
		);
		
		try {
			R::getAll($_query, $_params);
			return(true);
		}
		catch (\Exception $e) {
			return(false);
		}
	}

	function getItemFormat($id) {
		
		$output = array();
		
		$_query = 
			'SELECT
				item_format.formatterm 
			FROM 
				item_format 
			WHERE 
				item_format.item_id = ? 
			ORDER BY 
				item_format.formatterm ASC';
		$_params = array(
			$id
		);
		$result = R::getAll($_query, $_params);
		
		for ($i = 0; $i < count($result); $i++) {
			array_push($output, $result[$i]["formatterm"]);
		}
			
		return ($output);
	}


	function setItemFormat($id, $formatterm) {
	
		$_query = 
			'INSERT INTO 
				item_format 
			SET 
				item_id = :id, 
				formatterm = :format_term';
		$_params = array(
			';item_id' => $id,
			':format_term' => $formatterm
		);
		
		try {
			$result = R::getAll($_query, $_params);
			return(true);
		} catch (\Exception $e) {
			return(false);
		}		
	}

	function removeItemFormat($id, $formatterm) {
	
		$_query = 
			'DELETE FROM 
				item_format 
			WHERE 
				item_format.item_id = :id  AND 
				item_format.formatterm = :format_term 
			LIMIT 1';
		$_params = array(
			':id' => $id,
			':format_term' => $formatterm
		);
		
		try {
			R::getAll($_query, $_params);
			return(true);
		}
		catch (\Exception $e) {
			return(false);	
		}
	}
	
	function getItemSeries($id) {
		
		$output = array();
		
		$_query = 
			'SELECT 
				Series.series_id, 
				Series.series_title 
			FROM 
				Series 
			LEFT JOIN 
				item_series 
			ON 
				Series.series_id = item_series.series_id
			LEFT JOIN 
				item 
			ON 
				item_series.item_id = item.item_id 
			WHERE 
				item.item_id = ?
			ORDER BY 
				Series.series_title ASC;';
		$_params = array($id);
		$result = R::getAll($_query, $_params);
		
		for ($i = 0; $i < count($result); $i++) {
			array_push($output, $result[$i]["series_id"]);
			array_push($output, $result[$i]["series_title"]);
		}
			
		return ($output);
	}


	function setItemSeries($id, $series_id) {
	
		$_query = 
			'INSERT INTO 
				item_series 
			SET 
				item_id = :id, 
				series_id = :series_id';
				
		$_params = array(
			':id' => $id,
			':series_id' => $series_ID
		);
		
		try {
			$result = R::getAll($_query, $_params);
			return(true);
		} catch (\Exception $e) {
			return(false);
		} 		

	}

	function removeItemSeries($id, $series_id) {
	
		$_query = 
			'DELETE FROM 
				item_series 
			WHERE 
				item_series.item_id = :id AND 
				item_series.series_id = :series_id 
			LIMIT 1';
		$_params = array(
			':id' => $id,
			':series_id' => $series_id,
		);
		
		try {
			R::getAll($_query, $_params);
			return(true);
		}
		catch (\Exception $e) {
			return(false);
		}
	}

	function getItemSource($id) {
		
		$_query = 
			'SELECT 
				item.dc_source 
			FROM 
				item 
			WHERE item.item_id = ?';
		
		try {
			$result = R::getAll($_query, array($id));
			$output = "";
			for ($i = 0; $i < count($result); $i++) {
				$output .= $result[$i]["dc_source"];
			}
			return($output);
		} catch (\Exception $e) {
			return(false);
		}
	}


	function setItemSource($id, $dc_source) {
	
		$_query = 
			'UPDATE 
				item 
			SET 
				item.dc_source = ? 
			WHERE 
				item.item_id = ? 
			LIMIT 1';
			
		$_params = array(
			$dc_source,
			$id
		);
		
		try {
			$result = R::getAll($_query, $_params);
			return(true);
		}
		catch (\Exception $e) {
			return(false);
		}
	}

	function getItemCopyright($id) {
		
		$_query = 
			'SELECT 
				item.copyright 
			FROM 
				item 
			WHERE 
				item.item_id = ?';
		
		$_params = array($id);
		try {
			$result = R::getAll($_query, $_params);
			$output = "";
			for ($i = 0; $i < count($result); $i++) {
				$output .= $result[$i]["copyright"];
			}		
			return($output);
		}
		catch (\Exception $e) {
			return(false);
		}
	}


	function setItemCopyright($id, $copyright) {
	
		$_query =
			'UPDATE 
				item 
			SET 
				item.copyright = ? 
			WHERE 
				item.item_id = ? LIMIT 1;';
				
		$_params = array($copyright, $id);
		
		try {
			$result = R::getAll($_query, $_params);
			
			return(true);
		} catch (\Exception $e) {
			return(false);
		}
	}


	function getItemTitle($id) {
		
		$_query = 
			'SELECT 
				item.dc_title 
			FROM 
				item 
			WHERE 
				item.item_id = ?';
		$_params = array($id);
		
		try {
			$result = R::getAll($_query, $_params);
			$output = "";
			for ($i = 0; $i < count($result); $i++) {
				$output .= $result[$i]["dc_title"];
			}		
			return($output);
		} catch (\Exception $e) {
			return(false);
		}
	}

	function setItemTitle($id, $dc_title) {
	
		$_query = 
			'UPDATE 
				item 
			SET 
				item.dc_title = ?
			WHERE 
				item.item_id = ?
			LIMIT 1';
		$_params = array(
			$dc_title,
			$id
		);
		
		try {
			$result = R::getAll($_query, $_params);
			return(true);
		}
		catch (\Exception $e) {
			return(false);
		}
	}

	function getItemAlternativeTitle($id) {
		
		$_query = 
			'SELECT 
				item.dcterms_alternative 
			FROM 
				item 
			WHERE 
				item.item_id = ?';
		$_params = array(
			$id
		);
		
		try {
			$result = R::getAll($_query, $_params);
			$output = "";
		
			for ($i = 0; $i < count($result); $i++) {
				$output .= $result[$i]["dcterms_alternative"];		
			}
			return($output);		
		}
		catch (\Exception $e) {
			 return(false);
		}		
	}

	function setItemAlternativeTitle($id, $dcterms_alternative) {
	
		$_query = 
			'UPDATE 
				item 
			SET 
				item.dcterms_alternative = ?  
			WHERE 
				item.item_id = ? 
			LIMIT 1';
		$_params = array(
			$dcterms_alternative,
			$id,
		);
		
		try {
			$result = R::getAll($_query, $_params);
			return(true);
		}
		catch (\Exception $e) {
			return(false);
		}
	}

	function getItemDescription($id) {
		
		$_query = 
			'SELECT 
				item.dc_description 
			FROM 
				item 
			WHERE 
				item.item_id = ?';
		$_params = array($id);

		try {
			$result = R::getAll($_query, $_params);
			$output = "";
		
			for ($i = 0; $i < count($result); $i++) {
				$output .= $result[$i]["dc_description"];
		
			}
		
			return($output);
		} catch (\Exception $e) {
			return(false);
		}
	}

	function setItemDescription($id, $dc_description) {
	
		$_query = 
			'UPDATE 
				item 
			SET 
				item.dc_description = ?
			WHERE 
				item.item_id = ?
			LIMIT 1';
		$_params = array(
			$dc_description,
			$id
		);
		try {
			$result = R::getAll($_query, $_params);
			return(true);
		} catch (\Exception $e) {
			return(false);
		}
	}

	function getItemNotes($id) {
		
		$_query = "SELECT item.notes FROM item WHERE item.item_id = ?;";
		$_params = array(
			$id
		);
		
		try {
			$result = R::getAll($_query, $_params);
			$output = "";
		
			for ($i = 0; $i < count($result); $i++) {
				$output .= $result[$i]["notes"];
			}
			return($output);
		} catch (\Exception $e) {
			return(false);
		}
	}


	function setItemNotes($id, $notes) {
	
		$_query = "UPDATE item SET item.notes = ? WHERE item.item_id = ? LIMIT 1;";
		$_params = array(
			$notes,
			$id
		);

		try {
			$result = R::getAll($_query, $_params);
			return(true);
		}
		catch (\Exception $e) {
			return(false);
		}	

	}

	function getItemNotes2($id) {
		
		$_query = "SELECT item.notes2 FROM item WHERE item.item_id = ?;";
		$_params = array(
			$id
		);
		
		try {
			$result = R::getAll($_query, $_params);
			$output = "";		
			for ($i = 0; $i < count($result); $i++) {
				$output .= $result[$i]["notes2"];
			}
			return($output);
		} catch (\Exception $e) {
			return(false);
		}
	}


	function setItemNotes2($id, $notes) {
	
		$_query = "UPDATE item SET item.notes2 = ? WHERE item.item_id = ? LIMIT 1;";
		$_params = array(
			$notes,
			$id
		);
		
		try {
			$result = R::getAll($_query, $_params);
			return(true);
		} 
		catch (\Exception $e) {
			return(false);	
		}
	}
  
  function getItemRanges($id) {

  	$_query = "SELECT item.ranges FROM item WHERE item.item_id = ?;";
		$_params = array(
			$id
		);
		try {
			$result = R::getAll($_query, $_params);
			$output = "";

			for ($i = 0; $i < count($result); $i++) {
							$output .= $result[$i]["ranges"];

			}

			return($output);
		} catch (\Exception $e) {
			return(false);
		}
	}

	function setItemRanges($id, $ranges) {

		$_query = "UPDATE item SET item.ranges = ? WHERE item.item_id = ? LIMIT 1;";
		$_params = array(
			$ranges,
			$id
		);
	
		try {
			$result = R::getAll($_query, $_params);
			return(true);
		} catch (\Exception $e) {
			return(false);
		}
	}

	function getItemDimensions($id) {

		$_query = "SELECT item.dimensions FROM item WHERE item.item_id = ?;";
		$_params = array(
			$id
		);

		try {
			$result = R::getAll($_query, $_params);
			$output = "";

			for ($i = 0; $i < count($result); $i++) {
				$output .= $result[$i]["dimensions"];
			}

			return($output);
		} catch (\Exception $e) {
			return(false);
		}
	}

	function setItemDimensions($id, $dimensions) {

		$_query = "UPDATE item SET item.dimensions = ? WHERE item.item_id = ? LIMIT 1;";
		$_params = array(
			$dimensions,
			$id
		);

		try {
			$result = R::getAll($_query, $_params);
			return(true);
		}
		catch (\Exception $e) {
			return(false);
		}
	}


	function getItemPageCount($id) {

		$_query = "SELECT item.page_count FROM item WHERE item.item_id = ?;";
		$_params = array(
			$id
		);
		
		try {
			$result = R::getAll($_query, $_params);
			$output = "";

			for ($i = 0; $i < count($result); $i++) {
							$output .= $result[$i]["page_count"];

			}

			return($output);
		} catch (\Exception $e) {
			return(false);
		}
	}

	function setItemPageCount($id, $page_count) {

		$_query = "UPDATE item SET item.page_count = ? WHERE item.item_id = ? LIMIT 1;";
		$_params = array(
			$page_count,
			$id
		);
		
		try {
			$result = R::getAll($_query, $_params);
			return(true);
		} catch (\Exception $e) {
			return(false);
		}
	}

	function getItemDate($id) {
		
 		$_query = "SELECT item.dc_date FROM item WHERE item.item_id = ?;";
		$_params = array(
			$id
		);
		
		try {
			$result = R::getAll($_query, $_params);
			$output = "";
		
			for ($i = 0; $i < count($result); $i++) {
				$output .= $result[$i]["dc_date"];
		
			}	
			return($output);
		} catch (\Exception $e) {
			return(false);
		}
	}

	function setItemDate($id, $dc_date) {
	
		$_query = "UPDATE item SET item.dc_date = ? WHERE item.item_id = ? LIMIT 1;";
		$_params = array(
			$dc_date,
			$id
		);
		
		try {
 			$result = R::getAll($_query, $_params);
			return(true);
		} 
		catch (\Exception $e) {
			return(false);
		}
	}

	function getItemSubject($id) {
		
		$output = array();
		
		$_query = "SELECT item_subject.subjterm, item_subject.source FROM item_subject WHERE item_subject.item_id = ? ORDER BY item_subject.subjterm ASC;";
		$_params = array(
			$id
		);
		
		try {
			$result = R::getAll($_query, $_params);
		
			for ($i = 0; $i < count($result); $i++) {
				array_push($output, $result[$i]["subjterm"]);
				array_push($output, $result[$i]["source"]);
			}
			
			return ($output);
		} catch (\Exception $e) {
			return(false);
		}
	}

	function getSubject() {
		
		$output = array();
		
		$_query = "SELECT DISTINCT(item_subject.subjterm) FROM item_subject WHERE item_subject.subjterm <> '' ORDER BY item_subject.subjterm ASC;";
		$_params = array();
		try {
			$result = R::getAll($_query, $_params);
		
			for ($i = 0; $i < count($result); $i++) {
				array_push($output, $result[$i]["subjterm"]);
			}
			
			return ($output);
		} catch (\Exception $e) {
			return(false);
		}
	}

	function getSource() {
		
		$output = array();
		
		$_query = "SELECT DISTINCT(item_subject.source) FROM item_subject WHERE item_subject.source <> '' ORDER BY item_subject.subjterm ASC;";
		$_params = array();
		try {
			$result = R::getAll($_query, $_params);
		
			for ($i = 0; $i < count($result); $i++) {
				array_push($output, $result[$i]["source"]);
			}
			
			return ($output);
		} catch (\Exception $e) {
			return(false);
		}

	}


	function setItemSubject($id, $subjterm, $source) {
	
		$_query = "INSERT INTO item_subject SET item_subject.item_id = ?, item_subject.subjterm = ?, item_subject.source = ?;";
		$_params = array(
			$id,
			$subjterm,
			$source
		);
		
		try {	
			$result = R::getAll($_query, $_params);
			return(true);
		} catch (\Exception $e) {
			return(false);
		}
	
	}

	function removeItemSubject($id, $subjterm, $source) {
	
		$_query = "DELETE FROM 
			item_subject 
		WHERE 
			item_subject.item_id = ? AND 
			item_subject.subjterm = ? AND 
			item_subject.source = ? 
		LIMIT 1;";
		$_params = array(
			$id,
			$subjterm,
			$source
		);
		try {
			$result = R::getAll($_query, $_params);
			return(true);
		} 
		catch (\Exception $e) {
			return(false);
		}
	}

	function getItemSpatial($id) {
		
		$output = array();
		
		$_query = "SELECT item_spatial.Source, item_spatial.spatialTerm FROM item_spatial WHERE item_spatial.item_id = ? ORDER BY item_spatial.Source ASC;";
		$_params = array(
			$id
		);
		
		try {
			$result = R::getAll($_query, $_params);
		
			for ($i = 0; $i < count($result); $i++) {
				array_push($output, $result[$i]["Source"]);
				array_push($output, $result[$i]["spatialTerm"]);
			}
			
			return ($output);
		} catch (\Exception $e) {
			return(false);
		}
	}
	
	function setItemSpatial($id, $Source, $spatialTerm) {
	
		$_query = "INSERT INTO item_spatial SET item_spatial.item_id = ?, item_spatial.Source = ?, item_spatial.spatialTerm = ?;";
		$_params = array(
			$id,
			$Source,
			$spatialTerm
		);
		
		try {
			$result = R::getAll($_query, $_params);
			return(true);
		} catch (\Exception $e) {
			return(false);
		}
	}

	function removeItemSpatial($id, $Source, $spatialTerm) {
	
		$_query = "DELETE FROM item_spatial WHERE item_spatial.item_id = ? AND item_spatial.Source = ? AND item_spatial.spatialTerm = ? LIMIT 1;";
		$_params = array(
			$id,
			$Source,
			$spatialTerm
		);
		
		try {
			$result = R::getAll($_query, $_params);
			return(true);
		}
		catch (\Exception $e) {
			return(false);
		}
	}

	function getSeries() {
	
		$output = array();
		
		$_query = "SELECT series_id, series_title, daterange, description FROM Series ORDER BY Series.series_id ASC;";
		$_params = array(
		);
		
		try {
			$result = R::getAll($_query, $_params);
			for ($i = 0; $i < count($result); $i++) {
		
				array_push($output, $result[$i]["series_id"]);
				array_push($output, $result[$i]["series_title"]);
				array_push($output, $result[$i]["daterange"]);
				array_push($output, $result[$i]["description"]);
		
			}		
			return ($output);
		}
		catch (\Exception $e) {
			return(false);
		}
	}
	
	function setSeries($series_id, $series_title, $daterange, $description) {
	
		// This function is a bit different, we first check to see if the series currently exists,
		// If so, we then we issue the update command, otherwise we will insert the new record into
		// the dataset
		
		$_query = "SELECT Series.series_id FROM Series WHERE series_id = ? LIMIT 1;";
		$_params = array(
			$series_id
		);
		try {
			$record = R::getAll($_query, $_params);
				
			if (count($record) == 0) {
				// The record does not exists
				$_query = "INSERT INTO Series SET series_id = ?, series_title = ?, daterange = ?, description = ?;";
				$_params = array(
					$series_id,
					$series_title,
					$daterange,
					$description
				);
				$result = R::getAll($_query, $_params);
				
				return(true);

			} else {
				// The record currently exists in the collection
				$_query = "UPDATE Series SET series_id = ?, series_title = ?, daterange = ?, description = ? WHERE Series.series_id = ?;";
				$_params = array(
					$series_id,
					$series_title,
					$daterange,
					$description,
					$series_id
				);
				$result = R::getAll($_query, $_params);

				return(true);
			}
		}
		catch (\Exception $e) {
			return(false);
		}
	}
	
	function showItem() {
	
		$_query = "SELECT item.item_id, item.dc_title, item.category FROM item ORDER BY item.category, item.dc_title ASC;";
		$_params = array(
		);

		try {
			$result = R::getAll($_query, $_params);
			$oldCategory = "";
			$outArray = array();
			$count = 0;
		
			for ($i = 0; $i < count($result); $i++) {
				if (@$result[$i]["category"] != $oldCategory) {
					$outArray[$count] = array();
					$outArray[$count]["category"] = $result[$i]["category"];
					$outArray[$count]["dc_title"] = array();
					$outArray[$count]["item_id"] = array();
					$oldCategory = $result[$i]["category"];
					$count++;
				}

				array_push($outArray[$count-1]["dc_title"], $result[$i]["dc_title"]);
				array_push($outArray[$count-1]["item_id"], $result[$i]["item_id"]);

			}
	
			return ($outArray);
		}
		catch (\Exception $e) {
			return(false);
		}	
	
	}
	
	function removeItem($item_id) {
		$_query = "DELETE FROM item WHERE item.item_id = ?;";
		$_params = array(
			$item_id
		);
		
		try {
			$result = R::getAll($_query, $_params);
			return(true);
		}
		catch (\Exception $e) {
			return(false);
		}
	}
	
	function removeSeries($series_id) {
	
		$_query = "DELETE FROM Series WHERE Series.series_id = ?;";
		$_params = array($series_id);
	
		try {
			$result = R::getAll($_query, $_params);
			return(true);
		}
		catch (\Exception $e) {
			return(false);
		}
	}
}


?>
