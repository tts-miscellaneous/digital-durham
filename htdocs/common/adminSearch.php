<?php


// Allows a user to enter and update an entry in the Series table
include_once("../common/adminUpdate.php");

$au = new adminUpdate();

if (isset($_REQUEST['search'])) {

	if ($_REQUEST['title'] != "") {
		searchTitle($_REQUEST['title']);
	} else if ($_REQUEST['description'] != "") {
		searchDescription($_REQUEST['description']);
	} else if ($_REQUEST['byid'] != "") {
		searchId($_REQUEST['byid']);
	}
	

} else {

	print displayMenu();
		
}


function displayMenu() {

	$PHP_SELF = $_SERVER['PHP_SELF'];
	
	$dOut = "";
	$dOut .= "<b>Digitial Durham Admin Tool</b><P>";
	$dOut .= "<form method=\"POST\" action=\"$PHP_SELF\">";
	$dOut .= "<ul><li>Search by title: <input type=\"text\" name=\"title\" size=\"30\"></li>";
	$dOut .= "<li>Search by description: <input type=\"description\" name=\"title\" size=\"30\"></li>";
	$dOut .= "<li>Go to ID: <input type=\"text\" name=\"byid\" size=\"5\"></li></ul><P>";
	$dOut .= "<input type=\"hidden\" name=\"search\">";
	$dOut .= "<input type=\"submit\" value=\"Send\">";

	return ($dOut);

}

?>