                                                                     
                                                                     
                                                                     
                                             
<?php

include_once("xmlParser.php");

/*
$test = new teiParse("02034.xml");
$test->parse();
*/

class teiParse {

var $xml;
var $title;
var $funder;
var $transcription;
var $edition;
var $publication;
var $source;
var $profile;
var $text;

	function teiParse($xmlFile) {
		
		if ($xmlFile == "") {
			die("Unable to load XML file.");
		} else {
			$this->xml = file_get_contents($xmlFile);
		}
	}
	
	
	function parse() {

		// The first thing that we need to do is to replace
		// all of the references to XML code with the appropriate
		// Unicode Hex(16) values
		
		$this->xml = preg_replace("/&percnt;/", "&#x0025;", $this->xml); // Symbol: %
		$this->xml = preg_replace("/&dollar;/", "&#x0024;", $this->xml); // Symbol: $
		$this->xml = preg_replace("/&nbsp;/", " ", $this->xml); // Symbol: <<space>>
		$this->xml = preg_replace("/&half;/", "<sup>1</sup>/<sub>2</sub>", $this->xml);	// Symbol: 1/2	
		$this->xml = preg_replace("/&plus;/", "+", $this->xml);	// Symbol: +	
		
		
		$parser = new XMLParser($this->xml);
		
		$parser->Parse();
		
		
		// In actuality, we not really need to parse these documents.  There is
		// structural information available that we can allow to be downloaded 
		// directly from the TEI document, but for the most part, we only need
		// the <BODY> information.
		
		// Author and Title Information
		foreach ($parser->document->teiheader[0]->filedesc[0]->titlestmt as $data) {
			
			for ($i = 0; $i < count($data->tagChildren); $i++) {
				
				$tag = $data->tagChildren[$i]->tagName;
				$tagData = $data->tagChildren[$i]->tagData;
				
				switch($tag) {
				
				
					case "title":
						if (!isset($title)) $title = array();
						$sData = null;						
						$sData[$tag] = $tagData;
						array_push($title, $sData);
						$this->setTitle($title);
						break;
				
					case "author":
						if (!isset($author)) $author = array();
						$sData = null;
						$sData[$tag] = $tagData;						
						array_push($author, $sData);
						$this->setAuthor($author);
						break;				


					case "funder":
						if (!isset($funder)) $funder = array();
						$sData = null;						
						$sData[$tag] = $tagData;
						array_push($funder, $sData);
						$this->setFunder($funder);
						break;
					
					case "respstmt":
							
						// This takes care of all of the special data case with the transcribe, name, etc
						for ($j = 0; $j < count($data->tagChildren[$i]); $j++) {

								$tag = $data->tagChildren[$i]->tagChildren[$j]->tagData;
								$tagData = $data->tagChildren[$i]->tagChildren[$j+1]->tagData;


								$sData = null;						
								$sData[$tag] = $tagData;							

							if (!isset($special)) $special = array();
								array_push($special, $sData);
								$this->setTranscription($special);
							}
					
						break;
						
				}
			
			}
			
		// Edition Information
		foreach ($parser->document->teiheader[0]->filedesc[0]->editionstmt as $data) {
			
			for ($i = 0; $i < count($data->tagChildren); $i++) {

				$tag = $data->tagChildren[$i]->tagName;
				$tagData = $data->tagChildren[$i]->tagData;
				
				switch($tag) {						
				
					case "edition":
						if (!isset($edition)) $edition = array();
						
						$sData = null;						
						$sData[$tag] = $tagData;
						array_push($edition, $sData);
								
						$this->setEdition($edition);
						
						for ($j = 0; $j < count($data->tagChildren[$i]->tagChildren); $j++) {
							
							$tag = $data->tagChildren[$i]->tagChildren[$j]->tagName;
							$tagData = $data->tagChildren[$i]->tagChildren[$j]->tagData;
							
							$sData = null;						
							$sData[$tag] = $tagData;
								
							array_push($edition, $sData);						
							$this->setEdition($edition);
						}
						
						break;
				
				}
			}
			
		}
			


		// Publication Information
		foreach ($parser->document->teiheader[0]->filedesc[0]->publicationstmt as $data) {
				
				for ($i = 0; $i < count($data->tagChildren); $i++) {
	
					$tag = $data->tagChildren[$i]->tagName;
					$tagData = $data->tagChildren[$i]->tagData;
					
					switch($tag) {						
					
						case "publisher":
							if (!isset($publish)) $publish = array();
						
							$sData = null;						
							$sData[$tag] = $tagData;
							array_push($publish, $sData);						
							$this->setPublication($publish);						
							break;
					
						case "pubPlace":
							if (!isset($publish)) $publish = array();
							
							$sData = null;						
							$sData[$tag] = $tagData;
							array_push($publish, $sData);						
							$this->setPublication($publish);						
							break;

						case "date":
							if (!isset($publish)) $publish = array();
							
							$sData = null;						
							$sData[$tag] = $tagData;
							array_push($publish, $sData);						
							$this->setPublication($publish);						
							break;	
							
						case "availability":
							if (!isset($publish)) $publish = array();

							$sData = array($tag, $tagData);							
							for ($j = 0; $j < count($data->tagChildren[$i]->tagChildren); $j++) {
								if ($data->tagChildren[$i]->tagChildren != "") {
									$tagData = $data->tagChildren[$i]->tagChildren[$j]->tagData;
									$sData = null;						
									$sData[$tag] = $tagData;
								}
							}
								
							array_push($publish, $sData);						
							$this->setPublication($publish);						
							break;						
					}
				}
				
			}


		// Source Information	
		foreach ($parser->document->teiheader[0]->filedesc[0]->sourcedesc as $data) {
				if (!isset($source)) $source = array();

				foreach($data->tagChildren as $child) {
					
					foreach ($child->tagChildren as $tagChild) {
					
						for ($k = 0; $k < count($tagChild->tagChildren); $k++) {
							
							$tag = $tagChild->tagChildren[$k]->tagName;
							$tagData = $tagChild->tagChildren[$k]->tagData;
							
							$sData = null;						
							$sData[$tag] = $tagData;
							array_push($source, $sData);
						}
					
					}
					
					$this->setSource($source);

				
				}				
			}
	

		// Profile Information	
		foreach ($parser->document->teiheader[0]->profiledesc as $data) {
				
				//print_r($data);
				if (!isset($profile)) $profile = array();

				foreach($data->tagChildren as $child) {
					
					foreach ($child->tagChildren as $tagChild) {
					
						foreach ($tagChild->tagChildren as $ttChild) {
							for ($k = 0; $k < count($ttChild->tagChildren); $k++) {
								
								$tag = $ttChild->tagChildren[$k]->tagName;
								$tagData = $ttChild->tagChildren[$k]->tagData;
								
								$sData = null;						
								$sData[$tag] = $tagData;
								array_push($profile, $sData);
							}
						}					
					}
					
					$this->setProfile($profile);

				
				}				
			}	
	

			// Now we will slurp the entire body from <text> tag to the </text> tag
			
			$startTag = "<text>";
			$endTag = "</text>";
			
			$sPos = strpos($this->xml, $startTag);
			$ePos = strpos($this->xml, $endTag);
			$length = $ePos - $sPos + 7;			
			$text = substr($this->xml, $sPos, $length);
			
			$this->setText($text);


			/*
			print_r($this->getTitle());
			print_r($this->getAuthor());
			print_r($this->getTranscription());
			print_r($this->getEdition());
			print_r($this->getPublication());
			print_r($this->getSource());
			print_r($this->getProfile());
			print_r($this->getText());
			print ("i will exit");
			exit;
			*/
		
		}
		
	}
	
	
	function setTitle($a) {
		$this->title = $a;
	}
	
	function getTitle() {
		return ($this->title);
	}

	function setAuthor($a) {
		$this->author = $a;
	}
	
	function getAuthor() {
		return ($this->author);
	}
	
	function setFunder($a) {
		$this->funder = $a;
	}
	
	function getFunder() {
		return ($this->funder);
	}

	function setTranscription($a) {
		$this->transcription = $a;
	}
	
	function getTranscription() {
		return ($this->transcription);
	}

	function setEdition($a) {
		$this->edition = $a;
	}
	
	function getEdition() {
		return ($this->edition);
	}

	function setPublication($a) {
		$this->publication = $a;
	}
	
	function getPublication() {
		return ($this->publication);
	}

	function setSource($a) {
		$this->source = $a;
	}
	
	function getSource() {
		return ($this->source);
	}	

	function setProfile($a) {
		$this->profile = $a;
	}
	
	function getProfile() {
		return ($this->profile);
	}	

	function setText($a) {
		$this->text = $a;
	}
	
	function getText() {
		return ($this->text);
	}	

}

?>