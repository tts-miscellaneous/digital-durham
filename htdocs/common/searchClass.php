<?php

// The search class

class Search {
	
	// The database names of the options in the search
	private $dduMenuDbName = array("dc_description", "dc_title", "dc_creator", "subjectheading", "spatial", "dc_date", "notes", "dc_source");

	// The human-readable version of the titles
	private $dduMenuName = array("Keyword", "Title", "Author", "LC Subject Heading", "Geographic", "Date", "Notes", "Source"); 
	
	// Constant variables
	private $_orderByList = array("null", "dc_date", "dc_title", "dc_creator");
	private $additionalTable;
	private $searchDisplay;
	private $sqlSelect;
	private $sqlOrder;

	// Also, override the ordering, if available
	public function Search($DBin = "") {
		if (isset($_REQUEST['orderBy']) && in_array(strtolower($_REQUEST['orderBy']), $this->_orderByList)) {
			$this->sqlOrder = " ORDER BY " . $_REQUEST['orderBy'] . " ASC";
		}
	}
	
	// Performs search
	function getSearch() {
	
		// Build menu options that are select value, passed to javascript
		$dduSelect = array();
		$dduSelect["dc_format"] = $this->getJsSelectItem("dc_format");		
	
		// Write all of the possible javascript options to page
		$jsMainMenu = $this->getJsMainMenu($this->dduMenuDbName, $this->dduMenuName, $dduSelect);
		
		$data = "";
		
		$data .= <<<END_DATA
		
		
		<script type="text/javascript" src="/scripts/search.js"></script>
		<script type="text/javascript">
			<!--
			
			$jsMainMenu
			-->
		</script>
		
		
		<form method="GET" action="?x=search">
		
			<div>
				<table id="dduTable" cellspacing="0" cellpadding="0" border="0">
				<tbody id="dduTBody">	
				<tr>
                                  <td colspan="2"><p class="header">Digital Durham Search</p></td>
                                </tr>
                                <tr>
				<td class="search_criteria" colspan="2">
                                  [ <a href="javascript:advancedSearch();">Advanced Search</a> ]<span id="advancedSearch"></span><br /><a href="javascript:createNewMenu(ddu_menu_option);">Add Criteria</a> / <a href="javascript:removeMenu();">Remove Criteria</a>   
				</td></tr>
				</tbody>
				</table>
                                <div class="search_button">
                                  <input type="submit" name="SendSearch" value="Search">
                                  <input type="hidden" name="x" value="search">
                                  <input type="hidden" name="SendSearch" value="1">
                                </div>
			</div>
		
		
		
		
		
		<script type="text/javascript">
			<!--
			createNewMenu(ddu_menu_option);
			-->
		</script>
		
		</form>
		
END_DATA;
		

		return ($data);		
	}	
	
	
	// Function that constructs command for the javascript menu
	function getJsMainMenu($dbName, $menuName, $select) {
	
	$dOut = "";
	$selectOption = "";
	
		if (count($dbName) != 0) {
			$dOut .= "var ddu_menu_option = new Array();\n";
		}
		
		for ($i = 0; $i < count($dbName); $i++) {
			$dOut .= "ddu_menu_option[$i] = new Array();\n";
			$dOut .= "ddu_menu_option[$i][\"name\"] = \"$dbName[$i]\";\n";
			$dOut .= "ddu_menu_option[$i][\"value\"] = \"$menuName[$i]\";\n";
			
			if (isset($select[$dbName[$i]])) {
				$dOut .= "ddu_menu_option[$i][\"type\"] = \"select\";\n";
			
				$selectOption .= "var $dbName[$i] = new Array();\n";
	
				for ($j = 0; $j < count($select[$dbName[$i]]); $j++) {
					$selectOption .= $dbName[$i] . "[$j] = \"" . $select[$dbName[$i]][$j] . "\";\n";
				}
			} else {
			
				$dOut .= "ddu_menu_option[$i][\"type\"] = \"text\";\n";
			
			}
		}

		$dOut .= $selectOption;
		
		return ($dOut);
	
	}
	
	// Populates items that are needed for the select menu.
	// Just pass a database connection and the name of the row
	function getJsSelectItem($dbName) {
	
		$dOut = array();
		
		$_query = 'SELECT 
			DISTINCT '.$dbName.' AS '.$dbName.' 
		FROM item 
			WHERE 
		'.$dbName.' IS NOT NULL 
		ORDER BY '.$dbName.' ASC';
		
		try {
			$result = R::getAll($_query);
		} catch (\Exception $e) {
			$result = array();
		}
				
		for ($i = 0; $i < count($result); $i++) {
			array_push($dOut, $result[$i]["$dbName"]);
		}
		
		return ($dOut);
	}
	
	
	// Return the results for a search
	function getResult($boolean = "AND") {
	
		$searchType = array();
		$searchValue = array();
		
		$typePattern = "/dduMenu_[0-9]*$/";
		$valuePattern = "/dduMenu_[0-9]*_value$/";
		
		$searchType = $this->parseRequest($_REQUEST, $typePattern);
		$searchValue = $this->parseRequest($_REQUEST, $valuePattern);
		
		$result = $this->populateSearch($searchType, $searchValue);

		if ($searchValue == null) {
			$result = ("<b>One or more of your search fields remains empty.  Please go to the <a href=\"javascript:history.go(-1)\">previous page</a> and complete all empty fields.");
			return ($result);
		}

		$_queryParams = $this->getQueryBuilder($searchType, $searchValue, $boolean);

		$result .= $this->getQueryResult($_queryParams);

		return($result);
	
	}

	// Returns an unformmated version of the search results
	// You are responsible for querying the correct fields
	function getResultData($boolean = "AND") {
	
		$searchType = array();
		$searchValue = array();
		
		$typePattern = "/dduMenu_[0-9]*$/";
		$valuePattern = "/dduMenu_[0-9]*_value$/";
		
		$searchType = $this->parseRequest($_REQUEST, $typePattern);
		$searchValue = $this->parseRequest($_REQUEST, $valuePattern);
		
		$result = $this->populateSearch($searchType, $searchValue);

		if ($searchValue == null) {
			$result = ("<b>One or more of your search fields remains empty.  Please go to the <a href=\"javascript:history.go(-1)\">previous page</a> and complete all empty fields.");
			return ($result);
		}

		$_queryParams = $this->getQueryBuilder($searchType, $searchValue, $boolean);

		$result = $this->getQueryResultData($_queryParams);
				
		return($result);
	
	}


	// Actually create the javascript menus
	function populateSearch($type, $value) {
		return('');
	}
	
	
	// This function will go through all of the javascript menus and collect
	// the name of the field to search and the value
	function parseRequest($request, $pattern) {	
		
		$outArray = array();
		
		foreach($request as $s => $svalue) {
		
			if (preg_match($pattern, $s)) {
				if ($svalue == "") {
					return (null);
				}
				array_push($outArray, $svalue);
			} 
		}
	
		return ($outArray);
	
	}
	
	// Actually performs the search againsts the database.  
	// This is performed by concatenating all of the remaining
	// search menu values
	function getQueryBuilder($type, $value, $boolean = "AND") {

		if ($boolean !== "AND") {
			$boolean = 'OR';
		}
		$_params = array();
		
		$this->additionalTable = "";
		$this->sqlSelect = 
			"	item.item_id, 
				item.collection_prefix, 
				item.dc_title, 
				item.dc_description, 
				item.category, 
				item.folder, 
				item.item_number 
			FROM 
				item";
		$whereClause = "";
		
		if ($this->sqlOrder == "") {
			//$this->sqlOrder = " ORDER BY item.dc_source ASC, item.dc_date ASC";
		}
		
		$_params = array();
		for ($i = 0; $i < count($type); $i++) {
			$localType = $localValue = '';
			if (! empty($type[$i]))
				$localType = $type[$i];
			if (! empty($value[$i]))
				$localValue = $value[$i];
				
			if ($i != 0) {
				$_queryParams = $this->getQueryString($localType, $localValue);
				$whereClause .= " $boolean " . $_queryParams['query'];
				$_params = array_merge($_queryParams['params'], $_params);
			} else {
				$_queryParams = $this->getQueryString($localType, $localValue);
				$whereClause .= $_queryParams['query'];
				$_params = array_merge($_queryParams['params'], $_params);
			}
		}
		
		$query = "SELECT " . $this->sqlSelect . $this->additionalTable . " WHERE ";

		$query .= $whereClause;
	
		$query .= " " . $this->sqlOrder;
	
		$query .= ";";
		
		return(array(
			'query' => $query,
			'params' => $_params
		));
	
	}
	
	// Performs limited SQL-injection detection
	// Also determines the most efficient query for the 
	// selected menu option
	function getQueryString($type, $value) {
		
		$_params = array();
		
		$string = "";
		
		$value = preg_replace("/'/", "''", $value);
		$value = preg_replace("/;/", "", $value);
		$value = preg_replace("/\\\/", "", $value);
		$value = preg_replace("/%/", "", $value);
		$value = preg_replace("/\*/", "", $value);
		
		if ($value == "") {
			$type = "null";	
		}
			$_randNum = rand(1,10000);

		switch($type) {
		
			case "dc_title":
				$string .= "(item.dc_title LIKE :title OR item.dcterms_alternative$_randNum LIKE :dcterms_alternative$_randNum) ";
				$_params[':title'.$_randNum] = "%$value%";
				$_params[':dcterms_alternative'.$_randNum] = "%$value%";
				$this->setSearchDisplay("Title is ".htmlentities($value));
				break;
			
			case "dc_description":
				$string .= "item.dc_description LIKE :dc_description$_randNum ";
				$_params[':dc_description'.$_randNum] = "%$value%";
				$this->setSearchDisplay("Description is ".htmlentities($value));
				break;
			
			case "reference":
				$string .= "item.reference LIKE :reference$_randNum ";
				$_params[':reference'.$_randNum] = "%$value%";
				$this->setSearchDisplay("Reference is ".htmlentities($value));
				break;
			
			case "dc_format":
				$string .= "item.dc_format = :dc_format$_randNum ";
				$_params[':dc_format'.$_randNum] = "$value";
				$this->setSearchDisplay("Format is ".htmlentities($value));
				break;
			
			case "dc_creator":

				$this->sqlSelect = "item.item_id, item.collection_prefix, item.dc_title, item.dc_description, item.category, item.folder, item.item_number FROM item ";	
				$this->additionalTable .= ", item_creator";
				$string .= "item.item_id = item_creator.item_id AND item_creator.creator LIKE :creator$_randNum ";
				$_params[':creator'.$_randNum] = "%$value%";
				$this->setSearchDisplay("Author is ".htmlentities($value));
				break;
			
			case "dc_date":
				$string .= "item.dc_date LIKE :dc_date$_randNum ";
				$_params[':dc_date'.$_randNum] = "%$value%";
				$this->setSearchDisplay("Date is ".htmlentities($value));
				break;
				
			case "file":
				$string .= "item.file LIKE :file$_randNum ";
				$_params[':file'.$_randNum] = "%$value%";
				$this->setSearchDisplay("Filing is ".htmlentities($value));
				break;
			
			case "notes":
				$string .= "item.notes2 LIKE :notes2$_randNum ";
				$_params[':notes2'.$_randNum] = "%$value%";
				$this->setSearchDisplay("Note is ".htmlentities($value));
				break;
				
			case "dc_source":
				$string .= "item.dc_source LIKE :dc_source$_randNum ";
				$_params[':dc_source'.$_randNum] = "%$value%";
				$this->setSearchDisplay("Source is ".htmlentities($value));
				break;
		
			case "subjectheading":
				$this->additionalTable .= ", item_subject$_randNum";
				$string .= "item_subject.subjterm LIKE :subjterm$_randNum AND item.item_id = item_subject.item_id";
				$_params[':subjterm'.$_randNum] = "%$value%";
				$this->setSearchDisplay("LC Subject Heading is ".htmlentities($value));
				break;
			case "category":
				$string .= "item.category LIKE :category$_randNum";
				$_params[':category'.$_randNum] = "$value";
				$this->setSearchDisplay("Category is ".htmlentities($value));
				break;
			case "spatial":
				$this->additionalTable .= ", item_spatial";
				$string .= "item_spatial.spatialTerm LIKE :spatialTerm$_randNum AND item.item_id = item_spatial.item_id";
				$_params[':spatialTerm'.$_randNum] = "%$value%";		
				$this->setSearchDisplay("Spatial term is ".htmlentities($value));
				break;
			case "series":
				if (isset($_REQUEST['roleStatus']) && isset($_REQUEST['role']) && $_REQUEST['role'] != "null") {
					if ($_REQUEST['roleStatus'] == "Both") {
						$roleStatus = "(item_creator.role LIKE :Author$_randNum OR item_creator.role LIKE :Recipient$_randNum)";
						$_params[':Author'.$_randNum] = '%Author';
						$_params[':Recipient'.$_randNum] = '%Recipient%';
					} else {
						$roleStatus = "item_creator.role LIKE :role ";
						$_params[':role'] = '%'.$_REQUEST['roleStatus'].'%';
					}	
					
					$this->additionalTable .= ", item_series, Series, item_creator";
					$string .= "item.item_id = item_series.item_id AND 
						item_series.series_id = Series.series_id AND 
						item_creator.item_id = item.item_id AND 
						item_creator.creator LIKE :creator$_randNum AND 
						$roleStatus AND item.category = 'ms' AND 
						Series.series_id LIKE :series_id$_randNum AND 
						Series.series_id NOT LIKE '1.4' ";
						$_params[':creator'.$_randNum] = '%'.$_REQUEST['role'].'%';
						$_params[':series_id'.$_randNum] =  '%'.$value.'%';
				} else {
					$this->additionalTable .= ", item_series, Series";
					$string .= "item.item_id = item_series.item_id AND 
						item_series.series_id = Series.series_id AND 
						item.category = 'ms' AND 
						Series.series_id LIKE :series_id$_randNum ";
						$_params[':series_id'.$_randNum] = $value.'%';
				}
			
				switch ($value) {
					case "1.1":
						$mValue = "Richard Harvey Wright Letters";
						break;
					case "1.3":
						$mValue = "Southgate Jones Letters";
						break;
					case "1.2":
						$mValue = "James Southgate Letters";
						break;
					case "1.4":
						$mValue = "Atlas Rigsbee Ledgerbook";
						break;
					default:
						$mValue = "Personal Letters";
						break;
				}
				
				$this->setSearchDisplay("Series is ".htmlentities($mValue));
				break;
		case "seriesListing":
				$this->sqlSelect = "Series.series_id, Series.series_title, Series.daterange, Series.description, count(*) AS series_count FROM item";
				$this->additionalTable .= ", item_series, Series, item_type";
				$string .= "item.item_id = item_series.item_id AND 
					item_series.series_id = Series.series_id AND 
					item_series.item_id = item_type.item_id AND 
					Series.series_id LIKE :series_id$_randNum AND 
					item_type.typeterm LIKE '%Letters (correspondence)%' 
					GROUP BY Series.series_id";
					$_params[':series_id'.$_randNum] = $value.'%';
				
				$this->setSearchDisplay("Series is ".htmlentities($value));
				break;
		case "loc":
				$this->sqlSelect = "item.item_id, item.collection_prefix, item.dc_title, item.dc_description, item.category, item.folder, item.item_number FROM item ";
				$this->additionalTable .= ", item_subject";
				$string .= "item.item_id = item_subject.item_id AND 
					item_subject.subjterm LIKE :subjterm$_randNum ";
				$_params[':subjterm'.$_randNum] = $value;

				$this->setSearchDisplay("LC Subject Heading is $value");
				break;		
		case "locListing":
				$this->sqlSelect = "DISTINCT(subjterm) AS subject, count(DISTINCT(item_subject.item_id)) AS subject_count FROM item_subject";
				$string .= "item_subject.item_id = item_subject.item_id GROUP BY (item_subject.subjterm) ";
				$this->sqlOrder = "ORDER BY item_subject.subjterm ";
				$this->setSearchDisplay("Library of Congress Subject Heading");
				break;		
		}

		return(array(
			'query' => $string,
			'params' => $_params
		));
	}
	
	// Returns only the query data.  No additional prepartion of
	// the data occurs, therefore you must perform all of the stylistic
	// changes in your source code
	function getQueryResultData($query) {
		
		$data = $this->getQueryResult($query, false);
		return ($data);
	}
	
	
	// Formats in HTML the returned query results
	// This should be changed to make it look better
	function getQueryResult($_queryParams, $format = true) {

		try {
			$result = R::getAll($_queryParams['query'], $_queryParams['params']);
		}
		catch (\Exception $e) {
			$result = array();
		}		
		$itemType = '';
		if (! empty($_GET['dduMenu_0_value']))
			$itemType = $_GET['dduMenu_0_value'];
			
		$dOut = "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">";
		//$imageBase = "http://trinity1.aas.duke.edu/images/digitaldurham/150";
		include("common/paths.php");
        
        if ($_GET['dduMenu_0'] == "loc") {
        	$itemType = "loc";
        }
						switch($itemType) {
						
										case "pw":
														// Printed Works
														// They are special type of books
														$displayType = "Printed Works";
														break;
										case "ma":
														// Maps
														$displayType = "Maps";
														break;						
										case "br":
														// Business Records
														$displayType = "Business Records";
														break;						
										case "ep":
														// Ephemera
														$displayType = "Miscellany";
														break;						
										case "ph":
														// Photographs
														$displayType = "Photographs";
														break;
										case "ms":
														// Maps
														$displayType = "Personal Papers";
														break;
										case "1.1":
												// Letter Series
												$displayType = "<a href=\"hueism.php?x=browse&dduMenu_0_value=1&dduMenu_0=seriesListing&listing&SendSearch=1\">Richard Harvey Wright Letters</a>";
												break;
				case "1.2":
												// Letter Series
												$displayType = "<a href=\"hueism.php?x=browse&dduMenu_0_value=1&dduMenu_0=seriesListing&listing&SendSearch=1\">James Southgate Letters</a>";
												break;
				case "1.3":
												// Letter Series
												$displayType = "<a href=\"hueism.php?x=browse&dduMenu_0_value=1&dduMenu_0=seriesListing&listing&SendSearch=1\">Southgate Jones Letters</a>";
												break;
				case "1.4":
												// Letter Series
												$displayType = "Atlas Rigsbee Ledgerbook";
												break;
										case "loc":
												// LC Subject Heading
												$displayType = "<a href=\"hueism.php?x=browse&dduMenu_0_value=1&dduMenu_0=locListing&listing&SendSearch=1\">LC Subject Heading</a>";
												break;
										default:
														$displayType = "Your Search";
														break;
						
                }
		
		if (count($result) > 0) {
				
				if (!$format) {
					return ($result);
				}
				
				$dOut .= "<tr><td colspan=\"3\"><p class=\"header\"><a href=\"hueism.php?x=browse\">Browse Collection</a> &rsaquo; ".$displayType." &rsaquo; " . count($result) . " ";
				
					if (count($result) == 1) {
						$dOut .= "record ";
					} else {
						$dOut .= "records ";
					}
					
				$dOut .= "found where " . $this->getSearchDisplay() . "</p></td></tr>";

				
				for ($i = 0; $i < count($result); $i++) {
				
					$images = $search_imageBase .  "/" . $result[$i]["collection_prefix"] . $result[$i]["category"] . $result[$i]["folder"] . $result[$i]["item_number"] . "0010.jpg";
					
					
					if ($i%2==0) {
						$dOut .= "<tr class=\"search_row_on\">";
					} else {
						$dOut .= "<tr class=\"search_row_off\">";
					}
					
					// We also need to determine the type of content so that we can direct it to the appropriate
					// link.  This is based on the item category
					
					$category = $result[$i]["category"];
					switch($category) {
					
						case "pw":
							// Printed Works
							// They are special type of books
							$link = "<a href=\"hueism.php?x=printedwork&id=" . $result[$i]["item_id"] . "\">";
							break;
						case "ma":
							// Maps
							$link = "<a href=\"hueism.php?x=map&id=" . $result[$i]["item_id"] . "\">";
							break;		
						case "br":
							// Business Records
							$link = "<a href=\"hueism.php?x=businessrecord&id=" . $result[$i]["item_id"] . "\">";
							break;						
						case "ms":
							// Manuscripts Records
							if ($itemType == "1.4") {
								$link = "<a href=\"hueism.php?x=ledger&id=" . $result[$i]["item_id"] . "\">";
							} else {
								$link = "<a href=\"hueism.php?x=letter&id=" . $result[$i]["item_id"] . "\">";
							}
							break;					
						case "ep":
							// Ephemera
							$link = "<a href=\"hueism.php?x=ephemera&id=" . $result[$i]["item_id"] . "\">";
							break;						
						case "ph":
							// Maps
							$link = "<a href=\"hueism.php?x=photograph&id=" . $result[$i]["item_id"] . "\">";
							break;
						default:
							$link = "<a href=\"hueism.php?x=letter&id=" . $result[$i]["item_id"] . "\">";
							break;
					
					}
          
					$title = $result[$i]["dc_title"];
          
					if ($this->file_url_exists("$imageBase_basepath/$images")) {
						$dOut .= "<td class=\"search_image\">$link<img src=\"$images\" border=\"0\"></a></td>";
					} else {
						$dOut .= "<td class=\"search_image\">$link<img src=\"/images/notfound150.gif\" border=\"0\"></a></td>";
					}
          
					$dOut .= "<td class=\"search_text\">";
					
					// Trim lines so they are not too large
					//if (strlen($result[$i]["dc_title"]) > 100) {
					//	$title = substr($result[$i]["dc_title"], 0, 100) . "...";
					//} else {
						
					//}
					
					if (strlen($result[$i]["dc_description"]) > 1000) {
						$description = substr($result[$i]["dc_description"], 0, 1000) . "...";
					} else {
						$description = $result[$i]["dc_description"];
					}
					
					$dOut .= "<b>".$link." $title</a></b><br />";
					$dOut .= "$description<br />";
					$dOut .= "</td>";
                                        
          $dOut .= "</tr>";
				
				}
				
				$dOut .= "</table>";
				
		} else {
			
			$dOut .= "<b>Your search returned 0 records.</b> Please refine your search and try again.</table>";
			
		}
	
		
			return ($dOut);

	}
	
	function setSearchDisplay($valueIn) {
		
		if (isset($_REQUEST['boolean']) && $_REQUEST['boolean'] != "") {
			$boolean = $_REQUEST['boolean'];
		} else {
			$boolean = "AND";
		}
		
		if ($this->searchDisplay == "") {
			$this->searchDisplay .= $valueIn;
		} else {
			$this->searchDisplay .= " $boolean $valueIn";
		}
	}
	
	function getSearchDisplay() {
		return ($this->searchDisplay);
	}
	

	function getOrderByMenu() {
	
		$output = "";
	
		// Ok, so what are all of the possibilities that we wish to organize data by?
		$orderListDisplay = array("Select Order...", "Date", "Title", "Creator");
		$orderListName = array("null", "dc_date", "dc_title", "dc_creator");

		$output .= "<select name=\"orderBy\" id=\"dd_select_menu\" onchange=\"changeUrl(this);\">\n";
		
		for ($i = 0; $i < count($orderListDisplay); $i++) {
			if (isset($_REQUEST['orderBy']) && strtolower($_REQUEST['orderBy']) == $orderListName[$i]) {
				$select = " selected";
			} else {
				$select = "";
			}
			$_url = $_SERVER['REQUEST_URI'];
			$_url = preg_replace('/\&+orderBy\=.*\&*/i', '', $_url);
			if ($orderListName[$i] == "null") {
				$output .= '<option value="'.$_url.'&orderBy='.$orderListName[$i].'" disabled>'.$orderListDisplay[$i].'</option>';
			} else {
				$output .= '<option value="'.$_url.'&orderBy='.$orderListName[$i].'" '.$select.'>'.$orderListDisplay[$i].'</option>';
			}
		}
		
		$output .= "</select>\n";

		$output .= "
			 <script>
			 function changeUrl(element) {
			 	window.location = element.value;
			 }
			</script>
			";
		
		
		return ($output);
	
	}
	
	
	function getRecipientSearch() {
	
		$output = "";
		
		$output .= "<select name=\"roleStatus\">\n";
		$output .= "<option value=\"Author\">Author</option>\n";
		$output .= "<option value=\"Recipient\">Recipient</option>\n";
		$output .= "<option value=\"Both\">Both</option>\n";
		$output .= "</select>&nbsp; ";
		
		$_params = array();
		$_query = 'SELECT 
			DISTINCT creator AS role 
		FROM 
			item_creator 
		WHERE 
			role LIKE :author OR role LIKE :recipient ORDER BY creator ASC';
		$_params[':author'] = 'Author';
		$_params[':recipient'] = 'Recipient';
		
		try {
			$result = R::getAll($_query, $_params);
		} catch (\Exception $e) {
			$result = array();
		}
				
			$output .= "<select name=\"role\">\n";
			$output .= "<option value=\"null\">Select Creator...</option>";
			
			for ($i = 0; $i < count($result); $i++) {
			if (isset($_REQUEST['role']) && $_REQUEST['role'] == $result[$i]["role"]) {
				$select = " selected";
			} else {
				$select = "";
			}
			
			$output .= "<option value=\"" . $result[$i]["role"] . "\" $select>" . substr($result[$i]["role"], 0, 30) . "</option>\n";
			
		}
		
			$output .= "</select>\n";
						
			
		return ($output);
	
	}
	
	function file_url_exists($url) {
		$handle = @fopen($url, "r");
		 if ($handle === false)
			  return false;
		 fclose($handle);
		 return true;
	}

}

?>
