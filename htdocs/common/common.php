<?php

include_once('rb.php');

$_DB_CONFIG_PARAMS = parse_ini_file(dirname(__DIR__).'/../data/config.ini', true);

$_env = 'production';
// determine the envionment
if ((! empty($_DB_CONFIG_PARAMS['setup'])) && (! empty($_DB_CONFIG_PARAMS['setup']['env'])))
	$_env = $_DB_CONFIG_PARAMS['setup']['env'];

// use the production settinhs
$_DB_CONFIGS = $_DB_CONFIG_PARAMS[$_env];
unset($_DB_CONFIG_PARAMS);

R::setup( 'mysql:host='.$_DB_CONFIGS['host'].';dbname='.$_DB_CONFIGS['database'], $_DB_CONFIGS['user'], $_DB_CONFIGS['password']);
unset($_DB_CONFIGS);