<?xml version="1.0" encoding="iso-8859-1"?>
<?xml-stylesheet href="./styles/tei-nc.xsl" type="text/xsl"?>
<!DOCTYPE TEI.2 PUBLIC "-//TEI//DTD TEI Lite XML ver. 1//EN" 
"./dtds/teixlite.dtd" [<!ENTITY tei "Text Encoding Initiative">
<!ENTITY plus   "&#x002B;"> <!ENTITY dollar "&#x0024;">
<!ENTITY nbsp   "&#x00A0;"><!ENTITY percnt "&#x0025;">
<!ENTITY half   "&#x00BD;">]>


<TEI.2>

	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title>Broadside from James Southgate, July 1, 1881: Electronic Edition.</title>
				<author>Southgate, James, 1832-1914</author>

				<funder>Funding by the Institute of Museum of Library Services (IMLS) and the federal Library Services and Technology Act (LSTA), with support provided through North Carolina ECHO.</funder>
				<respStmt>
					<resp>Text transcribed by</resp>
					<name>Kelly Clark</name>
				</respStmt>	
				<respStmt>
					<resp>Images scanned by</resp>
					<name>Digital Production Center</name>
				</respStmt>
				<respStmt>
					<resp>Text encoded by</resp>
					<name>Katherine M. Wisser</name>
				</respStmt>
			</titleStmt>

			<editionStmt>
				<edition>First edition, 
					<date>2006</date>
				</edition>
			</editionStmt>

			<publicationStmt>
				<publisher>Duke University Libraries</publisher>
				<pubPlace>Durham, NC, USA</pubPlace>
				<date>2006</date>
				<availability><p>&#x00A9; This work is the property of the Duke University Libraries. It may be used freely by individuals for research, teaching, and personal use as long as this statement of availability is included in the text.</p></availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="title page">Broadside from James Southgate, July 1, 1881</title>
						<author>James Southgate</author>
					</titleStmt>
					
					<extent>1 p.</extent>
					<publicationStmt>
						<date>18810701</date>
					</publicationStmt>
					<notesStmt>
						
						<note>In an effort to help his daughter obtain a teaching position, James Southgate distributes a broadside describing Lessie Muse Southgate's qualifications in vocal and instrumental music. The broadside gives an overview of Lessie's education at the Wesleyan Female Institute in Staunton, Virginia and the Grand Conservatory of Music in New York. Mr. Southgate gives Professor Ernst Eberhard of the Grand Conservatory and E. Louis Ide of the Wesleyan Female Institute as references.  The broadside notes that Lessie is a member of the Methodist Episcopal Church, but is willing to work as music instructor for any denomination.</note> 
					</notesStmt>
<sourceDesc>
						<p>James Southgate papers, 1794-1944 and undated, Rare Book, Manuscript and Special Collections Library, Duke University.</p>					
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<encodingDesc>
			<projectDesc>
				<p>This electronic edition is part of the Duke University Libraries digitization project, <hi rend="italics">Digital Durham</hi>
				</p>
			</projectDesc>	

			<editorialDecl>
				<p>The text has been encoded using the recommendations for Level 4 of the TEI.</p>
				
				<p>Original, grammar, punctuation and spelling have been preserved. Encountered typographical errors have been preserved with corrections provided. In some cases, we've put sic in square brackets where we've identified misspellings or lack of punctuation but do not require corrections in order to understand the text. Where older spellings occurred, the original spelling is preserved and a modern equivalent of the word is provided.</p>
				
				<p>Any hyphens occurring in line breaks have been removed, and the trailing part of a word has been joined to the preceding line.</p>
				
				<p>All dollar signs, plus signs, and percentage signs, fractions, and ampersands have been transcribed as entity references.</p>
				
				<p>Blank spaces have been indicated with 3 &nbsp; characters where they are necessary because of lack of punctuation in the original text.</p>
				
				<p>In the case of writers who put ill-defined ink marks in places where commas or periods should exist, we take the liberty to transcribe them as either commas, periods, or semicolons to make the meaning of the text more clear.</p>
				
				<p>In the case where words or parts of words cannot be discerned, a gap, represented by a series of underscores, has been included where the work or part of the word appear in the text.</p>
				
				<p>Indentation has not been preserved. Paragraph breaks were included only when it was obvious.</p>
				
			</editorialDecl>

			<classDecl>
				<taxonomy id="LCSH">
					<bibl>
						<title>Library of Congress Subject Headings</title>
					</bibl>
				</taxonomy>
			</classDecl>
		</encodingDesc>

		<profileDesc>
			<langUsage>
				<language id="eng">English</language>
			</langUsage>

			<textClass>
				<keywords scheme="LCSH">
					<list>
<item>Music -- Instruction and study -- Southern states -- 19th century</item>
<item>Methodist church -- North Carolina -- 19th century</item>
<item>Durham (N.C.) -- History</item>
<item>Women -- Education</item>
<item>Simmons, Celestia Muse Southgate, 1863-1914</item>
<item>Grand Conservatory of Music (New York, N.Y.)</item>
<item>Education -- Virginia -- Social aspects -- History -- 19th century</item>
<item>Education -- New York -- Social aspects -- History -- 19th century</item>
<item>Women -- North Carolina -- Social conditions -- 19th century</item>
<item>Women teachers -- History</item>
<item>Music -- Instruction and study -- New York (State) -- New York -- 19th century</item>
<item>Southgate, James, 1832-1914</item>
<item>Fathers and daughters -- North Carolina -- History -- 19th century</item>
<item>Boarding schools -- Virginia</item>
<item>Wesleyan Female Institute (Staunton, Va.)</item>
<item>Durham (N.C.)</item>
<item>New York (N.Y.)</item>
<item>Staunton (Va.)</item>
<item>Broadsides</item>
					</list>
				</keywords>

			</textClass>
		</profileDesc>

	<!--		<revisionDesc>
			<change>
				<date></date>
				<respStmt>
					<name></name>
					<resp></resp>
				</respStmt>
				<item></item>
			</change>
		</revisionDesc> -->
	</teiHeader>

	<text>
<body>

<div1 type="broadside">
<pb id="p1" n="1"/>
<opener>
<dateline>
<name type="place">Durham, N.C., </name>
<date>July 1st, 1881.</date>
</dateline>
</opener>

<p>I wish to obtain a situation for my daughter to teach in a College or High School, and she will be ready to begin with the Fall Session.  She is prepared to give thorough instruction in Music, including Piano, Organ, Thorough Bass, Harmony and Vocalization--is a first-class Performer on Piano and Organ and took both Diploma and Medal on Instrumental Music at the Wesleyan Female Institute, Staunton, Virginia.  She also will teach Calisthenics in all its branches, and Elocution, having taken Medals in both of those studies.</p>
	<p>She has just returned from the Grand Conservatory of New York and obtained flattering testimonials of advancement and proficiency from some of the first Professors in this country.</p>
	<p>She has had some experience in teaching and has given perfect satisfaction, evidenced by the rapid advancement of her pupils and the faculty of endearing the pupils to her as a teacher and friend.</p>
	<p>She is a member of the Methodist Episcopal Church South, but does not object to teaching in an institution of another denomination.</p>

<p>References:<lb/>
Prof. Ernst Eberhard, Director Grand Conservatory of Music, No. 21, East 14th St., New York.<lb/>
Prof. E. Louis Ide, Principal Department of Music, Wesleyan Female Institute, Staunton, Va.</p>

<p>Address communications to Miss Lessie M. Southgate, Durham, N.C.</p>


 

<closer>
<salute>Very respectfully,</salute>
<signed>James Southgate.</signed>
</closer>

	</div1>
</body>

	</text>
</TEI.2>
