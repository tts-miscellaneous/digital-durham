<?xml version="1.0" encoding="iso-8859-1"?>
<?xml-stylesheet href="./styles/tei-nc.xsl" type="text/xsl"?>
<!DOCTYPE TEI.2 PUBLIC "-//TEI//DTD TEI Lite XML ver. 1//EN" 
"./dtds/teixlite.dtd" [<!ENTITY tei "Text Encoding Initiative">
<!ENTITY plus   "&#x002B;"> <!ENTITY dollar "&#x0024;">
<!ENTITY nbsp   "&#x00A0;"><!ENTITY percnt "&#x0025;">
<!ENTITY half   "&#x00BD;">]>


<TEI.2>

	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title>Letter from Mollie Page to Mattie Logan Southgate, January 29, 1883: Electronic Edition.</title>
				<author>Page, Mollie</author>
				<author>Jones, Mattie Logan Southgate</author>

				<funder>Funding by the Institute of Museum of Library Services (IMLS) and the federal Library Services and Technology Act (LSTA), with support provided through North Carolina ECHO.</funder>
				<respStmt>
					<resp>Text transcribed by</resp>
					<name>Trudi Abel</name>
				</respStmt>	
				<respStmt>
					<resp>Images scanned by</resp>
					<name>Digital Production Center</name>
				</respStmt>
				<respStmt>
					<resp>Text encoded by</resp>
					<name>Katherine M. Wisser</name>
				</respStmt>
			</titleStmt>

			<editionStmt>
				<edition>First edition, 
					<date>2006</date>
				</edition>
			</editionStmt>

			<publicationStmt>
				<publisher>Duke University Libraries</publisher>
				<pubPlace>Durham, NC, USA</pubPlace>
				<date>2006</date>
				<availability><p>&#x00A9; This work is the property of the Duke University Libraries. It may be used freely by individuals for research, teaching, and personal use as long as this statement of availability is included in the text.</p></availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="title page">Letter from Mollie Page to Mattie Logan Southgate, January 29, 1883</title>
						<author>Mollie Page</author>
					</titleStmt>
					
					<extent>5 p.</extent>
					<publicationStmt>
						<date>18830129</date>
					</publicationStmt>
					<notesStmt>
						<note>Writing under her nickname "Mary Eddie," Mollie Page sends her friend Mattie Logan Southgate ("Tom") news about principal William A. Harris' plan for resuming classes at the Wesleyan Female Institute. Mollie reports that Dr. Harris sent her father a letter with his proposal to start  school in February, but she doubts that she will be able to attend. Mollie tells Mattie of her plans to travel to Charlottesville with her father and promises to send a photograph of herself. She shares news of their friends Lela, "Dean,"  Pattie, Cherry and her brother Bob. Mollie closes with a discussion of men, marriage, and tobacco.</note> 
					</notesStmt>
<sourceDesc>
						<p>James Southgate papers, 1794-1944 and undated, Rare Book, Manuscript and Special Collections Library, Duke University.</p>					
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<encodingDesc>
			<projectDesc>
				<p>This electronic edition is part of the Duke University Libraries digitization project, <hi rend="italics">Digital Durham</hi>
				</p>
			</projectDesc>	

			<editorialDecl>
<p>The text has been encoded using the recommendations for Level 4 of the TEI.</p>
				
				<p>Original, grammar, punctuation and spelling have been preserved. Encountered typographical errors have been preserved with corrections provided. In some cases, we've put sic in square brackets where we've identified misspellings or lack of punctuation but do not require corrections in order to understand the text. Where older spellings occurred, the original spelling is preserved and a modern equivalent of the word is provided.</p>
				
				<p>Any hyphens occurring in line breaks have been removed, and the trailing part of a word has been joined to the preceding line.</p>
				
				<p>All dollar signs, plus signs, and percentage signs, fractions, and ampersands have been transcribed as entity references.</p>
				
				<p>Blank spaces have been indicated with 3 &nbsp; characters where they are necessary because of lack of punctuation in the original text.</p>
				
				<p>In the case of writers who put ill-defined ink marks in places where commas or periods should exist, we take the liberty to transcribe them as either commas, periods, or semicolons to make the meaning of the text more clear.</p>
				
				<p>In the case where words or parts of words cannot be discerned, a gap, represented by a series of underscores, has been included where the work or part of the word appear in the text.</p>
				
				<p>Indentation has not been preserved. Paragraph breaks were included only when it was obvious.</p>
				
			</editorialDecl>

			<classDecl>
				<taxonomy id="LCSH">
					<bibl>
						<title>Library of Congress Subject Headings</title>
					</bibl>
				</taxonomy>
			</classDecl>
		</encodingDesc>

		<profileDesc>
			<langUsage>
				<language id="eng">English</language>
			</langUsage>

			<textClass>
				<keywords scheme="LCSH">
					<list>
<item>Durham (N.C.) -- History</item>
<item>Page, Mollie</item>
<item>Jones, Mattie Logan Southgate</item>
<item>Wesleyan Female Institute (Staunton, Va.)</item>
<item>Friendship in adolescence -- Southern states -- 19th century</item>
<item>Women in communication -- Southern states -- History -- 19th century</item>
<item>Diseases -- Virginia -- History -- 19th century</item>
<item>Wesleyan Female Institute (Staunton, Va.) -- Students -- History -- 19th century</item>
<item>Marriage -- 19th century</item>
<item>Harris, William A., b. 1830</item>
<item>Batesville (Va.)</item>
<item>Durham (N.C.)</item>
<item>Letters (correspondence)</item>
					</list>
				</keywords>

			</textClass>
		</profileDesc>

	<!--		<revisionDesc>
			<change>
				<date></date>
				<respStmt>
					<name></name>
					<resp></resp>
				</respStmt>
				<item></item>
			</change>
		</revisionDesc> -->
	</teiHeader>

	<text>
<body>

<div1 type="letter">
<pb id="p1" n="1"/>
<opener>
<dateline>
<name type="place">Batesville</name>
<date>Jan 29 1883</date>
</dateline>
<salute>My dearest "Tom";--</salute>
</opener>

<p>I received your letter last week and suppose you have gotten mine by this time, but will write you a short one to-night. I have just finished writing to Eddie and it is late, but I will not slight you.  I got a long letter from "Dean" this evening saying she was so sorry her mother and father had been sick.  Lela has been sick too, but both of us are better.  I went to church yesterday for the first time since <pb id="p2" n="2"/><abbr expan="Christmas">Xmas</abbr>.  I am trying to be well enough to go to Charlottesville Thursday with Pa.  I will have a nice time going now as he has to go every week to see to the bank that he is vic[e] president of, so I can go when I want to. If I have time I am going to have my picture [taken] and will send you one if you will send <abbr expan="yours">y'r</abbr>. I told you all about the fever in my last letter.  I think you have an idea of coming back to school I would be delighted if I were to go back, and you would come but it is doubtful whether I will go  Pa got a letter from Dr Harris saying that they were aiming to start the school the <pb id="p3" n="3"/>middle of Feb.[ruary] and would keep it until the middle of July.  I don't think they will keep it that long because the girls will not stay. they will have I suppose about half of a school.  I am having a lonely old time here by myself but soon Bob will be at home, he says he is having a lovely time in New York going to see the girls. he is awfully sly  I wrote to him the other night &amp; wrote twelve pages and could have written more but I was so sleepy I couldn't sit up   Mattie I would write you a long letter, but I have <pb id="p4" n="4"/>I am going up to call on Prof wife &amp; Ida this evening and will write you then all about the school.  I have been back since <abbr expan="Christmas">Xmas</abbr>.  Cora wrote to me last week and she is at home. say's she does not think her father will let her come back.  Pattie &amp; Cherry are in Millbora about eighty miles from S<hi rend="underline">&nbsp;</hi> Cherry wouldn't let her go home so took her out there to stay until school commences. Now you must write me a long letter this is two for me to your one. Pa say's [says] he is waiting for you to write to him and he wants to see you also send's much love to you.  Why do you want to know if he uses tobacco? I am sorry to say he does, and when you get married don't marry a man that <pb id="p5" n="5"/>uses tobacco.  I love for them to smoke but not chew. "Tom" I want to see you so bad: I wish you would come on and see me I could tell you everything  in  a month and can't begin to write you all.  Don't treat me so badly as you have done and I will alway's [sic] answer your letters promptly.  Write me  along letter very soon. Much love and tub full of kisses to my devoted son "Tom" from " Mary Eddie"</p>

<p>Don't forget to send your picture in your next [letter] a cabinet &amp; <pb id="p4a" n="4a"/>I will send you mine as soon as I can get them</p>

	</div1>
</body>

	</text>
</TEI.2>
