<?xml version="1.0" encoding="iso-8859-1"?>
<?xml-stylesheet href="./styles/tei-nc.xsl" type="text/xsl"?>
<!DOCTYPE TEI.2 PUBLIC "-//TEI//DTD TEI Lite XML ver. 1//EN" 
"./dtds/teixlite.dtd" [<!ENTITY tei "Text Encoding Initiative">
<!ENTITY plus   "&#x002B;"> <!ENTITY dollar "&#x0024;">
<!ENTITY nbsp   "&#x00A0;"><!ENTITY percnt "&#x0025;">
<!ENTITY half   "&#x00BD;">]>


<TEI.2>

	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title>Mattie's Report Card from Wesleyan Female Institute, December 1881: Electronic Edition.</title>
				<author>Wesleyan Female Institute (Staunton, Va.)</author>				
				<author>Harris, William A., b. 1830</author>

				<funder>Funding by the Institute of Museum of Library Services (IMLS) and the federal Library Services and Technology Act (LSTA), with support provided through North Carolina ECHO.</funder>
				<respStmt>
					<resp>Text transcribed by</resp>
					<name>Kelly Clark</name>
				</respStmt>
				<respStmt>
					<resp>Images scanned by</resp>
					<name>Digital Production Center</name>
				</respStmt>
				<respStmt>
					<resp>Text encoded by</resp>
					<name>Katherine M. Wisser</name>
				</respStmt>
			</titleStmt>

			<editionStmt>
				<edition>First edition, 
					<date>2006</date>
				</edition>
			</editionStmt>

			<publicationStmt>
				<publisher>Duke University Libraries</publisher>
				<pubPlace>Durham, NC, USA</pubPlace>
				<date>2006</date>
				<availability><p>&#x00A9; This work is the property of the Duke University Libraries. It may be used freely by individuals for research, teaching, and personal use as long as this statement of availability is included in the text.</p></availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="title page">Mattie's Report Card from Wesleyan Female Institute, December 1881</title>
						<author>Wesleyan Female Institute</author>
					</titleStmt>
					
					<extent>1 p.</extent>
					<publicationStmt>
						<date>188112</date>
					</publicationStmt>
					<notesStmt>
						
						<note>This  December 1881 report card from the Wesleyan Female Institute at Staunton, Virginia assesses Mattie Southgate's progress in writing, natural philosophy, rhetoric, elocution, French, German, vocal music, instrumental music, calisthenics, and conduct. Directed by principal William A. Harris, the Wesleyan Female Institute offered a range of courses including those on Butler's Analogy, evidence of Christianity, Geometry -- plane and analytical, philosophy, mental and moral, and book keeping.</note> 
					</notesStmt>
<sourceDesc>
						<p>James Southgate papers, 1794-1944 and undated, Rare Book, Manuscript and Special Collections Library, Duke University.</p>					
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<encodingDesc>
			<projectDesc>
				<p>This electronic edition is part of the Duke University Libraries digitization project, <hi rend="italics">Digital Durham</hi>
				</p>
			</projectDesc>	

			<editorialDecl>
			<p>The text has been encoded using the recommendations for Level 4 of the TEI.</p>
				
				<p>Original, grammar, punctuation and spelling have been preserved. Encountered typographical errors have been preserved with corrections provided. In some cases, we've put sic in square brackets where we've identified misspellings or lack of punctuation but do not require corrections in order to understand the text. Where older spellings occurred, the original spelling is preserved and a modern equivalent of the word is provided.</p>
				
				<p>Any hyphens occurring in line breaks have been removed, and the trailing part of a word has been joined to the preceding line.</p>
				
				<p>All dollar signs, plus signs, and percentage signs, fractions, and ampersands have been transcribed as entity references.</p>
				
				<p>Blank spaces have been indicated with 3 &nbsp; characters where they are necessary because of lack of punctuation in the original text.</p>
				
				<p>In the case of writers who put ill-defined ink marks in places where commas or periods should exist, we take the liberty to transcribe them as either commas, periods, or semicolons to make the meaning of the text more clear.</p>
				
				<p>In the case where words or parts of words cannot be discerned, a gap, represented by a series of underscores, has been included where the work or part of the word appear in the text.</p>
				
				<p>Indentation has not been preserved. Paragraph breaks were included only when it was obvious.</p>
				
			</editorialDecl>

			<classDecl>
				<taxonomy id="LCSH">
					<bibl>
						<title>Library of Congress Subject Headings</title>
					</bibl>
				</taxonomy>
			</classDecl>
		</encodingDesc>

		<profileDesc>
			<langUsage>
				<language id="eng">English</language>
			</langUsage>

			<textClass>
				<keywords scheme="LCSH">
					<list>
<item>Wesleyan Female Institute (Staunton, Va.) -- Students -- History -- 19th century</item>
<item>Wesleyan Female Institute (Staunton, Va.)</item>
<item>Boarding schools -- Virginia</item>
<item>Harris, William A., b. 1830</item>
<item>Jones, Mattie Logan Southgate</item>
<item>Durham (N.C.) -- History</item>
<item>Education -- Virginia -- Social aspects -- History -- 19th century</item>
<item>Women -- Education</item>
<item>Women -- North Carolina -- Social conditions -- 19th century</item>
<item>Report cards</item>
<item>Durham (N.C.)</item>
<item>Staunton (Va.)</item>

					</list>
				</keywords>

			</textClass>
		</profileDesc>

	<!--		<revisionDesc>
			<change>
				<date></date>
				<respStmt>
					<name></name>
					<resp></resp>
				</respStmt>
				<item></item>
			</change>
		</revisionDesc> -->
	</teiHeader>

	<text>
<body>

<div1 type="letter">
<pb id="p1" n="1"/>
<opener>
<dateline>Wesleyan Female Institute<lb/>
Staunton, Virginia,<lb/>
Rev. Wm. A. Harris, President, with Twenty Teachers &amp; Officers.</dateline>
<salute></salute>
</opener>

<p>We have but one Session-That is the Scholastic year from September to June.  All pupils, without a special contract to the contrary, are entered and charged for the Scholastic year, or from date of entrance to the close of the Scholastic year.  No deduction will be made on account of WITHDRAWAL of a pupil except for protracted sickness OF THE PUPIL, on the certificate of the attending Physician of the Institute.</p>

<p>Official Report of Miss Mattie Southgate of North Carolina for the month of December 1881.</p>

<p>
<table rows="40" cols="3">
<row>
<cell>STUDIES.</cell><cell>STANDING.</cell><cell>ABSENCE.</cell></row>

<row>
<cell>Orthography,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell></row>
<row>
<cell>Reading,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Writing,</cell>	<cell>92</cell>
<cell>&nbsp;</cell></row>
<row>
<cell>Geography,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>English Grammar,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Composition,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>History,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Arithmetic,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Algebra,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Geometry, Plane,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Trigonometry,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Analytical Geometry,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Astronomy,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Geology,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Natural Philosophy,</cell><cell>90</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Chemistry,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>[P]hysiology,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Botany,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Rhetoric,</cell><cell>92</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Elocution,</cell><cell>93</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>English Literature,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Mental Philosophy,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row><cell>Moral Philosophy,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Evidence of Christianity,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Butler's Analogy,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Latin,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Greek,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>French,</cell><cell>89</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>German,</cell><cell>86</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Spanish,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Vocal Music,</cell><cell>93</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Instrumental Music,</cell><cell>88</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Guitar Music,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Painting,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Drawing,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Ornamental Work,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Calisthenics,</cell><cell>97</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Book-keeping,</cell><cell>&nbsp;</cell><cell>&nbsp;</cell>
</row>
<row>
<cell>Conduct,</cell><cell>92</cell><cell>&nbsp;</cell>
</row>
</table></p>

		

<p>The Grade of Proficiency of young ladies in their several studies, is indicated by numbers, according to the following scale:</p>
<p><table rows="2" cols="2">
<row>
<cell>70 to 80 signifies good-sustained.</cell><cell>80 to 90 signifies very good.</cell>
</row>
<row>
<cell>90 to 100 signifies distinguished.</cell><cell>100 signifies "perfect."</cell>
</row>
</table></p>



<p>Conduct.-Perfection in conduct, or "no demerit," is marked 100.  Every young lady, on entering the Institution, is credited with 100 in conduct.  Each demerit afterwards received takes one from the maximum-100</p>

<p>Remarks:</p>


<closer>

<signed>Wm A. Harris<lb/>
President W. F. I.</signed>
</closer>

	</div1>
</body>

	</text>
</TEI.2>
