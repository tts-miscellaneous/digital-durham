<?xml version="1.0" encoding="iso-8859-1"?>
<?xml-stylesheet href="./styles/tei-nc.xsl" type="text/xsl"?>
<!DOCTYPE TEI.2 PUBLIC "-//TEI//DTD TEI Lite XML ver. 1//EN" 
"./dtds/teixlite.dtd" [<!ENTITY tei "Text Encoding Initiative">
<!ENTITY plus   "&#x002B;"> <!ENTITY dollar "&#x0024;">
<!ENTITY nbsp   "&#x00A0;"><!ENTITY percnt "&#x0025;">
<!ENTITY half   "&#x00BD;">]>


<TEI.2>

	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title>Letter from James Southgate to Delia Haywood Southgate, September 18, 1883: Electronic Edition.</title>
				<author>Southgate, James, 1832-1914</author>
				<author>Southgate, Delia Haywood Wynne</author>

				<funder>Funding by the Institute of Museum of Library Services (IMLS) and the federal Library Services and Technology Act (LSTA), with support provided through North Carolina ECHO.</funder>
				<respStmt>
					<resp>Text transcribed by</resp>
					<name>Kelly Clark</name>
				</respStmt>	
				<respStmt>
					<resp>Images scanned by</resp>
					<name>Digital Production Center</name>
				</respStmt>
				<respStmt>
					<resp>Text encoded by</resp>
					<name>Katherine M. Wisser</name>
				</respStmt>
			</titleStmt>

			<editionStmt>
				<edition>First edition, 
					<date>2006</date>
				</edition>
			</editionStmt>

			<publicationStmt>
				<publisher>Duke University Libraries</publisher>
				<pubPlace>Durham, NC, USA</pubPlace>
				<date>2006</date>
				<availability><p>&#x00A9; This work is the property of the Duke University Libraries. It may be used freely by individuals for research, teaching, and personal use as long as this statement of availability is included in the text.</p></availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="title page">Letter from James Southgate to Delia Haywood Southgate, September 18, 1883</title>
						<author>James Southgate</author>
					</titleStmt>
					
					<extent>4 p.</extent>
					<publicationStmt>
						<date>18830918</date>
					</publicationStmt>
					<notesStmt>
						<note>James Southgate writes his wife Delia Southgate about his medical regimen at the Retreat of the Sick, in Richmond and the news he has heard about  illnesses in Durham. Responding to reports from Durham of the  Whitaker family's illness, Southgate advises that Mr. Whitaker eliminate his hogs, dismantle the hog pen, and put in a  layer of lime on the top of the soil. He advises his family to clean their property and use lime to purify the grounds and sleeping rooms. He expresses his  concern about the ill health of his Durham friends Mrs. Blackwell, her daughter Mary Blackwell and his daughter's school mate Minnie. He notes his correspondence with Grandma Zach about the Richmond fruit market and makes plans with Delia purchase peaches and pears from Chapel Hill for canning. He describes the mischievous antics of sailors at the Retreat who baptize a brood of kittens. In closing, Southgate  mentions his correspondence and visits with  friends and relatives including Kate Sanborn, Cousin Augusta Christian, Sarah "Sallie" Raboteau, Hunter, and Tom and Mary ___________.</note> 
					</notesStmt>
					<sourceDesc>
						<p>James Southgate papers, 1794-1944 and undated, Rare Book, Manuscript and Special Collections Library, Duke University.</p>					
					</sourceDesc>

				</biblFull>
			</sourceDesc>
		</fileDesc>
		<encodingDesc>
			<projectDesc>
				<p>This electronic edition is part of the Duke University Libraries digitization project, <hi rend="italics">Digital Durham</hi>
				</p>
			</projectDesc>	

			<editorialDecl>
<p>The text has been encoded using the recommendations for Level 4 of the TEI.</p>
				
				<p>Original, grammar, punctuation and spelling have been preserved. Encountered typographical errors have been preserved with corrections provided. In some cases, we've put sic in square brackets where we've identified misspellings or lack of punctuation but do not require corrections in order to understand the text. Where older spellings occurred, the original spelling is preserved and a modern equivalent of the word is provided.</p>
				
				<p>Any hyphens occurring in line breaks have been removed, and the trailing part of a word has been joined to the preceding line.</p>
				
				<p>All dollar signs, plus signs, and percentage signs, fractions, and ampersands have been transcribed as entity references.</p>
				
				<p>Blank spaces have been indicated with 3 &nbsp; characters where they are necessary because of lack of punctuation in the original text.</p>
				
				<p>In the case of writers who put ill-defined ink marks in places where commas or periods should exist, we take the liberty to transcribe them as either commas, periods, or semicolons to make the meaning of the text more clear.</p>
				
				<p>In the case where words or parts of words cannot be discerned, a gap, represented by a series of underscores, has been included where the work or part of the word appear in the text.</p>
				
				<p>Indentation has not been preserved. Paragraph breaks were included only when it was obvious.</p>
				
			</editorialDecl>

			<classDecl>
				<taxonomy id="LCSH">
					<bibl>
						<title>Library of Congress Subject Headings</title>
					</bibl>
				</taxonomy>
			</classDecl>
		</encodingDesc>

		<profileDesc>
			<langUsage>
				<language id="eng">English</language>
			</langUsage>

			<textClass>
				<keywords scheme="LCSH">
					<list>
<item>Durham (N.C.) -- History</item>
<item>Diseases -- North Carolina -- History -- 19th century</item>
<item>Farmers (Durham, N.C.)</item>
<item>Southgate, Annie</item>
<item>Sanborn, Kate Christine</item>
<item>Retreat of the Sick (Richmond, Va.)</item>
<item>Communication in the family -- Southern states -- History -- 19th century</item>
<item>Southgate, James, 1832-1914</item>
<item>Moore, Minnie, d. 1883</item>
<item>Disease outbreaks -- North Carolina -- History -- 19th century</item>
<item>Jones, Mattie Logan Southgate</item>
<item>Southgate, Delia Haywood Wynne</item>
<item>Christian, Augusta</item>
<item>Family -- Social life and customs -- Southern states</item>
<item>Disease outbreaks -- Virginia -- History -- 19th century</item>
<item>Family -- North Carolina -- History</item>
<item>Medical care -- United States -- History -- 19th century</item>
<item>Blackwell, Emma E., 1855 - 1885</item>
<item>Blackwell, Mary E.</item>
<item>Grandma Zack</item>
<item>Canning and preserving -- History -- 19th century</item>
<item>Raboteau, Sallie</item>
<item>Southgate, Cordelia Hunter</item>
<item>Chapel Hill (N.C.)</item>
<item>Durham (N.C.)</item>
<item>Richmond (Va.)</item>
<item>Letters (correspondence)</item>
					</list>
				</keywords>

			</textClass>
		</profileDesc>

	<!--		<revisionDesc>
			<change>
				<date></date>
				<respStmt>
					<name></name>
					<resp></resp>
				</respStmt>
				<item></item>
			</change>
		</revisionDesc> -->
	</teiHeader>

	<text>
<body>

<div1 type="letter">
<pb id="p1" n="1"/>
<opener>
<dateline>
307 N. 12th Street<lb/>
<name type="place">Richmond Va </name>
<date>Sept 18th 1883
</date>
</dateline>
<salute>My dear Old 'oman</salute>
</opener>

<p>I will let some of the <hi rend="underline">chaps</hi> rest today and write a letter all to my old [']oman if you <sic corr="can't">cant</sic> write as often to Old man as they. I received three letters from home this morning which made up in some degree for my disappointment in not getting any monday morning and I have a big scold laid up for the next day [if] any fail to write. I just as certainly expect letters when 8 oclock a. m [sic] arrives as I expect my breakfast and though that meal may amount to very little yet it is a meal- The <abbr expan="Doctor">D<hi rend="super">r</hi></abbr> gave me a soft boiled egg for my breakfast this morning and I ate it with much relish- I passed the day yesterday without pain, had a good nights rest and have thus far been very comfortable except a little uneasy feeling about 12 m-which soon passed off and I am very much in hopes we may be getting the best of those horrid attacks and that in a few days they may be overcome entirely- I am surely sick and tired of them if ever a poor mortal was and I trust the day is not far distant, when my health will be on a firm basis again. The <abbr expan="Doctor">D<hi rend="super">r</hi></abbr> is trying to make me think so any how and it looks as though he ought to know, for a man who has been 27 years laboring with and studying the sick ought to be able to diagnose a case like mine and find the remedy. He has not yet been able to get my liver to do its full share of the work and the kidneys consequently have to take much of the bile which the liver should carry off-but he is working in the right direction and I have great confidence in his skill and tact in managing difficult diseases. <pb id="p2" n="2"/>I am glad to hear such good news from our household each day. That you all keep out of the bed and are able to be up and about- Should any one be sick I should be strongly tempted to pull up stakes &plus; travel homewards. I fear Mr Whitakers sickness shows some local cause of disease in our neighborhood and you should see that our lot is kept well cleaned &plus; lime used freely-I think de-odorizers should be used in the sleeping rooms also. I expect the hog pen in Whitaker's lot is the cause of sickness- It is a great nuisance to any premises and should not be allowed by the authorities, for a man has no right to make his non-family sick any more than to make his neighbor sick- I have feared that pen all this while. Tell him I say, to sell his hogs clean up the pen and put lime two inches thick on it- I am pained to hear of Mrs Blackwells affliction and so much fear she will lose her little girl. It would be terrible on Mr B were he to lose his children &plus; if it is really Dyptheria it will be most likely to take them both. If this dire disease should take root in Durham  it will indeed be a terrible calamity. I know of nothing more fatal- Do keep our children away from it by all means- I am glad to know Minnie is improving as also the others who were sick. I do so sympathise with those who are not in the enjoyment of health-we cannot appreciate the blessing of a sound body until we get sick. How unthankful we are to the Great Giver of all good! Who crowns our lives with loving kindness and tender mercies- I received a letter from Mattie last Saturday but sent it to Kate Sanbom as I did Kate's to Matt, so they will both hear from each other <pb id="p3" n="3"/>as well as from me at the same time. Kate's letter was a tirade for not letting Matt visit her last winter and a pressing invitation for her to come this winter- I wrote her that my health was too feeble now to be making any promises for the future, that <hi rend="underline">if</hi> I recovered my health it would then be time to talk about pleasure trips &plus; such. She seemed to be entirely in ignorance of my ill health and I guess will be greatly surprised to hear such accounts of her old friend "not yet 60." She may think now it is not such a joke after all that her friend may not reach 60- I wrote Grandma Zack today telling her about the Richmond fruit market, and advising that she [not?] purchase here at the prices- I wrote to D [Mcleanly?] at Chapel Hill to keep a look out for some peaches and pears and if he sends more than you need to complete your supply you can let her take them at what they cost. He may send pears but I doubt if he can get the peaches, for they seem to be very scarce this season. I have about concluded to burn coal this winter at home + have written Jimmie to get the prices of coal in Durham both hard &plus; soft- I am in love with the soft coal used in grates here- It makes a lovely cheerful fire and can be graduated to suit a warm or cold day at pleasure- Wood is such an expense and trouble- Mrs Jenkins the Prist [priest] of the Retreat has just been in to make a call and seems to try to make herself quite agreeable- She is a High Church Episcopalian and tries to run the Retreat somewhat on that Schedule- There are generally a number of Sailors here sick and a ward in the basement is set apart for them- <pb id="p4" n="4"/>Dr Wheat tells a joke on Sister Jenkins- A Miss Williams of Wilmington was here for treatment 2 or 3 years ago and it so happened that she wished to be baptized here. Mrs Jenkins thought to make a good impressions upon the Sailors and had the ceremony performed with great solemnity in the sailors ward- They looked on very devoutly and she though much good had been accomplished- Imagine her chagrin the next day when she was told that the impious wretches had taken a brood of Kittens, that happened to be about 8 or 10 days old, and performed the rite of baptism upon them, going through with all the ceremony they had witnessed the day before- Sailors are a queer nation of folk and when wicked are very bad. Cousin Augusta Christian has not called to see me yet and I am at a loss to know why- Joe has been only once but has threatened to call several times since his first visit. Folks take queer notions sometimes. I have heard from Hunter only once and sent that letter home. I am expecting to hear from Tom tonight or rather from Mary to whom I note making enquiry as to the particulars of Tom's accident on the sound during the storm. I will have to write Cats tomorrow as she writes so seldom. As she is employed, I cant expect more than one per week from her- How about Old 'oman- Only one letter from you since I left home, and you promised so faithfully to write often-if not every day.  Wonder if Sallie Raboteau will answer my letter to her- Think I am powerfully smart to write so much to the household besides my other correspondence- I close this letter at 4 &half; p. m [sic] after having had quite a comfortable day and all my symptoms satisfactory. I report this with great pleasure- Tell Jimmie I <sic corr="haven't">hav'nt</sic> been out today- but the reason is it has been raining most of the time- Love to all</p>
<closer>
<signed>Your aff South</signed><lb/>
Excuse mistakes as I <sic corr="haven't">hav'nt</sic> time to review-
</closer>

	</div1>
</body>

	</text>
</TEI.2>
