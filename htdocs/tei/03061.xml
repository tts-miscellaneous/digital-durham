<?xml version="1.0" encoding="iso-8859-1"?>
<?xml-stylesheet href="./styles/tei-nc.xsl" type="text/xsl"?>
<!DOCTYPE TEI.2 PUBLIC "-//TEI//DTD TEI Lite XML ver. 1//EN" 
"./dtds/teixlite.dtd" [<!ENTITY tei "Text Encoding Initiative">
<!ENTITY plus   "&#x002B;"> <!ENTITY dollar "&#x0024;">
<!ENTITY nbsp   "&#x00A0;"><!ENTITY percnt "&#x0025;">
<!ENTITY half   "&#x00BD;">]>


<TEI.2>

	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title>Letter from James Haywood Southgate to Mattie Logan Southgate Jones, November 26, 1889: Electronic Edition.</title>
				<author>Southgate, James Haywood, 1859-1916</author>
				<author>Jones, Mattie Logan Southgate</author>

				<funder>Funding by the Institute of Museum of Library Services (IMLS) and the federal Library Services and Technology Act (LSTA), with support provided through North Carolina ECHO.</funder>
				<respStmt>
					<resp>Text transcribed by</resp>
					<name>Kelly Clark</name>
				</respStmt>	
				<respStmt>
					<resp>Images scanned by</resp>
					<name>Digital Production Center</name>
				</respStmt>
				<respStmt>
					<resp>Text encoded by</resp>
					<name>Katherine M. Wisser</name>
				</respStmt>
			</titleStmt>

			<editionStmt>
				<edition>First edition, 
					<date>2006</date>
				</edition>
			</editionStmt>

			<publicationStmt>
				<publisher>Duke University Libraries</publisher>
				<pubPlace>Durham, NC, USA</pubPlace>
				<date>2006</date>
				<availability><p>&#x00A9; This work is the property of the Duke University Libraries. It may be used freely by individuals for research, teaching, and personal use as long as this statement of availability is included in the text.</p></availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="title page">Letter from James Haywood Southgate to Mattie Logan Southgate Jones, November 26, 1889</title>
						<author>James Haywood Southgate</author>
					</titleStmt>
					
					<extent>1 p.</extent>
					<publicationStmt>
						<date>18891126</date>
					</publicationStmt>
					<notesStmt>
						<note>In this letter, from James Haywood Southgate to, his sister, Mattie Logan Southgate	Jones, James congratulates Mattie Southgate Jones on the birth of her son, Thomas Decatur Jones.  Her brother prays to God for her speedy recovery from the pains of childbirth.  He writes the letter on F. Fishbate Wholesale and Retail: Clothier, Hatter, and Furnisher's stationary, which was  located in Greensboro, North Carolina.</note> 
					</notesStmt>
					<sourceDesc>
						<p>James Southgate papers, 1794-1944 and undated, Rare Book, Manuscript and Special Collections Library, Duke University.</p>					
					</sourceDesc>

				</biblFull>
			</sourceDesc>
		</fileDesc>
		<encodingDesc>
			<projectDesc>
				<p>This electronic edition is part of the Duke University Libraries digitization project, <hi rend="italics">Digital Durham</hi>
				</p>
			</projectDesc>	

			<editorialDecl>
<p>The text has been encoded using the recommendations for Level 4 of the TEI.</p>
				
				<p>Original, grammar, punctuation and spelling have been preserved. Encountered typographical errors have been preserved with corrections provided. In some cases, we've put sic in square brackets where we've identified misspellings or lack of punctuation but do not require corrections in order to understand the text. Where older spellings occurred, the original spelling is preserved and a modern equivalent of the word is provided.</p>
				
				<p>Any hyphens occurring in line breaks have been removed, and the trailing part of a word has been joined to the preceding line.</p>
				
				<p>All dollar signs, plus signs, and percentage signs, fractions, and ampersands have been transcribed as entity references.</p>
				
				<p>Blank spaces have been indicated with 3 &nbsp; characters where they are necessary because of lack of punctuation in the original text.</p>
				
				<p>In the case of writers who put ill-defined ink marks in places where commas or periods should exist, we take the liberty to transcribe them as either commas, periods, or semicolons to make the meaning of the text more clear.</p>
				
				<p>In the case where words or parts of words cannot be discerned, a gap, represented by a series of underscores, has been included where the work or part of the word appear in the text.</p>
				
				<p>Indentation has not been preserved. Paragraph breaks were included only when it was obvious.</p>
				
			</editorialDecl>

			<classDecl>
				<taxonomy id="LCSH">
					<bibl>
						<title>Library of Congress Subject Headings</title>
					</bibl>
				</taxonomy>
			</classDecl>
		</encodingDesc>

		<profileDesc>
			<langUsage>
				<language id="eng">English</language>
			</langUsage>

			<textClass>
				<keywords scheme="LCSH">
					<list>
<item>Childbirth -- Southern states -- 19th century</item>
<item>Durham (N.C.) -- History</item>
<item>Communication in the family -- Southern states -- History -- 19th century</item>
<item>F. Fishbate Wholesale and Retail</item>
<item>Religion -- North Carolina -- 19th century</item>
<item>Families, White -- Social life and customs -- North Carolina -- Durham -- 19th century</item>
<item>Diseases -- North Carolina -- History -- 19th century</item>
<item>Durham (N.C.)</item>
<item>Greensboro (N.C.)</item>
<item>Letters (correspondence)</item>

					</list>
				</keywords>

			</textClass>
		</profileDesc>

	<!--		<revisionDesc>
			<change>
				<date></date>
				<respStmt>
					<name></name>
					<resp></resp>
				</respStmt>
				<item></item>
			</change>
		</revisionDesc> -->
	</teiHeader>

	<text>
<body>

<div1 type="letter">
<pb id="p1" n="1"/>
<opener>
<dateline>Office of<lb/>
F. Fishblate,<lb/>
Wholesale and Retail<lb/>
Clothier, Hatter and Furnisher.<lb/>
C. M. Vansdory, Manager.<lb/>
P. O. Lock Box 194.<lb/>
<name type="place">Greensboro, N.C., </name>
<date>26<hi rend="super">th</hi> Nov 1889</date>
</dateline>
<salute>My dear Matt: </salute>
</opener>

<p>Have just learned thro Doctor Yates that God has given you another fine boy and that you are getting along nicely.</p>
	<p>I congratulate you from my heart and pray you may soon be entirely well and that God's favor may rest on you and this blessed little boy-heavens [sic] own gift-forever-

</p>
<closer>
<salute>Your aff Bro',</salute>
<signed>J. H. Southgate.</signed>
</closer>

	</div1>
</body>

	</text>
</TEI.2>
