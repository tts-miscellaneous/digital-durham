<?xml version="1.0" encoding="iso-8859-1"?>
<?xml-stylesheet href="./styles/tei-nc.xsl" type="text/xsl"?>
<!DOCTYPE TEI.2 PUBLIC "-//TEI//DTD TEI Lite XML ver. 1//EN" 
"./dtds/teixlite.dtd" [<!ENTITY tei "Text Encoding Initiative">
<!ENTITY plus   "&#x002B;"> <!ENTITY dollar "&#x0024;">
<!ENTITY nbsp   "&#x00A0;"><!ENTITY percnt "&#x0025;">
<!ENTITY half   "&#x00BD;"><!ENTITY frac14 "&#x00BC;">]>


<TEI.2>

	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title>Letter from James Southgate to Delia Haywood Southgate, Annie Moore Southgate, Mattie Logan Southgate, James Haywood Southgate, and Lessie Muse Southgate, September 23, 1883: Electronic Edition.</title>
				<author>Southgate, James, 1832-1914</author>
				<author>Jones, Mattie Logan Southgate</author>
				<author>Southgate, Delia Haywood Wynne</author>
				<author>Southgate, James Haywood, 1859-1916</author>
				<author>Southgate, Annie</author>
				<author>Simmons, Celestia Muse Southgate</author>
				<funder>Funding by the Institute of Museum of Library Services (IMLS) and the federal Library Services and Technology Act (LSTA), with support provided through North Carolina ECHO.</funder>
				<respStmt>
					<resp>Text transcribed by</resp>
					<name>Kelly Clark</name>
				</respStmt>	
				<respStmt>
					<resp>Images scanned by</resp>
					<name>Digital Production Center</name>
				</respStmt>
				<respStmt>
					<resp>Text encoded by</resp>
					<name>Katherine M. Wisser</name>
				</respStmt>
			</titleStmt>

			<editionStmt>
				<edition>First edition, 
					<date>2006</date>
				</edition>
			</editionStmt>

			<publicationStmt>
				<publisher>Duke University Libraries</publisher>
				<pubPlace>Durham, NC, USA</pubPlace>
				<date>2006</date>
				<availability><p>&#x00A9; This work is the property of the Duke University Libraries. It may be used freely by individuals for research, teaching, and personal use as long as this statement of availability is included in the text.</p></availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="title page">Letter from James Southgate to Delia Haywood Southgate, Annie Moore Southgate, Mattie Logan Southgate, James Haywood Southgate, and Lessie Muse Southgate, September 23, 1883</title>
						<author>James Southgate</author>
					</titleStmt>
					
					<extent>4 p.</extent>
					<publicationStmt>
						<date>18830923</date>
					</publicationStmt>
					<notesStmt>
						<note>James Southgate writes his wife, children and sister, about the inclement weather in Richmond and the medical treatment he receives there. He remarks about his weight loss and relates the different theories that his physicians, his sister Sarah Raboteau and his cousin Augusta Christian have about treating "feeble digestion" or "Dyspepsia."   He shares details of his medical regimen and the effects of the medications on his mouth. While watching church goers in the rainy streets of Richmond, James ruminates on the weather's effect on religious practice.  James expresses his wish to  visit his relations in eastern Virginia with his  physician's approval. He responds to news from his family about Grandma Zack and Ed Sym and notes that he dined on oysters.</note> 
					</notesStmt>
					<sourceDesc>
						<p>James Southgate papers, 1794-1944 and undated, Rare Book, Manuscript and Special Collections Library, Duke University.</p>					
					</sourceDesc>

				</biblFull>
			</sourceDesc>
		</fileDesc>
		<encodingDesc>
			<projectDesc>
				<p>This electronic edition is part of the Duke University Libraries digitization project, <hi rend="italics">Digital Durham</hi>
				</p>
			</projectDesc>	

			<editorialDecl>
<p>The text has been encoded using the recommendations for Level 4 of the TEI.</p>
				
				<p>Original, grammar, punctuation and spelling have been preserved. Encountered typographical errors have been preserved with corrections provided. In some cases, we've put sic in square brackets where we've identified misspellings or lack of punctuation but do not require corrections in order to understand the text. Where older spellings occurred, the original spelling is preserved and a modern equivalent of the word is provided.</p>
				
				<p>Any hyphens occurring in line breaks have been removed, and the trailing part of a word has been joined to the preceding line.</p>
				
				<p>All dollar signs, plus signs, and percentage signs, fractions, and ampersands have been transcribed as entity references.</p>
				
				<p>Blank spaces have been indicated with 3 &nbsp; characters where they are necessary because of lack of punctuation in the original text.</p>
				
				<p>In the case of writers who put ill-defined ink marks in places where commas or periods should exist, we take the liberty to transcribe them as either commas, periods, or semicolons to make the meaning of the text more clear.</p>
				
				<p>In the case where words or parts of words cannot be discerned, a gap, represented by a series of underscores, has been included where the work or part of the word appear in the text.</p>
				
				<p>Indentation has not been preserved. Paragraph breaks were included only when it was obvious.</p>
				
			</editorialDecl>

			<classDecl>
				<taxonomy id="LCSH">
					<bibl>
						<title>Library of Congress Subject Headings</title>
					</bibl>
				</taxonomy>
			</classDecl>
		</encodingDesc>

		<profileDesc>
			<langUsage>
				<language id="eng">English</language>
			</langUsage>

			<textClass>
				<keywords scheme="LCSH">
					<list>
<item>Durham (N.C.) -- History</item>
<item>Diet -- Virginia -- History -- 19th century</item>
<item>Diseases -- North Carolina -- History -- 19th century</item>
<item>Christian, Augusta</item>
<item>Religion -- North Carolina -- 19th century</item>
<item>Communication in the family -- Southern states -- History -- 19th century</item>
<item>Medical care -- United States -- History -- 19th century</item>
<item>Raboteau, Sarah Ann Wynne, 1816-1895</item>
<item>Family -- North Carolina -- History</item>
<item>Southgate, James, 1832-1914</item>
<item>Medicine -- Formulae, receipts, prescriptions -- History -- 19th century</item>
<item>Southgate, Delia Haywood Wynne</item>
<item>Southgate, Annie</item>
<item>Jones, Mattie Logan Southgate</item>
<item>Southgate, James Haywood, 1859-1916</item>
<item>Sym, Ed</item>
<item>Grandma Zack</item>
<item>Asheville (N.C.)</item>
<item>Durham (N.C.)</item>
<item>Richmond (Va.)</item>
<item>Wilmington (N.C.)</item>
<item>Letters (correspondence)</item>
					</list>
				</keywords>

			</textClass>
		</profileDesc>

	<!--		<revisionDesc>
			<change>
				<date></date>
				<respStmt>
					<name></name>
					<resp></resp>
				</respStmt>
				<item></item>
			</change>
		</revisionDesc> -->
	</teiHeader>

	<text>
<body>

<div1 type="letter">
<pb id="p1" n="1"/>
<opener>
<dateline>
<name type="place">Richmond Va </name>
<date>Sept 23rd 1883</date>
</dateline>
<salute>To the Household</salute>
</opener>

<p>I have caught up with all my correspondents at home and to avoid doubling up and giving  some cause to complain I will load my gun so as to make a "scatteration" shot and address myself to the entire family, which I imagine is seated around a cozy fire this damp rainy cool Sabbath morning. The equinox is giving us a full benefit this year &plus; I awake to find the wind blowing, the rain pouring, and thought that all my arrangements for the day would be interrupted. And so it is. I must remain in my room a prisoner and converse with the loved ones far away who may even at this moment be thinking or talking of papa and wishing he were at home. You cannot wish me there any more than I do to be there. Each day now hangs heavily with me and I long to be out at work again, and in the possession of health, vigor and comfort. <pb id="p2" n="2"/>I am congratulating myself that I have been permitted to pass a whole week without the slightest pain or uneasiness of any kind which pertains to my late sickness. I do so hope I may never be called upon to pass through such ordeals again in this life. I am perfectly ravenous for something to eat. I feel sometimes, when I smell the Bread baking or the dinner cooking that I must break all bounds, rush to the kitchen, <sic corr="besiege">beseige</sic> the cook and grab the <sic corr="first">frist</sic> thing I can put my hands on. I long for the time to come when my now poor and feeble stomach will be able to digest proper food. I am amazed frequently to see how much I have lost in flesh, by reason of feeble digestion. Cousin Augusta Christian told me that the great D<hi rend="super">r</hi>  Smith of Baltimore holds the theory that starving will not do for Dyspepsia, that the stomach will finally become so irritable for want of nourishment that it revolts at the slightest and most delicate food-and that the true theory is to let the stomach have food and gain something which will assist it to digest. That is Sallie Raboteau's doctrine-at least the first part of it is-"Eat a heap if colics do come- It isn't what one eats- It is the change in the weather or it is something else- One of these nice rolls or muffins or a slice of this cake never hurt any body in the world- I never heard of such pudding as this hurting any one in my life." It may be that she is right- I certainly am inclined to that doctrine about this time and so would any other starving man. I take all that is allowed me, and shall tackle the Doctor today for a [s__a]. I think I can digest one now, and am almost brave enough to try a beef steak. However I desire to be prudent and very cautious, will crawl along and suffer the pangs of hunger yet awhile longer as I have for several month, never eating &frac14; what I really wanted. I am truly thankful that I have been brought this far towards recovery and shall do nothing to impede my progress onward towards the goal of perfect restoration to health if it be God's will that I should ever be well again. I am resting in hope &plus; if I am permitted to regain my former health + strength I will try to do more than ever in my masters vineyards. <pb id="p3" n="3"/> I have thus far been an improfitable servant and have not come up to the full measure of my duties and responsibilities and feel unworthy even of notice but I rest upon the all powerful plane of redemption which is able to succor all the fallen sons of Adam without exception, if they do their duty. I am trying to hold a conversation with all of you Today but but I find myself moralizing and coming back to myself all this while. You must excuse this for I flatter myself that when  you get my letters you want to hear about me-my condition physically and mentally. I notice a few persons in the street today wending their way to their several places of worship, but I judge the congregations will be small in all the churches and yet it should not be so- For if this were Monday, more would got to their places of business all the same. We as a people do not esteem church as we should. It is a great privilege to be allowed to worship God in His sanctuary on His Holy day. I wrote several letters to friends in different parts of the country and among others, one to Bob Spencer, who is merchandizing at West Point about 2 hours run by rail from this city, and a little boat runs up the river to Clifton in &half; hour from West Point. Just to think I am in 2 &half;  hours travel of nearly all the relatives I have left in the world &plus; cant get to them. I have a notion when I get a little stronger to ask the Dr if I may run down and remain 2 days if no more-  I can visit Bob- Aunt Maria and Dr Bland all in 2 days &plus; the entire trip will not cost over &dollar;5- The Dr didnt [sic] get to see me yesterday at all, but I certainly look for him Today, and if he comes before I mail this letter I will tell you what he says about it. I must get so I can eat something though before I talk about going abroad- I was much amused at Mrs Fleishman- She thought German like, that it would be very impolite while in her house unless she should ask me to eat something- She went over a long bill of fare and even put in lager-beer &plus; insisted that I could take that simple drink- Of course I had to refuse all &plus; so it would be any where- I know I am impatient, but pray every day that I may be more patient &plus; wait. <pb id="p4" n="4"/> I could for one dollar take a boat here on some Saturday run down to Norfolk, spend Sunday with Tom and return here Monday, have a delightful trip down James River and eat Sorat oysters all the route ("root")     I have nothing else to do but build air castles like Mattie &plus; Kate Sanbom used to do, and there is no harm in having these pleasant day dreams should they never be realized- It is not so very nice to be shut up in a room to one's self where he is isolated almost as though he were on an island like Robinson Crusoe. I do not know a single patient in the house. I see the Dr of the house about twice a day &plus; then it is only to ask the same question "How do you feel today"    Is your medicine out yet? &plus; and such gab as that- Very few friends call now because they have seen me on the street once or twice &plus; conclude I am well enough to take care of myself &plus; so I am- Any how, that leaves me monarch of all I survey in my own room. My tongue is sore yet though I use carbolic mouth wash and chlorate of Potash through the day and a portion of the night. My gums are better though not entirely recovered from the salivation- This sore mouth really keeps me back as the Dr seems to be waiting to give me other remedies, but cannot risk them while my mouth is in this condition. I received a very pleasant letter yesterday from Mr White- He sympathizes with me, admonishes me to be prudent &plus;c &plus;c- Wonder if he [dont?] think I have as much interest in being prudent as any one else can possibly. Munson of Wilmington also has written me a very nice letter and desired to be remembered to all of you when I wrote, and I now send his greetings. I am surprised to hear that Grandma Zack has at last succeeded in taking the old man Zack with her to Wilmington. That is quite a Triumph for the old lady. I think this is his first trip with her to that place since they have been married. I have not received an answer to the letter I wrote her + presume she had left town before it reached her. I hope Ed Sym continues to improve. He has trouble exactly opposite to mine + requires different treatment. <pb id="p1a" n="1a"/>I have written up my sheet and am about to cross this page but I know Old oman will object because it is hard enough for her to read straight lines plainly written since her eyesight is so bad- but postage stamps are a consideration and she must make some of the younger ones read this for her- I can see Sallie Raboteau with [mop? moss?] in mouth trudging along through one of [her] brother's letters and wishing all the time he would write a larger and plainer hand. If all the rest fail to make it out hand once to Jimmie who says he can read every word I write. Tell him "Cris" my body guard has left- He went off Thursday night promising to be back next morning, but he has not put in an appearance since. Dr Wheat has hired a white man to take his place- He is a German and says he has had much experience in army hospitals. He is pretty attentive but you cant do with him exactly as we did with "Cris." guess Cris is off to get ready for the opening of the session at the Medical College 1st October. I put up my pen at this point awaiting the coming of the <abbr expan="Doctor">D<hi rend="super">r</hi></abbr>, but he has not yet arrived, so I will finish up this and when the boy goes for mail at 4 oclock he can take this &plus; be sure of its going out tonight as Sunday, its mails are uncertain. Mr Jno D Gordon the Insurance man come in at &frac14; past 1 just I had whetted my appetite for oysters and stayed till &frac14; past 3- He was'nt out of sight before 7 good fats ones were on the way down my throat- I enjoyed them as I always do good ones. I am on my second box of Granum and hope by [the] time I finish that I will be able to take more substantial diet. I will write fully tomorrow what the Dr has to say when he comes today. I think you cant [sic] complain that I have not written enough lately. My letters are too long for any one- Especially as they amount to so little after all. I shall expect an answer to this from every member of the household even enchanting Kate- I can only subscribe myself</p>
<closer>
<signed>"papa"</signed>
</closer>

	</div1>
</body>

	</text>
</TEI.2>
