<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" indent="yes"/>
	<xsl:strip-space elements="*"/>

<xsl:template match="TEI.2">
	<html>
		<head>
			<style type="text/css">
				h1 {color:red}
				h2 {color:blue}
				body {background-image: 
url('NCEchologo0200.jpg');
background-repeat: 
no-repeat;
background-position: 
 center; 
}

			</style>


		</head>
		<body>
			<xsl:apply-templates select="teiHeader"/>
			<xsl:call-template name="contents"/>
			<xsl:apply-templates select="*[not(self::teiHeader)]"/>
		</body>
	</html>
</xsl:template>


<xsl:template name="contents">
<h2>
	<xsl:text>Letter</xsl:text>
</h2>

<xsl:for-each select="div1[@type]">
	<xsl:apply-templates select="head" mode="toc"/>
</xsl:for-each>
</xsl:template>

<xsl:template match="text/body/div1[@type]" mode="toc">
	<b>
		<a href="#{generate-id()}">
			<p style="text-indent:25pt">
				<xsl:apply-templates/>
			</p>
		</a>
	</b>
</xsl:template>

<xsl:template match="teiHeader">
	<h1 style="text-align:center">
	<xsl:apply-templates select="titleStmt/title"/>
	</h1>
	<h2 style="text-align:center">
		<xsl:apply-templates select="titleStmt/author"/>
	</h2>
</xsl:template>

<xsl:template match="*/lb">
<xsl:element name="br">
	<xsl:apply-templates/>
</xsl:element>
</xsl:template>

<xsl:template match="p">
<xsl:element name="p">
	<xsl:apply-templates/>
</xsl:element>
</xsl:template>

<xsl:template match="salute">
	<p>
		<xsl:apply-templates/>
	</p>
</xsl:template>

<xsl:template match="sic">
<em style="text_color:red">
		<xsl:apply-templates/>

</em>

</xsl:template>

<xsl:template match="table">
<xsl:element name="table">
<xsl:apply-templates/>
</xsl:element>
</xsl:template>

<xsl:template match="row">
<xsl:element name="tr">
<xsl:apply-templates/>
</xsl:element>
</xsl:template>

<xsl:template match="cell">
<xsl:element name="td">
<xsl:apply-templates/>
</xsl:element>
</xsl:template>



<xsl:template match="sic">
<xsl:text>[</xsl:text>
<xsl:element name="font">
	<xsl:attribute name="color"><xsl:text>#FF0000</xsl:text></xsl:attribute>
	<xsl:apply-templates select="@corr"/>
</xsl:element>
<xsl:text>] </xsl:text>
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="gap">
<xsl:text>_____</xsl:text>
<xsl:apply-templates/>
</xsl:template>

<xsl:template match="hi[@rend=underline]">
<xsl:element name="u">
<xsl:apply-templates/>
</xsl:element>
</xsl:template>

<xsl:template match="emph[@rend=super]">
<xsl:element name="sup">
<xsl:apply-templates/>
</xsl:element>
</xsl:template>

<xsl:template match="pb">
<xsl:element name="hr">
<xsl:apply-templates/>
</xsl:element>
</xsl:template>


</xsl:stylesheet>










