<!-- Formal Public Identifier:
"-//IATH//DTD iathchar.ent (IATH Special Characters)//EN"
-->

<!-- ========================================================== -->
<!--  NAME:      IATH SPECIAL CHARACTERS                        -->
<!--                                      (File = iathchar.ent) -->
<!--                                      (Version = 0.0)       -->
<!--                                      (Vers Date 20030303)  -->
<!--                                                            -->
<!--  PURPOSE:   To call in ISO special character sets for use  -->
<!--             in IATH documents.                             -->
<!--                                                            -->
<!--                                                            -->
<!--  CONTACT:   Daniel Pitti. Institute for Advanced Technology-->
<!--             in the Humanites, University of Virginia.      -->
<!--  WRITTEN:   Daniel V. Pitti                                -->
<!--             IATH                                           -->
<!--             DATE: 1997-05                                  -->
<!--             BASED ON EADCHARS.ENT                          -->
<!-- ********************************************************** -->

<!-- ********************************************************** -->
<!--                     Changes                                -->
<!-- Added TEI Classical Greek set dvp 980211                   -->
<!-- Added Tibet Special Characters dvp 980223                  -->


<!-- ********************************************************** -->
<!--                  SPECIAL CHARACTER ISO ENTITY SETS         -->
<!-- ********************************************************** -->

<![%sgml;[
<!ENTITY % xml
  'IGNORE'
                                                                             >
<!ENTITY % isolat1 PUBLIC 
  'ISO 8879:1986//ENTITIES Added Latin 1//EN' 
  'isolat1.gml' 
                                                                             >
<!ENTITY % isolat2 PUBLIC 
  'ISO 8879:1986//ENTITIES Added Latin 2//EN'  
  'isolat2.gml'  
                                                                             >
<!ENTITY % isonum PUBLIC
  'ISO 8879:1986//ENTITIES Numeric and Special Graphic//EN'
  'isonum.gml'
                                                                             >
<!ENTITY % isopub PUBLIC
  'ISO 8879:1986//ENTITIES Publishing//EN'
  'isopub.gml'
                                                                             >
<!ENTITY % isotech PUBLIC
  'ISO 8879:1986//ENTITIES General Technical//EN'
  'isotech.gml'
                                                                             >
<!ENTITY % isodia PUBLIC
  'ISO 8879:1986//ENTITIES Diacritical Marks//EN'
  'isodia.gml'
                                                                             >
<!ENTITY % isocyr1 PUBLIC
  'ISO 8879:1986//ENTITIES Russian Cyrillic//EN'
  'isocyr1.gml'
                                                                             >
<!ENTITY % isocyr2 PUBLIC
  'ISO 8879:1986//ENTITIES Non-Russian Cyrillic//EN'
  'isocyr2.gml'
                                                                             >
<!ENTITY % isogrk1 PUBLIC
  'ISO 8879:1986//ENTITIES Greek Letters//EN'
  'isogrk1.gml'
                                                                             >
<!ENTITY % isogrk2 PUBLIC
  'ISO 8879:1986//ENTITIES Monotoniko Greek//EN'
  'isogrk2.gml'
                                                                             >
<!ENTITY % isogrk3 PUBLIC
  'ISO 8879:1986//ENTITIES Greek Symbols//EN'
  'isogrk3.gml'
                                                                             >
<!ENTITY % isogrk4 PUBLIC
  'ISO 8879:1986//ENTITIES Alternative Greek Symbols//EN'
   'isogrk4.gml'
                                                                             >
<!ENTITY % teigrk PUBLIC
       "-//TEI TR1 W4:1992//ENTITIES Extra Classial Greek Letters//EN">

<!ENTITY % tibetchar PUBLIC 
       "-//University of Virginia::Institute for Advanced Technology in
the Humanities//ENTITIES (Tibetan Special Characters)//EN" >
<!--
    Character Entity References
      By default, all declared sets are referenced. Sets that are not
      needed can be removed our "commented"
                                                                           -->
%isolat1; %isolat2;

%isonum; %isopub; %isotech; %isodia;

%isocyr1; %isocyr2;

%isogrk1; %isogrk2; %isogrk3; %isogrk4;
                                                                           ]]>
<!--=======================================================================-->
<!--
    E. CONDITIONAL SECTION: XML Character Entities

    Entity name: xmlchar
    Default: IGNORE
                                                                           -->
<!--=======================================================================-->
<!ENTITY % xml
  'INCLUDE'
                                                                            >
<![%xml;[
<!ENTITY % isolat1 PUBLIC
  'ISO 8879:1986//ENTITIES Added Latin 1//EN//XML'
  'file:/c:\eac\shared\char_ent\iso-lat1.ent'
                                                                             >
<!ENTITY % isolat2 PUBLIC
  'ISO 8879:1986//ENTITIES Added Latin 2//EN//XML'
  'file:/c:\eac\shared\char_ent\iso-lat2.ent'
                                                                             >
<!ENTITY % isonum PUBLIC
  'ISO 8879:1986//ENTITIES Numeric and Special Graphic//EN//XML'
  'file:/c:\eac\shared\char_ent\iso-num.ent'
                                                                             >
<!ENTITY % isopub PUBLIC
  'ISO 8879:1986//ENTITIES Publishing//EN//XML'
  'file:/c:\eac\shared\char_ent\iso-pub.ent'
                                                                             >
<!ENTITY % isotech PUBLIC
  'ISO 8879:1986//ENTITIES General Technical//EN//XML'
  'file:/c:\eac\shared\char_ent\iso-tech.ent'
                                                                             >
<!ENTITY % isodia PUBLIC
  'ISO 8879:1986//ENTITIES Diacritical Marks//EN//XML'
  'file:/c:\eac\shared\char_ent\iso-dia.ent'
                                                                             >
<!ENTITY % isocyr1 PUBLIC
  'ISO 8879:1986//ENTITIES Russian Cyrillic//EN//XML'
  'file:/c:\eac\shared\char_ent\iso-cyr1.ent'
                                                                             >
<!ENTITY % isocyr2 PUBLIC
  'ISO 8879:1986//ENTITIES Non-Russian Cyrillic//EN//XML'
  'file:/c:\eac\shared\char_ent\iso-cyr2.ent'
                                                                             >
<!ENTITY % isogrk1 PUBLIC
  'ISO 8879:1986//ENTITIES Greek Letters//EN//XML'
  'file:/c:\eac\shared\char_ent\iso-grk1.ent'
                                                                             >
<!ENTITY % isogrk2 PUBLIC
  'ISO 8879:1986//ENTITIES Monotoniko Greek//EN//XML'
  'file:/c:\eac\shared\char_ent\iso-grk2.ent'
                                                                             >
<!ENTITY % isogrk3 PUBLIC
  'ISO 8879:1986//ENTITIES Greek Symbols//EN//XML'
  'file:/c:\eac\shared\char_ent\iso-grk3.ent'
                                                                             >
<!ENTITY % isogrk4 PUBLIC
  'ISO 8879:1986//ENTITIES Alternative Greek Symbols//EN//XML'
  'file:/c:\eac\shared\char_ent\iso-grk4.ent'
                                                                             >
<!--
    Character Entity References
      By default, all declared sets are referenced. Sets that are not
      needed can be removed our "commented"
                                                                           -->
%isolat1; %isolat2;

%isonum; %isopub; %isotech; %isodia;

%isocyr1; %isocyr2;

%isogrk1; %isogrk2; %isogrk3; %isogrk4;
                                                                           ]]>

                                                               
<!-- =============  END OF IATHCHAR DTD FRAGMENT ============== -->
