<?xml version="1.0" encoding="iso-8859-1"?>
<?xml-stylesheet href="./styles/tei-nc.xsl" type="text/xsl"?>
<!DOCTYPE TEI.2 PUBLIC "-//TEI//DTD TEI Lite XML ver. 1//EN" 
"./dtds/teixlite.dtd" [<!ENTITY tei "Text Encoding Initiative">
<!ENTITY plus   "&#x002B;"> <!ENTITY dollar "&#x0024;">
<!ENTITY nbsp   "&#x00A0;"><!ENTITY percnt "&#x0025;">
<!ENTITY half   "&#x00BD;">]>


<TEI.2>

	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title>Letter from James Southgate to James Haywood Southgate, September 1, 1887: Electronic Edition.</title>
				<author>Southgate, James, 1832-1914</author>
				<author>Southgate, James Haywood, 1859-1916</author>

				<funder>Funding by the Institute of Museum of Library Services (IMLS) and the federal Library Services and Technology Act (LSTA), with support provided through North Carolina ECHO.</funder>
				<respStmt>
					<resp>Text transcribed by</resp>
					<name>Kelly Clark</name>
				</respStmt>	
				<respStmt>
					<resp>Images scanned by</resp>
					<name>Digital Production Center</name>
				</respStmt>
				<respStmt>
					<resp>Text encoded by</resp>
					<name>Katherine M. Wisser</name>
				</respStmt>
			</titleStmt>

			<editionStmt>
				<edition>First edition, 
					<date>2006</date>
				</edition>
			</editionStmt>

			<publicationStmt>
				<publisher>Duke University Libraries</publisher>
				<pubPlace>Durham, NC, USA</pubPlace>
				<date>2006</date>
				<availability><p>&#x00A9; This work is the property of the Duke University Libraries. It may be used freely by individuals for research, teaching, and personal use as long as this statement of availability is included in the text.</p></availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="title page">Letter from James Southgate to James Haywood Southgate, September 1, 1887</title>
						<author>James Southgate</author>
					</titleStmt>
					
					<extent>4 p.</extent>
					<publicationStmt>
						<date>18870901</date>
					</publicationStmt>
					<notesStmt>
						<note>In this letter to his son James H. Southgate, James Southgate discusses his medical condition diagnosed by Dr. A.G. Carr, Dr. Bartholow, and Dr. Frank D. Cunningham. James Southgate worries about Eugene Morehead's health and the future of the Durham bank should Morehead die.  Southgate describes his diet at his hotel in Saratoga Springs and gives a report on a prayer meeting held by Reverend Dr. Simpson of t he New York Tabernacle. He anticipates his journey home and wonders if he will, upon his return, feel like Rip Van Winkle.</note> 
					</notesStmt>
					<sourceDesc>
						<p>James Southgate papers, 1794-1944 and undated, Rare Book, Manuscript and Special Collections Library, Duke University.</p>					
					</sourceDesc>

				</biblFull>
			</sourceDesc>
		</fileDesc>
		<encodingDesc>
			<projectDesc>
				<p>This electronic edition is part of the Duke University Libraries digitization project, <hi rend="italics">Digital Durham</hi>
				</p>
			</projectDesc>	

			<editorialDecl>
<p>The text has been encoded using the recommendations for Level 4 of the TEI.</p>
				
				<p>Original, grammar, punctuation and spelling have been preserved. Encountered typographical errors have been preserved with corrections provided. In some cases, we've put sic in square brackets where we've identified misspellings or lack of punctuation but do not require corrections in order to understand the text. Where older spellings occurred, the original spelling is preserved and a modern equivalent of the word is provided.</p>
				
				<p>Any hyphens occurring in line breaks have been removed, and the trailing part of a word has been joined to the preceding line.</p>
				
				<p>All dollar signs, plus signs, and percentage signs, fractions, and ampersands have been transcribed as entity references.</p>
				
				<p>Blank spaces have been indicated with 3 &nbsp; characters where they are necessary because of lack of punctuation in the original text.</p>
				
				<p>In the case of writers who put ill-defined ink marks in places where commas or periods should exist, we take the liberty to transcribe them as either commas, periods, or semicolons to make the meaning of the text more clear.</p>
				
				<p>In the case where words or parts of words cannot be discerned, a gap, represented by a series of underscores, has been included where the work or part of the word appear in the text.</p>
				
				<p>Indentation has not been preserved. Paragraph breaks were included only when it was obvious.</p>
				
			</editorialDecl>

			<classDecl>
				<taxonomy id="LCSH">
					<bibl>
						<title>Library of Congress Subject Headings</title>
					</bibl>
				</taxonomy>
			</classDecl>
		</encodingDesc>

		<profileDesc>
			<langUsage>
				<language id="eng">English</language>
			</langUsage>

			<textClass>
				<keywords scheme="LCSH">
					<list>


<item>Cunningham, Frank D.</item>
<item>Morehead, Eugene Lindsay, 1845-1889</item>
<item>Banks and banking -- North Carolina -- Durham -- 19th century</item>
<item>Carr, A. G.</item>
<item>Bartholow, Dr.</item>
<item>Diseases -- North Carolina -- History -- 19th century</item>
<item>Durham (N.C.) -- History</item>
<item>Communication in the family -- Southern states -- History -- 19th century</item>
<item>Family -- Social life and customs -- Southern states</item>
<item>Medical care -- United States -- History -- 19th century</item>
<item>Religion -- North Carolina -- 19th century</item>
<item>Durham (N.C.)</item>
<item>Richmond (Va.)</item>
<item>Saratoga Springs (N.Y.)</item>
<item>Letters (correspondence)</item>
					</list>
				</keywords>

			</textClass>
		</profileDesc>

	<!--		<revisionDesc>
			<change>
				<date></date>
				<respStmt>
					<name></name>
					<resp></resp>
				</respStmt>
				<item></item>
			</change>
		</revisionDesc> -->
	</teiHeader>

	<text>
<body>

<div1 type="letter">
<pb id="p1" n="1"/>
<opener>
<dateline>"The American,"<lb/>
Geo. A. Farnham, Owner and Prop.<lb/>
<name type="place">Saratoga Springs, N. Y. </name>
<date>Sept 1 1887</date>
</dateline>
<salute>Dear Jimmie</salute>
</opener>

<p>Your letter of Aug 30 Enclosing D<hi rend="super">r</hi> Bartholows has been rec<hi rend="super">d</hi>. I do not agree with him as to the cause of the existence of Albumen. I had not had a kidney attack for several months previous to those liver or stomach attacks which made their appearance early in May. Dr Carr first found Albumen in after a series of those stomach troubles, and in my judgment they and not the gravel are the cause of the Albumen. I think moreover the D<hi rend="super">r</hi> is wrong in his diagnosis of the stomach pains. I think they come from weak digestion &plus; low nervous circulation &plus; inability of the stomach to grapple with food put <pb id="p2" n="2"/>in it &plus; I am more disposed to Dr Cunningham's than B's theory. I have had most excellent results from the use of the Hamilton spring water. The kidneys have resumed their wanted activity and the results are very satisfactory. I have discontinued the use of this water, it having accomplished its mission and gone back to the Hathorns?. My strength is gradually coming back &plus; the pain in the loins has subsided so that I can walk a short distance at a time &plus; thereby take some exercise. I went to the Depot at 8 30 a. m to see Maj[or] Tucker &plus; his family off. They remain in N. York city a week before going to Raleigh. Col[onel] Bridges &plus; Mr Alexander are the only men left from N.C. besides myself and we manage to meet once each day anyhow. I went out to the 10 A.M. prayer <pb id="p3" n="3"/>meeting and heard Rev <abbr expan="Doctor">D<hi rend="super">r</hi></abbr> Simpson of the N.Y. Tabernacle Madison Ave &plus; 42nd Street- He is a very forcible speaker     I will try to hear him again this p. m &plus; tonight as he preaches in town at both those hours. My overcoat came today &plus; I will be able to go out at night. The weather here is just charming. This has been like Indian summer the thermometer varying from 65 to 70. I am enjoying it in full measure. I am grieved to hear of Mr Moreheads continued ill-health and fear he will not survive the winter. If he shd die  Mrs M[orehead] may leave Durham &plus; in that event I shd think her interest in the Bank would be taken away, but of this I may be mistaken. <pb id="p4" n="4"/>I am sorry to see by the paper the failure of J. B. Warren &plus; Co- What did you ever do about renewing the Mebane mill risk     I thought it ought not to be renewed unless put on a paying basis. I am counting the days and almost the hours when I shall start towards home- It seems to me I have been gone a year, banished to some far off country. I fear I shall have to be introduced anew to many I once knew. I feel after the style of old Rip Van Winkle- Since I left the street cars have been put on- The stores on the corner of Main &plus; Depot Str[eets] have been built- The Kramer &plus; Parrish Stores on the corner of Main &plus; Church built up &plus; many things been done which will be all new to me. I will rejoice to get back once more- Give best love to every body


</p>
<closer>
<salute>Your aff father</salute>
<signed>J. Southgate  </signed>
</closer>

	</div1>
</body>

	</text>
</TEI.2>
