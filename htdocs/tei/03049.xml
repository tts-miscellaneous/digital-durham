<?xml version="1.0" encoding="iso-8859-1"?>
<?xml-stylesheet href="./styles/tei-nc.xsl" type="text/xsl"?>
<!DOCTYPE TEI.2 PUBLIC "-//TEI//DTD TEI Lite XML ver. 1//EN" 
"./dtds/teixlite.dtd" [<!ENTITY tei "Text Encoding Initiative">
<!ENTITY plus   "&#x002B;"> <!ENTITY dollar "&#x0024;">
<!ENTITY nbsp   "&#x00A0;"><!ENTITY percnt "&#x0025;">
<!ENTITY half   "&#x00BD;">]>


<TEI.2>

	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title>Letter from Kate Christine Sanborn to James Southgate, July 17, 1884: Electronic Edition.</title>
				<author>Sanborn, Kate Christine</author>
				<author>Southgate, James, 1832-1914</author>

				<funder>Funding by the Institute of Museum of Library Services (IMLS) and the federal Library Services and Technology Act (LSTA), with support provided through North Carolina ECHO.</funder>
				<respStmt>
					<resp>Text transcribed by</resp>
					<name>Trudi Abel</name>
				</respStmt>	
				<respStmt>
					<resp>Images scanned by</resp>
					<name>Digital Production Center</name>
				</respStmt>
				<respStmt>
					<resp>Text encoded by</resp>
					<name>Katherine M. Wisser</name>
				</respStmt>
			</titleStmt>

			<editionStmt>
				<edition>First edition, 
					<date>2006</date>
				</edition>
			</editionStmt>

			<publicationStmt>
				<publisher>Duke University Libraries</publisher>
				<pubPlace>Durham, NC, USA</pubPlace>
				<date>2006</date>
				<availability><p>&#x00A9; This work is the property of the Duke University Libraries. It may be used freely by individuals for research, teaching, and personal use as long as this statement of availability is included in the text.</p></availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="title page">Letter from Kate Christine Sanborn to James Southgate, July 17, 1884</title>
						<author>Kate Christine Sanborn</author>
					</titleStmt>
					
					<extent>8 p.</extent>
					<publicationStmt>
						<date>18840717</date>
					</publicationStmt>
					<notesStmt>
						<note>Kate Christine Sanborn tells James Southgate, that she has heard rumors that his daughter Mattie is spending  time  with a buyer of bright leaf tobacco.  She wants Mattie's father to reveal the identity of this man who keeps her friend "in the parlor till 1 a.m. three nights a week." Kate comments on the nomination of Grover Cleveland and the placement of Thomas A. Hendricks on the Democratic ticket. She notes that the Durham papers have suggested that the younger James Southgate might be a suitable legislative candidate. She closes with a discussion of her unmarried status.</note> 
					</notesStmt>
					<sourceDesc>
						<p>James Southgate papers, 1794-1944 and undated, Rare Book, Manuscript and Special Collections Library, Duke University.</p>					
					</sourceDesc>

				</biblFull>
			</sourceDesc>
		</fileDesc>
		<encodingDesc>
			<projectDesc>
				<p>This electronic edition is part of the Duke University Libraries digitization project, <hi rend="italics">Digital Durham</hi>
				</p>
			</projectDesc>	

			<editorialDecl>
<p>The text has been encoded using the recommendations for Level 4 of the TEI.</p>

<p>Original, grammar, punctuation and spelling have been preserved. Encountered typographical errors have been preserved with corrections provided. In some cases, we've put sic in square brackets where we've identified misspellings or lack of punctuation but do not require corrections in order to understand the text. Where older spellings occurred, the original spelling is preserved and a modern equivalent of the word is provided.</p>

<p>Any hyphens occurring in line breaks have been removed, and the trailing part of a word has been joined to the preceding line.</p>

<p>All dollar signs, plus signs, and percentage signs, fractions, and ampersands have been transcribed as entity references.</p>

<p>Blank spaces have been indicated with 3 &nbsp; characters where they are necessary because of lack of punctuation in the original text.</p>

<p>In the case of writers who put ill-defined ink marks in places where commas or periods should exist, we take the liberty to transcribe them as either commas, periods, or semicolons to make the meaning of the text more clear.</p>

<p>In the case where words or parts of words cannot be discerned, a gap, represented by a series of underscores, has been included where the work or part of the word appear in the text.</p>

<p>Indentation has not been preserved. Paragraph breaks were included only when it was obvious.</p>
			</editorialDecl>

			<classDecl>
				<taxonomy id="LCSH">
					<bibl>
						<title>Library of Congress Subject Headings</title>
					</bibl>
				</taxonomy>
			</classDecl>
		</encodingDesc>

		<profileDesc>
			<langUsage>
				<language id="eng">English</language>
			</langUsage>

			<textClass>
				<keywords scheme="LCSH">
					<list>
<item>Tobacco industry -- North Carolina -- Durham -- 19th century</item>
<item>Gossip -- Southern states -- 19th century</item>
<item>Jones, Mattie Logan Southgate</item>
<item>Southgate, James, 1832-1914</item>
<item>Sanborn, Kate Christine</item>
<item>Courtship -- North Carolina -- 19th century</item>
<item>Cleveland, Grover, 1837-1908</item>
<item>Hendricks, Thomas A. (Thomas Andrews), 1819-1885</item>
<item>Southgate, James Haywood, 1859-1916</item>
<item>Durham (N.C.)</item>
<item>Letters (correspondence)</item>
					</list>
				</keywords>

			</textClass>
		</profileDesc>

	<!--		<revisionDesc>
			<change>
				<date></date>
				<respStmt>
					<name></name>
					<resp></resp>
				</respStmt>
				<item></item>
			</change>
		</revisionDesc> -->
	</teiHeader>

	<text>
<body>

<div1 type="letter">
<pb id="p1" n="1"/>
<opener>
<dateline>
<name type="place">Home</name>
<date>17-7-'84</date>
</dateline>
</opener>

<p>I beg leave to inquire Mr. Southgate, what  has become of your conscience[?] "A long, long time has indeed intervened since you have had a letter from Kate."  Whose fault? Should I have a written a second letter just because the first reached you too soon? You seem to have entertained some such expectation. 'tis well I did not know of it till now-you might have realized your gloomy foreboding &amp; received several.  Those dear little vines[?], if I could invest them with sufficient spirit of mischief-they would take you for <pb id="p2" n="2"/><sic corr="their">thier</sic> truths  th__ twist twine &amp; intertwine until you found yourself in extricable tangles compelled to cry to us in plaintive accents <hi rend="underline">peccavi</hi>.  See you are entirely too self-complacent, you would be more apt to get out of the way of those vines then audaciously threaten "we girls" with dire calamity if they refused you free pass.  I should require full fare every time.  In my absence you have much for gratulation.</p>

<p>I have not heard from Mattie in ever so long, there are rumors afloat that a buyer of bright tobacco keeps her in the parlor till 1 a.m. three nights each week.   I am anxious over this "business" would feel relieved to know more of him.  What's his name &amp; what are his pros<pb id="p3" n="3"/>pects of success with out "Mitz."  This state of "affairs" explains how her time has become so much engrossed by her b.t.b. (bright tobacco buyer!) that  not a wee thought of me could scrounge in.  I might write her a plaint 'gainst all these "goings on" without my knowledge, but will forgive her as she used her influence to induce you to send a very welcome &amp; charming epistle full of all the topics I love so well to have you discuss in your inimitable style.  The dearest <hi rend="underline">darlingest</hi>  flowers came &plus; were almost fresh so slightly were they withered on thier [sic] way to me.  Now do I know when they came from &amp; who sent them. No need of information on that point-I know the peculiar fragrance of Durham flowers. <pb id="p4" n="4"/>&amp; the great heart of her best citizen.  Thanks my kind friend.  I appreciate them more than I can tell.   Vines tuberosis, heliotropes, violets etc!  how hard you make it for me to send regrets &amp; thanks to your cordial invitation to return to your city of cool breezes &amp; untold delights.  You seem not to place much weight on what Kate says. Perhaps your memory is at fault.  I will refresh it.  In the happy days that are gone Kate asserted positively that Durham should know her no more until members of your family or yourself did her the honor to make her a visit.  Her sentiments have undergone no change.  poor little Bessie has had much sorrow.  I hoped to have had her with me was <pb id="p5" n="5"/>bitterly disappointed.  I trust she will run up to N.C.  She would be greatly benefitted.</p>

<p>Dear girl I owe her more than I can ever repay for her patience sympathy &amp; love for my unworthy self.</p>

<p>Yes I suppose I shall have to make the best of the Chicago nominations-since I couldn't be there in person to regulate these much mooted questions.  The nomination of Cleveland was what I expected, tho' Hendricks place on the ticket was a surprise.</p>  

<p>I'll see that Ga. does her duty on that score no one has any fears.  However you have great need to look after you State which is like "them white girls" you delight quoting <pb id="p6" n="6"/> against, <hi rend="underline">it is mighty ansartain</hi>. [sic]  Bessie sent me an extract from a D_ [Durham] paper quite complimentary to Mr. James &amp; suggesting him as suitable candidate for legislative honors.  I would be glad to cast my vote for him.  Hope he will one day distinguish himself in the national Congress.  How cruel to revive memories of handsome Capt. Ed. P. &amp; then cast a gloom over all by reverting to the charms &amp; unprecedented health of Mrs. P__ [underline in original] ah! that wounds too deeply to be healed by the balm of your next sentence, viz. that the fascinating Capt. smiles blandly &amp; kindly when my name is mentioned.  I think that proceeding very unkind on his part-he should pull a long face, heave a tremendous <pb id="p7" n="7"/>sigh utterly refuse your bouquets &amp; abandon himself to grief.  Perhaps this hopeless hero worship explains the "why" you seem to think so mysterious when you remember Kate is  <hi rend="underline">amiss</hi>[?] (pardon the pun-a subject so solemn admits of no levity.) No, No, I'm no misanthrope.  I admire in fact adore the other sex what a mistaken idea to suppose otherwise.</p>  

<p>I should like to know if the boys &amp; especially the widowers pay you commission for your services in <sic corr="their">thier</sic> behalf.  If so charge the latter 100 per c[en]t extra on account of your pleading being[?] <sic corr="irresistable">irrisistible</sic> and-well-a man never could keep a secret[?] so-nothing.  My letter will exhaust your patience in the <pb id="p8" n="8"/> reading therefore for I must insert a period here.</p>  







<closer>
<salute>With best love to all &amp; hoping your dear ones are well &amp; happy &amp; that yu will not let them nor your-self forget your very sincere</salute>
<signed>Kate</signed><lb/>
I recently suffered a nervous fever attack.  This accounts for the dizzy appearance of my letter, it partakes of the nature of my head which is quite light &amp; airy &amp; swims with admirable ease &amp; grace.
</closer>

	</div1>
</body>

	</text>
</TEI.2>
