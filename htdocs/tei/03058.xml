<?xml version="1.0" encoding="iso-8859-1"?>
<?xml-stylesheet href="./styles/tei-nc.xsl" type="text/xsl"?>
<!DOCTYPE TEI.2 PUBLIC "-//TEI//DTD TEI Lite XML ver. 1//EN" 
"./dtds/teixlite.dtd" [<!ENTITY tei "Text Encoding Initiative">
<!ENTITY plus   "&#x002B;"> <!ENTITY dollar "&#x0024;">
<!ENTITY nbsp   "&#x00A0;"><!ENTITY percnt "&#x0025;">
<!ENTITY half   "&#x00BD;">]>


<TEI.2>

	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title>Obituary by A.S. DeVlarning for Thomas Decatur Jones, November 8, 1889: Electronic Edition.</title>
				<author>DeVlarning, A.S., 1852-1889</author>
				
				<funder>Funding by the Institute of Museum of Library Services (IMLS) and the federal Library Services and Technology Act (LSTA), with support provided through North Carolina ECHO.</funder>
				<respStmt>
					<resp>Text transcribed by</resp>
					<name>Trudi Abel</name>
				</respStmt>	
				<respStmt>
					<resp>Images scanned by</resp>
					<name>Digital Production Center</name>
				</respStmt>
				<respStmt>
					<resp>Text encoded by</resp>
					<name>Katherine M. Wisser</name>
				</respStmt>
			</titleStmt>

			<editionStmt>
				<edition>First edition, 
					<date>2006</date>
				</edition>
			</editionStmt>

			<publicationStmt>
				<publisher>Duke University Libraries</publisher>
				<pubPlace>Durham, NC, USA</pubPlace>
				<date>2006</date>
				<availability><p>&#x00A9; This work is the property of the Duke University Libraries. It may be used freely by individuals for research, teaching, and personal use as long as this statement of availability is included in the text.</p></availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="title page">Obituary by A.S. DeVlarning for Thomas Decatur Jones, November 8, 1889</title>
						<author>A.S. DeVlarning</author>
					</titleStmt>
					
					<extent>6 p.</extent>
					<publicationStmt>
						<date>18891108</date>
					</publicationStmt>
					<notesStmt>
						<note>A. S. DeVlarming writes an obituary for his employer Thomas Decatur Jones (1852-1889) on Hotel Claiborn stationery. The Claiborn hotel  was located in Durham, North Carolina during the 1880s.</note> 
					</notesStmt>
					<sourceDesc>
						<p>James Southgate papers, 1794-1944 and undated, Rare Book, Manuscript and Special Collections Library, Duke University.</p>					
					</sourceDesc>

				</biblFull>
			</sourceDesc>
		</fileDesc>
		<encodingDesc>
			<projectDesc>
				<p>This electronic edition is part of the Duke University Libraries digitization project, <hi rend="italics">Digital Durham</hi>
				</p>
			</projectDesc>	

			<editorialDecl>
<p>The text has been encoded using the recommendations for Level 4 of the TEI.</p>
				
				<p>Original, grammar, punctuation and spelling have been preserved. Encountered typographical errors have been preserved with corrections provided. In some cases, we've put sic in square brackets where we've identified misspellings or lack of punctuation but do not require corrections in order to understand the text. Where older spellings occurred, the original spelling is preserved and a modern equivalent of the word is provided.</p>
				
				<p>Any hyphens occurring in line breaks have been removed, and the trailing part of a word has been joined to the preceding line.</p>
				
				<p>All dollar signs, plus signs, and percentage signs, fractions, and ampersands have been transcribed as entity references.</p>
				
				<p>Blank spaces have been indicated with 3 &nbsp; characters where they are necessary because of lack of punctuation in the original text.</p>
				
				<p>In the case of writers who put ill-defined ink marks in places where commas or periods should exist, we take the liberty to transcribe them as either commas, periods, or semicolons to make the meaning of the text more clear.</p>
				
				<p>In the case where words or parts of words cannot be discerned, a gap, represented by a series of underscores, has been included where the work or part of the word appear in the text.</p>
				
				<p>Indentation has not been preserved. Paragraph breaks were included only when it was obvious.</p>
				
			</editorialDecl>

			<classDecl>
				<taxonomy id="LCSH">
					<bibl>
						<title>Library of Congress Subject Headings</title>
					</bibl>
				</taxonomy>
			</classDecl>
		</encodingDesc>

		<profileDesc>
			<langUsage>
				<language id="eng">English</language>
			</langUsage>

			<textClass>
				<keywords scheme="LCSH">
					<list>
<item>Obituaries -- 19th century</item>
<item>Jones, Thomas Decatur, 1852-1889</item>
<item>Dead</item>
<item>Durham (N.C.) -- History</item>
<item>Death -- Social aspects -- History -- 19th century</item>
<item>DeVlarming, A. S.</item>
<item>Durham (N.C.)</item>
<item>Hotel Claiborn (Durham, N. C.)</item>

					</list>
				</keywords>

			</textClass>
		</profileDesc>

	<!--		<revisionDesc>
			<change>
				<date></date>
				<respStmt>
					<name></name>
					<resp></resp>
				</respStmt>
				<item></item>
			</change>
		</revisionDesc> -->
	</teiHeader>

	<text>
<body>

<div1 type="letter">
<pb id="p1" n="1"/>
<opener>
<dateline>Hotel Claiborn<lb/>
A.B. Sites, Proprietor<lb/>
<name type="place">Durham, N.C., </name>
<date>Nov 8 1889</date>
</dateline>
</opener>

<p>Thos  D Jones</p>

<p>At his home in Durham N C on the Evening of the 30th of Octo after a long and patient illness  Mr. Thos D. Jones in the thirty seventh year of his age.</p>

<p>There had been days &amp; weeks of intense &amp; anxious watching &amp; waiting on the part of an interested and sympathizing community hoping almost against hope to the last that the life of one so valued &amp; esteemed &amp; seeming so much needed might be spared. &amp; it was with a heavy sadness that the announcement of the fact of this death was received <pb id="p2" n="2"/>throughout the City &amp; Country with which he was so intimately &amp; prominently connected.  For several months previous signs of failing health had begun sufficiently to manifest themselves to be detected by the eye of friends &amp; to awaken uneasiness on their part.  But it was not until about two months before his death that he yielded to the demands of increasing weakness so far as to cease from the action [and] performance of the duties of his office &amp; to seek rest.  But it was too late for he was forced to take his bed where he continued gradually <pb id="p3" n="3"/>growing weaker, a painful but patient sufferer bravely struggling for a life which he himself could but wish to have prolonged for the sake of others until death came to his release  Neither could the best medical skill nor the most tender &amp; gentle nursing nor the combined interest &amp; hope &amp; prayers of sympathizing friends avail to save one whose life course on earth was determined in the counsel of a wise &amp; holy God.  And so it remains for an afflicted family an [sic] community in Reverent submission in this bereavment [sic] which has befallen them. <pb id="p4" n="4"/>The subject of this notice was well known throughout a large section of [the] country [sic]   He came to Durham in M[ar]ch 1881 &amp; has been continuously identified with its business &amp; social life ever since.  At the time of his death he was a director in the First Nat[ional] Bank &amp; was a city alderman up to a few week when he resigned on account of his health   he was Chairman of the f<gap/>man com. also which responsible positions he had for some time filled with great faithfulness. <pb id="p5" n="5"/>   In all his business acts &amp; dealings he was pure honest &amp; upright.  His great willingness to serve those who sought him as a friend or advisor won for him the unreserved respect &amp; confidence of those with whom he wide business intercourse was maintained.  His genial disposition  great kindness of heart refined manner cultivated intelligence won for him a large circle of friends.  His death therefore is necessarily felt beyond the limits of family &amp; kindred.  In his death the <pb id="p6" n="6"/>City of Durham has lost a man &amp; citizen who will be sadly missed   He leaves a bereaved wife to mourn his loss &amp; two little boys-But there is one thing to brighten our grief over the death of this noble man &amp; servant of the Lord for it is given to us to feel that his service has only been transferred to a higher &amp; happier sphere.



</p>
<closer>

<signed>A.S. deVlarning</signed><lb/>
<date>Nov. 8th 1889</date>
</closer>

	</div1>
</body>

	</text>
</TEI.2>
