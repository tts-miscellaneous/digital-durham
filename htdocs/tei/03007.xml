<?xml version="1.0" encoding="iso-8859-1"?>
<?xml-stylesheet href="./styles/tei-nc.xsl" type="text/xsl"?>
<!DOCTYPE TEI.2 PUBLIC "-//TEI//DTD TEI Lite XML ver. 1//EN" 
"./dtds/teixlite.dtd" [<!ENTITY tei "Text Encoding Initiative">
<!ENTITY plus   "&#x002B;"> <!ENTITY dollar "&#x0024;">
<!ENTITY nbsp   "&#x00A0;"><!ENTITY percnt "&#x0025;">
<!ENTITY half   "&#x00BD;">]>


<TEI.2>

	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title>Letter from F.D. Cunningham to James Southgate, March 10, 1885: Electronic Edition.</title>
				<author>Cunningham, F. D.</author>
				<author>Southgate, James, 1832-1914</author>

				<funder>Funding by the Institute of Museum of Library Services (IMLS) and the federal Library Services and Technology Act (LSTA), with support provided through North Carolina ECHO.</funder>
				<respStmt>
					<resp>Text transcribed by</resp>
					<name>Kelly Clark</name>
				</respStmt>	
				<respStmt>
					<resp>Images scanned by</resp>
					<name>Digital Production Center</name>
				</respStmt>
				<respStmt>
					<resp>Text encoded by</resp>
					<name>Katherine M. Wisser</name>
				</respStmt>
			</titleStmt>

			<editionStmt>
				<edition>First edition, 
					<date>2006</date>
				</edition>
			</editionStmt>

			<publicationStmt>
				<publisher>Duke University Libraries</publisher>
				<pubPlace>Durham, NC, USA</pubPlace>
				<date>2006</date>
				<availability><p>&#x00A9; This work is the property of the Duke University Libraries. It may be used freely by individuals for research, teaching, and personal use as long as this statement of availability is included in the text.</p></availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="title page">Letter from F. D. Cunningham to James Southgate, March 10, 1885</title>
						<author>F. D. Cunningham</author>
					</titleStmt>
					
					<extent>2 p.</extent>
					<publicationStmt>
						<date>18850310</date>
					</publicationStmt>
					<notesStmt>
						<note>Dr. Frank. D. Cunningham, director of the Retreat of the Sick, Richmond, Virginia. writes James Southgate with a description of his daughter's condition.  The physician refers to Annie Southgate's   "nervous attacks" and terms her condition Hyster Epilepsy.</note> 
					</notesStmt>
<sourceDesc>
						<p>Southgate-Jones Family Papers, 1760-1982, Rare Book, Manuscript and Special Collections Library, Duke University.</p>					
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<encodingDesc>
			<projectDesc>
				<p>This electronic edition is part of the Duke University Libraries digitization project, <hi rend="italics">Digital Durham</hi>
				</p>
			</projectDesc>	

			<editorialDecl>
				<p>The text has been encoded using the recommendations for Level 4 of the TEI.</p>
				
				<p>Original, grammar, punctuation and spelling have been preserved. Encountered typographical errors have been preserved with corrections provided. In some cases, we've put sic in square brackets where we've identified misspellings or lack of punctuation but do not require corrections in order to understand the text. Where older spellings occurred, the original spelling is preserved and a modern equivalent of the word is provided.</p>
				
				<p>Any hyphens occurring in line breaks have been removed, and the trailing part of a word has been joined to the preceding line.</p>
				
				<p>All dollar signs, plus signs, and percentage signs, fractions, and ampersands have been transcribed as entity references.</p>
				
				<p>Blank spaces have been indicated with 3 &nbsp; characters where they are necessary because of lack of punctuation in the original text.</p>
				
				<p>In the case of writers who put ill-defined ink marks in places where commas or periods should exist, we take the liberty to transcribe them as either commas, periods, or semicolons to make the meaning of the text more clear.</p>
				
				<p>In the case where words or parts of words cannot be discerned, a gap, represented by a series of underscores, has been included where the work or part of the word appear in the text.</p>
				
				<p>Indentation has not been preserved. Paragraph breaks were included only when it was obvious.</p>
				
			</editorialDecl>

			<classDecl>
				<taxonomy id="LCSH">
					<bibl>
						<title>Library of Congress Subject Headings</title>
					</bibl>
				</taxonomy>
			</classDecl>
		</encodingDesc>

		<profileDesc>
			<langUsage>
				<language id="eng">English</language>
			</langUsage>

			<textClass>
				<keywords scheme="LCSH">
					<list>
<item>Diseases -- Virginia -- History -- 19th century</item>
<item>Durham (N.C.) -- History</item>
<item>Women -- North Carolina -- Social conditions -- 19th century</item>
<item>Southgate, Annie</item>
<item>Southgate, James, 1832-1914</item>
<item>Physicians -- United States -- History --19th century</item>
<item>Medical care -- United States -- History -- 19th century</item>
<item>Cunningham, Frank D.</item>
<item>Diseases -- North Carolina -- History -- 19th century</item>
<item>Letters (correspondence)</item>
<item>Durham (N.C.)</item>
<item>Richmond (Va.)</item>
						
					</list>
				</keywords>

			</textClass>
		</profileDesc>

	<!--		<revisionDesc>
			<change>
				<date></date>
				<respStmt>
					<name></name>
					<resp></resp>
				</respStmt>
				<item></item>
			</change>
		</revisionDesc> -->
	</teiHeader>

	<text>
<body>

<div1 type="letter">
<pb id="p1" n="1"/>
<opener>
<dateline>F. D. Cunningham, M.D.,<lb/>
717 Franklin Street.<lb/>
<name type="place">Richmond, Va. </name>
<date>March 10? 1885</date>
</dateline>
<salute>My dear Mr Southgate
</salute>
</opener>

<p>Since you left Miss Annie <gap/> very well and yesterday morning was quite cheerful when I paid my visit about 12AM but afterwards from some <sic corr="trifling">triffling</sic> exciting cause she had one of her nervous attacks. This time it assumed the form of Hyster. Epilepsy and she was to all appearances unconscious and subject to slight muscular convulsions from 3 PM to about 9 PM when it <gap/> off and she had a good night     Today she was cheerful but complaining of <gap/>, the result of the muscular spasms &plus; had no knowledge of events yesterday after her attack and I dont think she is any the worse from the attack and hope she will be walking about again in a day or two [as well?] as when you left. She asked me about getting a special nurse since one had recommended her and I was non committal until I saw the nurse myself &plus; the material of which she was made. I think Miss Annie had been quite fretted at the Dr out mending her on Sunday &plus; doing what I must instead of what she wishes and that may have culminated in the attack of yesterday. I was <gap/> <sic corr="surprised">surprized</sic> to see it as <pb id="p2" n="2"/>Dr Carr had said as much in his letter which you gave me and it was only one of the many forms of Hysteria that we have to contend with</p>    
<p>As I said before today her condition was quite satisfactory and  I hope we will [get in?] <gap/> for the [explosion?] as the cause will be avoided in future as far as possible.</p>
	<p>As requested I enclose a statement of your <gap/> with me to March 12. I will just charge so much a week for attending Miss Annie and not by the visit for of course it <gap/> not be <gap/> for me to see as often as I now do when I have other patients also in the same building that require me to [go there?] regularly to look after them. The charge in my books against you is for some letters last year and in /83 after you went home.</p>
	<p>Hoping this will find you better yourself and with a [promise?] that Miss Annie is doing well tonight as I have just learned from the D<hi rend="sup">r</hi></p>



<closer>
<salute>Yours Sincerely</salute>
<signed>Dr. F. D. Cunningham</signed>
</closer>

	</div1>
</body>

	</text>
</TEI.2>
