<html>
<head>
<title>Digital Durham</title>
<link rel="stylesheet" type="text/css" media="screen" href="/ui/css/style.css" />
</head>

<body>

<div id="contain">
<div id="top">

<div id="search_box_top"><div id="search_box_left"><form action="index.php?x=search" method="GET"><input id="searchField" class="search" type="text" name="dduMenu_0_value" value="" /><input type="hidden" name="SendSearch" value="1"><input type="hidden" name="x" value="search"><input type="hidden" name="dduMenu_0" value="dc_description"> <input class="searchGo" type="submit" value="Search " name="searchSubmit" /></form></div></div>

<a href="/new/hueism/"><img src="dd_logo3.gif" /></a>

</div>

<div id="nav">
  <?php

if(isset($x) && $x != "")
  include_once($x.".php");
else
  include_once("nav.php");

?>
</div>

<div id="content">
<p class="header">Teachers' Corner
</p>
<p class="headerInText">Lesson Plans</p>
  <p>Youth and Education in Durham, North Carolina during Industrialization, 1880-1910 (<a href="lesson.pdf">PDF</a>, <a href="lesson.doc">MS Word</a>)</p>
  <p class="headerInText">
    Lesson Plan Documents</p>
  <p>Rules and Regulations Governing the Durham Graded and High School with the Course of Study, 1885 (<a href="schoolreport.pdf">PDF</a>)
  <p><br>
&quot;Durham and the Tobacco Factories&quot; from John Moore's School History of North Carolina, 1882 (<a href="schoolhistory.pdf">PDF</a>)  
  <p><br>
  Oration by Hon. Frederick Douglass on The Occasion of The Second Annual Exposition of the Colored People of North Carolina, Delivered on Friday October 1st, 1880 (<a href="FredrickDouglassOrationRev2.doc">MS Word</a>)  
  <p><br>
&quot;Type Stories of Workers&quot; from Mary Cowper's unpublished study, &quot;Cotton Cloth: A Type Study of Community Process.&quot; (<a href="workerstories150.pdf">PDF</a>)<br>  
      <p>
  <p><a href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a> &middot; <a href="index.php?x=overview">About this site</a> &middot; Copyright � 2006. Trudi J. Abel. All Rights Reserved. </p>
</div>
</div>

<div id="copyright">
<div class="margins">
</div>
</div>


</body>
