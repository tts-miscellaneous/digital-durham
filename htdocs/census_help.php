<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Digital Durham</title>
<link rel="stylesheet" type="text/css" media="screen" href="/ui/css/style.css" />
</head>

<body>

<div id="contain">
<div id="top">

<div id="search_box_top"><div id="search_box_left">
<form method="get" action="/search">
      <input class="form_textbox" id="q" name="q" type="text" alt="Search Box" style="width:140px;" />
	  <input type="submit" name="submit" value="Go" />
      <input type="hidden" name="site" value="duke_collection" />
      <input type="hidden" name="client" value="digitaldurham" />
      <input type="hidden" name="proxystylesheet" value="digitaldurham" />
      <input type="hidden" name="output" value="xml_no_dtd" />
    </form>
</div></div>

<a href="/"><img src="/images/dd_logo3.gif" /></a>

</div>

<div id="nav">
  <?php

if(isset($x) && $x != "")
  include_once($x.".php");
else
  include_once("nav.php");

?>
</div>
<div id="content">

<div class="margins">

  <p class="header"><a href="reference.php">Reference</a></p>  
<p class="header">
Census Search Help Guide<br>
1880 Federal Population Census Database<br>
</blockquote>
</p>
<blockquote>
<p><a href="/census_help.php#soundex">Soundex Search</a>&nbsp;&nbsp;&nbsp;
<a href="/census_help.php#simple">Simple Search</a>&nbsp;&nbsp;&nbsp;
<a href="/census_help.php#intermediate">Intermediate 
Search</a>&nbsp;&nbsp;&nbsp;
<a href="/census_help.php#advanced">Advanced Search</a>&nbsp;&nbsp;&nbsp;
<p class="header">About the Source</p>
<p>
Durham Township, Orange County - 1880 Federal Population Census 
Two men enumerated the inhabitants of Durham Township in 1880. W. W. Woods, a 
forty-five year old farmer visited the residents of South Durham Township, 
Orange County between June 1, 1880 and June 21, 1880 to record their personal 
data. Another Durham Township resident enumerated inhabitants who lived north 
of the railroad tracks. Between June 1, 1880 and July 6, 1880, Archie D. 
Wilkinson gathered data on residents of the North Durham Township. He was 48 
and listed his occupation as Deputy U.S. Marshal on the census schedules. 
</p><p>
Together, these two men gathered data on 5507 residents. Each man collected 
data on large lined pages. These were sent to Washington and tallied by 
workers in the Census Bureau. In the 1930s, the government microfilmed the 
manuscript census ledgers. Information in our census database was transcribed 
from the National Archives, Series no. T-9, reels 975 and 976.
</p><p>
The Digital Durham website contains a beta-version of the 1880 federal 
population census data for Durham Township, Orange County. There are errors in 
the data, which may make it unsuitable for formal research projects. The data 
can be used in an educational setting for locating individuals who lived in 
Durham in the 1880s or identifying demographic trends from this time period.
</p><p class="header">

Digital Durham Census Search Feature

</p><p>
The website provides users with four different search options. The Soundex 
Search is geared to genealogists who know the surnames of the individuals they 
seek and have a Soundex code.  The Simple Search contains eight key fields. 
This will be an especially useful starting point for researchers who are new 
to databases and teachers who are working with elementary school students.  
The Intermediate Search enables researchers to search on sixteen fields while 
the Advanced Search enables searches on 32 fields. Descriptions of these 
fields and their meanings are given below.
 
</p><p class="header"><a name="soundex">
<h3>Soundex Search</h3></a>
</p><p>
Genealogists who know the surnames of the individuals they seek will often use 
Soundex. The National Archives web site has an explanation about the Soundex 
code.   See <a href="http://www.archives.gov/genealogy/index.html">
http://www.archives.gov/genealogy/index.html</a>. 
</p><p>
The Soundex system 
was created in the early 1920s as a means of indexing surnames.  This coding 
system allows genealogists to search for a particular surname while at the 
same time accounting for discrepancies in spellings.  A Soundex code generator 
can be found on the Jewish Genealogy web site. With this computer tool, 
researchers can code surnames of interest.  See <a 
href="http://www.jewishgen.org/jos/jossound.htm">
http://www.jewishgen.org/jos/jossound.htm</a>.  The four-digit code generated 
by this web site can be used in the Soundex field to search the Durham census. 

</p><p class="header"><a name="simple">
<h3>Simple Search</h3></a>
</p><p>
<b>Enumeration District</b>- Enumeration District (ED)-Census enumerators 
designated 
residents living north of Durham' s train tracks as inhabitants of ED 195 and 
residents on the south side of the tracks were assigned to ED 196. 
</p><p>
<b>Last name</b>-Capitalization is not necessary in this field.  Durham's 
census 
takers used a variety of spellings for surnames that share the same sounds.  
If a researcher is searching for members of the Bivans or Bivuns family, s/he 
could use a wildcard symbol in the search field. The Last Name field would 
contain B%s.  The wildcard in this database is a percentage sign (%). The % 
character allows users to introduce a degree of uncertainty into their search.  
If, for example, a user was uncertain if a family spelled its name "Lawrence," 
"Laurence," or "Lawrance" a wildcard could be very useful.  If the user 
searches for "L%ce," s/he will find all the records that match these 
parameters.  
</p><p>
<b>First name</b>-Capitalization is not necessary in this field.  Census 
takers, in 
many cases, provided an initial instead of a complete first name. If a user 
searches for Brodie Duke, by typing "Brodie, s/he will find zero records 
matching.  If the user expands the search by typing B% in the First Name 
field, then the database will return data on Brodie Duke who is listed as B. 
Duke in the census. The use of the wildcard (%) should be used exactly as it 
is used in the Last Name field. 
</p><p>
<b>Middle name</b>-Many residents provided the enumerators with a middle 
name or 
initial. During the transcription process, we found many middle initials 
difficult to interpret. A capital L could, for instance, look like a capital 
S. If the researcher is searching for a John S. Lockhart and finds John L. 
Lockhart, there is a strong chance that the census data pertains to John S.  
We advise researchers to verify their data by consulting the original 
microfilm.
</p><p>
<b>Color</b>-The United States Government gave census enumerators specific 
instructions as to how to classify individuals by "color." Census Bureau 
directed its census takers to categorize inhabitants as either --white W, 
black B, Mulatto, Mu, Chinese, C, Indian, I. The directions further directed 
census takers use the term "mulatto" to describe "quadroons, octoroons, and 
all persons having any perceptible trace of African blood."
In compiling the census database, it became clear that Durham's enumerators 
did not follow these guidelines.  Both "mulatto" and "black" were categories 
used to indicate an individual with African ancestry.  Census takers 
subjectively determined which residents would be categorized as "black" or 
"mulatto." The distinction between the two was made purely based on 
appearance; darker-toned individuals were categorized as "black," while 
lighter skin toned inhabitants were described as "mulatto."  None of Durham's 
residents were described as being of Chinese or Indian background.
</p><p>
This field allows users to select more than one category for analysis.  This 
feature allows users to readily determine how many individuals with African 
forebears lived in Durham. 
</p><p>
<b>Sex</b>-This field uses a drop down menu, allowing users to select either 
male or 
female.  If no sex is chosen, the database will automatically search for both 
male and female.
</p><p>
<b>Age</b>-Using the drop down menu, users can find people who are exactly a 
certain 
age, older than, younger than, or within a certain age range.  In this field, 
all entries must be numbers; the wildcard function does not work in the Age 
field.  If a user were to search for all people of color ("black" and 
"mulatto") between the ages of 22 and 26 who lived in the 195th enumeration 
district, the database would return 157 records.  Searching for all 
individuals of color in the same enumeration district who are older than 26 
would return a result of 338 records.  It is important to note that when a 
user searches for a range of ages the parameters are inclusive.  The results 
for a search of individuals between the ages of 22 and 26, for instance, will 
include 22- and 26-year-old individuals.  On the other hand, the results page, 
for a search of individuals older than 26 will not include 26 year-old 
individuals.  
</p><p>
<b>Occupation List</b>-The drop down menu is not a comprehensive list of 
occupations, 
but it does include the most common trades.  Some terms like "hostler" will be 
unfamiliar to students, others like "nurse," will have a specific meaning that 
was current in the nineteenth century. We suggest that students and teachers 
consult our <a href="/occupation.php"> Occupations Glossary</a>, which can be 
found via the reference link on the web site.  The 
wildcard is very helpful in the blank box that follows the words "type 
occupation here".   When a search for "house servant" from the drop down menu 
is done, 3 records show up.  On the other hand, if one types "%servant%"  into 
the occupation open field, 16 records show up. These discrepancies in the 
returns are a reminder that there were two different enumerators who used 
different terms to describe common occupations. Archie Wilkinson from ED 195 
preferred "teacher" to describe educators and "works on farm" to describe farm 
laborers. His counterpart in ED 196 used "school teacher" to describe 
educators and "work on farm" and "farm hand" to describe agricultural workers.

</p><p>
<b>Reset</b>- The Reset button clears all search fields. A user can amend the 
search 
screen by making modifications and not pressing the reset button. That is to 
say, if we search for all "35" year-old "Black" "Females," we will get 25 
matching records.  If we do not press 'Reset' but we do designate the 
Enumeration District as '195' and then hit the search button we will come up 
with 15 records.  These results are all '35' year old 'Black' 'Females' who 
live in district 195.  On the other hand if we were to have pressed "Reset" 
after our first search and then designated the Enumeration District as 195, we 
would have come up with all 3362 individuals who lived in Durham on the north 
side of the railroad tracks.   
</p><p>
<b>SEARCH</b>--Users must initiate their searches by clicking on the Search 
button.  
After designating one' s search fields (i.e. Black for "color" and  "Nurses" 
for "occupation") pressing SEARCH will allow the database to find all matching 
records within the database.
</p><p>
<b>Show All Fields</b>-Census enumerators asked inhabitants a series of 26 
questions 
as they collected their data. We have added five fields (e.g. infant age, 
household rank, etc.) and a notes field during the transcription process. By 
pressing "Show All Fields," one can see all 32 fields for each inhabitant in 
any of the results screens. 

</p><p class="header"><a name="intermediate">
<h3>Intermediate Search</h3></a>
</p><p>
<b>Page Number</b>-The original census data was handwritten on large ledger 
pages in 
1880.  It was not until the 1930's that the government microfilmed the 
manuscript census ledgers.  Information in our census database was transcribed 
from the National Archives Series no. T-9, reels 975 and 976.  This search 
field allows users to search certain pages using the mathematical symbols of 
=, <, >.  When looking at the census data with respect to what entries are on 
what pages, users should note that if two entries are close to each other in 
the document itself (for example, both on the same page and ED) it is likely 
that these two individuals lived in the same area.  Because there were two 
enumerators in two different districts, using two different books, many of the 
page numbers appear twice in the database.  In order to distinguish between 
the two books, the Digital Durham Project assigned an 'a' to all page numbers 
pertaining to the ED 195. The fourteenth page is recorded as "14a." The letter 
'b' immediately follows all pages enumerating the 196th district.  One must 
add an 'a' or 'b' in order for the search function to work.  It is also 
important to note that users must put a zero in the tens place when searching 
for data from pages one through nine. Thus one would have to type 01a, 02a, 
etc when searching for a page with a single digit.
</p><p>
<b>Dwelling Number</b>- The Census Bureau required each enumerator to "visit 
personally each dwelling house in his subdivision, and each family therein, 
and each individual living out of a family in any place of abode�." The Bureau 
requested that the enumerator interrogate the head of each family to obtain 
the required information. In such cases where the residents were not home, the 
enumerator could obtain information from neighbors. Dwelling houses were to be 
numbered in the order of visitation. "A dwelling house, for the purpose of the 
census means any building or place of abode�in which any person is at the time 
living, whether in a room above a warehouse or factory, a loft above a stable 
or a wigwam on the outskirts of a settlement, equally with a dwelling house in 
the usual, ordinary sense of the term." (History and Growth of United States 
Census, 170).  
</p><p>
With this search field we can find out who lived together in a physical 
structure (eg. house or hotel), or who lived in proximity to one another.  For 
example, it is likely that dwelling number 3 and dwelling number 4 were close 
to one another. When conducting dwelling number searches, pay attention to 
which district each dwelling is assigned.  If we do a search for dwelling 
number 12, we see that there is a dwelling number 12 in both districts.  
Although they have the same dwelling number, the Green family did not live 
with the Lockharts.  Users should use the enumeration district and page number 
data to confirm that the Greens and the Lockharts do not live together.
</p><p>
<b>Family Number</b>--"The word family, for the purposes of the census, 
includes 
persons living alone, as previously described, equally with families in the 
ordinary sense of that term, and also all larger aggregations of people having 
only the tie of a common roof and table. A hotel, with all its inmates 
constitutes but one family within the meaning of this term. " (History of 
census, 171)
</p><p>
The structure of the census data collection sheet should have enabled our 
enumerators to indicate when two different families shared one dwelling house. 
When the Project staff transcribed the census data, it became clear that the 
enumerators had difficulty keeping count of the families and seemed to create 
their own practice of enumeration.
</p><p>
<b>Household Rank</b>-This is a field that the Digital Durham Project staff 
created.  
It enables us to label individuals in the order in which they appear in the 
original census. The Census Bureau would regard an individual with household 
rank of 1 as "head of household".
</p><p>
<b>Infant Age</b> (in months)-This is a field that the Digital Durham Project 
staff 
created.  Enumerators indicated the age of a baby using a fraction such as 
3/12.  This field enables us to provide the same information in digital 
format.  
</p><p>
<b>Relationship</b>-This field indicates a person's relationship within a 
dwelling.  
These relationships are usually relative to the head of the household (the 
person who holds the number 1 house rank).  
</p><p>
<b>Civil Condition</b>-Each individual that entered the census was categorized 
as 
'single,' 'married,' or 'widowed/divorced' this field allows one to separate 
these three groups.  Divorce was relatively uncommon in the nineteenth 
century, so the user should assume that most individuals have lost a spouse. 
In three cases, the census takers put a D in the widowed/divorced column see 
our "notes field" for Tom Craig, Jack Strayhorn, and Julia James.
</p><p>
<b>Place of Birth</b>-The Census Bureau required enumerators to report the 
place of 
birth (abbreviated as POB) of every person upon the schedule. The Census 
Bureau requested its enumerators to write the state or territory of origin for 
U.S.-born citizens and the specific country for those who were foreign born. 
The Bureau preferred that the enumerators indicate Wales rather than Great 
Britain. This field provides data that can be used to explore questions 
regarding migration and immigration.  The entries are limited to states and 
countries, and in one case, a continent.  
</p><p class="header"><a name="advanced">
<h3>Advanced Search</h3></a>

</p><p>
<b>Suffix</b>-This search field allows the user to search for anyone who had a 
suffix 
at the end of their name (Jr. Sr.). These suffixes were used infrequently by 
Durham's enumerators.
</p><p>
<b>Months Unemployed</b>-The Census Bureau asked its enumerators to document 
the 
number of months during which individuals were unemployed. This field must be 
a number in order to work, and the wild card (%) does not function in this 
field.
</p><p>
<b>Sickness</b>-Census takers used this field to document the chronic 
illnesses 
and 
disabilities of Durham's inhabitants. The wildcard % can be used in this 
field. Ailments like "typhoid fever, measles, and dislocated ankles" are 
registered here. The reference section of the web site contains a 
<a href="/medical.php"> Medical Glossary</a>.
</p><p>
<b>Blind?</b>-If an individual was blind the enumerator indicated so with a 
checkmark 
for 'yes.'  This is coded as "Y" in the database. If an individual was not 
identified with this disability, the field was left blank.
</p><p>
<b>Deaf?</b>-If an individual was deaf the enumerator indicated so with a 'y' 
for 
'yes.' If an individual was not identified with this disability, the field was 
left blank.
</p><p>
<b>Idiotic?</b>- Nineteenth-century Americans used the term "idiotic" to 
describe 
individuals who were born without the "ordinary intellectual powers of man." 
The enumerator placed a checkmark on field 18 of the census. Our census 
database has a 'y' for 'yes' under the column with the header of "idiotic."  
If an individual was not identified with this disability, the field was left 
blank.
</p><p>
<b>Insane?</b> - If the census enumerator judged a citizen as suffering from 
madness 
or insanity, he marked his census data sheet with a "y" for "yes" in this 
column. If an individual was not identified with this disability, the field 
was left blank.
</p><p>
<b>Attended School?</b>- If an individual attended school during the census 
year, 
the 
enumerator indicated so with a 'y' for 'yes.'  If an individual did not attend 
school in that year, the field was left blank.  Searches using this field give 
a good indication as to which children in the Durham community had access to 
more formal forms of education.
</p><p>
<b>Cannot Read?</b>- If an individual could not read, the enumerator 
indicated so 
with a 'y' for 'yes.'  If an individual could read the field was left blank. 
</p><p>
<b>Cannot Write?</b>- If an individual could not write the enumerator 
indicated so 
with a y" for "yes."  If an individual could write the field was left blank.  
</p><p>
<b>Foreign Born?</b>- The Digital Durham Project created this field to 
facilitate 
searching for all individuals who were foreign born. A "y" indicates that the 
individual was born in a nation or region outside of the states and 
territories belonging to the United States. 
</p><p>
<b>Father POB</b>-This field allows one to find out where an individual's 
father was 
born.  The types of entries are limited to states and countries, except in the 
case of one individual whose father listed as from the continent of  "Europe."
</p><p>
<b>Father Foreign Born?</b>- The Digital Durham Project created this field to 
facilitate searching for all fathers who were  foreign born If an individual's 
father was foreign born the database indicates so with a "y" for "yes."  If an 
individual's father was not foreign born the field was left blank.
</p><p>
<b>Mother POB</b>-This field allows one to find out where an individual's 
mother was 
born.  The types of entries are limited to states and countries with one 
exception.
</p><p>
<b>Mother Foreign Born?</b>- The Digital Durham Project created this field to 
facilitate searching for all mothers who were  foreign born If an individual's 
mother was foreign born the enumerator indicated so with a "y" for "yes."  If 
an individual's mother was not foreign born the field was left blank.
</p><p>
<b>Notes</b>--The Digital Durham Project created this field to provide space 
to 
indicate problems with the interpretation of data during the transcription 
phase. 
</p><p class="header">

 
Troubleshooting
</p><p>
As a means of assuring accurate data, always press the RESET button before 
starting a new search.  Many times a search delivers a '0 out of 5507' message 
because search fields were not cleared entirely from previous searches.  If, 
for example, a user searches for 'Smith' and gets "0 out of 5507," it is not 
because the database is malfunctioning, it is more likely because the fields 
from the previous search were not reset.  How many Smiths are blind, deaf, 
male and heads of their household?  This search would generate a '0 out of 
5507' response.  
</p><p class="header">

Sorting
</p><p>
Search results are generated in the large window of the database on the right 
side of the screen. Once a set of results is generated, the results may be 
sorted.  The grey field headers above each column in the results window are 
also buttons that can be used to initiate sorting.  If we search for "Smith," 
the database generates 75 matching records. If the researcher wants to 
organize all 75 records in chronological order based on "Age," s/he can click 
on the heading "Age" and the 75 results will be reorganized from youngest to 
oldest.  The same type of sorting may be achieved by clicking on any column 
heading.  If the user clicks on "First Name," the same 75 results would be 
sorted in alphabetical order.  
</p><p class="header">

Bibliographic Source
</p><p>
U.S. Congress. Senate. The History and Growth of the United States Census 
Prepared for the Senate Committee on the Census by Carroll D. Wright, 
Commissioner of Labor, S. Doc. 194, 56th Congress, 1st Session, 1900.
</p>



   
  <p>&nbsp;</p>
  <p><a 
href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a> 
&middot; <a href="/about.php">About this site</a> &middot; Copyright &#169; 
2001 - 2006. 
Trudi J. Abel. All Rights Reserved. </p>  
  </div>
  <div id="copyright">
    <p>The copyright interest in the material in this digital collection has not been transferred to the Digital Durham project. These text and images may not be used for any commercial purpose without the permission of the Duke University Rare Book, Manuscript, and Special Collections Library and the Digital Durham Project. Copyright permission for subsequent uses is the responsibility of the user.</p>
  </div></div>
</div>
</body>
</html>
