var currentMenu = 0;
var savedArrayValue = new Array();

function getMenuOption(menu, ddu_menu) {

	var x = document.getElementById(menu);
	
	

}


function createNewMenu(ddu_menu) {

	var table = document.getElementById("dduTable");
	var tbody = document.getElementById("dduTBody");
	var tr = document.createElement("tr");
	tr.setAttribute("id", "dduTr_" + currentMenu);
	
	var td = document.createElement("td");
	td.setAttribute("id", "dduTd_" + currentMenu);
        td.setAttribute("class", "search_criteria");
	
	var select = document.createElement("select");

	select.setAttribute("name", "dduMenu_" + currentMenu);
	select.setAttribute("currentmenu", "" + currentMenu + "");
	select.setAttribute("id", "dduMenu_" + currentMenu);
	select.setAttribute("class", "search2");


	
	for (var q = 0; q < ddu_menu.length; q++) {
		select.options[q] = new Option(ddu_menu[q]["value"], ddu_menu[q]["name"], false, false);
			
	}
	
	td.appendChild(select);
	
	tr.appendChild(td);
	tbody.appendChild(tr);

	var sname = document.getElementById("dduMenu_" + currentMenu);
	sname.onchange=function(){
		sCurrentMenu = sname.getAttribute("currentmenu");
		showMenuInput(this, sCurrentMenu, this.selectedIndex);
	}

	showMenuInput(select, currentMenu, select.selectedIndex);
	currentMenu = currentMenu + 1;

}

function removeMenu() {
	
	if (currentMenu > 1) {
		currentMenu = currentMenu - 1;

	        var tbody = document.getElementById("dduTBody");
		var tr = document.getElementById("dduTr_" + currentMenu);
	
		tbody.removeChild(tr);
	}
}

function showMenuInput(item, menuNumber, position) {

	var menu = document.getElementById("dduTr_" + menuNumber);
	var itemName = item.options[item.selectedIndex].value;

	
	if (document.getElementById("dduOption_" + menuNumber) != null) {
	
		var td = document.getElementById("dduOption_" + menuNumber);

		if (document.getElementById("dduMenu_" + menuNumber + "_value").type == "text") { 

			if (savedArrayValue[position] == null) {
				savedArrayValue[position] = new Array();
			}


			savedArrayValue[position] = document.getElementById("dduMenu_" + menuNumber + "_value").value;

		} else {
		
			if (savedArrayValue[position] != null) {
				savedArrayValue[position] = savedArrayValue[position];
			}
		
		}
		
			
		td.innerHTML = "";
		
	
	} else {
	
		var td = document.createElement("td");
		td.setAttribute("id", "dduOption_" + menuNumber);
		td.setAttribute("width", "100%");

	}
	
	if (ddu_menu_option[position]["type"] == "select") {
		
		
		itemName = eval(itemName);
		var type = document.createElement("select");
		type.setAttribute("name", item.name + "_value");
		type.setAttribute("id", item.name + "_value");
		
		
		for (var i = 0; i < itemName.length; i++) {
			type.options[i] = new Option(itemName[i], itemName[i], false, false);
		}
			

	} else {
		
		var type = document.createElement("input");
		type.setAttribute("name", item.name + "_value");
		type.setAttribute("id", item.name + "_value");
		type.setAttribute("size", "35");
                type.setAttribute("class", "search2");
		
		if (savedArrayValue[position] != null) {
			type.setAttribute("value", savedArrayValue[position]);
		}
		
	}
	
	td.appendChild(type);
	menu.appendChild(td);
	
	
	
}


function selectMenuType(menu, menuOption) {



}

function closeAdvanced() {

	if (document.getElementById("advancedSearch") != null) {
		var x = document.getElementById("advancedSearch");
		
		x.innerHTML = "";
	}

}

function advancedSearch() {

	if (document.getElementById("advancedSearch") != null) {
		var x = document.getElementById("advancedSearch");
		
		x.innerHTML = "<P style=\"width: 70%;display:block;border:1px solid gray;background:#f8f8f8;padding:5px;\">The Digital Durham advanced search interface is already integrated into this page.  Simply select the Add Criteria option from below for each new term you wish to add to your search.  Use the drop-drop menu to select the type of information you wish to search from the collection. <br /><a href=\"javascript:closeAdvanced();\">Close</a></P>";
	}

}
