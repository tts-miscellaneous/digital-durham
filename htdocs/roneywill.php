<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Digital Durham</title>
<link rel="stylesheet" type="text/css" media="screen" href="/ui/css/style.css" />
</head>

<body>

<div id="contain">
<div id="top">

<div id="search_box_top"><div id="search_box_left">
<form method="get" action="/search">
      <input class="form_textbox" id="q" name="q" type="text" alt="Search Box" style="width:140px;" />
	  <input type="submit" name="submit" value="Go" />
      <input type="hidden" name="site" value="duke_collection" />
      <input type="hidden" name="client" value="digitaldurham" />
      <input type="hidden" name="proxystylesheet" value="digitaldurham" />
      <input type="hidden" name="output" value="xml_no_dtd" />
    </form>
</div></div>

<a href="/"><img src="/images/dd_logo3.gif" /></a>

</div>

<div id="nav">
  <?php

if(isset($x) && $x != "")
  include_once($x.".php");
else
  include_once("nav.php");

?>
</div>

<div id="content">
<div class="margins">
<div class="home_right"><p class="header"><a href="/public.php">Public
Records</a></p>

<p class="header"><a href="/wills.php">Wills</a></p>
<p class="header"><i><b>Will of Elizabeth Roney</b></i></p>
      <hr>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="19%"></td>
          <td width="81%"><b>This being the twenty 
fourth 
            day of August In the year of our Lord Eighteen Hundred and Eighty 
            Two. I Elizabeth L. Roney of the Town of Durham and the State of 
North 
            Carolina being of sound mind and memory, do this day make my last 
            will and testament. First of all I will my body to the grave, and 
            my soul to God who gave it. It is my will after paying all my 
burial 
            expenses and all my just debts what little of this worlds good I 
have 
            left I will try to advise the best I know how God being my helper. 
            </b></td>

        </tr>
        <tr> 
          <td width="19%"> 
            <div align="center"><b>Item 1st</b></div>
          </td>
          <td width="81%"><b>I will and bequeath to my 
Great  Neice Mary Washington Lyon Three Hundred Dollars in Money and my 
best bed and bedding.</b></td>
        </tr>
        <tr> 
          <td width="19%"> 
            <div align="center"><b>Item 2nd</b></div>

          </td>
          <td width="81%"><b>I will and bequeath my widowed 
            sister-Mrs. M. J. Rogers. One Hundred and Fifty Dollars</b></td>
        </tr>
        <tr> 
          <td width="19%"> 
            <div align="center"><b>Item 3rd</b></div>
          </td>
          <td width="81%"><b>I will and bequeath to 
Jennie 
            Proctor. One Hundred Dollars in Money and one bed and bedding. 
Also 
            the set of Furniture in the room I now occupy.</b></td>

        </tr>
        <tr> 
          <td width="19%"> 
            <div align="center"><b>Item 4th</b></div>
          </td>
          <td width="81%"><b>I will and bequeath to my 
sister 
            Annie Five Hundred Dollars in Money and anything else about the 
house 
            that belongs</b></td>
        </tr>
      </table>
      <p><a href="/images/RONEY1.JPG" target="_BLANK">View Page 1 of Roney 
Will</a></p>

      <hr>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="19%"></td>
          <td width="81%"><b>to me that she wants.</b></td>
        </tr>
        <tr> 
          <td width="19%" height="21"> 
            <div align="center"><b>Item 5th </b></div>

          </td>
          <td width="81%" height="21"><b>I will and bequeath 
            to my Neice Mary-Fielder Roney my watch and chain</b></td>
        </tr>
        <tr> 
          <td width="19%"> 
            <div align="center"><b>Item 6th </b></div>
          </td>
          <td width="81%"><b>I will and bequeath to the 
            Woman's Missionary Aid Society of the M.E. Church South at Durham. 
            Fifty Dollars.</b></td>

        </tr>
        <tr> 
          <td width="19%"> 
            <div align="center"><b>Item 7th</b></div>
          </td>
          <td width="81%"><b>I will and bequeath to my little 
            namesake and Neice Bettie Roney. One Hundred Dollars.</b></td>
        </tr>
        <tr> 
          <td width="19%"> 
            <div align="center"><b>Item 8th </b></div>

          </td>
          <td width="81%"><b>I will and bequeath to my brother 
            Joseph Roney. Fifty Dollars in Money </b></td>
        </tr>
        <tr> 
          <td width="19%"></td>
          <td width="81%"> 
            <p><b>If there is anything left I want my Nephew 
              J.B. Duke to have it. It is also my will for him to attend to this 
              matter for me, and see that everything is done right.</b></p>
                    <p><b>Elizabeth L. Roneys {seal} </b></p>

            <p><b>Witness R.E. Lyon </b></p>
          </td>
        </tr>
      </table>

      <p><a href="/images/roney2.jpg" target="_BLANK">View Page 2 of Roney 
Will</a></p>

 <p><a 
href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a> 
&middot; <a href="/about.php">About this site</a> &middot; Copyright 
� 2001 - 2006. Trudi J. Abel. All Rights Reserved. </p>
<div id="copyright">
    <p>The copyright interest in the material in this digital collection has not been transferred to the Digital Durham project. These text and images may not be used for any commercial purpose without the permission of the Duke University Rare Book, Manuscript, and Special Collections Library and the Digital Durham Project. Copyright permission for subsequent uses is the responsibility of the user.</p>
  </div>
</div>
</div></div></div>


</body>
</html>