<?php

$data =<<< END_DATA

<p class="header">Browse Collection</p>

<p><a href="hueism.php?x=browse&dduMenu_0_value=1&dduMenu_0=locListing&listing&SendSearch=1">Library 
of Congress Subject Browser</a></p>
<p class="indent">Browse the collection through Library of Congress Subject Headings.</p>

<p><a href="hueism.php?x=search">Search the entire collection</a></p>
<p class="indent">Perform basic or advanced searches through the Digital Durham collection.</p>


<table cellpadding="0" cellspacing="0">
  <tr>
    <td class="browse_container"><p><a 
href="hueism.php?x=browse&dduMenu_0_value=1&dduMenu_0=seriesListing&listing&SendSearch=1">Personal Papers</a></p><a href="hueism.php?x=browse&dduMenu_0_value=1&dduMenu_0=seriesListing&listing&SendSearch=1"><img width="185px" src="/images/pp.jpg" /></a></td>
    <td class="browse_container"><p><a 
href="hueism.php?x=browse&dduMenu_0_value=1.4&dduMenu_0=series&SendSearch=1">Ledgers</a></p><a 
href="hueism.php?x=browse&dduMenu_0_value=1.4&dduMenu_0=series&SendSearch=1"><img 
width="185px" src="/images/le.jpg" /></a></td>
    <td class="browse_container"><p><a 
href="hueism.php?x=browse&dduMenu_0_value=ma&dduMenu_0=category&SendSearch=1">Maps</a></p><a 
href="hueism.php?x=browse&dduMenu_0_value=ma&dduMenu_0=category&SendSearch=1"><img 
width="185px" src="/images/ma.jpg" /></a></td>
    <td class="browse_container"><p><a 
href="hueism.php?x=browse&dduMenu_0_value=ph&dduMenu_0=category&SendSearch=1">Photographs</a></p><a 
href="hueism.php?x=browse&dduMenu_0_value=ph&dduMenu_0=category&SendSearch=1"><img 
width="185px" src="/images/ph.jpg" /></a></td>
  </tr>
  <tr>
    <td class="browse_container"><p><a 
href="hueism.php?x=browse&dduMenu_0_value=pw&dduMenu_0=category&SendSearch=1">Printed 
Works</a></p><a 
href="hueism.php?x=browse&dduMenu_0_value=pw&dduMenu_0=category&SendSearch=1"><img 
width="185px" src="/images/pw.jpg" /></a></td>
    <td class="browse_container"><p><a 
href="hueism.php?x=browse&dduMenu_0_value=br&dduMenu_0=category&SendSearch=1">Business Records</a></p><a 
href="hueism.php?x=browse&dduMenu_0_value=br&dduMenu_0=category&SendSearch=1"><img 
width="185px" src="/images/br.jpg" /></a></td>
    <td class="browse_container"><p><a 
href="hueism.php?x=browse&dduMenu_0_value=ep&dduMenu_0=category&SendSearch=1">Miscellany</a></p><a 
href="hueism.php?x=browse&dduMenu_0_value=ep&dduMenu_0=category&SendSearch=1"><img 
width="185px" src="/images/mi.jpg" /></a></td>
    <td class="browse_container"><p><a href="public.php">Public 
Records</a></p><a href="public.php"><img width="185px" 
src="/images/public.jpg" /></a></td>
  </tr>
  <tr>
    <td class="browse_container"><p><a href="audio.php">Audio</a></p><a 
href="audio.php"><img width="185px" src="/images/audio.jpg" /></a></td>
  </tr>
</table>

<!--Public Records<br />
Audio<br />
Reference-->

END_DATA;


// Create a new search object
include_once("common/common.php");
include_once("common/searchClass.php");
$search = new Search();

if (isset($_REQUEST['boolean'])) {
	$boolean = $_REQUEST['boolean'];
} else {
	$boolean = "AND";
}

if (isset($_REQUEST['listing'])) {

	$PHP_SELF = 
"hueism.php?x=browse&dduMenu_0_value=1&dduMenu_0=series&SendSearch=1";
	$result = $search->getResultData($boolean);
	$dOut = "";


	// Show all of the items in the series listing
	if (isset($_REQUEST['dduMenu_0']) && $_REQUEST['dduMenu_0'] == "seriesListing") {

		$dOut .= "<p class=\"header\"><a 
href=\"hueism.php?x=browse\">Browse Collection</a> &rsaquo; Personal Papers 
&rsaquo; " . count($result) . " records found</p>";

		$dOut .= "<form method=\"POST\" action=\"$PHP_SELF\">";
		$dOut .= "<p class=\"header\">Search for individual's records within these personal papers: <br />" . $search->getRecipientSearch() . " <input type=\"submit\" value=\"Search\"></p>";
		$dOut .= "</form>";
		
		$dOut .= "<p class=\"header\">Or view an entire personal paper collection:</p>";
//use for ($i = 0;  when need to display Harvey Wright Letters
                for ($i = 1; $i < count($result); $i++) {		
//		for ($i = 0; $i < count($result); $i++) {
// Harvey Wright Letters
                    
			$dOut .= ("<a href=\"hueism.php?x=browse&dduMenu_0_value=" . $result[$i]["series_id"] . "&dduMenu_0=series&SendSearch=1&orderBy=dc_date\">" . $result[$i]["series_title"] . "</a> " . $result[$i]["daterange"] . " ( <b>" . $result[$i]["series_count"] . "</b> records )<br />\n");
			
			if ($result[$i]["description"] == "") {
				$dOut .= ("The " . $result[$i]["series_title"] . " contains over " . $result[$i]["series_count"] . " records in digital form in this series.  These items are dated between the years of " . $result[$i]["daterange"] . ". <br /><br />\n");
			} else {
				$dOut .= ($result[$i]["description"] . "<br /><br />");
			}
		}
	}


	if (isset($_REQUEST['dduMenu_0']) && $_REQUEST['dduMenu_0'] == "locListing") {

		$dOut .= "<p class=\"header\"><a href=\"hueism.php?x=browse\">Browse Collection</a> &rsaquo; Browse LC Subject Headings &rsaquo; " . count($result) . " records found</p>";
	

//		$dOut .= "<p class=\"header\">These records are in alphabetical order: </p>";
			
		for ($i = 0; $i < count($result); $i++) {
			
			if ($result[$i]["subject_count"] == 1) {
				$record = "record";
			} else {
				$record = "records";
			}

			$dOut .= ("<a 
href=\"hueism.php?x=browse&dduMenu_0_value=" 
. $result[$i]["subject"] . "&dduMenu_0=loc&SendSearch=1\">" . $result[$i]["subject"] . "</a> "  . " ( <b>" . $result[$i]["subject_count"] . "</b> $record )<br />\n");
			
		}
	}	
	
	print ($dOut);
	
} else if (isset($_REQUEST['SendSearch'])) {
	
	
	if (!isset($_REQUEST['roleStatus'])) {
		print ("Order these records by: " . $search->getOrderByMenu() . "<br /><br />");


	}
	
	
	print ($search->getResult($boolean));
	
	

} else {

	print ($data);
	
}



?>

