<?php

include_once("common/common.php");
include_once("common/searchClass.php");

// Create a new search object
$search = new Search();

if (isset($_REQUEST['SendSearch'])) {
	
	// A user submits a search query
	print ($search->getSearch());
	print ($search->getResult());

} else	{

	// Default search page
	print ($search->getSearch());

}

?>
