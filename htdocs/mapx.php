<?php

include_once("common/common.php");
$pdfImages = array();

if (isset($_GET['id']) && $_GET['id'] != "" && is_numeric($_GET['id'])) {
	$workID = (int)$_GET['id'];
} else {
	$dOut = "Our apologies.  We are unable to find this item in our collection.  Please try again in a few moments.";
	print ($dOut);
	exit;
}

if (isset($_GET['p']) && $_GET['p'] != "" && is_numeric($_GET['p'])) {
	$currentPage = $_GET['p'];
} else {
	$currentPage = 1;
}

// Ok, let's try to find this record in our collection
$_query = 
	"SELECT 
		item.collection_prefix, 
		item.item_id, 
		item.category, 
		item.page_count, 
		item.folder, 
		item.item_number, 
		item.dc_title, 
		item.dc_description, 
		item.dc_date, 
		item.copyright, 
		item.Height, 
		item.Width, 
		item.dimensions, 
		item.dc_source, 
		item_creator.creator, 
		item_creator.role, 
		item.notes2,
		item.dc_source, 
		item.reference, 
		item.page_count 
	FROM 
		item, item_creator 
	WHERE 
		item.item_id = ? ANd 
		item.item_id = item_creator.item_id 
	LIMIT 1;";
$_params = array($workID);

try {
$result = R::getAll($_query, $_params);

	if (count($result) == 0) {
		$dOut = "Our apologies.  We are unable to find this item in our collection.  Please try again in a few moments.";
		print ($dOut);
		exit;
	}
} catch (\Exception $e) {
	$dOut = "Our apologies.  We are unable to find this item in our collection.  Please try again in a few moments.";
	print ($dOut);
	exit;
}

$totalPage = 1;

for ($i = 0; $i < count($result); $i++) {

	$title = $result[$i]["dc_title"];
	$collection_prefix = $result[$i]["collection_prefix"];
        $creator = $result[$i]["creator"];
        $role = $result[$i]["role"];
	$category = $result[$i]["category"];
	$folder = $result[$i]["folder"];
	$item_number = $result[$i]["item_number"];
	$description = $result[$i]["dc_description"];
	$source = $result[$i]["dc_source"];
        $date = $result[$i]["dc_date"];
        $copyright = $result[$i]["copyright"];
        $notes = $result[$i]["notes2"];
	$height = $result[$i]["Height"];
	$width = $result[$i]["Width"];
	$dimension = $result[$i]["dimensions"];
	$totalPage = $result[$i]["page_count"];
}

$PHP_SELF = $_SERVER['PHP_SELF'];
$skipMenu = "<select name=\"jumpTo\" ONCHANGE=\"javascript:change(this);\">\n";

// Generate a menu with quick page
for ($i = 0; $i < $totalPage; $i++) {
	if (($i+1) == $currentPage) {
		$selected = "selected";
	} else {
		$selected = "";
	}
	
	$skipMenu .= "<option value=\"$PHP_SELF?x=mapx&id=$workID&p=" . ($i+1) . "\" $selected>" . ($i+1) . "</option>\n";
}

$skipMenu .= "</select>\n";

		if ($currentPage < 10) {
			$cntItem = "00" . ($i+1) . "0";
			$cntLargeImage = "00" . "$currentPage" . "0";
		} else if ($currentPage >= 100) {
			$cntItem =  ($i+1) . "0";
			$cntLargeImage =  "$currentPage" . "0";
		} else {
			$cntItem = ($i+1) ;
			$cntLargeImage = "0" . "$currentPage" . "0";
		}
			
	for ($q = 1; $q < $totalPage+1; $q++) {


		if (($q+1) < 10) {
			$pdfImage = "00" . "$q" . "0";
		} else if (($q+1) >= 100) {
			$pdfImage = "$q" . "0";
		} else {
			$pdfImage = "0" . "$q" . "0";
		}	
		
		$pdfItem = $imageBase . "/600/" . $collection_prefix . $category . $folder . $item_number . $pdfImage . ".jpg";
		array_push($pdfImages, $pdfItem);


	}


	$miniImages = "";	
	$largeLetterImage = $imageBase . "/full/" . $collection_prefix . $category . $folder . $item_number . $cntLargeImage . ".jpg";
	$medLetterImage = $imageBase . "/600/" . $collection_prefix . $category . $folder . $item_number . $cntLargeImage . ".jpg";

	if (!file_url_exists("$imageBase_basepath/$medLetterImage")) {
		$medLetterImage = "/images/notfound400.gif";
	} else {
		$medWork = "<a href=\"$largeLetterImage\" target=\"_new\"><img src=\"$medLetterImage\" border=\"0\" width=\"400px\"/></a>";
	}

	$largeWork = "<a href=\"$largeLetterImage\" target=\"_new\"><img src=\"$medLetterImage\" border=\"0\" width=\"600px\"/></a>";

	if (!file_url_exists("$imageBase_basepath/$largeLetterImage")) {
		$largeWork = "<a href=\"/images/notfound400.gif\" target=\"_new\"><img src=\"/images/notfound400.gif\" border=\"0\" width=\"400px\"/></a>";
	}
	
	try {
		$loc = R::getAll("SELECT item_subject.subjterm FROM item_subject WHERE item_id = ?", array($workID));
	}
	catch (\Exception $e) {
		$loc = array();
	}

	$subject = "";
	
	for ($i = 0; $i < count($loc); $i++) {
		$subject .= "<a 
href=\"hueism.php?dduMenu_0=subjectheading&dduMenu_0_value=" . $loc[$i]["subjterm"] . "&SendSearch=Search&x=search&SendSearch=1\">" . $loc[$i]["subjterm"] . "</a><br />";
	}

	try {
		$spat = R::getAll("SELECT item_spatial.spatialTerm FROM item_spatial WHERE item_id = ?", array($workID));
	}
	catch (\Exception $e) {
		$spat = array();
	}
	$spatial = "";
	
	if (count($spat) > 0) {
		$spatial .= "<b>Geographic Term:</b><br />";
	}
		
	for ($i = 0; $i < count($spat); $i++) {
		$spatial .= "<a 
href=\"hueism.php?dduMenu_0=spatial&dduMenu_0_value=" . 
$spat[$i]["spatialTerm"] . 
"&SendSearch=Search&x=search&SendSearch=1\">" . $spat[$i]["spatialTerm"] . "</a><br />";
	}

	if ($title != "") {
		$titleData = "<b>Title:</b><br />$title<br />";
	} else {
		$titleData = "";
	}

	if ($description != "") {
		$descriptionData = "<b>Description:</b><br />$description<br />";
	} else {
		$descriptionData = "";
	}

	if ($source != "") {
		$sourceData = "<b>Source:</b><br />$source<br />";
	} else {
		$sourceData = "";
	}

	if ($height != "") {
		$heightData = "<b>Height:</b><br />$height<br />";
	} else {
		$heightData = "";
	}

	if ($width != "") {
		$widthData = "<b>Width:</b><br />$width<br />";
	} else {
		$widthData = "";
	}

	if ($dimension != "") {
		$dimensionData = "<b>Dimensions:</b><br />$dimension<br />";
	} else {
		$dimensionData = "";
	}


        if ($creator != "") {
                $creatorData = "<b>Creator:</b><br />$creator, $role<br />";
        } else {
                $creatorData = "creatornot here";
        }


        if ($notes != "") {
                $notes2Data = "<b>Notes:</b><br />$notes<br />";
        } else {
                $notes2Data = "";
        }


	if ($subject != "") {
		$subjectData = "<br /><b>Library of Congress Subject Headings:</b><br />$subject";
	} else {
		$subjectData = "";
	}
	
	if ($spatial != "") {
		$spatialData = "<br />" . $spatial;
	} else {
		$spatialData = "";
	}
        if ($date != "") {
                $dateData = "<b>Date</b>:<br />$date<br />";
        } else {
                $dateData = "";
        }
        if ($copyright != "") {
                $copyrightData = "<b>Copyright</b>:<br />$copyright<br />";
        } else {
                $copyrightData = "";
        }

// Now we will create the thumbnails for the images for this map
for ($i = 0; $i < $totalPage; $i++) {

        if (($i+1) < 10) {
                $cntItem = "00" . ($i+1) . "0";

        } elseif (($i+1) < 100 && ($i+1) >= 10) {
                $cntItem = "0" . ($i+1) . "0";
                $cntItem = "0" . ($i+1) . "0";
        }

        $thumbnailMap =  $imageBase . "/150/" . $collection_prefix .$category . $folder . $item_number . $cntItem . ".jpg";

        if (!file_url_exists("$imageBase_basepath/$thumbnailMap")) {
                $thumbnailMap = "/images/notfound150.gif";
        }

        if (file_url_exists("$imageBase_basepath/$miniImages")) {
                $miniImages  .= "<a href=\"hueism.php?x=mapx&p=" . ($i+1) ."&id=$workID\"> <img class=\"thumb\" src=\"/images/notfound150.gif\"border=\"0\" width=\"50px\"/></a>";
        } else {
                $miniImages  .= "<a href=\"hueism.php?x=mapx&p=" . ($i+1) ."&id=$workID\"> <img class=\"thumb\" src=\"$thumbnailMap\" border=\"0\"width=\"50px\"/></a>";
        }

}

$nextPage = $_SERVER['PHP_SELF'];

if ($currentPage == 1 && $totalPage != 1) {
	$nextPage = "&laquo; Previous | <a href=\"$nextPage?x=mapx&p=" . ($currentPage+1) . "&id=$workID\">Next &raquo;</a>";
} elseif ($currentPage < $totalPage) {
	$nextPage = "<a href=\"$nextPage?x=mapx&p=" . ($currentPage-1) . "&id=$workID\">&laquo; Previous</a> | <a href=\"$nextPage?x=mapx&p=" . ($currentPage+1) . "&id=$workID\">Next &raquo;</a>";
} elseif($currentPage != 1) {
	$nextPage = "<a href=\"$nextPage?x=mapx&p=" . ($currentPage-1) . "&id=$workID\">&laquo; Previous</a> | Next &raquo;";
} else {
	$nextPage = "";
}

$nextPageMod = ereg_replace("&middot; ", "", $nextPage);


if (isset($_REQUEST['print'])) {
	include_once("print.php");
	makePDF($pdfImages);
	exit;
}


$goto = '';
if ($totalPage != '1') {
  $goto = "Go to Page: $skipMenu of $totalPage";
}
$dOut =<<< END_DATA

<script type="text/javascript">

	function change(obj) {
		window.location.href = obj.value;
	}

</script>

<p class="header"><a href="hueism.php?x=browse">Browse Collection</a> &rsaquo; <a href="hueism.php?x=browse&dduMenu_0_value=ma&dduMenu_0=category&SendSearch=1">Maps</a> &rsaquo; $title</p>

<table cellpadding="0" cellspacing="0">
  <tr>
 	 <td class="map_left"><div align="center">$goto<br />$medWork</div>
    <p>
<br/>
        $miniImages
   </p>
</td>
    <td>
		<p class="headerInText">Page $currentPage/$totalPage <br 
/>$nextPage</p><P>[ <a href="mapx.php?id=$workID&print">Printer-Friendly Version</a> ]</P>
      <!-- <p>(<a href="javascript:toggle();">Toggle MetaData View</a>)</p> -->

		<div id="metadata" class="spacing"></div>
			$titleData
                        $creatorData
                        $dateData
			$descriptionData
			$sourceData
			$dimensionData
                        $copyrightData
			$notes2Data
			$subjectData
			$spatialData
		<!-- </div>	-->	
				

	</td>
   </tr>
</table>

<script language="javascript" type="text/javascript">
	document.getElementById("metadata").style.display = "none";

function toggle() {
  if(document.getElementById("metadata").style.display == "none")
	document.getElementById("metadata").style.display = "block";
  else
    document.getElementById("metadata").style.display = "none";
}

</script>
END_DATA;

print ($dOut);



function file_url_exists($url) {
	$handle = @fopen($url, "r");
	 if ($handle === false)
		  return false;
	 fclose($handle);
	 return true;
}


?>
