<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Digital Durham</title>
<link rel="stylesheet" type="text/css" media="screen" href="/ui/css/style.css" />
</head>

<body>

<div id="contain">
<div id="top">

<div id="search_box_top"><div id="search_box_left">
<form method="get" action="/search">
      <input class="form_textbox" id="q" name="q" type="text" alt="Search Box" style="width:140px;" />
	  <input type="submit" name="submit" value="Go" />
      <input type="hidden" name="site" value="duke_collection" />
      <input type="hidden" name="client" value="digitaldurham" />
      <input type="hidden" name="proxystylesheet" value="digitaldurham" />
      <input type="hidden" name="output" value="xml_no_dtd" />
    </form>
	
</div></div>

<a href="/"><img src="/images/dd_logo3.gif" /></a>

</div>

<div id="nav">
  <?php

if(isset($x) && $x != "")
  include_once($x.".php");
else
  include_once("nav.php");

?>
</div>

<div id="content">
<div class="margins">

  <div class="home_right"><p class="header"><a href="about.php">About this site</a> | Overview | <a href="funding.php">Funding</a> </p>

    <p>For many years scholars have recognized that late nineteenth-century Durham, North Carolina makes an ideal case study for examining emancipation, industrialization, immigration, and urbanization in the context of the New South. &quot;With its tobacco factories, textile mills, black entrepreneurs, and new college,&quot; the historian Syd Nathans observes, &quot;Durham was a hub of enterprise and hope.&quot; By the early twentieth century, Durham became renowned for its vibrant entrepreneurial spirit. Both W.E.B. Du Bois and Booker T. Washington wrote articles for the national press about their visits with members of Durham's African-American community. After his visit in 1910, Booker T. Washington dubbed Durham the &quot;City of Negro Enterprises.&quot; </p>
    <p>The Digital Durham website offers students, teachers, and researchers a range of primary sources with which they can investigate the economic, social, cultural, and political history of a post-bellum southern community. Letters from mothers to daughters, parents to children, and husbands and wives give insight into the domestic lives of some of Durham's elite citizens. &nbsp; Entries from Atlas Rigsbee's general store ledger together with data from the 1880 census provide a view into the social experience of those Durham citizens who have not left written documents. Taken together the new materials on Digital Durham touch on over 600 topics including African American business enterprise, the emergence of textiles, tobacco production and marketing, child labor, prohibition, evangelical revivalism, nineteenth-century medical practices, women's experience of childbirth, and public and private education. </p>
    <p>The holdings of the website provide access to a wide range of manuscript and printed materials from the 1870s through the 1920s. The site also features a new collection of maps that depict Durham from the late 1860s to the present day. Digital Durham offers its users a selection of manuscript letters taken from the Southgate-Jones family papers and James Southgate papers, accounts from Atlas M. Rigsbee's general store ledger, photographs, ephemera, census data,  printed works as well as a rich collection of maps. The site also includes audio postcards created by Duke students in an undergraduate research seminar on the history of Durham and the New South. Additionally, the site includes resources for K-12 educators--a reference section with glossary of terms used in the 1880 census and lesson plans that tie to the North Carolina Standard Course of Study. <br>
    </p>
    <p><a 
href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a> 
&middot; <a href="about.php">About this site</a> &middot; Copyright &#169; 2001 - 2009. 
Trudi J. Abel. All Rights Reserved. <br>
<br>
    </p>
<div id="copyright">
    <p>The copyright interest in the material in this digital collection has not been transferred to the Digital Durham project. These text and images may not be used for any commercial purpose without the permission of the Duke University Rare Book, Manuscript, and Special Collections Library and the Digital Durham Project. Copyright permission for subsequent uses is the responsibility of the user.</p>
  </div></div></div>
</div>
</div>





</body>
</html>
