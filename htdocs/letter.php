<?php

include_once("common/teiParse.php");
include_once("common/common.php");
include_once("common/paths.php");

if (isset($_GET['id']) && $_GET['id'] != "" && is_numeric($_GET['id'])) {
	$letterID = (int)$_GET['id'];
} else {
	$dOut = "Our apologies.  We are unable to find this item in our collection.  Please try again in a few moments.";
	print ($dOut);
	exit;
}

// Ok, let's try to find this record in our collection

$_query =
	"SELECT 
		item.collection_prefix, 
		item.item_id, 
		item.category, 
		item.folder, 
		item.item_number, 
		item.dc_title, 
		item.dc_description, 
		item.reference, 
		item.page_count 
	FROM 
		item 
	WHERE 
		item_id = ?
	LIMIT 1;";
$_params = array($letterID);

try {
	$result = R::getAll($_query, $_params);
	if (count($result) == 0) {
		$dOut = "Our apologies.  We are unable to find this item in our collection.  Please try again in a few moments.";
		print ($dOut);
		exit;
	}
}
catch (\Exception $e) {
	$dOut = "Our apologies.  We are unable to find this item in our collection.  Please try again in a few moments.";
	print ($dOut);
	exit;
}

$xmlDocument = "";
$noticeText = "";
for ($z = 0; $z < count($result); $z++) {
	$xmlDocument = "tei/" . $result[$z]["folder"] . $result[$z]["item_number"] . ".xml";
	$pageCount = $result[$z]["page_count"];
	$tempTitle = $result[$z]["dc_title"];
	
	$collection_prefix = $result[$z]["collection_prefix"];
	$category = $result[$z]["category"];
	$folder = $result[$z]["folder"];
	$item_number = $result[$z]["item_number"];
	
	
}

$tei = true;

if (!file_exists($xmlDocument)) {
	$tei = false;
}

if ($tei) {
	$xml = new teiParse($xmlDocument);
	$xml->parse();
	
	$dOut = "";
	
	$xmlRecord = $xml->getTitle();
	$title = "";
	
	for ($i = 0; $i < count($xmlRecord); $i++) {
		foreach($xmlRecord[$i] as $heading => $value) {
			if ($heading == "title") {
				$title .= $value . "<br />";
			}
		}
	}
	
	$xmlRecord = $xml->getAuthor();
	$author = "";
	
	for ($i = 0; $i < count($xmlRecord); $i++) {
		foreach($xmlRecord[$i] as $heading => $value) {
			if ($heading == "author") {
				$author .= $value . "<br />";
			}
		}
	}
	
	$xmlRecord = $xml->getFunder();
	$funder = "";
	
	for ($i = 0; $i < count($xmlRecord); $i++) {
		foreach($xmlRecord[$i] as $heading => $value) {
			if ($heading == "funder") {
				$funder .= $value . "<br />";
			}
		}
	}
	
	$xmlRecord = $xml->getTranscription();
	$transcription = "";
	
	for ($i = 0; $i < count($xmlRecord); $i++) {
		foreach($xmlRecord[$i] as $heading => $value) {
				$transcription .= ucfirst($heading) . ": " . $value . "<br />";
		}
	}
	
	$xmlRecord = $xml->getEdition();
	$edition = "";
	
	for ($i = 0; $i < count($xmlRecord); $i++) {
		foreach($xmlRecord[$i] as $heading => $value) {
				$edition .= ucfirst($heading) . ": " . $value . "<br />";
		}
	}
	
	
	$xmlRecord = $xml->getPublication();
	$publication = "";
	
	for ($i = 0; $i < count($xmlRecord); $i++) {
		foreach($xmlRecord[$i] as $heading => $value) {
				$publication .= ucfirst($heading) . ": " . $value . "<br />";
		}
	}
	
	$xmlRecord = $xml->getSource();
	$source = "";
	
	for ($i = 0; $i < count($xmlRecord); $i++) {
		foreach($xmlRecord[$i] as $heading => $value) {
				$source .= ucfirst($heading) . ": " . $value . "<br />";
		}
	}
	try {
		$metadata_a = R::getAll("SELECT item_subject.subjterm FROM item_subject WHERE item_subject.item_id = ?", array($letterID));
		$metadata_b = R::getAll("SELECT item_creator.creator AS subjterm FROM item_creator WHERE item_creator.item_id = ?", array($letterID));
		$metadata_c = R::getAll("SELECT item_spatial.spatialTerm AS subjterm FROM item_spatial WHERE item_spatial.item_id = ?", array($letterID));
		$metadata_q = array_merge($metadata_a, $metadata_b, $metadata_c);		
	}
	catch (\Exception $e) {
		$metadata_q = array();
	}
	
	//$xmlRecord = $xml->getProfile();
	$profile = "";
	$profileLite = ""; 

	for($i=0; $i < count($metadata_q); $i++) {
		$profile .= "Class: <!--<a href=\"hueism.php?dduMenu_0=subjectheading&dduMenu_0_value=".$metadata_q[$i]["subjterm"]."&SendSearch=Search&x=search&SendSearch=1\">-->".$metadata_q[$i]["subjterm"]."<!--</a>-->" . "<br />";
		$profileLite .= "<!--<a class='subject' href=\"hueism.php?dduMenu_0=subjectheading&dduMenu_0_value=".$metadata_q[$i]["subjterm"]."&SendSearch=Search&x=search&SendSearch=1\">-->".$metadata_q[$i]["subjterm"]."<!--</a>-->" . "<br />";

	}
		
	
	// Now that we have assembled the metadata, let's actually get
	// the text for this document
	$text = $xml->getText();


}  else {

	// No TEI document found.
	$text = "";
	//$text = "A Text Encoding Initative (TEI) file was not found.  Therefore no accompanying metadata or transcription information can be provided for this resource.<br />";
	$title = $tempTitle . "<br />";
	$noticeText = "A Text Encoding Initative (TEI) file was not found.  Therefore no accompanying metadata or transcription information can be provided for this resource.<br />";


	$author = "Unknown<br />";
	$funder = "Unknown<br />";
	$transcription = "Unknown<br />";
	$edition = "Unknown<br />";
	$publication = "Unknown<br />";
	$source = "Unknown<br />";
	$profile = "Unknown<br />";
	$profileLite = "Unknown<br />";
	$dOut = "";


	try {
		$metadata_a = R::getAll("SELECT item_subject.subjterm FROM item_subject WHERE item_subject.item_id = ?", array($letterID));
		$metadata_b = R::getAll("SELECT item_creator.creator AS subjterm FROM item_creator WHERE item_creator.item_id = ?", array($letterID));
		$metadata_c = R::getAll("SELECT item_spatial.spatialTerm AS subjterm FROM item_spatial WHERE item_spatial.item_id = ?", array($letterID));
		$metadata_q = array_merge($metadata_a, $metadata_b, $metadata_c);
	}
	catch (\Exception $e) {
		$metadata_q = array();
	}	
	//$xmlRecord = $xml->getProfile();
	$profile = "";
	$profileLite = ""; 

	for($i=0; $i < count($metadata_q); $i++) {
		$profile .= "Class: <!--<a href=\"hueism.php?dduMenu_0=subjectheading&dduMenu_0_value=".$metadata_q[$i]["subjterm"]."&SendSearch=Search&x=search&SendSearch=1\">-->".$metadata_q[$i]["subjterm"]."<!--</a>-->" . "<br />";
		$profileLite .= "<!--<a class='subject' href=\"hueism.php?dduMenu_0=subjectheading&dduMenu_0_value=".$metadata_q[$i]["subjterm"]."&SendSearch=Search&x=search&SendSearch=1\">-->".$metadata_q[$i]["subjterm"]."<!--</a>-->" . "<br />";

	}
	
}

// Now we only want to show the current page that we are viewing. 
// Therefore, we will remove any content, after the body tags that
// is not on the current page.

if (isset($_GET['p'])) {
	$page = (int)$_GET['p'];
} else {
	$page = 1;
}


// Let's get a total page count for the entire
// number of documents

if ($pageCount == "") {
	$pageCount = 1;
}


// If we are on the first page, the we will search for the subsequent 
// next page.  If found, we will remove it



if ($tei) {
	if ($page == 1) {
		$startPattern = "<body>";
		$endPattern = "<pb id=\"p" . ($page+1) . "\" n=\"" . ($page+1) . "\"/>";
		$sPos = strpos($text, $startPattern);
		$ePos = strpos($text, $endPattern);
		$diffLength = $ePos - $sPos;
		$text = substr($text, $sPos, $diffLength);
	
	} else {
	
		// So we are not at the first page.
		// The start position would naturally be 
		// where this portion of the letter starts and the
		// end position will be either the next page or 
		// the end of the document, whichever comes first
	
		$startPattern = "<pb id=\"p" . ($page) . "\" n=\"" . ($page) . "\"/>";	
		$endPattern = "<pb id=\"p" . ($page+1) . "\" n=\"" . ($page+1) . "\"/>";
		$endOfDoc = "</body>";
		
		$sPos = strpos($text, $startPattern);
		$ePos = strpos($text, $endPattern);
		$ePos2 = strpos($text, $endOfDoc);
			
		if ($sPos == "") {
			// This page is invalid, assume first page
			$page = 1;
			$startPattern = "<body>";
			$endPattern = "<pb id=\"p" . ($page+1) . "\" n=\"" . ($page+1) . "\"/>";
			$sPos = strpos($text, $startPattern);
			$ePos = strpos($text, $endPattern);
			$diffLength = $ePos - $sPos;
			$text = substr($text, $sPos, $diffLength);		
		
		} else {
	
			// The page exists, therefore we will continue to find the end of the 
			// document
			
			if ($ePos == "") {
				// So there is no next page, therefore, use the end of document
				$ePos = $ePos2;
			}
			
			$diffLength = $ePos - $sPos;
			$text = substr($text, $sPos, $diffLength);		
		}
	
	}
} else {

	// There is no TEI document
	$page = $page;

}


$miniImages = "";
//$imageBase = "http://trinity1.aas.duke.edu/images/digitaldurham";
if ($page < 10) {
                $cntLargeImage = "00" . ($page) . "0";
}
else {
                $cntLargeImage = "0" . ($page) . "0";
}

// Now we will create the thumbnails for the images for this personal letter
for ($i = 0; $i < $pageCount; $i++) {
	
	if (($i+1) < 10) {
		$cntItem = "00" . ($i+1) . "0";
		
	} elseif (($i+1) < 100 && ($i+1) >= 10) {
		$cntItem = "0" . ($i+1) . "0";
		$cntItem = "0" . ($i+1) . "0";

	}
	
	
	$thumbnailLetter = $imageBase . "/150/" . $collection_prefix . $category . $folder . $item_number . $cntItem . ".jpg";
	
		$miniImages .= "<a href=\"hueism.php?x=letter&p=" . ($i+1) . "&id=$letterID\"><img class=\"thumb\" src=\"$thumbnailLetter\" border=\"0\" width=\"50px\"/></a>";
	
	
	$largeLetterImage = $imageBase . "/full/" . $collection_prefix . $category . $folder . $item_number . $cntLargeImage . ".jpg";
	$medLetterImage = $imageBase . "/600/" . $collection_prefix . $category . $folder . $item_number . $cntLargeImage . ".jpg";

		$largeLetter = "<a href=\"$largeLetterImage\" target=\"_new\"><img src=\"$medLetterImage\" border=\"0\" width=\"400px\"/></a>";
}

// We now need to do some preparation before we can send
// the text directly to the browser
$text = ereg_replace("<body>", "", $text);
$text = ereg_replace("</body>", "", $text);
$text = ereg_replace("<lb/>", "<br />", $text);
$text = ereg_replace("</dateline>", "</dateline><br />", $text);
$text = ereg_replace("</salute>", "</salute><p />", $text);



//preg_match("/\<sic corr=\"([A-Za-z0-9]+)\">([A-Za-z0-9]+)<\/sic>/",  $text, $matches);
$text = preg_replace("/\<sic corr=\"([^\"]+)\">([^<]+)<\/sic>/",  "$2 (Correction: $1)", $text);
$text = preg_replace("/\<abbr expan=\"([^\"]+)\">([^<]+)<\/abbr>/",  "$2 (Expanded: $1)", $text);
$text = preg_replace("/\<emph rend=\"underline\">([^<]+)<\/emph>/",  "<u>$1</u>", $text);
$text = preg_replace("/\<emph rend=\"super\">([^<]+)<\/emph>/",  "<sup>$1</sup>", $text);
$text = preg_replace("/\<emph rend=\"sub\">([^<]+)<\/emph>/",  "<sub>$1</sub>", $text);
$text = preg_replace("/\<hi rend=\"super\">([^<]+)<\/hi>/",  "<sup>$1</sup>", $text);
$text = preg_replace("/\<hi rend=\"sub\">([^<]+)<\/hi>/",  "<sub>$1</sub>", $text);



$nextPage = $_SERVER['PHP_SELF'];

if ($page == 1) {
	$nextPage = "&middot; &laquo; Previous | <a href=\"$nextPage?x=letter&p=" . ($page+1) . "&id=$letterID\">Next &raquo;</a>";
} elseif ($page < $pageCount) {
	$nextPage = "&middot; <a href=\"$nextPage?x=letter&p=" . ($page-1) . "&id=$letterID\">&laquo; Previous</a> | <a href=\"$nextPage?x=letter&p=" . ($page+1) . "&id=$letterID\">Next &raquo;</a>";
} elseif($page != 1) {
	$nextPage = "&middot; <a href=\"$nextPage?x=letter&p=" . ($page-1) . "&id=$letterID\">&laquo; Previous</a> | Next &raquo;";
} else {
	$nextPage = "";
}

$dOut .=<<< END_DOC


<p class="header"><a href="hueism.php?x=browse">Browse Collection</a> &rsaquo; 
<a 
href="hueism.php?x=browse&dduMenu_0_value=1&dduMenu_0=seriesListing&listing&SendSearch=1">Personal 
Papers</a> &rsaquo; $title</p>

<table cellpadding="0" cellspacing="0">
  <tr>
  <td class="letter_left">$largeLetter</td>
  <td>
	<p class="headerInText">Page $page/$pageCount $nextPage</p>
    <p>(<a href="javascript:toggle();">Toggle MetaData View</a> / <a href="javascript:toggleSubject();">View Subject Headings</a> / <a href="$xmlDocument">TEI Transcription</a> / <a href="printedwork.php?id=$letterID&print">Printer-Friendly Version</a>)</p>
    <div id="metadata" class="spacing">
		<b>Title:</b><br />$title
		<b>Author:</b><br />$author
		<b>Funder:</b><br />$funder
		<b>Transcription Information:</b><br />$transcription
		<b>Transcription Edition:</b><br />$edition
		<b>Publication Information:</b><br />$publication
		<b>Source Information:</b><br />$source
		<b>Library of Congress Subject Headings:</b><br />$profile
		<hr />
    </div>
    <div id="subjectHeading" class="spacing">
		<b>Library of Congress Subject Headings:</b><br />$profileLite
		<hr />
	</div>
	$text
	$noticeText
	<br />
    <p>
   
    	$miniImages
   </p>
	
  </td>
</tr>

</table>


<script language="javascript" type="text/javascript">
document.getElementById("metadata").style.display = "none";
document.getElementById("subjectHeading").style.display = "none";
function toggle() {
  if(document.getElementById("subjectHeading").style.display == "block")
    document.getElementById("subjectHeading").style.display = "none";

  if(document.getElementById("metadata").style.display == "none")
    document.getElementById("metadata").style.display = "block";
  else
    document.getElementById("metadata").style.display = "none";
}
function toggleSubject() {
  if(document.getElementById("metadata").style.display == "block")
    document.getElementById("metadata").style.display = "none";

  if(document.getElementById("subjectHeading").style.display == "none")
    document.getElementById("subjectHeading").style.display = "block";
  else
    document.getElementById("subjectHeading").style.display = "none";
}
</script>

END_DOC;


print ($dOut);


function file_url_exists($url) {
	$handle = @fopen($url, "r");
	 if ($handle === false)
		  return false;
	 fclose($handle);
	 return true;
}



?>
