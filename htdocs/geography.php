<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Digital Durham</title>
<link rel="stylesheet" type="text/css" media="screen" href="/ui/css/style.css" />
</head>

<body>

<div id="contain">
<div id="top">

<div id="search_box_top"><div id="search_box_left">
<form method="get" action="/search">
      <input class="form_textbox" id="q" name="q" type="text" alt="Search Box" style="width:140px;" />
	  <input type="submit" name="submit" value="Go" />
      <input type="hidden" name="site" value="duke_collection" />
      <input type="hidden" name="client" value="digitaldurham" />
      <input type="hidden" name="proxystylesheet" value="digitaldurham" />
      <input type="hidden" name="output" value="xml_no_dtd" />
    </form>
</div></div>

<a href="/"><img src="/images/dd_logo3.gif" /></a>

</div>

<div id="nav">
  <?php

if(isset($x) && $x != "")
  include_once($x.".php");
else
  include_once("nav.php");

?>
</div>

<div id="content">
<div class="margins">
  <p class="header"><a href="reference.php">Reference</a></p>  

<p class=header>The 
        Geography of the Piedmont Region</p>
      <p>One of the 
        leading businessmen of Durham, Eugene Morehead, depicted Durham on his 
        bank checks as the hub of a wheel. Leo De Colange's <i>National 
Gazetteer</i> 
        published in 1884 illustrates that Durham was an emerging town in 
1880, 
        a rising economic power still dwarfed by the surrounding metropolises 
        of Raleigh, Greensborough, Lynchburg, Richmond, and Norfolk. The 
following 
        descriptions of nearby cities are drawn from De Colange's National 
Gazetteer. 
        Abbreviations have been spelled out in most cases and typographical 
errors 
        have been silently corrected. </p>
      <div style="border-top: 1px solid #CCC;">&nbsp;</div>

      <p><b>Charlotte, N.C., </b>a 
        city, capital of Mecklenburg County, on Sugar Creek, 125 miles. W.S.W. 
        of Raleigh. The Charlotte, Columbia, and Augusta, the Wilmington, 
Charlotte 
        and Rutherford and the North Carolina Railroads meet here. A plank 
road 
        120 miles long connects it with Fayetteville. The city is situated 
upon 
        the gold range of the Atlantic States, and its prosperity is 
principally 
        owing to the working of the mines in its vicinity. A branch mint for 
coining 
        gold was established here in 1838. Under the coinage act of 1873, this 
        establishment ceased to be operated as a mint, but is continued as an 
        assay office. Charlotte contains several schools, churches and cotton 
        factories and 4 national banks. It has 2 daily, 2 tri-weekly, and 4 
weekly 
        papers, and a monthly periodical. The "Mecklenburg Declaration of 
Independence" 
        was adopted here, May 31, 1775. The British troops occupied Charlotte 
        in 1780, and for a time it was the American headquarters. Population 
7,094; 
        of township, 10,547.</p>
      <hr>
      <p><b>Durham, N.C.</b> post-village, 
        money order office and township, Orange County, 26 miles N.W. Raleigh 
        by Richmond & Danville Railroad. It has several snuff and tobacco 
factories, 
        1 bank and 3 weekly papers. The surrender of J.E. Johnson, April 25, 
1865, 
        took place here. Population 2,041 of township 5,507.</p>
      <hr>
      <p><b>Greensborough, N.C.</b>, post-village, 
        money order office, in Gilmer township, capital of Guilford County, 48 
        miles S.W. Danville by Richmond & Danville Railroad. It has a graded 
school, 
        a public hall, 1 national and 1 other bank, a female college. 
Manufacturing, 
        iron, tobacco, lumber, spokes and handles, sashes and blinds &c. 
Population 
        4,996.</p>

      <div style="border-top: 1px solid #CCC;">&nbsp;</div>
      <p><b>Lynchburg, Va</b>., city, money 
        order office, Campbell County, on the S. bank of James River, and on 
the 
        James River and Kanawha Canal, at the junction of Norfolk & Western 
Railroad 
        with Western Maryland line, and also on Richmond & Alleghany Railroad, 
        146 miles W. by S. from Richmond, and 123 miles W. of Petersburg. It 
occupies 
        a steep acclivity rising gradually from the river bank, and breaking 
away 
        into numerous hills, whose terraced walks and ornamented dwellings 
give 
        a picturesque and romantic appearance to the town. Lynchburg is 
favorably 
        situated for a large inland commerce and for manufactures. It has 
tributary 
        to it a great extent of magnificent country, enjoys almost 
inexhaustible 
        water-power, which is yet, however, undeveloped, and is in the 
neighborhood 
        of vast field of coal and iron ore. The celebrated Botetour iron works 
        are not far distant. Tobacco manufacturing, which is the chief 
industry, 
        employs about 40 establishments, and there are two iron foundries, 
besides 
        the extensive machine shops of the Norfolk & Western Railroad company. 
        There are 3 national banks, an insurance and banking company, 2 
savings 
        banks, a court-house, jail, smallpox hospital, female orphan asylum, 2 
        high schools, several private schools, 2 daily, 3 tri-weekly and 3 
weekly 
        papers, and 15 churches. Lynchburg was laid out in 1786 and 
incorporated 
        in 1805. Population in 1870, 6,625; in 1880, 15,959.</p>
      <div style="border-top: 1px solid #CCC;">&nbsp;</div>
      <p><b>Raleigh, N.C.</b>, a city, 
money 
        order office, capital of the State and of Wake County, 6 miles W of 
Neuse 
        River, on the Raleigh & Gaston, Raleigh & Augusta Air Line, and the 
Richmond 
        & Danville Railroads., 97 miles S. W. Weldon, 48 miles N.W. 
Goldsborough 
        and 149 miles N. from Wilmington. Its soil is elevated and healthy. It 
        is very regularly laid out, and possesses many fine streets, shades 
with 
        old oaks, which having attained a giant growth, have conferred upon 
the 
        city the title of the "City of Oaks." The principal public buildings 
are 
        the State House, the new United States Court-House and a post-office, 
        the State geological museum, the State institution for the deaf, dumb, 
        and the blind, the State insane asylum, the State penitentiary, the 
county 
        court-house, and the county jail. There is a large trade in cotton and 
        dry goods. The city contains the shops of the Raleigh & Gaston and the 
        Raleigh & Augusta Air Line railroads, 2 iron foundries, 2 cigar 
manufactures, 
        a manufacturer of pumps, several printing and binding establishments, 
        and 3 national banks. There are 2 public halls, separate public 
schools 
        for white and colored children, 3 female seminaries, several private 
schools, 
        2 libraries in the State house ( the law library with 4,000 volumes, 
and 
        the State library with 25,000 volumes), and 3 daily, 7 weekly and 2 
monthly 
        papers. Raleigh was incorporated as a city in 1794. Population 9,265; 
        of township 13,843. </p>

      <div style="border-top: 1px solid #CCC;">&nbsp;</div>
      <p><b>Sanford, N.C.</b>, 
         post-village, in Jonesboro township, Moore County, 44 
        miles S. of Raleigh by Raleigh & Augusta Air Line. <i>Population 
236</i>. 
  </p><br /><br />  
  <p><a 
href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a> 
&middot; <a href="/about.php">About this site</a> &middot; Copyright � 
2001 - 2006. 
Trudi J. Abel. All Rights Reserved. </p> 
 <div id="copyright">
    <p>The copyright interest in the material in this digital collection has not been transferred to the Digital Durham project. These texts and images may not be used for any commercial purpose without the permission of the Duke University Rare Book, Manuscript, and Special Collections Library and the Digital Durham Project. Copyright permission for subsequent uses is the responsibility of the user.</p>
  </div>
</div></div>

</div>
</body>
</html>
