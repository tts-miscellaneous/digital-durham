<?php

$_authenticated = false;
$_userName = 'trudi';
$_password = '3d98ef72274d4af3c0b2c38b1363cd5cbca5e5839c332fdfb4c0da40cd2551ff';

$_testUser = null;
$_testPass = null;

// the user requires authentication
if ((! empty($_COOKIE['user']) && ! empty($_COOKIE['pw']))) {
	$_testUser = $_COOKIE['user'];
	$_testPass = $_COOKIE['pw'];
}
// the user name HTTP variables were sent
elseif ((! empty($_SERVER['PHP_AUTH_USER'])) && (! empty($_SERVER['PHP_AUTH_PW']))) {
	$_testUser = $_SERVER['PHP_AUTH_USER'];
	$_testPass = hash_hmac('sha256', $_SERVER['PHP_AUTH_PW'], $_testUser);
}

// make sure that the information matches
if ((($_testUser != null) && ($_testPass != null)) && 
		($_testUser == $_userName) && ($_testPass == $_password)) {
	$_authenticated = true;
	setcookie('user', $_testUser);
	setcookie('pw', $_testPass);
}

if (! $_authenticated) {
	header('WWW-Authenticate: '.sprintf('Basic realm="%s"', 'Requires Authentication.'));
	header('HTTP/1.1 401 Unauthorized');
	print('Authentication Required');
	exit;
}

include_once("../common/adminUpdate.php");

$continue = 1;
$error_message = '';
$aU = new adminUpdate;
if(isset($_POST['id'])) {
  $itemNum = $_POST['id'];
  $initial_itemId = $aU->getItemId($itemNum);
  if($initial_itemId == '')
    $error_message = 'Not a valid item Id. Please try again.';
  else
    $continue = 2;
}
if(isset($_GET['id']) && !(isset($_GET['action']))) {
  $itemNum = $_GET['id'];
  $initial_itemId = $aU->getItemId($itemNum);
  if($initial_itemId == '')
    $error_message = 'Not a valid item Id. Please try again.';
  else
    $continue = 2;
}
if((isset($_GET['id'])) && (isset($_GET['action'])) && ($_GET['action'] == "delete") && (! isset($_GET['final']))) {
  $itemNum = $_GET['id'];
  $error_message = 'Are you sure you want to delete "'.$aU->getItemId($itemNum).'"? (<a href="index.php?id='.$itemNum.'&action=delete&final=yes">Yes</a>) (<a href="index.php">Cancel</a>)';
}
if((isset($_GET['id'])) && (isset($_GET['action'])) && ($_GET['action'] == "delete") && (isset($_GET['final'])) && ($_GET['final'] == "yes")) {
  $itemNum = $_GET['id'];
  $aU->removeItem($itemNum);
  header("Location: index.php");
  exit;
}

//echo $error_message;

if(isset($_POST['addNewItem'])) {
  $returnedId = $aU->addItem($_POST['newCollectionPrefix'], $_POST['newCategory'], $_POST['newFolder'], $_POST['newItemNumber']);
  $itemNum = $returnedId;
  if($returnedId != '')
    $continue = 2;
  else {
    $continue = 1;
    $error_message = 'All fields are required when adding a new item.';
  }
}

if(isset($_POST['saveItem'])) {
  $itemNum = $_POST['thisItemId'];
  if($aU->setItemSource($itemNum, $_POST['source'])) {}
  else echo "Source failed to save.";
  
  if($aU->setItemTitle($itemNum, $_POST['title'])) {}
  else echo "Title failed to save.";
  
  if($aU->setItemAlternativeTitle($itemNum, $_POST['alternativeTitle'])) {}
  else echo "Alternative Title failed to save.";
  
  if($aU->setItemCopyright($itemNum, $_POST['copyright'])) {}
  else echo "Copyright failed to save.";
  
  if($aU->setItemDescription($itemNum, $_POST['description'])) {}
  else echo "Description failed to save.";
  
  if($aU->setItemNotes($itemNum, $_POST['notes'])) {}
  else echo "Notes failed to save.";

  if($aU->setItemRanges($itemNum, $_POST['ranges'])) {}
  else echo "Ranges failed to save.";

  if($aU->setItemPageCount($itemNum, $_POST['page_count'])) {}
  else echo "Page Count failed to save.";

  if($aU->setItemDimensions($itemNum, $_POST['dimensions'])) {}
  else echo "Dimensions failed to save.";

 if($aU->setItemNotes2($itemNum, $_POST['notes2'])) {}
  else echo "Notes2 failed to save.";
  
  if($aU->setItemDate($itemNum, $_POST['date'])) {}
  else echo "Date failed to save.";
  
  $continue = 2;
}
if(isset($_POST['addType'])) { $itemNum = $_POST['thisItemId']; $aU->setItemType($itemNum, $_POST['newType']); $continue = 2; }
if(isset($_POST['addCreator'])) { $itemNum = $_POST['thisItemId']; $aU->setItemCreator($itemNum, $_POST['newCreator'], $_POST['newRole']); $continue = 2; }
if(isset($_POST['addFormat'])) { $itemNum = $_POST['thisItemId']; $aU->setItemFormat($itemNum, $_POST['newFormat']); $continue = 2; }
if(isset($_POST['addSeries'])) { $itemNum = $_POST['thisItemId']; $aU->setItemSeries($itemNum, $_POST['newSeriesId']); $continue = 2; }
if(isset($_POST['addSubject'])) { $itemNum = $_POST['thisItemId']; $aU->setItemSubject($itemNum, $_POST['newSubject'], $_POST['newSource']); $continue = 2; }
if(isset($_POST['addSpatial'])) { $itemNum = $_POST['thisItemId']; $aU->setItemSpatial($itemNum, $_POST['newSpatialSource'], $_POST['newSpatial']); $continue = 2; }

if(isset($_GET['remove']) && $_GET['remove'] == "itemType") { $itemNum = $_GET['id']; $aU->removeItemType($_GET['id'], $_GET['typeTerm']); $continue = 2; }
if(isset($_GET['remove']) && $_GET['remove'] == "itemCreator") { $itemNum = $_GET['id']; $aU->removeItemCreator($_GET['id'], $_GET['creator'], $_GET['role']); $continue = 2; }
if(isset($_GET['remove']) && $_GET['remove'] == "itemFormat") { $itemNum = $_GET['id']; $aU->removeItemFormat($_GET['id'], $_GET['formatterm']); $continue = 2; }
if(isset($_GET['remove']) && $_GET['remove'] == "itemSubject") { $itemNum = $_GET['id']; $aU->removeItemSubject($_GET['id'], $_GET['subjectterm'], $_GET['source']); $continue = 2; }
if(isset($_GET['remove']) && $_GET['remove'] == "itemSpatial") { $itemNum = $_GET['id']; $aU->removeItemSpatial($_GET['id'], $_GET['source'], $_GET['spatialterm']); $continue = 2; }

$INPUT_WIDTH = '400px;';

$pageContent = '<html>
<head>
<title>Digital Durham Admin</title>
<style type="text/css">
a { color: blue; }
a:hover { color: red; }

.hiddenDiv { 
  display: none;
  margin: 0 15px;
  background: #648b7a;
  padding: 2px 5px;
  color: #ffe;
}
</style>

</head>
<script type="text/javascript">


function changeSubject(name) {
	if (document.getElementById("newSubject") != null && name.value != null) {
		document.getElementById("newSubject").value = name.value;
	}
}

function changeSource(name) {
	if (document.getElementById("newSource") != null && name.value != null) {
		document.getElementById("newSource").value = name.value;
	}
}

</script>
<body style="text-align: center; font-family: verdana, arial; font-size: 12px; background: #ffc; margin: 0;">

<div style="width: 800px; margin: 0 auto; text-align: left;">

<p style="margin-top: 0; padding-top: 0;"><a href="../"><img style="border: 0;" src="/images/dd_logo3.gif" /></a></p>

<form action="index.php" method="post">';

if($continue == 1) {
  if($continue == 1)
    $pageContent .= '<p><b>'.$error_message.'</b></p>';
  
  $INPUT_WIDTH = '200px;';
  
  $pageContent .= '
  <p>(<a href="javascript:toggle(\'addNewPane\');">Add New Item</a>)</p>
  <div id="addNewPane" class="hiddenDiv">
  <p>Collection Prefix:</p>
  <p><input style="width: '.$INPUT_WIDTH.'" type="text" name="newCollectionPrefix" value="" /></p>
  <p>Category:</p>
  <p><input style="width: '.$INPUT_WIDTH.'" type="text" name="newCategory" value="" /></p>
  <p>Folder:</p>
  <p><input style="width: '.$INPUT_WIDTH.'" type="text" name="newFolder" value="" /></p>
  <p>Item Number:</p>
  <p><input style="width: '.$INPUT_WIDTH.'" type="text" name="newItemNumber" value="" /></p>
  <p><input type="submit" name="addNewItem" value="Add " /></p>
  </div></form>
  <form action="index.php" method="post">
  <p>Please enter an item Id to edit: <input type="text" name="id" /> <input type="submit" name="" value="Get item " /></p></form>
  <p style="padding-left: 15px;"><b>-- or --</b></p>
  <p>Select the item to edit:</p>
  ';
  
  $pageContent .= "
  <script type=\"text/javascript\">
  document.getElementById('addNewPane').style.display = 'none';
  
  function toggle(id) {
    if(document.getElementById(id).style.display == 'none') document.getElementById(id).style.display = 'block';
    else document.getElementById(id).style.display = 'none';
  }
  </script>
  ";
  
  $allItems = $aU->showItem();
  
  for($i = 0; $i < count($allItems); $i++) {
    $pageContent .= '<p><b>'.$allItems[$i]['category'].'</b></p>';
    for($j = 0; $j < count($allItems[$i]['dc_title']); $j++)
      $pageContent .= '<p style="padding-left: 15px;">'.$allItems[$i]['dc_title'][$j].' (<a href="index.php?id='.$allItems[$i]['item_id'][$j].'">edit</a>) (<a href="index.php?id='.$allItems[$i]['item_id'][$j].'&action=delete">delete</a>)</p>';
  }
}
else if($continue == 2) {
$pageContent .= '

<p><a href="index.php">&laquo; Back</a></p>

<p><b>Collection Id</b>: '.$aU->getItemId($itemNum).'</p>

<p><b>Type</b>: (<a href="javascript:toggle(\'typePane\');">Add</a>)</p>
<div id="typePane" class="hiddenDiv">
<p>Type:</p>
<p><input style="width: '.$INPUT_WIDTH.'" type="text" name="newType" value="" /></p>
<p><input type="submit" name="addType" value="Add " /></p>
</div>
<p><ul>';

foreach($aU->getItemType($itemNum) as $type) {
  $pageContent .= '<li>'.$type.' (<a href="index.php?remove=itemType&id='.urlencode($itemNum).'&typeTerm='.urlencode($type).'">Remove</a>)</li>';
}

$pageContent .= '</ul></p>

<p><b>Creator</b>: (<a href="javascript:toggle(\'creatorPane\');">Add</a>)</p>
<div id="creatorPane" class="hiddenDiv">
<p>Creator:</p>
<p><input style="width: '.$INPUT_WIDTH.'" type="text" name="newCreator" value="" /></p>
<p>Role:</p>
<p><input style="width: '.$INPUT_WIDTH.'" type="text" name="newRole" value="" /></p>
<p><input type="submit" name="addCreator" value="Add " /></p>
</div>
<p><ul>';

$creators = $aU->getItemCreator($itemNum);
for($i = 0; $i < count($creators); $i++) {
  if($i%2 == 0) {
    $pageContent .= '<li>';
    $pageContent .= $creators[$i];
  }
  else {
    $pageContent .= ', '.$creators[$i];
    $pageContent .= ' (<a href="index.php?remove=itemCreator&id='.urlencode($itemNum).'&creator='.urlencode($creators[$i-1]).'&role='.urlencode($creators[$i]).'">Remove</a>)</li>';
  }
}

$pageContent .= '</ul></p>

<p><b>Format</b>: (<a href="javascript:toggle(\'formatPane\');">Add</a>)</p>
<div id="formatPane" class="hiddenDiv">
<p>Format:</p>
<p><input style="width: '.$INPUT_WIDTH.'" type="text" name="newFormat" value="" /></p>
<p><input type="submit" name="addFormat" value="Add " /></p>
</div>
<p><ul>';

$format = $aU->getItemFormat($itemNum);
if(count($format) > 0) {
  for($i = 0; $i < count($format); $i++) {
    $pageContent .= '<li>';
    $pageContent .= $format[$i];
    $pageContent .= ' (<a href="index.php?remove=itemFormat&id='.urlencode($itemNum).'&formatterm='.urlencode($format[$i]).'">Remove</a>)</li>';
  }
}
else $pageContent .= '<li>No format specified</li>';

$pageContent .= '</ul></p>

<p><b>Source</b>:</p>
 <p><input style="width: '.$INPUT_WIDTH.'" type="text" name="source" 
value="'.$aU->getItemSource($itemNum).'" /></p>

<p><b>Page Count</b>:</p>
 <p><input style="width: '.$INPUT_WIDTH.'" type="text" name="page_count"
value="'.$aU->getItemPageCount($itemNum).'" /></p>

<p><b>Ranges</b>(example format 1-4):</p>
 <p><input style="width: '.$INPUT_WIDTH.'" type="text" name="ranges"
value="'.$aU->getItemRanges($itemNum).'" /></p>

<p><b>Dimensions</b>:</p>
 <p><input style="width: '.$INPUT_WIDTH.'" type="text" name="dimensions"
value="'.$aU->getItemDimensions($itemNum).'" /></p>

<p><b>Series</b>: (<a href="javascript:toggle(\'seriesPane\');">Assign</a>) (<a href="series.php" target="_blank">Manage Series\'</a>)</p>
<div id="seriesPane" class="hiddenDiv">
<p>Format:</p>

<select name="newSeriesId">';

$seriesAll = $aU->getSeries();

for($i = 0; $i < count($seriesAll); $i+=4) {
  $pageContent .= '<option value="'.$seriesAll[$i].'">'.$seriesAll[$i+1].'</option>';
}

$pageContent .= '

</select>

<p><input type="submit" name="addSeries" value="Assign " /></p>
</div>
<p><ul>';

$series = $aU->getItemSeries($itemNum);
for($i = 0; $i < count($series); $i++) {
  if($i%2 == 0) {
    $pageContent .= '<li>';
    $pageContent .= $series[$i];
  }
  else {
    $pageContent .= ', '.$series[$i];
    $pageContent .= '</li>';
  }
}

$sub = $aU->getSubject();


$subjTerm = "<option value=\"\">Select a Pre-Defined Subject...</option>";
for ($i = 0; $i < count($sub); $i++) {
	$subjTerm .= "<option value=\"$sub[$i]\">$sub[$i]</option>\n";
}

$sor = $aU->getSource();


$sourceTerm = "<option value=\"\">Select a Pre-Defined Source...</option>";
for ($i = 0; $i < count($sor); $i++) {
	$sourceTerm .= "<option value=\"$sor[$i]\">$sor[$i]</option>\n";
}

$pageContent .= '</ul></p>

<p><b>Title</b>:</p>
<p><input style="width: '.$INPUT_WIDTH.'" type="text" name="title" value="'.$aU->getItemTitle($itemNum).'" /></p>

<p><b>Alternative Title</b>:</p>
<p><input style="width: '.$INPUT_WIDTH.'" type="text" name="alternativeTitle" value="'.$aU->getItemAlternativeTitle($itemNum).'" /></p>

<p><b>Copyright</b>:</p>
<p><input style="width: '.$INPUT_WIDTH.'" type="text" name="copyright" value="'.$aU->getItemCopyright($itemNum).'" /></p>

<p><b>Description</b>:</p>
<p><textarea style="width: 400px;" rows="7" name="description">'.$aU->getItemDescription($itemNum).'</textarea></p>

<p><b>Notes</b>:</p>
<p><textarea style="width: 400px;" rows="7" name="notes">'.$aU->getItemNotes($itemNum).'</textarea></p>

<p><b>Notes2</b>:</p>
<p><textarea style="width: 400px;" rows="7" name="notes2">'.$aU->getItemNotes2($itemNum).'</textarea></p>


<p><b>Date (Year)</b>:</p>
<p><input style="width: '.$INPUT_WIDTH.'" type="text" name="date" value="'.$aU->getItemDate($itemNum).'" /></p>

<p><b>Subject</b>: (<a href="javascript:toggle(\'subjectPane\');">Add</a>)</p>
<div id="subjectPane" class="hiddenDiv">
<P>Pre-defined Subject Listings:</P> <select name="subjTerm" onChange="changeSubject(this);">' . $subjTerm . '</select><br />
<P>Pre-defined Source Listings:</P> <select name="sourceTerm" onChange="changeSource(this);">' . $sourceTerm . '</select><br />
<p>Subject:</p>
<p><input style="width: '.$INPUT_WIDTH.'" type="text" id="newSubject" name="newSubject" value="" /></p>
<p>Source:</p>
<p><input style="width: '.$INPUT_WIDTH.'" type="text" id="newSource" name="newSource" value="" /></p>
<p><input type="submit" name="addSubject" value="Add " /></p>
</div>
<p><ul>';

$subject = $aU->getItemSubject($itemNum);
for($i = 0; $i < count($subject); $i++) {
  if($i%2 == 0) {
    $pageContent .= '<li>';
    $pageContent .= $subject[$i];
  }
  else {
    $pageContent .= ', '.$subject[$i];
    $pageContent .= ' (<a href="index.php?remove=itemSubject&id='.urlencode($itemNum).'&subjectterm='.urlencode($subject[$i-1]).'&source='.urlencode($subject[$i]).'">Remove</a>)</li>';
  }
}

$pageContent .= '</ul></p>

<p><b>Spatial</b>: (<a href="javascript:toggle(\'spatialPane\');">Add</a>)</p>
<div id="spatialPane" class="hiddenDiv">
<p>Source:</p>
<p><input style="width: '.$INPUT_WIDTH.'" type="text" name="newSpatialSource" value="" /></p>
<p>Spatial:</p>
<p><input style="width: '.$INPUT_WIDTH.'" type="text" name="newSpatial" value="" /></p>
<p><input type="submit" name="addSpatial" value="Add " /></p>
</div>
<p><ul>';

$spatial = $aU->getItemSpatial($itemNum);
for($i = 0; $i < count($spatial); $i++) {
  if($i%2 == 0) {
    $pageContent .= '<li>';
    $pageContent .= $spatial[$i];
  }
  else {
    $pageContent .= ', '.$spatial[$i];
    $pageContent .= ' (<a href="index.php?remove=itemSpatial&id='.urlencode($itemNum).'&source='.urlencode($spatial[$i-1]).'&spatialterm='.urlencode($spatial[$i]).'">Remove</a>)</li>';
  }
}

$pageContent .= '</ul></p>

<input type="hidden" name="thisItemId" value="'.$itemNum.'" />

<input type="submit" value="Save " name="saveItem" />

</form>';

$pageContent .= "
<script type=\"text/javascript\">
document.getElementById('typePane').style.display = 'none';
document.getElementById('creatorPane').style.display = 'none';
document.getElementById('formatPane').style.display = 'none';
document.getElementById('seriesPane').style.display = 'none';
document.getElementById('subjectPane').style.display = 'none';
document.getElementById('spatialPane').style.display = 'none';

function toggle(id) {
  if(document.getElementById(id).style.display == 'none') document.getElementById(id).style.display = 'block';
  else document.getElementById(id).style.display = 'none';
}
</script>
";
}

$pageContent .= '</div>

</body>
</html>
';

print $pageContent;
?>
