<?php

$_authenticated = false;
$_userName = 'trudi';
$_password = '3d98ef72274d4af3c0b2c38b1363cd5cbca5e5839c332fdfb4c0da40cd2551ff';

$_testUser = null;
$_testPass = null;

// the user requires authentication
if ((! empty($_COOKIE['user']) && ! empty($_COOKIE['pw']))) {
	$_testUser = $_COOKIE['user'];
	$_testPass = $_COOKIE['pw'];
}
// the user name HTTP variables were sent
elseif ((! empty($_SERVER['PHP_AUTH_USER'])) && (! empty($_SERVER['PHP_AUTH_PW']))) {
	$_testUser = $_SERVER['PHP_AUTH_USER'];
	$_testPass = hash_hmac('sha256', $_SERVER['PHP_AUTH_PW'], $_testUser);
}

// make sure that the information matches
if ((($_testUser != null) && ($_testPass != null)) && 
		($_testUser == $_userName) && ($_testPass == $_password)) {
	$_authenticated = true;
	setcookie('user', $_testUser);
	setcookie('pw', $_testPass);
}

if (! $_authenticated) {
	header('WWW-Authenticate: '.sprintf('Basic realm="%s"', 'Requires Authentication.'));
	header('HTTP/1.1 401 Unauthorized');
	print('Authentication Required');
	exit;
}


// Allows a user to enter and update an entry in the Series tableO
include_once("../common/adminUpdate.php");

$au = new adminUpdate();

if (isset($_REQUEST['addSeries'])) {

	print addSeries();

} else if (isset($_REQUEST['remove'])) {

	if (removeSeries($au, $_REQUEST['remove'])) {
		print ("Item removed successfully.<P>");
		print displayMenu();
	} else {
		print ("<b>Unable to remove item.</b> Please try again at a later time.<P>");
		print displayMenu();
	}

} else if (isset($_REQUEST['editSeries'])) {

	print editSeries($au);

} else if (isset($_REQUEST['saveSeries'])) {

	if (saveSeries($au, $_REQUEST['series_id'], $_REQUEST['series_title'], $_REQUEST['daterange'], $_REQUEST['description'])) {
		print ("Series added or updated successfully.<P>");
		print displayMenu();
	} else {
		print ("<b>Unable to add item.</b> Please try again at a later time.<P>");
		print displayMenu();
	}

} else {

	print displayMenu();
}

function displayMenu() {

	$PHP_SELF = $_SERVER['PHP_SELF'];
	
	$dOut = "";
	$dOut .= "<b>Digitial Durham Series</a><P>";
	$dOut .= "<ul><li><a href=\"$PHP_SELF?addSeries\">Add A New Series</a></li>";
	$dOut .= "<li><a href=\"$PHP_SELF?editSeries\">Edit Current Series</a></b></li></ul>";

	return ($dOut);

}

function addSeries() {

	$PHP_SELF = $_SERVER['PHP_SELF'];
	
	$dOut = "";

	$dOut .= "<form method=\"POST\" action=\"$PHP_SELF\">\n";
	$dOut .= "<a href=\"javascript:history.go(-1);\">Back To Series</a><P>";
	$dOut .= "<table>";
	$dOut .= "<tr><td><b>Series ID:</b></td><td><input type=\"text\" name=\"series_id[]\" size=\"30\"></td></tr>";
	$dOut .= "<tr><td><b>Series Title:</b></td><td><input type=\"text\" name=\"series_title[]\" size=\"30\"></td></tr>";
	$dOut .= "<tr><td><b>Daterange:</b></td><td><input type=\"text\" name=\"daterange[]\" size=\"30\"></td></tr>";
	$dOut .= "<tr><td><b>Description:</b></td><td><textarea name=\"description[]\" cols=\"50\" rows=\"15\"></textarea></td></tr>";
	$dOut .= "</table><P>";
	$dOut .= "<input type=\"hidden\" name=\"saveSeries\">";
	$dOut .= "<input type=\"submit\" name=\"Add\" value=\"Add Series\"></form>";
	$dOut .= "</form>";

	return ($dOut);

}


function editSeries($au) {
	
	$PHP_SELF = $_SERVER['PHP_SELF'];
	
	// We know that there are four data balues per Series
	$series = $au->getSeries();
	
	$dOut = "";
	
	$dOut .= "<a href=\"javascript:history.go(-1);\">Back To Series</a><P>";
	$dOut .= "<form method=\"POST\" action=\"$PHP_SELF\">\n";
	
	for ($i = 0; $i < count($series); $i+=4) {
		$dOut .= "<table>";
		$dOut .= "<tr><td><b>Series ID:</b></td><td><input type=\"text\" name=\"series_id[]\" value=\"" . $series[$i+0] . "\" size=\"30\"></td></tr>";
		$dOut .= "<tr><td><b>Series Title:</b></td><td><input type=\"text\" name=\"series_title[]\" value=\"" . $series[$i+1] . "\" size=\"30\"></td></tr>";
		$dOut .= "<tr><td><b>Daterange:</b></td><td><input type=\"text\" name=\"daterange[]\" value=\"" . $series[$i+2] . "\" size=\"30\"></td></tr>";
		$dOut .= "<tr><td><b>Description:</b></td><td><textarea name=\"description[]\" cols=\"50\" rows=\"15\">" . $series[$i+3] . "</textarea></td></tr>";
		$dOut .= "<tr bgcolor=\"#cccccc\"><td colspan=\"2\">Remove <a href=\"$PHP_SELF?remove=" . $series[$i+0] . "\">" . ereg_replace(",", "", $series[$i+1]) . "</a> from database</td></tr>";
		$dOut .= "</table><P>";
	}
	

	$dOut .= "<input type=\"hidden\" name=\"saveSeries\">";
	$dOut .= "<input type=\"submit\" name=\"Update\" value=\"Update\"></form>";
	
	return ($dOut);

}


function saveSeries($au, $series_id, $series_title, $daterange, $description) {

	$noError = true;
	
	for ($i = 0; $i < count($series_id); $i++) {
		if (!$au->setSeries($series_id[$i], $series_title[$i], $daterange[$i], $description[$i])) {
			$noError = false;
		} 
	}

	return ($noError);

}

function removeSeries($au, $series_id) {

	
	if ($au->removeSeries($series_id)) {
		return (true);
	} else {
		return (false);
	}

}

?>