<?php

include_once("common/common.php");
include_once("common/paths.php");

if (isset($_GET['id']) && $_GET['id'] != "") {
	$businessID = $_GET['id'];
} else {
	$dOut = "Our apologies.  We are unable to find this item in our collection.  Please try again in a few moments.";
	print ($dOut);
	exit;
}

// Ok, let's try to find this record in our collection
$_query = 
	'SELECT 
		item.collection_prefix, 
		item.notes2, 
		item.dc_date, 
		item.item_id, 
		item.category, 
		item.folder, 
		item.item_number, 
		item.dc_title, 
		item.dc_description, 
		item.Height, 
		item.Width, 
		item.dimensions, 
		item.dc_source, 
		item.reference, 
		item.page_count 
	FROM 
		item 
	WHERE
		item_id = :businessId
	LIMIT 1;';
$result = R::getAll($_query, array(':businessId' => $businessID));

if (count($result) == 0) {
	$dOut = "Our apologies.  We are unable to find this item in our collection.  Please try again in a few moments.";
	print ($dOut);
	exit;
}

for ($i = 0; $i < count($result); $i++) {

	$title = $result[$i]["dc_title"];
	$collection_prefix = $result[$i]["collection_prefix"];
	$category = $result[$i]["category"];
	$folder = $result[$i]["folder"];
	$item_number = $result[$i]["item_number"];
	$description = $result[$i]["dc_description"];
	$source = $result[$i]["dc_source"];
	$height = $result[$i]["Height"];
	$width = $result[$i]["Width"];
	$dimension = $result[$i]["dimensions"];
	$notes2 = $result[$i]["notes2"];
	$date = $result[$i]["dc_date"];
}


	$cntItem = "00" . ($i+1) . "0";
	$cntLargeImage = "00" . "1" . "0";
		
	$largeBusinessImage = $imageBase . "/full/" . $collection_prefix . $category . $folder . $item_number . $cntLargeImage . ".jpg";
	$medBusinessImage = $imageBase . "/600/" . $collection_prefix . $category . $folder . $item_number . $cntLargeImage . ".jpg";

	
//	if (!file_url_exists("$imageBase_basepath/$medBusinessImage")) {
//		$medBusinessImage = "/images/notfound400.gif";
//	} 
		
		
	
//	if (file_url_exists("$imageBase_basepath/$largeBusinessImage")) {
		$largeBusinessImage = "<a href=\"$largeBusinessImage\" target=\"_new\"><img src=\"$medBusinessImage\" border=\"0\" width=\"400px\"/></a>";
//	}// else {
	//	$largeBusiness = "<a href=\"/images/notfound400.gif\" target=\"_new\"><img src=\"ui/images/notfound400.gif\" border=\"0\" width=\"400px\"/></a>";
	//}
	
	$_query = 
		'SELECT 
			item_subject.subjterm 
		FROM 
			item_subject 
		WHERE 
			item_id = :businessId';
			
	$loc = R::getAll($_query, array(':businessId' => $businessID));

	$subject = "";
	
	for ($i = 0; $i < count($loc); $i++) {
		$subject .= "<!--<a class=\"subject\" href=\"hueism.php?dduMenu_0=subjectheading&dduMenu_0_value=" . $loc[$i]["subjterm"] . "&SendSearch=Search&x=search&SendSearch=1\">-->" . $loc[$i]["subjterm"] . "<!--</a>--><br />";
	}

	$_query = 
		'SELECT 
			item_spatial.spatialTerm 
		FROM 
			item_spatial 
		WHERE 
			item_id = :businessId';
		
	$spat = R::getAll($_query, array(':businessId' => $businessID));
	$spatial = "";
	
	if (count($spat) > 0) {
		$spatial .= "<b>Geographic Term:</b><br />";
	}
		
	for ($i = 0; $i < count($spat); $i++) {
		$spatial .= "<a href=\"hueism.php?dduMenu_0=spatial&dduMenu_0_value=" . $spat[$i]["spatialTerm"] . "&SendSearch=Search&x=search&SendSearch=1\">" . $spat[$i]["spatialTerm"] . "</a><br />";
	}

	if ($title != "") {
		$titleData = "<b>Title:</b><br />$title<br />";
	} else {
		$titleData = "";
	}

	if ($description != "") {
		$descriptionData = "<b>Description:</b><br />$description<br />";
	} else {
		$descriptionData = "";
	}

	if ($source != "") {
		$sourceData = "<b>Source:</b><br />$source<br />";
	} else {
		$sourceData = "";
	}

	if ($height != "") {
		$heightData = "<b>Height:</b><br />$height<br />";
	} else {
		$heightData = "";
	}

	if ($width != "") {
		$widthData = "<b>Width:</b><br />$width<br />";
	} else {
		$widthData = "";
	}

	if ($dimension != "") {
		$dimensionData = "<b>Dimensions:</b><br />$dimension<br />";
	} else {
		$dimensionData = "";
	}

	if ($subject != "") {
		$subjectData = "<br /><b>Library of Congress Subject Headings:</b><br />$subject";
	} else {
		$subjectData = "";
	}
	
	if ($spatial != "") {
		$spatialData = "<br />" . $spatial;
	} else {
		$spatialData = "";
	}

	if ($notes2 != "") {
		$notesData = "<b>Notes</b>:<br />$notes2<br />";
	} else {
		$notesData = "";
	}

	if ($date != "") {
		$dateData = "<b>Date</b>:<br />$date<br />";
	} else {
		$dateData = "";
	}

$dOut =<<< END_DATA

<p class="header"><a href="hueism.php?x=browse">Browse Collection</a> &rsaquo; <a href="hueism.php?x=browse&dduMenu_0_value=ep&dduMenu_0=category&&dduMenu_1_value=br&dduMenu_1=category&boolean=OR&SendSearch=1">Miscellany</a> &rsaquo; $title</p>
      <p>Click on the record to view a larger version.</p>

<table cellpadding="0" cellspacing="0">
  <tr>
 	<td class="business_record_left">$largeBusinessImage</td>
    <td>
      <!-- <p>(<a href="javascript:toggle();">Toggle MetaData View</a>)</p> -->
		<P>[ <a href="printedwork.php?x=businessrecord&id=$businessID&print">Printer-Friendly Version</a> ]</p>
		<div id="metadata" class="spacing"></div>
			$titleData
			$dateData
			$descriptionData
			$sourceData
			$notesData
			$subjectData
			$spatialData
		<!-- </div>	-->	
				

	</td>
   </tr>
</table>

<script language="javascript" type="text/javascript">
	document.getElementById("metadata").style.display = "none";

function toggle() {
  if(document.getElementById("metadata").style.display == "none")
	document.getElementById("metadata").style.display = "block";
  else
    document.getElementById("metadata").style.display = "none";
}

</script>
END_DATA;

print ($dOut);

function file_url_exists($url) {
	$handle = @fopen($url, "r");
	 if ($handle === false)
		  return false;
	 fclose($handle);
	 return true;
}




?>

