<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Digital Durham</title>
<link rel="stylesheet" type="text/css" media="screen" 
href="/ui/css/style.css" 
/>
</head>


<body>
<div id="contain">
<div id="top">

<div id="search_box_top"><div id="search_box_left">
<form method="get" action="/search">
      <input class="form_textbox" id="q" name="q" type="text" alt="Search Box" style="width:140px;" />
	  <input type="submit" name="submit" value="Go" />
      <input type="hidden" name="site" value="duke_collection" />
      <input type="hidden" name="client" value="digitaldurham" />
      <input type="hidden" name="proxystylesheet" value="digitaldurham" />
      <input type="hidden" name="output" value="xml_no_dtd" />
    </form>
</div></div>

<a href="/"><img src="/images/dd_logo3.gif" /></a>

</div>

<div id="nav">
  <?php

if(isset($x) && $x != "")
  include_once($x.".php");
else
  include_once("nav.php");

?>
</div>
<p>
<div style="float:left; width:100%;">
<div style="float:left; width:30%">
<p id="content" class="header"><a href="/public.php">Public Records</a><br>
<a href="census_help.php">Census Help</a></p>
</div>
<div style="float:left; width:70%"><br>
<span id="content">
<h3>1880 Federal Population Census Database</h3>
</span>
</div></div>


 <div style="float:left; width:100%;">
<div style="float:left; width:25%">
<iframe src="/cgi-bin/dd/ddquery.pl" width="200" height="1000"  
frameborder="0" scrolling="auto" name="myInlineFrame" security="restricted">
</iframe>
</div>
<div style="float:left; width:75%">
<iframe src="/cgi-bin/dd/ddfound.pl" width="600" height="1000" 
frameborder="0" scrolling="auto" name="myInlineFrame2" security="restricted"></iframe>

</div></div>
</p>


  <p><a 
href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a> 
&middot; <a href="/about.php">About this site</a> &middot; 
Copyright � 2006. Trudi J. Abel. All Rights Reserved. </p>
</div>
</body>
</html>









