<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Digital Durham</title>
<link rel="stylesheet" type="text/css" media="screen" href="/ui/css/style.css" />
</head>

<body>

<div id="contain">
<div id="top">



<div id="search_box_top"><p style="margin-top: 0; margin-bottom: 0;">&nbsp;</p>
<div id="search_box_left">
<form action="/search" method="get" style="margin-bottom: 0">
      <input class="form_textbox" id="q" name="q" type="text" alt="Search Box" style="width:140px;" />
	  <input type="submit" name="submit" value="Go" />
      <input type="hidden" name="site" value="duke_collection" />
      <input type="hidden" name="client" value="digitaldurham" />
      <input type="hidden" name="proxystylesheet" value="digitaldurham" />
      <input type="hidden" name="output" value="xml_no_dtd" />
    </form>
</div></div>



<a href="/"><img src="/images/dd_logo3.gif" /></a>

</div>

<div id="nav">
  <?php

if(isset($x) && $x != "")
  include_once($x.".php");
else
  include_once("nav.php");

?>
</div>

<div id="content">
<div class="margins">
  <div class="home_left">
    <img src="images/index-images.gif" width="449" height="443" border="0" usemap="#Map">
    <map name="Map">
      <area shape="rect" coords="240,32,391,151" href="#" alt="permission for use: Duham County Public Library">
    </map>
  </div>

  <p style="margin-top: 0; margin-bottom: 0;">&nbsp;</p>
<div class="home_right">
  <p class="header" style="margin-top: 0;">Project Overview</p>
<p>For many years scholars have recognized that late nineteenth-century Durham, North Carolina makes an ideal case study for examining emancipation, industrialization, immigration, and urbanization in the context of the New South. &quot;With its tobacco factories, textile mills, black entrepreneurs, and new college,&quot; the historian Syd Nathans observes, &quot;Durham was a hub of enterprise and hope.&quot; By the early twentieth century, Durham became renowned for its vibrant entrepreneurial spirit.   <a href="overview.php">Continue...</a></p>
<p style="margin-bottom: 0;"><br>
  <a href="/funding.php">Funding</a> provided by Duke University and the North Carolina State Library: <a href="http://www.ncecho.org/"><img src="images/ncecho.gif" alt="NC ECHO" width="250" height="73" border="0"></a> <br>
</p>
</div></div>
 <p style="margin-top: 0; margin-bottom: 0;">&nbsp;</p>
<p style="margin-top: 0;"><a 
href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a> 
&middot; <a href="about.php">About this site</a> &middot; Copyright &#169; 2006-2009. 
Trudi J. Abel. All Rights Reserved. <br>
<br>
    </p>
<p style="margin-bottom: 0;">&nbsp;</p>
<div id="copyright">
    <p style="margin-bottom: 0">The copyright interest in the material in this digital collection has not been transferred to the Digital Durham project. These text and images may not be used for any commercial purpose without the permission of the Duke University Rare Book, Manuscript, and Special Collections Library and the Digital Durham Project. Copyright permission for subsequent uses is the responsibility of the user.</p>
  </div></div></div>




<script src="//www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-708499-4";
urchinTracker();
</script>
</body>
</html>
