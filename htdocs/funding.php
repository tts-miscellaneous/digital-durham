<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Digital Durham</title>
<link rel="stylesheet" type="text/css" media="screen" href="/ui/css/style.css" />
</head>

<body>

<div id="contain">
<div id="top">

<div id="search_box_top"><div id="search_box_left">
<form method="get" action="/search">
      <input class="form_textbox" id="q" name="q" type="text" alt="Search Box" style="width:140px;" />
	  <input type="submit" name="submit" value="Go" />
      <input type="hidden" name="site" value="duke_collection" />
      <input type="hidden" name="client" value="digitaldurham" />
      <input type="hidden" name="proxystylesheet" value="digitaldurham" />
      <input type="hidden" name="output" value="xml_no_dtd" />
    </form>
</div></div>

<a href="/"><img src="/images/dd_logo3.gif" /></a>

</div>

<div id="nav">
  <?php

if(isset($x) && $x != "")
  include_once($x.".php");
else
  include_once("nav.php");

?>
</div>

<div id="content">
<div class="margins">

  <div class="home_right">
    <p class="header"><a href="about.php">About this site</a> | <a href="overview.php">Overview</a> | Funding </p>
    <p>The Digital Durham project has received support from: </p>
    <blockquote>
      <p>NC ECHO, a digital initiative of the State Library of North Carolina </p>
      <p>Master of Arts in Liberal Studies Program (MALS) </p>
      <p>Rare Book, Manuscript, and Special Collections Library </p>
      <p>Dean of Trinity College </p>
      <p>Center for Instructional Technology </p>
      <p>Department of History </p>
      <p>Undergraduate Writing Program </p>
      <p>Office of Service Learning </p>
      <p>North Carolina Humanities Council </p>
    </blockquote>
    <p>FUNDING HISTORY </p>
    <p>In 1999 and 2000, Trudi Abel, Ph.D. received a grant from Duke University's Center for Instructional Technology to design and develop a website of primary source materials from Durham in the post-Civil War period. With funding from the Dean of Trinity College in 2001, Dr. Abel and her student researchers developed a beta-version of the Digital Durham website that featured an 1880 census database and a few examples of primary documents. Duke students tested the site in an undergraduate research seminar on the New South and in Helen McLeod's eighth-grade classroom at the Durham School of the Arts during the spring of 2001, 2002, and 2006. </p>
    <p>In 2005, Duke University's Rare Book, Manuscript, and Special 
Collections Library received a NC ECHO digitization grant from the State Library of North Carolina to support the expansion of the Digital Durham website. The Digital Production Center at Perkins Library digitized over 1000 images taken from manuscript letters, maps, ephemera, photographs, and printed works. Programmers from the Web Solutions Team of Arts &amp; Sciences Information Science and Technology and Hueism Inc. created a new website with a search interface. A team of graduate students from the Master of Arts in Liberal Studies Program worked together with Kathy Wisser from NC ECHO and Dr. Abel to create TEI transcriptions of the manuscript letters. </p>
    <blockquote>  <br>
</blockquote>
<p><a 
href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a> 
&middot; <a href="/about.php">About this site</a> &middot; Copyright &#169; 2001 - 2006. 
Trudi J. Abel. All Rights Reserved. </p>
  <div id="copyright">
    <p>The copyright interest in the material in this digital collection has not been transferred to the Digital Durham project. These text and images may not be used for any commercial purpose without the permission of the Duke University Rare Book, Manuscript, and Special Collections Library and the Digital Durham Project. Copyright permission for subsequent uses is the responsibility of the user.</p>
  </div>
  </div></div>
 
</div>
</div>




</body>
</html>
