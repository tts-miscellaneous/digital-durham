<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Digital Durham</title>
<link rel="stylesheet" type="text/css" media="screen" href="/ui/css/style.css" />
</head>

<body>

<div id="contain">
<div id="top">

<div id="search_box_top"><div id="search_box_left"><form method="get" action="/search">
      <input class="form_textbox" id="q" name="q" type="text" alt="Search Box" style="width:140px;" />
	  <input type="submit" name="submit" value="Go" />
      <input type="hidden" name="site" value="duke_collection" />
      <input type="hidden" name="client" value="digitaldurham" />
      <input type="hidden" name="proxystylesheet" value="digitaldurham" />
      <input type="hidden" name="output" value="xml_no_dtd" />
    </form></div></div>

<a href="/"><img src="/images/dd_logo3.gif" /></a>

</div>

<div id="nav">
  <?php

if(isset($x) && $x != "")
  include_once($x.".php");
else
  include_once("nav.php");

?>
</div>

<div id="content">
<div class="margins">
<p class="header">Teachers' Corner
</p>
<p class="headerInText">Lesson Plans  </p>
<p>Digital Durham: A Teacher's Guide (with introductory student exercises) (<a href="Digital Durham_Teacher's Guide_2009.pdf">PDF</a>, <a href="Digital Durham_Teacher's Guide_2009.doc">MS Word</a>) </p>
<p>Changing Places: Exploring Immigration and Migration through the North Carolina Experience (<a href="Changing Places_Immigration_Lesson_Plan.pdf">PDF</a>, <a href="Changing Places_Immigration_Lesson_Plan.doc">MS Word</a>)</p>
<p>Youth and Education in Durham, North Carolina during Industrialization, 1880-1910 (<a href="lesson.pdf">PDF</a>, <a href="lesson.doc">MS Word</a>) </p>
  <p>&nbsp;</p>
  <p class="headerInText">
    Lesson Plan Documents</p>
  <p>Rules and Regulations Governing the Durham Graded and High School with the Course of Study, 1885 (<a href="schoolreport.pdf">PDF</a>)
  <p>
&quot;Durham and the Tobacco Factories&quot; from John Moore's School History of North Carolina, 1882 (<a href="schoolhistory.pdf">PDF</a>)  
  <p>
  Oration by Hon. Frederick Douglass on The Occasion of The Second Annual 
Exposition of the Colored People of North Carolina, Delivered on Friday October 1st, 1880 (<a href="FrederickDouglassOrationRev2.doc">MS Word</a>)  
  <p>
&quot;Type Stories of Workers&quot; from Mary Cowper's unpublished study, &quot;Cotton Cloth: A Type Study of Community Process.&quot; (<a href="workerstories150.pdf">PDF</a>)<br>  

  <p><a 
href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a> 
&middot; <a href="about.php">About this site</a> &middot; Copyright &#169; 2001 - 
2009. Trudi J. Abel. All Rights Reserved. </p>
  <div id="copyright">
    <p>The copyright interest in the material in this digital collection has not been transferred to the Digital Durham project. These text and images may not be used for any commercial purpose without the permission of the Duke University Rare Book, Manuscript, and Special Collections Library and the Digital Durham Project. Copyright permission for subsequent uses is the responsibility of the user.</p>
  </div>
</div>
</div>
</div>



</body>
