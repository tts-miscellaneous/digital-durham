<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Digital Durham</title>
<link rel="stylesheet" type="text/css" media="screen" href="/ui/css/style.css" />
</head>

<body>

<div id="contain">
<div id="top">

<div id="search_box_top"><div id="search_box_left">
<form method="get" action="/search">
      <input class="form_textbox" id="q" name="q" type="text" alt="Search Box" style="width:140px;" />
	  <input type="submit" name="submit" value="Go" />
      <input type="hidden" name="site" value="duke_collection" />
      <input type="hidden" name="client" value="digitaldurham" />
      <input type="hidden" name="proxystylesheet" value="digitaldurham" />
      <input type="hidden" name="output" value="xml_no_dtd" />
    </form>
</div></div>

<a href="/"><img src="/images/dd_logo3.gif" /></a>

</div>

<div id="nav">
  <?php

if(isset($x) && $x != "")
  include_once($x.".php");
else
  include_once("nav.php");

?>
</div>

<div id="content">
<div class="margins">

  <p class="header">Reference</p>  

      <blockquote>
        <p><a href="geography.php">The Geography of the Piedmont Region</a></p>
      </blockquote>
      <div style="border-top: 1px solid #CCC;"><b>Glossary of terms used in the 1880 census of Durham, North Carolina</b>
  <blockquote>
    <p><a href="editorial.php">Editorial Notes</a></p>
    <p><a href="medical.php">Medical Glossary </a></p>
    <p><a href="occupation.php">Occupations Glossary</a> </p>
    <p><a href="relationship.php">Relationship Glossary</a> </p>
  </blockquote>
  </div>
    <div style="border-top: 1px solid #CCC;">
     <p><strong>Census</strong>&nbsp;</p>
     <blockquote>
       <p><a href="census_help.php">Census Help</a> </p>
     </blockquote>
   </div>
  <p>&nbsp;</p>
  <p><a 
href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a> 
&middot; <a href="/about.php">About this site</a> &middot; Copyright &#169; 
2001 - 2006. 
Trudi J. Abel. All Rights Reserved. </p>  
  <div id="copyright">
    <p>The copyright interest in the material in this digital collection has not been transferred to the Digital Durham project. These text and images may not be used for any commercial purpose without the permission of the Duke University Rare Book, Manuscript, and Special Collections Library and the Digital Durham Project. Copyright permission for subsequent uses is the responsibility of the user.</p>
  </div>
</div>
</div>
</div>
</body>
</html>
