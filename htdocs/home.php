<div class="home_left">
<img src="/images/index-images.gif" />
</div>

<div class="home_right"><p class="header">Project Overview</p>

<p>For many years scholars have recognized that late nineteenth-century Durham, North Carolina makes an ideal case study for examining emancipation, industrialization, immigration, and urbanization in the context of the New South. "With its tobacco factories, textile mills, black entrepreneurs, and new college," the historian Syd Nathans observes, "Durham was a hub of enterprise and hope." By the early twentieth century, Durham became renown for its vibrant entrepreneurial spirit. Both W.E.B. Du Bois and Booker T. Washington traveled to the town and wrote articles for the national press about their visits with members of the African-American community. After his visit in 1910, Booker T. Washington dubbed Durham the "City of Negro Enterprise." <a href="index.php?x=overview">Continue...</a></p></div>
