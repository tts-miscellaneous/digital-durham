<?php

include_once("common/common.php");

if (isset($_GET['id']) && $_GET['id'] != "" && is_numeric($_GET['id'])) {
	$photoID = (int)$_GET['id'];
} else {
	$dOut = "Our apologies.  We are unable to find this item in our collection.  Please try again in a few moments.";
	print ($dOut);
	exit;
}

// Ok, let's try to find this record in our collection
$_query =
	"SELECT 
		item.collection_prefix, 
		item.dc_date, 
		item.notes2, 
		item.copyright, 
		item.item_id, 
		item.category, 
		item.folder, 
		item.item_number, 
		item.dc_title, 
		item.dc_description, 
		item.Height, 
		item.Width, 
		item.dimensions, 
		item.dc_source, 
		item.reference, 
		item.page_count 
	FROM 
		item 
	WHERE 
		item_id = ? 
	LIMIT 1;";
$_params = array($photoID);

try {
	$result = R::getAll($_query, $_params);
	if (count($result) == 0) {
		$dOut = "Our apologies.  We are unable to find this item in our collection.  Please try again in a few moments.";
		print ($dOut);
		exit;
	}
} catch (\Exception $e) {
	$dOut = "Our apologies.  We are unable to find this item in our collection.  Please try again in a few moments.";
	print ($dOut);
	exit;	
}

for ($i = 0; $i < count($result); $i++) {

	$title = $result[$i]["dc_title"];
	$date = $result[$i]["dc_date"];
	$collection_prefix = $result[$i]["collection_prefix"];
	$category = $result[$i]["category"];
	$folder = $result[$i]["folder"];
	$item_number = $result[$i]["item_number"];
	$description = $result[$i]["dc_description"];
	$source = $result[$i]["dc_source"];
	$height = $result[$i]["Height"];
	$width = $result[$i]["Width"];
	$dimension = $result[$i]["dimensions"];
	$notes2 = $result[$i]["notes2"];
	$copyright = $result[$i]["copyright"];
}


	try {
		$spat = R::getAll("SELECT item_spatial.spatialTerm FROM item_spatial WHERE item_id = ?", array($photoID));
	}
	catch (\Exception $e) {
		$spat = array();
	}
	$spatial = "";
	
	if (count($spat) > 0) {
		$spatial .= "<b>Geographic Term:</b><br />";
	}

	$cntItem = "00" . ($i+1) . "0";
	$cntLargeImage = "00" . "1" . "0";
	
	$largeLetterImage = $imageBase . "/full/" . $collection_prefix . $category . $folder . $item_number . $cntLargeImage . ".jpg";
	$medLetterImage = $imageBase . "/600/" . $collection_prefix . $category . $folder . $item_number . $cntLargeImage . ".jpg";

	$largePhoto = "<a href=\"$largeLetterImage\" target=\"_new\"><img src=\"$medLetterImage\" border=\"0\" width=\"400px\"/></a>";	
	
	try {
		$loc = R::getAll("SELECT item_subject.subjterm FROM item_subject WHERE item_id = ?", array($photoID));
	} 
	catch (\Exception $e) {
		$loc = array();
	}

	$subject = "";
	
	for ($i = 0; $i < count($loc); $i++) {
		$subject .= "<!--<a class=\"subject\" href=\"hueism.php?dduMenu_0=subjectheading&dduMenu_0_value=" . $loc[$i]["subjterm"] . "&SendSearch=Search&x=search&SendSearch=1\">-->" . $loc[$i]["subjterm"] . "<!--</a>--><br />";
	}

	$spat = R::getAll("SELECT item_spatial.spatialTerm FROM item_spatial WHERE item_id = ?", array($photoID));
	$spatial = "";
	
	if (count($spat) > 0) {
		$spatial .= "<b>Geographic Term:</b><br />";
	}
		
	for ($i = 0; $i < count($spat); $i++) {
		$spatial .= "<a href=\"hueism.php?dduMenu_0=spatial&dduMenu_0_value=" . $spat[$i]["spatialTerm"] . "&SendSearch=Search&x=search&SendSearch=1\">" . $spat[$i]["spatialTerm"] . "</a><br />";
	}

	if ($title != "") {
		$titleData = "<b>Title:</b><br />$title<br />";
	} else {
		$titleData = "";
	}

	if ($date != "") {
		$dateData = "<b>Date:</b><br />$date<br />";
	} else {
		$dateData = "";
	}

	if ($description != "") {
		$descriptionData = "<b>Description:</b><br />$description<br />";
	} else {
		$descriptionData = "";
	}

	if ($source != "") {
		$sourceData = "<b>Source:</b><br />$source<br />";
	} else {
		$sourceData = "";
	}


	if ($subject != "") {
		$subjectData = "<br /><b>Library of Congress Subject Headings:</b><br />$subject";
	} else {
		$subjectData = "";
	}
	
	if ($spatial != "") {
		$spatialData = "<br />" . $spatial;
	} else {
		$spatialData = "";
	}

        if ($copyright != "") {
                $copyrightData = "<b>Copyright</b>:<br />$copyright<br />";
        } else {
                $copyrightData = "";
        }

	if ($notes2 != "") {
		$notesData = "<b>Notes</b>:<br />$notes2<br />";
	} else {
		$notesData = "";
	}


$dOut =<<< END_DATA

<p class="header"><a href="hueism.php?x=browse">Browse Collection</a> &rsaquo; <a href="hueism.php?x=browse&dduMenu_0_value=ph&dduMenu_0=category&SendSearch=1">Photographs</a> &rsaquo; $title</p>
      <p>Click on the photograph to view a larger version.</p>

<table cellpadding="0" cellspacing="0">
  <tr>
 	<td class="photograph_left">$largePhoto</td>
    <td>
      <!-- <p>(<a href="javascript:toggle();">Toggle MetaData View</a>)</p> -->


		<P>[ <a href="printedwork.php?id=$photoID&print">Printer-Friendly Version</a> ]</P>
		<div id="metadata" class="spacing"></div>
			$titleData
			$dateData
			$descriptionData
			$sourceData
                        $copyrightData
			$notesData
			$subjectData
			$spatialData
		<!-- </div>	-->	
				

	</td>
   </tr>
</table>

<script language="javascript" type="text/javascript">
	document.getElementById("metadata").style.display = "none";

function toggle() {
  if(document.getElementById("metadata").style.display == "none")
	document.getElementById("metadata").style.display = "block";
  else
    document.getElementById("metadata").style.display = "none";
}

</script>
END_DATA;

print ($dOut);

function file_url_exists($url) {
	$handle = @fopen($url, "r");
	 if ($handle === false)
		  return false;
	 fclose($handle);
	 return true;
}

?>




      
   
