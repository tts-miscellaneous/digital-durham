<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Digital Durham</title>
<link rel="stylesheet" type="text/css" media="screen" href="/ui/css/style.css" />
</head>

<body>

<div id="contain">
<div id="top">

<div id="search_box_top"><div id="search_box_left"><form method="get" action="/search">
      <input class="form_textbox" id="q" name="q" type="text" alt="Search Box" style="width:140px;" />
	  <input type="submit" name="submit" value="Go" />
      <input type="hidden" name="site" value="duke_collection" />
      <input type="hidden" name="client" value="digitaldurham" />
      <input type="hidden" name="proxystylesheet" value="digitaldurham" />
      <input type="hidden" name="output" value="xml_no_dtd" />
    </form></div></div>

<a href="/"><img src="/images/dd_logo3.gif" /></a>

</div>

<div id="nav">
  <?php

if(isset($x) && $x != "")
  include_once($x.".php");
else
  include_once("nav.php");

?>
</div>

<div id="content">
<div class="margins">
  <p class="header"><a href="reference.php">Reference</a></p>   
  <p class="header">Medical Terms </p>
    <blockquote>
	<table width="80%" border="0">
          <tr>
            <td width="35%" height="82"><b>Billious Fever:</b></td>
            <td width="65%" height="82">
              <p>Pertaining to the bile; disordered in respect to the bile; as a 
                bilious patient ; dependent on an excess of bile; as, bilious temperament; 
              bilious symptoms.</p>
              <p><b>Bile: </b>1.A yellow, greenish, bitter, viscid, nauseous fluid 
              secreted by the liver.</p>
              <blockquote> 
                <p> 2.Bitterness of feeling ; ill-humor; as, to stir one's bile.</p>
              </blockquote>
              <p><b>Fever: </b>A diseased state of the system, marked by increased 
                heat, acceleration of the pulse, and a general derangement of the 
                functions. Many diseases, of which fever is the most prominent symptom, 
                are denominated <i>fevers</i>; as, typhoid <i>fever</i>; yellow 
              <i>fever</i>. </p> </td>
          </tr>
          <tr> 
            <td width="35%" height="82"><b>Consumption: </b></td>
            <td width="65%" height="82">A gradual decay or diminution of the body; 
              especially, the disease called phthisis pulmonalis (pulmonary consumption), 
              a disease seated in the lungs, attended with a hectic fever, cough 
            &c. </td>
          </tr>
          <tr> 
            <td width="35%" height="28"><b>Deaf:</b> </td>
            <td width="65%" height="28">Wanting the sense of hearing either wholly 
            or in part; unable to perceive sounds. </td>
          </tr>
          <tr> 
            <td width="35%"><b>Diarrhea: </b></td>
            <td width="65%">A morbidly frequent evacuation of the intestines; a 
            relax; a flux.</td>
          </tr>
          <tr> 
            <td width="35%"><b>Dropsy: </b></td>
            <td width="65%"> 
              <p>An unnatural collection of serous fluid in any cavity of the body; 
              or in the areolar texture. <b>Serous: </b></p>
              <blockquote> 
                <p>1. Thin; watery; like whey; -- said of that part of the blood 
                  which separates in coagulation from the grumous or red part; also, 
                of the fluid which lubricates a serous membrane. </p>
                <p>2. Pertaining to serum. </p>
              </blockquote>
              <p><b>Areolar: </b>Pertaining to, or like, an areola. </p>
              <p><b>Areola: </b>An interstice, or small space. </p>            </td>
          </tr>
          <tr> 
            <td width="35%"><b>Dysentery: </b></td>
            <td width="65%"> Inflammation of the rectum or colon, attended with 
              griping pains, constant desire to evacuate the bowels, and to discharges 
            of mucus and blood. </td>
          </tr>
          <tr> 
            <td width="35%" height="24"><b>Fever: </b></td>
            <td width="65%" height="24">See <b>Billious Fever</b>.</td>
          </tr>
          <tr> 
            <td width="35%" height="26"><b>Hernia: </b></td>
            <td width="65%" height="26">A tumor of some part, which has escaped 
              its natural cavity by some aperture, and projects externally; as, 
              hernia of the brain, of the thorax, of the abdomen. That of the abdomen 
              is most common, and consists of the protrusion of the viscera through 
            natural or accidental apertures in the cavity of the abdomen. </td>
          </tr>
          <tr> 
            <td width="35%"><b>Maimed and crippled</b>: </td>
            <td width="65%">(Maim) To deprive of a necessary part; to cripple; to 
            disable.</td>
          </tr>
          <tr> 
            <td width="35%"><b>Maimed, crippled, bedridden: </b></td>
            <td width="65%">Confined to the bed by age or infirmity.</td>
          </tr>
          <tr> 
            <td width="35%"><b>Measels, Measles: </b></td>
            <td width="65%"> 
              <p>A contagious febrile disorder, commencing with catarrhal symptoms, 
                and marked by the appearance on the third day of an eruption of 
                distinct red circular spots, which coalesce in a crescentic form, 
                are slightly raised above the surface, and after the fourth day 
              of the eruption gradually decline. </p>
              <p> Febrile: Pertaining to fever; indicating fever or derived from 
              it; as febrile symptoms; febrile action. </p>
              <blockquote> 
                <p><b>Catarrhal:</b> Pertaining to catarrh, or produced by it. Catarrh: 
                  A discharge of fluid from the mucous membrane, especially of the 
                  nose, fauces, and bronchial passages, caused by a cold in the 
                  head. It is attended with cough, thirst, lassitude, and watery 
                eyes. </p>
                <p><b>Fauces:</b> The posterior part of the mouth, terminated by 
                the pharynx and larynx. </p>
                <p><b>Crescentic:</b> Crescent-shaped. </p>
              </blockquote>            </td>
          </tr>
          <tr> 
            <td width="35%"><b>Minengitis (Meningitis): </b></td>
            <td width="65%">Inflammation of the membranes of the brain or spinal 
            cord. </td>
          </tr>
          <tr> 
            <td width="35%"><b>Nephritis: </b></td>
            <td width="65%">An inflammation of the kidneys.</td>
          </tr>
          <tr> 
            <td width="35%"><b>Paralysis: </b></td>
            <td width="65%">Abolition of function, whether complete or partial; 
              -- usually applied to the loss of voluntary motion, with or without 
            that of sensation, in any part of the body; palsy. </td>
          </tr>
          <tr> 
            <td width="35%"><b>Pneuralgia (Neuralgia):</b> </td>
            <td width="65%">A disease, the chief symptom of which is a very acute 
              pain, exacerbating or intermitting, which follows the course of a 
              nervous branch, extends to its ramifications, and seems therefore 
            to be seated in the nerve. </td>
          </tr>
          <tr> 
            <td width="35%"><b>Pulmonary: </b></td>
            <td width="65%">Pertaining to the lungs; affecting the lungs; as, a 
            <i>pulmonary</i> disease or consumption; the <i>pulmonary</i> artery.          </td>
          </tr>
          <tr> 
            <td width="35%"><b>Scrofula: </b></td>
            <td width="65%"> 
              <p>A constitutional disease, generally hereditary, which affects the 
                lymphatic glands, oftenest those of the neck; but no organ is exempt 
                from its influence, and it frequently develops in pulmonary consumption; 
              king's evil; struma.</p>
              <p> <b>King's evil: </b>A disease of the scrofulous kind, formerly 
              supposed to be healed by the touch of a king. </p>
              <p><b>Struma:</b> The same as SCROFULA, q.v. </p>            </td>
          </tr>
          <tr> 
            <td width="35%" height="28"><b>Sick: </b></td>
            <td width="65%" height="28"> 
              <p>1. Affected with, or attended by, nausea; inclined or inclining 
              to vomit; as, <i>sick</i> at the stomach; a sick headache. </p>
              <p>2. Affected with disease of any kind; ill; indisposed; not in health; 
              -- followed by of; as, to be <i>sick</i> of a fever. </p>            </td>
          </tr>
          <tr> 
            <td width="35%"><b>Sunstroke: </b></td>
            <td width="65%">Any affection produced by the action of the sun on some 
              region of the body; especially, a sudden prostration of the physical 
              powers, with symptoms resembling those of apoplexy, occasioned by 
              exposure to excessive heat, and often terminating fatally; <i>coup 
            de soleil</i>; siriases. </td>
          </tr>
          <tr> 
            <td width="35%" height="30"><b>Typhoid fever:</b></td>
            <td width="65%" height="30"> 
              <p><b>Typhoid:</b> Of, pertaining to, or resembling, typhus; like 
                typhus of a low grade; as typhoid fever. </p>
              <p><b>Typhus: </b>A continuous fever lasting from two to three weeks, 
              and attended with great prostration and cerebral disorder. </p></td>
          </tr>
          <tr> 
            <td width="35%"><b>Whooping Cough: </b></td>
            <td width="65%">A violent, convulsive cough, returning at longer or 
              shorter intervals consisting of several expirations, followed by a 
            sonorous inspiration or whoop; chin-cough; hooping cough. </td>
          </tr>
  </table>    <p>&nbsp;</p>
    <p><a 
href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a> 
&middot; <a href="/about.php">About this site</a> &middot; Copyright &#169; 2001 - 2006. Trudi J. Abel. All Rights Reserved. 
</p>
    <div id="copyright">
    <p>The copyright interest in the material in this digital collection has not been transferred to the Digital Durham project. These text and images may not be used for any commercial purpose without the permission of the Duke University Rare Book, Manuscript, and Special Collections Library and the Digital Durham Project. Copyright permission for subsequent uses is the responsibility of the user.</p>
  </div>
</div>
</div>

</div>

</body>
