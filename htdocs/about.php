<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Digital Durham</title>
<link rel="stylesheet" type="text/css" media="screen" href="/ui/css/style.css" />
</head>

<body>

<div id="contain">
<div id="top">

<div id="search_box_top"><div id="search_box_left">
<form method="get" action="/search">
      <input class="form_textbox" id="q" name="q" type="text" alt="Search Box" style="width:140px;" />
	  <input type="submit" name="submit" value="Go" />
      <input type="hidden" name="site" value="duke_collection" />
      <input type="hidden" name="client" value="digitaldurham" />
      <input type="hidden" name="proxystylesheet" value="digitaldurham" />
      <input type="hidden" name="output" value="xml_no_dtd" />
    </form>
</div></div>

<a href="/"><img src="/images/dd_logo3.gif" /></a>

</div>

<div id="nav">
  <?php

if(isset($x) && $x != "")
  include_once($x.".php");
else
  include_once("nav.php");

?>
</div>

<div id="content">
<div class="margins">

  <div class="home_right">
    <p class="header">About this site | <a href="overview.php">Overview</a> | <a href="funding.php">Funding</a> </p>
    <p>Project Director: </p>
<blockquote>
  <p>Trudi Abel, Ph.D. <br>
  <p>Director, Digital Durham Project <br>
  <p><a 
href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a> </p>
  <p>Scholar-in-Residence <br>
  <p>Department of History <br>
  <p>Box 90719 <br>
  <p>Duke University <br>
  <p>Durham, NC 27708 <br>
  </p>
</blockquote>
<p>Research Assistants: </p>
<blockquote>
  <p>Stacy Torian (Master of Arts in Liberal Studies) </p>
  <p>Andrea DeGette (MALS) </p>
  <p>Barbara Darden Blue (MALS) </p>
  <p>Barbara Post (MALS) </p>
  <p>Kelly Clark (MALS) </p>
  <p>Judy Carrasco (MALS) </p>
  <p>Rod Clare (History) </p>
  <p>Kathy Castles (History) </p>
</blockquote>
<p>Undergraduates:</p>
<blockquote>
  <p>Sam Angiuoli, '00 </p>
  <p>Todd Delamielleure, '01 </p>
  <p>Brijal Padia '01 </p>
  <p>Angela Callanan, '02 </p>
  <p>Monica Belle, '02 </p>
  <p>Anna Chapman, '03 </p>
  <p>Bobbie Daniels, '03 </p>
  <p>James Darsie, '03 </p>
  <p>Marion Penning, '05 </p>
  </blockquote>
<p>Project Volunteers: </p>
<blockquote>
    <p>Ben Pahl </p>
    <p>Chris Roland </p>
    <p>Dan Roland </p>
    <br>
</blockquote>
<p><a 
href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a> 
&middot; <a href="/about.php">About this site</a> &middot; Copyright &#169; 2001 - 2006. 
Trudi J. Abel. All Rights Reserved. <br>
</p>
  <div id="copyright">
    <p>The copyright interest in the material in this digital collection has not been transferred to the Digital Durham project. These text and images may not be used for any commercial purpose without the permission of the Duke University Rare Book, Manuscript, and Special Collections Library and the Digital Durham Project. Copyright permission for subsequent uses is the responsibility of the user.</p>
  </div></div></div>

</div>

</div>




</body>
</html>
