O<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Digital Durham</title>
<link rel="stylesheet" type="text/css" media="screen" 
href="/scripts/style2.css" />
</head>

<body>

<div id="contain">
<div id="top">

<div id="search_box_top"><div id="search_box_left">
<form method="get" action="/search">
       <input class="form_textbox" id="q" name="q" type="text" alt="Search
Box" style="width:140px;" />
          <input type="submit" name="submit" value="Go" />
      <input type="hidden" name="site" value="duke_collection" />
      <input type="hidden" name="client" value="digitaldurham" />
      <input type="hidden" name="proxystylesheet" value="digitaldurham" />
      <input type="hidden" name="output" value="xml_no_dtd" />
    </form>
</div></div>

<a href="/"><img src="/images/dd_logo3.gif" /></a>

</div>

<div id="nav">
  <?php

if(isset($x) && $x != "")
  include_once($x.".php");
else
  include_once("nav.php");

?>
</div>

<div id="content">
<div class="margins">

  <p class="header">Audio Clips</p>  
  <p>
 These audio postcards were created in &quot;Digital Durham &amp; the New South,&quot; a research service-learning course taught by Professor Trudi Abel in the spring of 2006. </p>
  <ul>
    <li>  Theresa Mohin 
      (Class of 2007) talks with Durham resident Gwen Phillips about the experience 
      of African American women in post-Civil War Durham and the New South. <a 
href="/audio/Theresa_Mohin.mp3"><strong>[audio]</strong></a> <br>
    </li>
    <li>Corina Apostol (Class of 2009) speaks of her encounters with 
      rare advertising material in the Rare Book, Manuscript, and Special 
      Collections Library, Duke University, as she conducted her study of African 
      American Visual Culture.<a 
href="/audio/corina_podcast.wav"><strong>[audio]</strong></a> <br>
    </li>
    <li>Gordon Whitehouse (Class of 2007) takes his audience on a walk 
      through the American Tobacco Campus as he discusses the history of Durham's 
      tobacco industry. <a 
href="/audio/Gordon_Podcast.mp3"><strong>[audio]</strong></a> </li>
  </ul>
  <p>&nbsp;</p>
  <p><a 
href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a> 
&middot; <a href="about.php">About this site</a> &middot; Copyright �2001 -  
2006. Trudi J. Abel. All Rights Reserved. </p>
  <div id="copyright">
    <p>The copyright interest in the material in this digital collection has not been transferred to the Digital Durham project. These texts and images may not be used for any commercial purpose without the permission of the Duke University Rare Book, Manuscript, and Special Collections Library and the Digital Durham Project. Copyright permission for subsequent uses is the responsibility of the user.</p>
</div>
  </div>
</div>

</div>
</body>
</html>
