<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Digital Durham</title>
<link rel="stylesheet" type="text/css" media="screen" 
href="/ui/css/style.css" />
</head>

<body>

<div id="contain">
<div id="top">

<div id="search_box_top"><div id="search_box_left">
<form method="get" action="/search">
      <input class="form_textbox" id="q" name="q" type="text" alt="Search Box" style="width:140px;" />
	  <input type="submit" name="submit" value="Go" />
      <input type="hidden" name="site" value="duke_collection" />
      <input type="hidden" name="client" value="digitaldurham" />
      <input type="hidden" name="proxystylesheet" value="digitaldurham" />
      <input type="hidden" name="output" value="xml_no_dtd" />
    </form>
</div></div>

<a href="/"><img src="/images/dd_logo3.gif" /></a>

</div>

<div id="nav">
  <?php

if(isset($x) && $x != "")
  include_once($x.".php");
else
  include_once("nav.php");

?>
</div>

<div id="content">
<div class="margins">
  <p class="header">In The News</p>
  <p><a href="http://www.historians.org/perspectives/issues/2009/0905/0905for12.cfm"target="new">Trudi Abel, &quot;The Digital Durham Project: Creating Community through History, Technology, and Service Learning,&quot; <i>Perspectives in History</i>, May 2009.</a></p>
  <p><a href="http://www.dukenews.duke.edu/2009/05/digital_durham.html"target="new">&quot;New Map Collaboration Helps Tell Story of Durham's History,&quot; <em>Duke News</em>, 5 May 2009.</a></p>
  <p><a href="https://deimos.apple.com/WebObjects/Core.woa/BrowsePrivately/new.duke.edu.1301477303.01686570649.2107792785?i=1873170583"target="new">Trudi Abel and Shawn Miller, &quot;Visualizing the New South City: Historic Maps, Google Earth, and the Transformation of Durham,&quot; a presentation to the Tech and New Media Tuesday Forum, Duke University, 21 April 2009.</a> (Click link and then select item 10 for iTunes U video).</p>
<p><a href="http://lectopia.oit.duke.edu/ilectures/ilectures.lasso?ut=193&id=14139"=target="new">Trudi Abel, &quot;Seeing Through Time: Historic Maps, Google Earth, and the Transformation of Durham,&quot;
  a presentation to the Visualization Friday Forum, Duke University, 20 March 2009.</a> (Click link for video or audio).</p>
  <p><a href="http://www.indyweek.com/gyrobase/Content?oid=oid%3A258374"target="new">Ryan Vu, "What Google Earth doesn't show you," <i>Independent</i>, 21 May 2008.</a></p>
  <p><a href="http://news.duke.edu/2006/11/digital.html"target="new">&quot;Reliving Durham's History,&quot; 2006 </a></p>
<p><a href="/pdf/DDI_ipod2006.pdf"target="new">&quot;Digital Audio Enhances History Course,&quot; 2006 </a></p>
  <p><a href="/pdf/New_databaes_old_Durham.pdf"target="new">Vicki Cheng, "New Database, Old Durham," <i>News and Observer</i>, 9 May 2001</a></p>
  <p><a href="http://www.dukenews.duke.edu/2001/04/census420.html"target="new">"Census data takes class back to 1880s," <i>Duke News</i>, 20 April 2001</a></p>
<p><a href="http://magazine.lib.duke.edu/issue2/libmag0200.swf"target="new">Trudi Abel,"The Power and Poverty of Written Records," <i>Duke University Libraries</i>, Winter 2000</a> (flash required)</p>
<p><a href="http://magazine.lib.duke.edu/issue1/partners.html"target="new">Lynne O'Brien, "Partners in Pedagogy,"<i> Duke University Libraries</i>, Fall 1999</a></p>
<p><a href="http://www.historians.org/perspectives/issues/1997/9703/9703TEC.CFM"target="new">Trudi Abel,"Students as Historians: Lessons from an 'Interactive' Census Database Project," <i>Perspectives</i>, March 1997</a></p>
<p><a href="http://civic.duke.edu/ltmd/profiles/archived_profiles/FacultyAbel/trudiabel.html"target="new">Learning to Make a Difference Faculty Profile </a></p>
  <p>&nbsp;</p>
  <p><a 
href="mailto:d&#105;gital&#100;&#117;&#114;&#104;a&#109;&#64;&#100;&#117;&#107;e&#46;&#101;&#100;&#117;">digitaldurham@duke.edu</a> 
&middot; <a href="/about.php">About this site</a> &middot; Copyright � 
2001 - 2009. Trudi J. Abel. All Rights Reserved. </p> 

<div id="copyright">
    <p align="left">The copyright interest in the material in this digital collection has not been transferred to the Digital Durham project. These text and images may not be used for any commercial purpose without the permission of the Duke University Rare Book, Manuscript, and Special Collections Library and the Digital Durham Project. Copyright permission for subsequent uses is the responsibility of the user.</p>
</div>
</div>
</div>
</div>

</body>
</html>
