<?php

include_once("common/pdf-php/src/Cezpdf.php");
include_once("common/paths.php");
date_default_timezone_set('UTC');
$memoryNeeded = "90000K";
ini_set("memory_limit", "$memoryNeeded");
set_time_limit(120);  // Run for up to 2 minutes

function makePDF($info) {

	$pdf = new Cezpdf();
	
	
	$pdf->addInfo("Title", "Digital Durham");
	$pdf->addInfo("Author", "Digital Durham");
	$pdf->addInfo("Subject", "Priter Friendly");
	
	$pdf->ezSetMargins(-10,-10,0,0);
	
	$maxPage = 20;

	$dOut = "";

	$id = "";
	$b = "";
	$e = "";
	
	if (isset($_REQUEST['id'])) {
		$id = (int)$_REQUEST['id'];
	}

	if (isset($_REQUEST['b'])) {
		$b = $_REQUEST['b'];
	}

	if (isset($_REQUEST['e'])) {
		$e = $_REQUEST['e'];
	}

	if ((! empty($id)) && (! is_numeric($id))) {
		print('Error');
		exit;	
	}
	if ((! empty($b)) && (! is_numeric($b))) {
		print('Error');
		exit;	
	}
	if ((! empty($e)) && (! is_numeric($e))) {
		print('Error');
		exit;	
	}

	if (count($info) > $maxPage && ($e == "" || $b == "")) {
		$dOut .= "<b>Digital Durham</b><P>";
		$dOut .= "<P>This document contains more than <b>$maxPage</b> pages. <br /> To make it easier and faster to print, please select the section of the book you wish to print:</P>";
		
		$sectionNumber = round(count($info)/$maxPage);
		$base = 1;
		
		$dOut .= "<ul>";
	
		
		for ($z = 0; $z < $sectionNumber; $z++) {
		
			if ($base*$maxPage < count($info)) {
				$dOut .= "<li>Pages: <a href=\"printedwork.php?id=$id&b=" . ($base*$maxPage-$maxPage) . "&e=" . ($base*$maxPage) . "&print\">". ($base*$maxPage-$maxPage+1) . " through " . ($base*$maxPage) . "</a></li>";
			} else {
				$dOut .=  "<li>Pages: <a href=\"printedwork.php?id=$id&b=" . ($base*$maxPage-$maxPage) . "&e=" . count($info) . "&print\">". ($base*$maxPage-$maxPage+1) . " through " . count($info) . "</a></li>";
			}
			$base++;
		
		}
		
		$dOut .= "<li><a href=\"printedwork.php?id=$id&b=0&e=" . (count($info)-1) . "&print\">Print All Pages</a></li>";
		$dOut .= "</ul>";
		print ($dOut);
		exit;

	}

	if ($b != "") {
		$start = ($b);
	} else {
		$start = 0;
	}
	
	if ($e != "") {
		$end = ($e);
	} else {
		$end = count($info);
	}

	for ($i = $start; $i < $end; $i++) {
		if (file_url_exists($info[$i])) {
			$pdf->ezImage($info[$i], 1, 498, null, "center", null);
		}
	
	}


print $pdf->ezStream();
}




?>
